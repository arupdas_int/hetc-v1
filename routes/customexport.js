module.exports={
   mail             : require('../lib/sendmail'),
   sendnotification : require('../lib/sendnotification'),
   helper           : require('../helper/commonhelper'),
   authenticate     : require('../middleware/authenticate'),
   authenticateschool : require('../middleware/authenticate'),
   verifyapicalltoken : require('../middleware/verifyapicalltoken'),
   validateapi      : require('../middleware/validateapi'),
   verifyapicall    : require('../middleware/verifyapicall'),
   setting          : require('../config/setting')
}
