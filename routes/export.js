module.exports = {
    asyncForEach: require('async-foreach').forEach,
    moodle_client: require("moodle-client"),
    multer: require('multer'),
    mkdirp: require('mkdirp'),
    jwt: require('jsonwebtoken'),
    crypto: require('crypto'),
    fs: require('fs'),
    path: require('path'),
    async: require('async'),
    datetime: require('node-datetime'),
    session: require('express-session'),
    checktypes: require('check-types'),
    validator: require('validator'),
    csvstringify: require('csv-stringify'),
    csvdata: require('csvdata'),
    jsonParser: require('json2csv').Parser,
    restClient: require('node-rest-client').Client,
    mysql: require('mysql'),
    underscore: require('underscore')
}
