var settings = require('../../config/database');
var node_module = require('../export');
var pool = node_module.mysql.createPool(settings);
var model = {
    commonInsert: function(param, errorcallback, successcallback) {
        pool.getConnection(function(err, connection) {
            if (err == null) {
                var query = connection.query('INSERT INTO ' + param.tablename + ' SET ? ', param.data, function(error, results, fields) {
                    connection.release();
                    console.log('pool insert ', query.sql);
                    if (error) {
                        errorcallback(error);
                    } else {
                        successcallback(results);
                    }
                });
            } else {
                errorcallback(err);
            }
        });
    },
    commonSelect: function(param, errorcallback, successcallback) {
        var fields = (!param.fields) ? '*' : param.fields;
        var from = param.from;
        var join = (!param.join) ? '' : param.join;
        var where_condition = (!param.condition || param.condition == '') ? '' : param.condition;
        var groupby = (!param.groupby) ? '' : param.groupby;
        var orderby = (!param.orderby) ? '' : param.orderby;
        var limit = (!param.limit) ? '' : 'limit ' + param.limit;
        var sql = "Select " + fields + " " + from + " " + join + " " + where_condition + " " + groupby + " " + orderby + " " + limit;
        console.log("sql111:", sql);
        pool.getConnection(function(err, connection) {
            if (err == null) {
                var query = connection.query(sql, function(error, results, fields) {
                    connection.release();
                    console.log('pool select ', query.sql);
                    successcallback(results);
                });
            } else {
                errorcallback(err);
            }
        });
    },
    commonUpdate: function(param, errorcallback, successcallback) {
        var updateDataParam = {};
        var updateConditionParam = {};
        updateDataParam = param.data;
        updateConditionParam = param.condition;
        var updateColumn = '';
        var updateCondition = '';
        var updateColumnCounter = 0;
        var updateConditionCounter = 0;
        pool.getConnection(function(err, connection) {
            if (err == null) {
                node_module.async.forEachOf(param.data, (value, key, callback) => {
                    updateColumnCounter++;
                    if (updateColumnCounter == 1) {
                        updateColumn += ' SET ' + key + ' = ' + connection.escape(value) + ' ';
                    } else {
                        updateColumn += key + ' = ' + connection.escape(value) + ' ';
                    }
                    if (updateColumnCounter != Object.keys(updateDataParam).length) {
                        updateColumn += ' , ';
                    }
                    callback();
                }, asyncErr => {
                    if (asyncErr) console.error(asyncErr.message);
                    // configs is now a map of JSON data
                    node_module.async.forEachOf(param.condition, (value, key, callback) => {
                        updateConditionCounter++;
                        if (updateConditionCounter == 1) {
                            updateCondition += ' WHERE ' + key + ' = ' + connection.escape(value) + ' ';
                        } else {
                            updateCondition += key + ' = ' + connection.escape(value) + ' ';
                        }
                        if (updateConditionCounter != Object.keys(updateConditionParam).length) {
                            updateCondition += ' AND ';
                        }
                        callback();
                    }, asyncUpdateErr => {
                        if (asyncUpdateErr) console.error(asyncUpdateErr.message);
                        // configs is now a map of JSON data
                        var updateSql = "UPDATE " + param.tablename + " " + updateColumn + " " + updateCondition;
                        console.log(updateSql);
                        connection.query(updateSql, function(error, results, fields) {
                            connection.release();
                            console.log('pool update ', updateSql);
                            if (error) errorcallback("Uh oh! Couldn't update results: " + error.msg);
                            successcallback(true);
                        });
                    });
                });
            } else {
                errorcallback(err);
            }
        });
    },
    commonDelete: function(param, errorcallback, successcallback) {
        var deleteConditionParam = {};
        deleteConditionParam = param.condition;
        var deleteCondition = '';
        var deleteConditionCounter = 0;
        pool.getConnection(function(err, connection) {
            if (err == null) {
                // configs is now a map of JSON data
                node_module.async.forEachOf(param.condition, (value, key, callback) => {
                    deleteConditionCounter++;
                    if (deleteConditionCounter == 1) {
                        deleteCondition += ' WHERE ' + key + ' = ' + connection.escape(value) + ' ';
                    } else {
                        deleteCondition += key + ' = ' + connection.escape(value) + ' ';
                    }
                    if (deleteConditionCounter != Object.keys(deleteConditionParam).length) {
                        deleteCondition += ' AND ';
                    }
                    callback();
                }, asyncErr => {
                    if (asyncErr) console.error(asyncErr.message);
                    // configs is now a map of JSON data
                    var deleteSql = "DELETE FROM " + param.tablename + " " + deleteCondition;
                    console.log(deleteSql);
                    connection.query(deleteSql, function(error, results, fields) {
                        connection.release();
                        console.log('pool delete ', deleteSql);
                        if (error) errorcallback("Uh oh! Couldn't update results: " + error.msg);
                        successcallback(true);
                    });
                });
            } else {
                errorcallback(err);
            }
        });
    }
}
module.exports = model;