var database = require('../../config/database');
var sendGroupNotification = require('../../lib/sendnotification');
var SETTING = require('../../config/setting');
var customodule = require('../customexport');
var node_module = require('../export');
var Mastermodel = require('./mastermodel');
var RESPONSEMESSAGE = require('../../config/statuscode');

var model = {
    /* Role Management Section */
    sendPushNotification: function(data, errorcallback, successcallback) {
        var userId = data.userId;
        var courseId = data.courseId;
        var unitId = data.unitId;
        var userType = data.userType;
        var pushNotificationCode = data.pushNotificationCode;
        var fields = " mudt.device_id,mudt.device_token,mudt.device_type  ";
        var condition = "where 1";
        condition += "  AND mudt.user_id = '" + data.userId + "' ";
        var orderby = " ";
        var join = "";
        var param = {
            dbsetting: data.dbsetting,
            from: 'from mdl_user_device_token as mudt',
            condition: condition,
            fields: fields,
            join: join,
            orderby: orderby
        };
        Mastermodel.commonSelect(param, function(err) {
            console.log(err)
        }, function(getDeviceTokenRes) {
            //get the user detail
            var fields = " mu.email,mu.firstname ";
            var condition = "where 1";
            condition += "  AND mu.id = '" + data.userId + "' ";
            var orderby = " ";
            var join = "";
            var param = {
                dbsetting: data.dbsetting,
                from: 'from mdl_user as mu',
                condition: condition,
                fields: fields,
                join: join,
                orderby: orderby
            };
            Mastermodel.commonSelect(param, function(err) {
                console.log(err)
            }, function(userDetailRes) {
                //get course and unit detail
                if (courseId != 0 && unitId != 0) {
                    var fields = " mc.fullname as uname, mc.idnumber as ucode,  mcc.name as cname, mcc.idnumber as ccode ";
                    var condition = " where mc.id != 0 and mc.id =" + unitId;
                    var join = " LEFT JOIN mdl_course_categories as mcc  ON mc.category  = mcc.id ";
                    var param = {
                        dbsetting: data.dbsetting,
                        from: 'from mdl_course as mc',
                        condition: condition,
                        fields: fields,
                        join: join,
                        orderby: orderby
                    };
                } else {
                    var fields = " mcc.name as cname, mcc.idnumber as ccode ";
                    var condition = " where mcc.id != 0 and mcc.id =" + courseId;
                    var join = "  ";
                    var param = {
                        dbsetting: data.dbsetting,
                        from: 'from mdl_course_categories as mcc',
                        condition: condition,
                        fields: fields,
                        join: join,
                        orderby: orderby
                    };
                }
                Mastermodel.commonSelect(param, function(err) {
                    console.log(err)
                }, function(courseDetailRes) {
                    if (courseId != 0 && unitId != 0) {
                        var dynamicNotificationMessage = '';
                        dynamicNotificationMessage = 'Hi ' + userDetailRes[0]['firstname'] + ',' + data.pushNotificationCode.NOTIFICATIONMESSAGE + ' in Course ' + courseDetailRes[0]['cname'] + ' and Unit ' + courseDetailRes[0]['uname'];
                    } else {
                        dynamicNotificationMessage = 'Hi ' + userDetailRes[0]['firstname'] + ',' + data.pushNotificationCode.NOTIFICATIONMESSAGE + ' in Course ' + courseDetailRes[0]['cname'];
                    }

                    var emailObj = {
                        name: userDetailRes[0]['firstname'],
                        email: userDetailRes[0]['email'],
                        notificationtitle: data.pushNotificationCode.NOTIFICATIONTITLE,
                        notificationmessage: dynamicNotificationMessage,
                    }
                    console.log(emailObj);
                    customodule.mail('notificationmail', emailObj);
                    //save the push notification
                    var pushNotificationInsertdata = {
                        user_id: data.userId,
                        user_type: data.userType,
                        notification_title: data.pushNotificationCode.NOTIFICATIONTITLE,
                        notification_message: dynamicNotificationMessage
                    };
                    //get the current date
                    var today = new Date();
                    var dd = today.getDate();
                    var mm = today.getMonth() + 1; //January is 0!
                    var yyyy = today.getFullYear();
                    if (dd < 10) {
                        dd = '0' + dd
                    }
                    if (mm < 10) {
                        mm = '0' + mm
                    }
                    currentdate = mm + '/' + dd + '/' + yyyy;

                    var insertParam = {
                        dbsetting: data.dbsetting,
                        tablename: 'mdl_user_notification',
                        data: [pushNotificationInsertdata]
                    };
                    Mastermodel.commonInsert(insertParam, function(error) {
                        console.log('Insert', error);
                        errorcallback("error while insert data");
                    }, function(success) {
                        //push notification code 
                        var pushNotificationTitle = data.pushNotificationCode.NOTIFICATIONTITLE;
                        var pushNotificationDesc = 'Hi ' + userDetailRes[0]['firstname'] + ',' + dynamicNotificationMessage;
                        var pushNotificationTag = data.pushNotificationCode.NOTIFICATIONTAG;
                        var pushNotificationCode = data.pushNotificationCode.NOTIFICATIONCODE;
                        var pushNotificationDate = currentdate;
                        sendGroupNotification(pushNotificationTitle, getDeviceTokenRes, pushNotificationTag, pushNotificationDesc, pushNotificationCode, pushNotificationDate);
                        successcallback(data);
                    });
                });
            });
        });
    },
}
module.exports = model;