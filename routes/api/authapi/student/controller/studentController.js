express = require('express');
var router = express.Router();
var app = express();
var customodule = require('../../../../customexport');
var node_module = require('../../../../export');
var RESPONSEMESSAGE = require('../../../../../config/statuscode');
var Mastermodel = require('../../../mastermodel.js');
var asyncForEach = require('async-foreach').forEach;
var async = require('async');
var sendGroupNotification = require('../../../../../lib/sendnotification');
var NOTIFICATIONMESSAGE = require('../../../../../config/notificationcode');
var Masternotificationmodel = require('../../../commonPushNotificationModel.js');
var commonFunction = require('../../../../../config/commonFunction');
var SETTING = require('../../../../../config/setting');
var multer = require('multer');
var path = require('path');
const charge = require('../../../../../config/charge');
var pathEvidenceUpload = '../moodle/rplprocess/uploads/'; //need to change moodle as per site name
var pathTestUpload = './moodle/rplprocess/uploads/'; //need to change moodle as per site name
var pathTestUpload = "/Documents/nodeproject/moodle/rplprocess/uploads/test_file";
var database = require('../../../../../config/database');
var mysql = require('mysql');
var connection = mysql.createConnection(database);
var base_url_course_image = "http://52.63.96.113:4444/share/courseimage/";
var base_url_unit_image = "http://52.63.96.113:4444/share/unitimage/";
// ******************************* GET COURSE LIST CONTROLLER************
/*
 * Used to get resume contact information
 * Created On : 11-07-2017
 * Modified On
 */

router.post('/getResumeContactInformation', customodule.verifyapicalltoken, function (req, res) {
    var studentId = req.body.studentId;
    if (!studentId) {
        res.status(201).send(RESPONSEMESSAGE.RESUME.PROVIDEUSERDETAILS);
    } else if (isNaN(studentId)) {
        res.status(201).send(RESPONSEMESSAGE.RESUME.INVALIDUSER);
    } else {
        /***** Check valid user or not *******/
        var fields = "u.firstname,u.lastname,u.email";
        var condition = "where u.id = '" + studentId + "'";
        var param = {
            from: 'from mdl_user as u',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {
            if (userRes.length > 0) {
                /***** get user resume contact information *******/
                var condition = "where studentid = '" + studentId + "'";
                var param = {
                    from: 'from mdl_resume_contact_information',
                    condition: condition,
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (resumeRes) {
                    if (resumeRes.length > 0) {
                        var response = {
                            resCode: 200,
                            response: resumeRes[0]
                        };
                        res.status(200).send(response);
                    } else {
                        res.status(200).send(RESPONSEMESSAGE.RESUME.USERDOESNOTEXIST);
                    }
                });
            } else {
                res.status(201).send(RESPONSEMESSAGE.RESUME.USERDOESNOTEXIST);
            }
        });
    }
});


/*
 * Used to get resume cover letter
 * Created On : 11-07-2017
 * Modified On
 */
router.post('/getResumeIntroduction', customodule.verifyapicalltoken, function (req, res) {
    //console.log(req.body);
    var studentId = req.body.studentId;
    if (!studentId) {
        res.status(201).send(RESPONSEMESSAGE.RESUME.PROVIDEUSERDETAILS);
    } else if (isNaN(studentId)) {
        res.status(201).send(RESPONSEMESSAGE.RESUME.INVALIDUSER);
    } else {
        /***** Check valid user or not *******/
        var fields = "u.firstname";
        var condition = "where u.id = '" + studentId + "'";
        var param = {
            from: 'from mdl_user as u',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {
            if (userRes.length > 0) {
                /***** get resume cover letter data *******/
                var condition = "where studentid = '" + studentId + "'";
                var param = {
                    from: 'from mdl_resume_introduction',
                    condition: condition,
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (resumeRes) {
                    if (resumeRes.length > 0) {
                        var response = {
                            resCode: 200,
                            response: resumeRes[0]
                        };
                        res.status(200).send(response);
                    } else {

                        res.status(201).send(RESPONSEMESSAGE.RESUME.NODATAFOUND);
                    }
                });

            } else {
                res.status(201).send(RESPONSEMESSAGE.RESUME.USERDOESNOTEXIST);
            }
        });
    }
});


/*
 * Used to get resume cover letter
 * Created On : 11-07-2017
 * Modified On
 */
router.post('/getResumeCoverLetter', customodule.verifyapicalltoken, function (req, res) {
    //console.log(req.body);
    var studentId = req.body.studentId;
    if (!studentId) {
        res.status(201).send(RESPONSEMESSAGE.RESUME.PROVIDEUSERDETAILS);
    } else if (isNaN(studentId)) {
        res.status(201).send(RESPONSEMESSAGE.RESUME.INVALIDUSER);
    } else {
        /***** Check valid user or not *******/
        var fields = "u.firstname";
        var condition = "where u.id = '" + studentId + "'";
        var param = {
            from: 'from mdl_user as u',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {
            if (userRes.length > 0) {
                /***** get resume cover letter data *******/
                var condition = "where studentid = '" + studentId + "'";
                var param = {
                    from: 'from mdl_resume_cover_letter',
                    condition: condition,
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (resumeRes) {
                    if (resumeRes.length > 0) {
                        var response = {
                            resCode: 200,
                            response: resumeRes[0]
                        };
                        res.status(200).send(response);
                    } else {

                        res.status(201).send(RESPONSEMESSAGE.RESUME.NODATAFOUND);
                    }
                });

            } else {
                res.status(201).send(RESPONSEMESSAGE.RESUME.USERDOESNOTEXIST);
            }
        });
    }
});

/*
 * Used to get resume objective
 * Created On : 11-07-2017
 * Modified On
 */
router.post('/getResumeObjective', customodule.verifyapicalltoken, function (req, res) {
    //console.log(req.body);
    var studentId = req.body.studentId;
    if (!studentId) {
        res.status(201).send(RESPONSEMESSAGE.RESUME.PROVIDEUSERDETAILS);
    } else if (isNaN(studentId)) {
        res.status(201).send(RESPONSEMESSAGE.RESUME.INVALIDUSER);
    } else {
        /******** check valid user or not**************/
        var fields = "u.firstname";
        var condition = "where u.id = '" + studentId + "'";
        var param = {
            from: 'from mdl_user as u',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {

            if (userRes.length > 0) {
                /******** get resume objective data**************/
                var condition = "where studentid = " + studentId;
                var param = {
                    from: 'from mdl_resume_objective',
                    condition: condition,
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (resumeRes) {
                    if (resumeRes.length > 0) {
                        var response = {
                            resCode: 200,
                            response: resumeRes[0]
                        };
                        res.status(200).send(response);
                    } else {

                        res.status(201).send(RESPONSEMESSAGE.RESUME.NODATAFOUND);
                    }
                });

            } else {
                res.status(201).send(RESPONSEMESSAGE.RESUME.USERDOESNOTEXIST);
            }
        });
    }
});


/*
 * Used to get resume key qualification
 * Created On : 11-07-2017
 * Modified On
 */
router.post('/getResumeKeyQualification', customodule.verifyapicalltoken, function (req, res) {
    //console.log(req.body);
    var studentId = req.body.studentId;
    if (!studentId) {
        res.status(201).send(RESPONSEMESSAGE.RESUME.PROVIDEUSERDETAILS);
    } else if (isNaN(studentId)) {
        res.status(201).send(RESPONSEMESSAGE.RESUME.INVALIDUSER);
    } else {
        /****** Check valid user or not *******/
        var fields = "u.firstname";
        var condition = "where u.id = '" + studentId + "'";
        var param = {
            from: 'from mdl_user as u',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {

            if (userRes.length > 0) {
                /****** get user resume key qualification *******/
                var condition = "where studentid = " + studentId;
                var param = {
                    from: 'from mdl_resume_key_qualifications',
                    condition: condition,
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (resumeRes) {
                    if (resumeRes.length > 0) {
                        var response = {
                            resCode: 200,
                            response: resumeRes[0]
                        };
                        res.status(200).send(response);
                    } else {

                        res.status(201).send(RESPONSEMESSAGE.RESUME.NODATAFOUND);
                    }
                });

            } else {
                res.status(201).send(RESPONSEMESSAGE.RESUME.USERDOESNOTEXIST);
            }
        });
    }
});

/*
 * Used to get resume other experiences
 * Created On : 11-07-2017
 * Modified On
 */
router.post('/getResumeOtherExperiences', customodule.verifyapicalltoken, function (req, res) {
    //console.log(req.body);
    var studentId = req.body.studentId;
    if (!studentId) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.PROVIDEUSERDETAILS);
    } else if (isNaN(studentId)) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.INVALIDUSER);
    } else {

        var fields = "u.firstname";
        var condition = "where u.id = '" + studentId + "'";
        var param = {
            from: 'from mdl_user as u',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {

            if (userRes.length > 0) {

                var condition = "where studentid = " + studentId;
                var param = {
                    from: 'from mdl_resume_other_experiences',
                    condition: condition,
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (resumeRes) {
                    if (resumeRes.length > 0) {
                        var response = {
                            resCode: 200,
                            response: resumeRes[0]
                        };
                        res.status(200).send(response);
                    } else {

                        res.status(200).send(RESPONSEMESSAGE.RESUME.NODATAFOUND);
                    }
                });

            } else {
                res.status(200).send(RESPONSEMESSAGE.RESUME.USERDOESNOTEXIST);
            }
        });
    }
});

/*
 * Used to get resume work experiences
 * Created On : 11-07-2017
 * Modified On
 */
router.post('/getResumeWorkExperience', customodule.verifyapicalltoken, function (req, res) {
    //console.log(req.body);
    var studentId = req.body.studentId;
    if (!studentId) {
        res.status(201).send(RESPONSEMESSAGE.RESUME.PROVIDEUSERDETAILS);
    } else if (isNaN(studentId)) {
        res.status(201).send(RESPONSEMESSAGE.RESUME.INVALIDUSER);
    } else {

        var fields = "u.firstname";
        var condition = "where u.id = '" + studentId + "'";
        var param = {
            from: 'from mdl_user as u',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {

            if (userRes.length > 0) {

                var condition = "where studentid = " + studentId;
                var param = {
                    from: 'from mdl_work_experiences',
                    condition: condition,
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (resumeRes) {
                    if (resumeRes.length > 0) {
                        var response = {
                            resCode: 200,
                            response: resumeRes
                        };
                        res.status(201).send(response);
                    } else {

                        res.status(201).send(RESPONSEMESSAGE.RESUME.NODATAFOUND);
                    }
                });

            } else {
                res.status(201).send(RESPONSEMESSAGE.RESUME.USERDOESNOTEXIST);
            }
        });
    }
});

/*
 * Used to get resume education
 * Created On : 11-07-2017
 * Modified On
 */
router.post('/getResumeEducation', customodule.verifyapicalltoken, function (req, res) {
    //console.log(req.body);
    var studentId = req.body.studentId;
    if (!studentId) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.PROVIDEUSERDETAILS);
    } else if (isNaN(studentId)) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.INVALIDUSER);
    } else {

        var fields = "u.firstname";
        var condition = "where u.id = '" + studentId + "'";
        var param = {
            from: 'from mdl_user as u',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {

            if (userRes.length > 0) {

                var condition = "where studentid = " + studentId;
                var param = {
                    from: 'from mdl_education',
                    condition: condition,
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (resumeRes) {
                    if (resumeRes.length > 0) {
                        var response = {
                            resCode: 200,
                            response: resumeRes
                        };
                        res.status(200).send(response);
                    } else {

                        res.status(200).send(RESPONSEMESSAGE.RESUME.NODATAFOUND);
                    }
                });

            } else {
                res.status(200).send(RESPONSEMESSAGE.RESUME.USERDOESNOTEXIST);
            }
        });
    }
});

/*
 * Used to get resume template
 * Created On : 11-07-2017
 * Modified On
 */
router.post('/getResumeTemplate', customodule.verifyapicalltoken, function (req, res) {
    //console.log(req.body);
    var studentId = req.body.studentId;
    if (!studentId) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.PROVIDEUSERDETAILS);
    } else if (isNaN(studentId)) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.INVALIDUSER);
    } else {

        var fields = "u.firstname";
        var condition = "where u.id = '" + studentId + "'";
        var param = {
            from: 'from mdl_user as u',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {

            if (userRes.length > 0) {

                var fields = "mt.templateid, mr.*";
                var condition = "where studentid = " + studentId;
                var join = " LEFT JOIN mdl_resume_template_master mr ON mr.id=mt.templateid   ";
                var param = {
                    from: 'from mdl_resume_template mt',
                    condition: condition,
                    fields: fields,
                    join: join
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (resumeRes) {
                    if (resumeRes.length > 0) {
                        var response = {
                            resCode: 200,
                            response: resumeRes[0]
                        };
                        res.status(200).send(response);
                    } else {

                        res.status(200).send(RESPONSEMESSAGE.RESUME.NODATAFOUND);
                    }
                });

            } else {
                res.status(200).send(RESPONSEMESSAGE.RESUME.USERDOESNOTEXIST);
            }
        });
    }
});

/*
 * Used to get all resume template
 * Created On : 11-07-2017
 * Modified On
 */
router.post('/getAllResumeTemplates', customodule.verifyapicalltoken, function (req, res) {

    var fields = "*";
    var condition = "where 1";
    var param = {
        from: 'from mdl_resume_template_master',
        condition: condition,
        fields: fields
    };

    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (resumeRes) {
        if (resumeRes.length > 0) {
            var response = {
                resCode: 200,
                response: resumeRes
            };
            res.status(200).send(response);
        } else {

            res.status(201).send(RESPONSEMESSAGE.RESUME.NODATAFOUND);
        }
    });


});

/*
 * Used to add/edit resume cover letter
 * Created On : 12-07-2017
 * Modified On
 */
router.post('/addEditResumeCoverLetter', customodule.verifyapicalltoken, function (req, res) {
    var studentId = req.body.studentId;
    if (!studentId) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.PROVIDEUSERDETAILS);
    } else if (isNaN(studentId)) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.INVALIDUSER);
    } else if (req.body.date == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.DATEBLANKERROR);
    } else if (req.body.addressto == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.ADDRESSTOBLANKERROR);
    } else if (req.body.body == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.BODYBLANKERROR);
    } else if (req.body.body.split(" ").length > 100) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.BODYLIMITERROR);
    } else {

        var fields = "u.firstname";
        var condition = "where u.id = '" + studentId + "'";
        var param = {
            from: 'from mdl_user as u',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {

            if (userRes.length > 0) {

                var condition = "where studentid = '" + studentId + "'";
                var param = {
                    from: 'from mdl_resume_cover_letter',
                    condition: condition,
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (resumeRes) {
                    var data = {
                        studentid: studentId,
                        date: req.body.date,
                        addressto: req.body.addressto,
                        body: req.body.body,
                    };

                    if (resumeRes.length > 0) {

                        var updatecondition = {
                            id: resumeRes[0].id
                        };
                        var param = {
                            tablename: 'mdl_resume_cover_letter',
                            condition: updatecondition,
                            data: data
                        };

                        Mastermodel.commonUpdate(param, function (error) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (success) {
                            if (success) {
                                var response = {
                                    resCode: 200,
                                    resMessage: "Cover Letter updated succesfully",
                                    response: "Cover Letter updated succesfully"
                                };
                                res.status(200).send(response);
                            }
                        });

                    } else {

                        var insertParam = {
                            tablename: 'mdl_resume_cover_letter',
                            data: [data]
                        };
                        Mastermodel.commonInsert(insertParam, function (error) {
                            console.log('Insert', error);
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (success) {
                            var response = {
                                resCode: 200,
                                resMessage: "Cover letter created successfully",
                                response: "Cover Letter updated succesfully"
                            };
                            res.status(200).send(response);
                        });
                    }

                });

            } else {
                res.status(200).send(RESPONSEMESSAGE.RESUME.USERDOESNOTEXIST);
            }
        });
    }
});

/*
 * Used to add/edit resume introduction
 * Created On : 12-07-2017
 * Modified On
 */
router.post('/addEditResumeIntroduction', customodule.verifyapicalltoken, function (req, res) {
    var studentId = req.body.studentId;
    if (!studentId) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.PROVIDEUSERDETAILS);
    } else if (isNaN(studentId)) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.INVALIDUSER);
    } else if (req.body.body == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.BODYBLANKERROR);
    } else if (req.body.body.split(" ").length > 400) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.BODYLIMITERROR);
    } else {

        var fields = "u.firstname";
        var condition = "where u.id = '" + studentId + "'";
        var param = {
            from: 'from mdl_user as u',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {
            if (userRes.length > 0) {

                var condition = "where studentid = '" + studentId + "'";
                var param = {
                    from: 'from mdl_resume_introduction',
                    condition: condition,
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (resumeRes) {
                    var data = {
                        studentid: studentId,
                        body: req.body.body
                    };

                    if (resumeRes.length > 0) {

                        var updatecondition = {
                            id: resumeRes[0].id
                        };
                        var param = {
                            tablename: 'mdl_resume_introduction',
                            condition: updatecondition,
                            data: data
                        };

                        Mastermodel.commonUpdate(param, function (error) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (success) {
                            if (success) {
                                var response = {
                                    resCode: 200,
                                    resMessage: "Introduction updated succesfully",
                                    response: "Introduction updated succesfully"
                                };
                                res.status(200).send(response);
                            }
                        });

                    } else {

                        var insertParam = {
                            tablename: 'mdl_resume_introduction',
                            data: [data]
                        };
                        Mastermodel.commonInsert(insertParam, function (error) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (success) {
                            var response = {
                                resCode: 200,
                                resMessage: "Introduction created successfully",
                                response: "Introduction updated succesfully"
                            };
                            res.status(200).send(response);
                        });
                    }

                });

            } else {
                res.status(200).send(RESPONSEMESSAGE.RESUME.USERDOESNOTEXIST);
            }
        });
    }
});

/*
 * Used to add/edit resume objective
 * Created On : 12-07-2017
 * Modified On
 */
router.post('/addEditResumeObjective', customodule.verifyapicalltoken, function (req, res) {
    var studentId = req.body.studentId;
    if (!studentId) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.PROVIDEUSERDETAILS);
    } else if (isNaN(studentId)) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.INVALIDUSER);
    } else if (req.body.title == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.TITLEBLANKERROR);
    } else if (req.body.content == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.CONTENTBLANKERROR);
    } else if (req.body.content.split(" ").length > 100) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.CONTENTLIMITERROR);
    } else {

        var fields = "u.firstname";
        var condition = "where u.id = '" + studentId + "'";
        var param = {
            from: 'from mdl_user as u',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {

            if (userRes.length > 0) {

                var condition = "where studentid = '" + studentId + "'";
                var param = {
                    from: 'from mdl_resume_objective',
                    condition: condition,
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (resumeRes) {

                    var data = {
                        studentid: studentId,
                        title: req.body.title,
                        content: req.body.content,
                    };

                    if (resumeRes.length > 0) {

                        var updatecondition = {
                            id: resumeRes[0].id
                        };
                        var param = {
                            tablename: 'mdl_resume_objective',
                            condition: updatecondition,
                            data: data
                        };

                        Mastermodel.commonUpdate(param, function (error) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (success) {
                            if (success) {
                                var response = {
                                    resCode: 200,
                                    resMessage: "Objective updated succesfully",
                                    response: "Objective updated succesfully"
                                };
                                res.status(200).send(response);
                            }
                        });

                    } else {

                        var insertParam = {
                            tablename: 'mdl_resume_objective',
                            data: [data]
                        };
                        Mastermodel.commonInsert(insertParam, function (error) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (success) {
                            var response = {
                                resCode: 200,
                                resMessage: "Objective created successfully",
                                response: "Objective updated succesfully"
                            }
                            res.status(200).send(response);
                        });
                    }

                });

            } else {
                res.status(200).send(RESPONSEMESSAGE.RESUME.USERDOESNOTEXIST);
            }
        });
    }
});

/*
 * Used to add/edit resume Key Qualification
 * Created On : 12-07-2017
 * Modified On
 */
router.post('/addEditResumeKeyQualification', customodule.verifyapicalltoken, function (req, res) {

    var studentId = req.body.studentId;
    if (!studentId) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.PROVIDEUSERDETAILS);
    } else if (isNaN(studentId)) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.INVALIDUSER);
    } else if (req.body.title == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.TITLEBLANKERROR);
    } else if (req.body.content == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.CONTENTBLANKERROR);
    } else if (req.body.content.split(" ").length > 100) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.CONTENTLIMITERROR);
    } else {

        var fields = "u.firstname";
        var condition = "where u.id = '" + studentId + "'";
        var param = {
            from: 'from mdl_user as u',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {

            if (userRes.length > 0) {

                var condition = "where studentid = '" + studentId + "'";
                var param = {
                    from: 'from mdl_resume_key_qualifications',
                    condition: condition,
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (resumeRes) {

                    var data = {
                        studentid: studentId,
                        title: req.body.title,
                        content: req.body.content,

                    };

                    if (resumeRes.length > 0) {

                        var updatecondition = {
                            id: resumeRes[0].id
                        };
                        var param = {
                            tablename: 'mdl_resume_key_qualifications',
                            condition: updatecondition,
                            data: data
                        };

                        Mastermodel.commonUpdate(param, function (error) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (success) {
                            if (success) {
                                var response = {
                                    resCode: 200,
                                    resMessage: "Key Qualifications updated succesfully",
                                    response: "Key Qualifications updated succesfully"
                                };
                                res.status(200).send(response);
                            }
                        });

                    } else {

                        var insertParam = {
                            tablename: 'mdl_resume_key_qualifications',
                            data: [data]
                        };
                        Mastermodel.commonInsert(insertParam, function (error) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (success) {
                            var response = {
                                resCode: 200,
                                resMessage: "Key Qualifications created successfully",
                                response: "Key Qualifications updated succesfully"
                            };
                            res.status(200).send(response);
                        });
                    }

                });

            } else {
                res.status(200).send(RESPONSEMESSAGE.RESUME.USERDOESNOTEXIST);
            }
        });
    }
});

/*
 * Used to add/edit resume Other Experiences
 * Created On : 12-07-2017
 * Modified On
 */
router.post('/addEditResumeOtherExperiences', customodule.verifyapicalltoken, function (req, res) {

    var studentId = req.body.studentId;
    if (!studentId) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.PROVIDEUSERDETAILS);
    } else if (isNaN(studentId)) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.INVALIDUSER);
    } else if (req.body.title == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.TITLEBLANKERROR);
    } else if (req.body.content == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.CONTENTBLANKERROR);
    } else if (req.body.content.split(" ").length > 100) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.CONTENTLIMITERROR);
    } else {

        var fields = "u.firstname";
        var condition = "where u.id = '" + studentId + "'";
        var param = {
            from: 'from mdl_user as u',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {
            if (userRes.length > 0) {

                var condition = "where studentid = '" + studentId + "'";
                var param = {
                    from: 'from mdl_resume_other_experiences',
                    condition: condition,
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (resumeRes) {

                    var data = {
                        studentid: studentId,
                        title: req.body.title,
                        content: req.body.content,

                    };

                    if (resumeRes.length > 0) {

                        var updatecondition = {
                            id: resumeRes[0].id
                        };
                        var param = {
                            tablename: 'mdl_resume_other_experiences',
                            condition: updatecondition,
                            data: data
                        };

                        Mastermodel.commonUpdate(param, function (error) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (success) {
                            if (success) {
                                var response = {
                                    resCode: 200,
                                    resMessage: "Other Experiences updated succesfully",
                                    response: "Other Experiences updated succesfully"
                                }
                                res.status(200).send(response);
                            }
                        });

                    } else {

                        var insertParam = {
                            tablename: 'mdl_resume_other_experiences',
                            data: [data]
                        };
                        Mastermodel.commonInsert(insertParam, function (error) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (success) {
                            var response = {
                                resCode: 200,
                                resMessage: "Other Experiences created successfully",
                                response: "Other Experiences updated succesfully"
                            };
                            res.status(200).send(response);
                        });
                    }

                });

            } else {
                res.status(200).send(RESPONSEMESSAGE.RESUME.USERDOESNOTEXIST);
            }
        });
    }
});

/*
 * Used to add/edit resume template(select)
 * Created On : 12-07-2017
 * Modified On
 */
router.post('/addEditResumeTemplate', customodule.verifyapicalltoken, function (req, res) {

    var studentId = req.body.studentId;
    if (!studentId) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.PROVIDEUSERDETAILS);
    } else if (isNaN(studentId)) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.INVALIDUSER);
    } else if (req.body.templateId == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.TEMPLATEBLANKERROR);
    } else {

        var fields = "u.firstname";
        var condition = "where u.id = '" + studentId + "'";
        var param = {
            from: 'from mdl_user as u',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {
            if (userRes.length > 0) {

                var condition = "where studentid = '" + studentId + "'";
                var param = {
                    from: 'from mdl_resume_template',
                    condition: condition,
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (resumeRes) {

                    var data = {
                        studentid: studentId,
                        templateid: req.body.templateId,

                    };
                    if (resumeRes.length > 0) {

                        var updatecondition = {
                            id: resumeRes[0].id
                        };
                        var param = {
                            tablename: 'mdl_resume_template',
                            condition: updatecondition,
                            data: data
                        };

                        Mastermodel.commonUpdate(param, function (error) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (success) {
                            if (success) {
                                var response = {
                                    resCode: 200,
                                    resMessage: "Template selected succesfully",
                                    response: "Template selected succesfully"
                                };
                                res.status(200).send(response);
                            }
                        });

                    } else {

                        var insertParam = {
                            tablename: 'mdl_resume_template',
                            data: [data]
                        };
                        Mastermodel.commonInsert(insertParam, function (error) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (success) {
                            var response = {
                                resCode: 200,
                                resMessage: "Template selected successfully",
                                response: "Template selected succesfully"
                            };
                            res.status(200).send(response);
                        });
                    }

                });

            } else {
                res.status(200).send(RESPONSEMESSAGE.RESUME.USERDOESNOTEXIST);
            }
        });
    }
});

/*
 * Used to add/edit resume contact Information
 * Created On : 13-07-2017
 * Modified On
 */
router.post('/addEditResumeContactInformation', customodule.verifyapicalltoken, function (req, res) {

    var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var studentId = req.body.studentId;
    if (!studentId) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.PROVIDEUSERDETAILS);
    } else if (isNaN(studentId)) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.INVALIDUSER);
    } else if (req.body.firstname == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.FIRSTNAMEBLANKERROR);
    } else if (req.body.lastname == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.LASTNAMEBLANKERROR);
    } else if (req.body.address1 == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.ADDRESS1BLANKERROR);
    } else if (req.body.state == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.STATEBLANKERROR);
    } else if (req.body.zip == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.ZIPBLANKERROR);
    } else if (req.body.mobile == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.MOBILEBLANKERROR);
    } else if (isNaN(req.body.mobile)) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.INVALIDMOBILE);
    } else if (req.body.email == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.EMAILBLANKERROR);
    } else if (!pattern.test(req.body.email)) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.INVALIDEMAIL);
    } else {

        var fields = "u.firstname";
        var condition = "where u.id = '" + studentId + "'";
        var param = {
            from: 'from mdl_user as u',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {
            if (userRes.length > 0) {

                var condition = "where studentid = '" + studentId + "'";
                var param = {
                    from: 'from mdl_resume_contact_information',
                    condition: condition,
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (resumeRes) {
                    var data = {
                        studentid: studentId,
                        firstname: req.body.firstname,
                        lastname: req.body.lastname,
                        address1: req.body.address1,
                        address2: req.body.address2,
                        state: req.body.state,
                        zip: req.body.zip,
                        mobile: req.body.mobile,
                        email: req.body.email,
                    };

                    if (resumeRes.length > 0) {

                        var updatecondition = {
                            id: resumeRes[0].id
                        };
                        var param = {
                            tablename: 'mdl_resume_contact_information',
                            condition: updatecondition,
                            data: data
                        };

                        Mastermodel.commonUpdate(param, function (error) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (success) {
                            if (success) {
                                var response = {
                                    resCode: 200,
                                    resMessage: "Contact Information updated succesfully",
                                    response: "Contact Information updated succesfully"
                                };
                                res.status(200).send(response);
                            }
                        });

                    } else {

                        var insertParam = {
                            tablename: 'mdl_resume_contact_information',
                            data: [data]
                        };
                        Mastermodel.commonInsert(insertParam, function (error) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (success) {
                            var response = {
                                resCode: 200,
                                resMessage: "Contact Information created successfully",
                                response: "Contact Information updated succesfully"
                            };
                            res.status(200).send(response);
                        });
                    }

                });

            } else {
                res.status(200).send(RESPONSEMESSAGE.RESUME.USERDOESNOTEXIST);
            }
        });
    }
});


/*
 * Used to delete Resume work experiences
 * Created On : 11-07-2017
 * Modified On
 */
router.post('/deleteWorkExperience', customodule.verifyapicalltoken, function (req, res) {
    var workExpId = req.body.workExpId;
    if (!workExpId) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.PROVIDEWORKEXPDETAILS);
    } else if (isNaN(workExpId)) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.INVALIDWORKEXP);
    } else {

        var condition = "where id = " + workExpId;
        var param = {
            from: 'from mdl_work_experiences',
            condition: condition,
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (resumeRes) {
            if (resumeRes.length > 0) {

                var param = {
                    tablename: 'mdl_work_experiences',
                    condition: {
                        'id': workExpId
                    },
                };

                Mastermodel.commonDelete(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (success) {
                    var response = {
                        resCode: 200,
                        resMessage: "Work Experience deleted successfully",
                        response: "Work Experience deleted successfully"
                    };
                    res.status(200).send(response);
                });
            } else {

                res.status(200).send(RESPONSEMESSAGE.RESUME.NODATAFOUND);
            }
        });

    }
});

/*
 * Used to delete Resume Education Details
 * Created On : 11-07-2017
 * Modified On
 */
router.post('/deleteEducationDetails', customodule.verifyapicalltoken, function (req, res) {

    var eduDetailsId = req.body.eduDetailsId;
    if (!eduDetailsId) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.PROVIDEDUDETAILS);
    } else if (isNaN(eduDetailsId)) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.INVALIDEDUDETAILS);
    } else {

        var condition = "where id = " + eduDetailsId;
        var param = {
            from: 'from mdl_education',
            condition: condition,
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (resumeRes) {
            if (resumeRes.length > 0) {

                var param = {
                    tablename: 'mdl_education',
                    condition: {
                        'id': eduDetailsId
                    },
                };

                Mastermodel.commonDelete(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (success) {
                    var response = {
                        resCode: 200,
                        resMessage: "Education Details deleted successfully",
                        response: "Education Details deleted successfully"
                    };
                    res.status(200).send(response);
                });
            } else {

                res.status(200).send(RESPONSEMESSAGE.RESUME.NODATAFOUND);
            }
        });

    }
});



/*
 * Used to add/edit resume Work Experience
 * Created On : 13-07-2017
 * Modified On
 */
router.post('/addEditResumeWorkExperience', customodule.verifyapicalltoken, function (req, res) {

    if (!req.body.studentid) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.PROVIDUSERDETAILS);
    } else if (isNaN(req.body.studentid)) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.INVALIDUSERDETAILS);
    } else if (req.body.company == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.COMPANYBLANKERROR);
    } else if (req.body.startdate == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.STARTDATEBLANKERROR);
    } else if (req.body.currentcompany == '' || (req.body.currentcompany != 'Yes' && req.body.currentcompany != 'No')) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.CURRENTCOMPANYERROR);
    } else if (req.body.currentcompany == 'No' && req.body.enddate == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.ENDDATEBLANKERROR);
    } else if (req.body.currentcompany == 'No' && Date.parse(req.body.startdate) > Date.parse(req.body.enddate)) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.STARDATEGREATERERROR);
    } else if (req.body.position == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.POSITIONBLANKERROR);
    } else if (req.body.roles == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.ROLESBLANKERROR);
    } else if (req.body.roles.split(" ").length > 50) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.ROLESLIMIT);
    } else {
        var condition = "where id = '" + req.body.id + "'";
        var param = {
            from: 'from mdl_work_experiences',
            condition: condition,
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (resumeRes) {
            if (resumeRes.length > 0) {
                var param = {
                    tablename: 'mdl_work_experiences',
                    condition: {
                        'id': req.body.id
                    },
                };
                Mastermodel.commonDelete(param, function (err) {
                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                    },
                    function (success) {
                        var saveData = {};
                        saveData.studentid = req.body.studentid;
                        saveData.company = req.body.company;
                        saveData.startdate = req.body.startdate;
                        saveData.currentcompany = req.body.currentcompany;
                        saveData.enddate = req.body.enddate;
                        saveData.position = req.body.position;
                        saveData.roles = req.body.roles;
                        var param = {
                            tablename: 'mdl_work_experiences',
                            data: [saveData]
                        };
                        Mastermodel.commonInsert(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (data) {
                            var response = {
                                resCode: 200,
                                resMessage: "Work Experience added successfully",
                                response: "Work Experience updated successfully"
                            };
                            res.status(200).send(response);
                        });
                    });
            } else {
                var saveData = {};
                saveData.studentid = req.body.studentid;
                saveData.company = req.body.company;
                saveData.startdate = req.body.startdate;
                saveData.currentcompany = req.body.currentcompany;
                saveData.enddate = req.body.enddate;
                saveData.position = req.body.position;
                saveData.roles = req.body.roles;
                console.log(saveData);
                var param = {
                    tablename: 'mdl_work_experiences',
                    data: [saveData]
                }
                Mastermodel.commonInsert(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (data) {
                    var response = {
                        resCode: 200,
                        resMessage: "Work Experience updated successfully",
                        response: "Work Experience updated successfully"
                    };
                    res.status(200).send(response);
                });
            }
        });
    }
});



/*
 * Used to add/edit resume Education Details
 * Created On : 13-07-2017
 * Modified On
 */
router.post('/addEditResumeEducationDetails', customodule.verifyapicalltoken, function (req, res) {

    if (!req.body.studentid) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.PROVIDUSERDETAILS);
    } else if (isNaN(req.body.studentid)) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.INVALIDUSERDETAILS);
    } else if (req.body.qualification == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.QUALIFICATIONBLANKERROR);
    } else if (req.body.qualificationtype == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.QUALIFICATIONTYPEBLANKERROR);
    } else if (req.body.institution == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.INSTITUTIONBLANKERROR);
    } else if (req.body.date == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.DATEBLANKERROR);
    } else if (req.body.summary == '') {
        res.status(200).send(RESPONSEMESSAGE.RESUME.SUMMARYBLANKERROR);
    } else if (req.body.summary.split(" ").length > 50) {
        res.status(200).send(RESPONSEMESSAGE.RESUME.SUMMARYLIMIT);
    } else {
        var condition = "where id = '" + req.body.id + "'";
        var param = {
            from: 'from mdl_education',
            condition: condition,
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (resumeRes) {

            if (resumeRes.length > 0) {
                var param = {
                    tablename: 'mdl_education',
                    condition: {
                        'id': req.body.id
                    },
                };
                Mastermodel.commonDelete(param, function (err) {
                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                    },
                    function (success) {
                        var saveData = {};
                        saveData.studentid = req.body.studentid;
                        saveData.qualification = req.body.qualification;
                        saveData.qualificationtype = req.body.qualificationtype;
                        saveData.institution = req.body.institution;
                        saveData.date = req.body.date;
                        saveData.summary = req.body.summary;
                        var param = {
                            tablename: 'mdl_education',
                            data: [saveData]
                        };
                        Mastermodel.commonInsert(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (data) {
                            var response = {
                                resCode: 200,
                                resMessage: "Education Details  updated successfully",
                                response: "Education Details  updated successfully"
                            };
                            res.status(200).send(response);
                        });
                    });
            } else {
                var saveData = {};
                saveData.studentid = req.body.studentid;
                saveData.qualification = req.body.qualification;
                saveData.qualificationtype = req.body.qualificationtype;
                saveData.institution = req.body.institution;
                saveData.date = req.body.date;
                saveData.summary = req.body.summary;
                var param = {
                    tablename: 'mdl_education',
                    data: [saveData]
                };
                Mastermodel.commonInsert(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (data) {
                    var response = {
                        resCode: 200,
                        resMessage: "Education Details  added successfully",
                        response: "Education Details  added successfully"
                    };
                    res.status(200).send(response);
                });
            }
        });
    }
});



/*
 * Used to add work evidence
 * Created On : 02-08-2017
 * Modified On
 */
var work_evidence = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, pathEvidenceUpload + 'work_evidence');
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname)); //Appending extension
    }
})
router.post('/addWorkEvidence', customodule.verifyapicalltoken, multer({
    storage: work_evidence
}).single('img'), function (req, res) {
    if (req.body.title == '' || req.body.title == undefined) {
        res.status(200).send(RESPONSEMESSAGE.EVIDENCE.TITLEEMPTY);
    } else if (req.body.description == '' || req.body.description == undefined) {
        res.status(200).send(RESPONSEMESSAGE.EVIDENCE.DESCRIPTIONEMPTY);
    } else if (req.file.filename == '' || req.file.filename == undefined) {
        res.status(200).send(RESPONSEMESSAGE.EVIDENCE.FILEEMPTY);
    } else {
        var saveData = {};
        saveData.studentid = req.body.studentid;
        saveData.courseid = req.body.courseid;
        saveData.unitid = req.body.unitid;
        saveData.title = req.body.title;
        saveData.description = req.body.description;
        saveData.file = req.file.filename;
        saveData.issubmitted = 'N';
        saveData.rplsubmissionid = '0'; //need to check why this field value added
        var param = {
            tablename: 'mdl_work_evidence',
            data: [saveData]
        };

        Mastermodel.commonInsert(param, function (err) {
            res.status(200).send(RESPONSEMESSAGE.EVIDENCE.SOMETHINGWRONG);
        }, function (data) {
            res.status(200).send(RESPONSEMESSAGE.EVIDENCE.WORKEVIDENCESUCCESSUPLOAD);
        });
    }
});
/*
 * Used to add corporate training evidence
 * Created On : 02-08-2017
 * Modified On
 */
var corporate_training_evidence = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, pathEvidenceUpload + 'corporate_training_evidence')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
})
router.post('/addCorporateTrainingEvidence', customodule.verifyapicalltoken, multer({
    storage: corporate_training_evidence
}).single('img'), function (req, res) {
    if (req.body.title == '' || req.body.title == undefined) {
        res.status(200).send(RESPONSEMESSAGE.EVIDENCE.TITLEEMPTY);
    } else if (req.body.description == '' || req.body.description == undefined) {
        res.status(200).send(RESPONSEMESSAGE.EVIDENCE.DESCRIPTIONEMPTY);
    } else if (req.file.filename == '' || req.file.filename == undefined) {
        res.status(200).send(RESPONSEMESSAGE.EVIDENCE.FILEEMPTY);
    } else {
        var saveData = {};
        saveData.studentid = req.body.studentid;
        saveData.courseid = req.body.courseid;
        saveData.unitid = req.body.unitid;
        saveData.title = req.body.title;
        saveData.description = req.body.description;
        saveData.file = req.file.filename;
        saveData.issubmitted = 'N';
        saveData.rplsubmissionid = '0'; //need to check why this field value added
        var param = {
            tablename: 'mdl_corporate_training_evidence',
            data: [saveData]
        };
        Mastermodel.commonInsert(param, function (err) {
            res.status(200).send(RESPONSEMESSAGE.EVIDENCE.SOMETHINGWRONG);
        }, function (data) {
            res.status(200).send(RESPONSEMESSAGE.EVIDENCE.CORPORATETRAININGSUCCESSUPLOAD);
        });
    }
});
/*
 * Used to add education evidence
 * Created On : 02-08-2017
 * Modified On
 */
var corporate_training_evidence = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, pathEvidenceUpload + 'education_evidence');
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname)); //Appending extension
    }
})
router.post('/addeducationEvidence', customodule.verifyapicalltoken, multer({
    storage: corporate_training_evidence
}).single('img'), function (req, res) {
    if (req.body.title == '' || req.body.title == undefined) {
        res.status(200).send(RESPONSEMESSAGE.EVIDENCE.TITLEEMPTY);
    } else if (req.body.description == '' || req.body.description == undefined) {
        res.status(200).send(RESPONSEMESSAGE.EVIDENCE.DESCRIPTIONEMPTY);
    } else if (req.file.filename == '' || req.file.filename == undefined) {
        res.status(200).send(RESPONSEMESSAGE.EVIDENCE.FILEEMPTY);
    } else {
        var saveData = {};
        saveData.studentid = req.body.studentid;
        saveData.courseid = req.body.courseid;
        saveData.unitid = req.body.unitid;
        saveData.title = req.body.title;
        saveData.description = req.body.description;
        saveData.file = req.file.filename;
        saveData.issubmitted = 'N';
        saveData.rplsubmissionid = '0'; //need to check why this field value added
        var param = {
            tablename: 'mdl_education_evidence',
            data: [saveData]
        };

        Mastermodel.commonInsert(param, function (err) {
            res.status(200).send(RESPONSEMESSAGE.EVIDENCE.SOMETHINGWRONG);
        }, function (data) {
            res.status(200).send(RESPONSEMESSAGE.EVIDENCE.EDUCATIONEVIDENCESUCCESSUPLOAD);
        })
    }
});
/*
 * Used to add employer reference
 * Created On : 02-08-2017
 * Modified On
 */
var corporate_training_evidence = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, pathEvidenceUpload + 'employer_reference')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
});
router.post('/addEmployerReference', customodule.verifyapicalltoken, multer({
    storage: corporate_training_evidence
}).single('img'), function (req, res) {
    if (req.body.title == '' || req.body.title == undefined) {
        res.status(200).send(RESPONSEMESSAGE.EVIDENCE.TITLEEMPTY);
    } else if (req.body.description == '' || req.body.description == undefined) {
        res.status(200).send(RESPONSEMESSAGE.EVIDENCE.DESCRIPTIONEMPTY);
    } else if (req.file.filename == '' || req.file.filename == undefined) {
        res.status(200).send(RESPONSEMESSAGE.EVIDENCE.FILEEMPTY);
    } else {
        var saveData = {};
        saveData.studentid = req.body.studentid;
        saveData.courseid = req.body.courseid;
        saveData.unitid = req.body.unitid;
        saveData.title = req.body.title;
        saveData.description = req.body.description;
        saveData.file = req.file.filename;
        saveData.issubmitted = 'N';
        saveData.rplsubmissionid = '0'; //need to check why this field value added
        var param = {
            tablename: 'mdl_employer_reference',
            data: [saveData]
        };

        Mastermodel.commonInsert(param, function (err) {
            res.status(200).send(RESPONSEMESSAGE.EVIDENCE.SOMETHINGWRONG);
        }, function (data) {
            res.status(200).send(RESPONSEMESSAGE.EVIDENCE.EMPLOYERREFERENCESUCCESSUPLOAD);
        });
    }
});
/*
 * Used to add third party verification
 * Created On : 02-08-2017
 * Modified On
 */
var corporate_training_evidence = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, pathEvidenceUpload + 'third_party_verification');
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname)); //Appending extension
    }
});
router.post('/addThirdPartyVerification', customodule.verifyapicalltoken, multer({
    storage: corporate_training_evidence
}).single('img'), function (req, res) {
    if (req.body.title == '' || req.body.title == undefined) {
        res.status(200).send(RESPONSEMESSAGE.EVIDENCE.TITLEEMPTY);
    } else if (req.body.description == '' || req.body.description == undefined) {
        res.status(200).send(RESPONSEMESSAGE.EVIDENCE.DESCRIPTIONEMPTY);
    } else if (req.file.filename == '' || req.file.filename == undefined) {
        res.status(200).send(RESPONSEMESSAGE.EVIDENCE.FILEEMPTY);
    } else {
        var saveData = {};
        saveData.studentid = req.body.studentid;
        saveData.courseid = req.body.courseid;
        saveData.unitid = req.body.unitid;
        saveData.title = req.body.title;
        saveData.description = req.body.description;
        saveData.file = req.file.filename;
        saveData.issubmitted = 'N';
        saveData.rplsubmissionid = '0'; //need to check why this field value added
        var param = {
            tablename: 'mdl_third_party_verification',
            data: [saveData]
        };

        Mastermodel.commonInsert(param, function (err) {
            res.status(200).send(RESPONSEMESSAGE.EVIDENCE.SOMETHINGWRONG);
        }, function (data) {
            res.status(200).send(RESPONSEMESSAGE.EVIDENCE.THIRDPARTYVERIFICATIONSUCCESSUPLOAD);
        });
    }
});




/*
 * Used to get student course list
 * Created On : 05-09-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/getAllCourseList', customodule.verifyapicalltoken, function (req, res) {
    var searchText = req.body.searchText;
    var isAccrediated = req.body.isAccrediated;
    var studentId = req.body.studentId;
    var sortByParam = req.body.sortByParam;
    var base_url1 = "http://52.63.96.113:4444/share/courseimage/";

    if (node_module.validator.matches(searchText, SETTING.specialcharpattern)) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.INVALIDREQUESTSPECIALCHAR);
    } else {


        /**** Course Search By search Text and isAccrediated parameter  ****/
        var fields = " mcc.id,  mcc.name,  mcc.idnumber, mcc.description, mcc.isaccrediated , mcc.isentryteststudent , mcc.course_image as cimage,  (case when mcc.course_image=null then mcc.course_image else concat('" + base_url1 + "',mcc.course_image) end) as courseimage, msetm.is_qualified, msw.iswishlistadd, mslm.learning_mode, mslm.unit_id, mslm.course_id ";
        var condition = "where 1";
        if (searchText != "") {
            condition += " AND ( mcc.name LIKE '%" + searchText + "%' OR  mcc.idnumber LIKE '%" + searchText + "%' ) ";
        }
        if (isAccrediated != "") {
            condition += " AND  mcc.isaccrediated LIKE '%" + isAccrediated + "%'   ";
        }
        var join = " LEFT JOIN mdl_student_entry_test_master msetm   ON mcc.id = msetm.course_id  AND msetm.student_id = '" + studentId + "' ";
        join += " LEFT JOIN mdl_student_wishlist msw                 ON mcc.id = msw.courseid     AND msw.studentid    = '" + studentId + "' ";
        join += " LEFT JOIN mdl_student_learning_mode mslm           ON mcc.id = mslm.course_id   AND mslm.student_id  = '" + studentId + "'  AND ( mslm.unit_id = '' || mslm.unit_id IS NULL) ";
        if (sortByParam != undefined && sortByParam != '') {
            if (sortByParam == 'ascCourse') {
                var orderby = " order by mcc.name asc ";
            }
            if (sortByParam == 'descCourse') {
                var orderby = " order by mcc.name desc ";
            }
            if (sortByParam == 'ascPrice') {
                var orderby = " order by mcc.cprice asc ";
            }
            if (sortByParam == 'descPrice') {
                var orderby = " order by mcc.cprice desc ";
            }
        } else {
            var orderby = " order by mcc.id desc ";
        }
        var param = {
            from: 'from mdl_course_categories as mcc',
            condition: condition,
            fields: fields,
            join: join,
            orderby: orderby
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (courseRes) { //console.log(courseRes);
            if (courseRes.length > 0) {
                var response = {
                    resCode: 200,
                    response: courseRes
                };
                res.status(200).send(response);

            } else {
                res.status(201).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
            }
        });

    }
});



/*
 * Used to get trainer course list
 * Created On : 05-09-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/getAllTrainerCourseList', customodule.verifyapicalltoken, function (req, res) {
    var searchText = req.body.searchText;
    var isAccrediated = req.body.isAccrediated;
    /**** Course Search By search Text and isAccrediated parameter  ****/
    var fields = " mcc.id,  mcc.name,  mcc.idnumber, mcc.description, mcc.isaccrediated ";
    var condition = "where 1";
    if (searchText != "") {
        condition += " AND ( mcc.name LIKE '%" + searchText + "%' OR  mcc.idnumber LIKE '%" + searchText + "%' ) ";
    }
    if (isAccrediated != "") {
        condition += " AND  mcc.isaccrediated LIKE '%" + isAccrediated + "%'   ";
    }
    var param = {
        from: 'from mdl_course_categories as mcc',
        condition: condition,
        fields: fields
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (courseRes) { //console.log(courseRes);
        if (courseRes.length > 0) {
            var response = {
                resCode: 200,
                response: courseRes
            };
            res.status(200).send(response);

        } else {
            res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
        }
    });
});


/*
 * Used to get Trainer course details
 * Created On : 05-09-2017
 * Created By : Monalisa
 * Input      : courseid
 * Modified On
 */
router.post('/getTrainerCourseDetails', customodule.verifyapicalltoken, function (req, res) {
    var searchText = req.body.searchText;
    var isAccrediated = req.body.isAccrediated;
    var trainerid = req.body.trainerId;
    var sortByParam = req.body.sortByParam;
    /**** Course Search By search Text and isAccrediated parameter  ****/

    if (node_module.validator.matches(searchText, SETTING.specialcharpattern)) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.INVALIDREQUESTSPECIALCHAR);
    } else {

        var fields = " mcc.id,  mcc.name,  mcc.idnumber,mcc.course_image,  mcc.description, mcc.isaccrediated, mt.is_qualified, mt.unit_id, mtq.ispassed, mtq.isactiontaken, mtq.issubmited, mtq.comment , mtw.iswishlistadd ";
        var condition = "where 1";
        if (searchText != "") {
            condition += " AND ( mcc.name LIKE '%" + searchText + "%' OR  mcc.idnumber LIKE '%" + searchText + "%' ) ";
        }
        if (isAccrediated != "") {
            condition += " AND  mcc.isaccrediated LIKE '%" + isAccrediated + "%'   ";
        }
        var join = " LEFT JOIN mdl_entry_test_results_trainer mt     ON mcc.id = mt.course_id AND mt.unit_id = 0  AND mt.trainer_id = " + trainerid + " ";
        join += " LEFT JOIN mdl_trainer_qualification_master  mtq ON mcc.id = mtq.course_id AND mtq.unit_id = 0  AND mtq.trainer_id = " + trainerid + " AND mtq.isarchived != 1 ";
        join += " LEFT JOIN mdl_trainer_wishlist mtw              ON mcc.id  = mtw.course_id  AND mtw.trainer_id = " + trainerid + " AND mtw.unit_id = 0 ";
        if (trainerid != "") {
            condition += " AND ( mt.trainer_id = " + trainerid + " || mt.trainer_id = '' || mt.trainer_id IS NULL ) ";
        }
        //var orderby   = "order by mcc.id desc";
        if (sortByParam != undefined && sortByParam != '') {
            if (sortByParam == 'ascCourse') {
                var orderby = " order by mcc.name asc ";
            }
            if (sortByParam == 'descCourse') {
                var orderby = " order by mcc.name desc ";
            }
            if (sortByParam == 'ascPrice') {
                var orderby = " order by mcc.cprice asc ";
            }
            if (sortByParam == 'descPrice') {
                var orderby = " order by mcc.cprice desc ";
            }
        } else {
            var orderby = " order by mcc.id desc ";
        }
        var param = {
            from: 'from mdl_course_categories as mcc',
            condition: condition,
            fields: fields,
            join: join,
            orderby: orderby
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (courseRes) {
            if (courseRes.length > 0) {
                var response = {
                    resCode: 200,
                    response: courseRes
                };
                res.status(200).send(response);

            } else {
                res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
            }
        });


    }

});


/*
 * Used to get course details
 * Created On : 05-09-2017
 * Created By : Monalisa
 * Input      : courseid
 * Modified On
 */
router.post('/getCourseOverview', customodule.verifyapicalltoken, function (req, res) {
    var courseid = req.body.courseId;

    /**** Course Search By search Text and isAccrediated parameter  ****/
    var fields = " mcc.id,  mcc.name,  mcc.idnumber, mcc.description, mcc.isaccrediated ";
    var condition = "where 1";
    if (courseid != "") {
        condition += " AND  mcc.id = " + courseid + "  ";
    }
    var param = {
        from: 'from mdl_course_categories as mcc',
        condition: condition,
        fields: fields
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (courseRes) { //console.log(courseRes);
        if (courseRes.length > 0) {
            var response = {
                resCode: 200,
                response: courseRes[0]
            };
            res.status(200).send(response);

        } else {
            res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
        }
    });
});


/*
 * Used to get course unit details
 * Created On : 05-09-2017
 * Created By : Monalisa
 * Input      : courseId, trainerId
 * Modified On
 */
router.post('/getCourseUnitDetails', customodule.verifyapicalltoken, function (req, res) {

    var courseid = req.body.courseId;
    var trainerid = req.body.trainerId;

    /**** Course Search By search Text and isAccrediated parameter  ****/

    var fields = " mc.id, mc.id as uid, mc.category as cid, mc.fullname,  mc.shortname,  mc.idnumber, mc.summary, mc.unit_image, mt.is_qualified, mt.trainer_id,  mtq.ispassed,  mtq.isactiontaken,  mtq.issubmited, mtw.iswishlistadd";
    var condition = " where 1";
    if (courseid != "") {
        condition += " AND  mc.category = " + courseid + "   ";
    }
    /* if (trainerid != "")
         condition += " AND ( mt.trainer_id = " + trainerid + " || mt.trainer_id = '' || mt.trainer_id IS NULL ) ";*/
    var join = " LEFT JOIN mdl_entry_test_results_trainer    mt  ON mc.id  = mt.unit_id     AND mt.trainer_id = " + trainerid + " AND is_qualified = 'Y' ";
    join += " LEFT JOIN mdl_trainer_qualification_master  mtq ON mc.id  = mtq.unit_id    AND mtq.trainer_id = " + trainerid + " ";
    join += " LEFT JOIN mdl_trainer_wishlist mtw              ON mc.id  = mtw.unit_id    AND mtw.trainer_id = " + trainerid + " ";
    var param = {
        from: 'from mdl_course as mc',
        condition: condition,
        fields: fields,
        join: join
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (courseRes) { //console.log(courseRes);
        if (courseRes.length > 0) {
            var response = {
                resCode: 200,
                response: courseRes
            };
            res.status(200).send(response);

        } else {
            res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
        }
    });
});




/*
 * Used to get course details
 * Created On : 12-09-2017
 * Created By : Monalisa
 * Input      : courseid(int)
 * Modified On
 */
router.post('/getCourseDetails', customodule.verifyapicalltoken, function (req, res) {
    var courseId = req.body.courseId;
    if (!courseId) {
        res.status(200).send(RESPONSEMESSAGE.COURSE.NOCOURSEIDFOUND);
    } else if (isNaN(courseId)) {
        res.status(200).send(RESPONSEMESSAGE.COURSE.INVALIDCOURSE);
    } else {


        /**** Course details By courseId parameter  ****/
        var fields = " mcc.id,  mcc.name,  mcc.idnumber, mcc.description, mcc.isaccrediated,msetm.is_qualified ";
        var condition = " where 1  AND mcc.id = '" + courseId + "'";
        var join = " LEFT JOIN mdl_student_entry_test_master as msetm ON mcc.id = msetm.course_id";
        var param = {
            from: 'from mdl_course_categories mcc ',
            condition: condition,
            fields: fields,
            join: join
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (courseRes) { //console.log(courseRes);
            if (courseRes.length > 0) {

                if (courseRes[0].ispassed == 'Y') // if passed course details with unit details will send
                {


                    var fields = " mc.id, mc.fullname, mc.shortname, mc.idnumber, mc.summary ";
                    var condition = " where mc.category = '" + courseId + "' ";
                    var param = {
                        from: 'from mdl_course as mc',
                        condition: condition,
                        fields: fields
                    };
                    Mastermodel.commonSelect(param, function (err) {
                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                    }, function (unitRes) {

                        if (unitRes.length > 0) {
                            courseRes[0].unitdetails = unitRes;
                            res.status(200).send(courseRes[0]);
                        } else {
                            res.status(200).send(courseRes[0]);
                        }
                    });
                } else // if fail only course details will send
                {
                    var response = {
                        resCode: 200,
                        response: courseRes
                    };
                    res.status(200).send(courseRes);
                }

            } else {
                res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
            }
        });
    }
});


/*
 * Used to add Student Wishlist
 * Created On : 11-09-2017
 * Created By : Monalisa
 * Input      : studentId(int), courseId(int)
 * Modified On
 */
router.post('/addStudentWishlist', customodule.verifyapicalltoken, function (req, res) {
    if (req.body.studentId == '' || req.body.studentId == undefined) {
        res.status(200).send(RESPONSEMESSAGE.WISHLIST.NOSTUDENTIDFOUND);
    } else if (req.body.courseId == '' || req.body.courseId == undefined) {
        res.status(200).send(RESPONSEMESSAGE.WISHLIST.NOCOURSEIDFOUND);
    } else {
        /*** This sql used for checking whether this course already present the student wishlist or not. ***/
        var studentId = req.body.studentId;
        var courseId = req.body.courseId;
        var fields = " msw.id ";
        var condition = " where msw.studentid = '" + studentId + "' AND msw.courseid = '" + courseId + "' ";
        var param = {
            from: 'from mdl_student_wishlist as msw',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (wishlistRes) {
            if (wishlistRes.length > 0) {
                res.status(200).send(RESPONSEMESSAGE.WISHLIST.COURSEALREADYEXISTSINSTUDENTWISHLIST);
            } else {
                var fields = "id";
                var condition = "where id='" + courseId + "'";
                var param = {
                    from: 'from mdl_course_categories ',
                    condition: condition,
                    fields: fields
                };

                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (ansRes) {

                    if (ansRes.length > 0) {
                        /*** This is used for add data to trainer wishlist ***/
                        var saveData = {};
                        saveData.studentId = studentId;
                        saveData.courseId = courseId;
                        saveData.isactiontaken = 'N';
                        var d = new Date();
                        var m1 = d.getMonth() + 1;
                        var datetime = d.getFullYear() + "-" + m1 + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
                        saveData.addeddatetime = datetime;
                        var param = {
                            tablename: 'mdl_student_wishlist',
                            data: [saveData]
                        };
                        Mastermodel.commonInsert(param, function (err) {
                            res.status(200).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
                        }, function (data) {
                            res.status(200).send(RESPONSEMESSAGE.WISHLIST.STUDENTWISHLISTADDEDSUCCESSFULLY);
                        });


                    } else {
                        res.status(200).send(RESPONSEMESSAGE.WISHLIST.INVALIDCOURSEIDFOUND);
                    }


                });
            }
        });
    }
});



/*
 * Used to add Student Wishlist
 * Created On : 11-09-2017
 * Created By : Monalisa
 * Input      : studentId(int), courseId(int)
 * Modified On
 */
router.post('/addStudentCourselistaddpath', customodule.verifyapicalltoken, function (req, res) {
    if (req.body.studentId == '' || req.body.studentId == undefined) {
        res.status(200).send(RESPONSEMESSAGE.WISHLIST.NOSTUDENTIDFOUND);
    } else if (req.body.courseId == '' || req.body.courseId == undefined) {
        res.status(200).send(RESPONSEMESSAGE.WISHLIST.NOCOURSEIDFOUND);
    } else {
        /*** This sql used for checking whether this course already present the student wishlist or not. ***/
        var studentId = req.body.studentId;
        var courseId = req.body.courseId;
        var fields = " msw.id ";
        var condition = " where msw.student_id = '" + studentId + "' AND msw.course_id = '" + courseId + "' ";
        var param = {
            from: 'from mdl_student_entry_test_master as msw',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (wishlistRes) {
            if (wishlistRes.length > 0) {
                res.status(200).send(RESPONSEMESSAGE.WISHLIST.COURSEALREADYEXISTSINSTUDENTMYLIST);
            } else {

                var fields = "id";
                var condition = "where id='" + courseId + "'";
                var param = {
                    from: 'from mdl_course_categories ',
                    condition: condition,
                    fields: fields
                };

                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (ansRes) {

                    if (ansRes.length > 0) {


                        /*** This is used for add data to trainer wishlist ***/
                        var saveData = {};
                        saveData.student_id = studentId;
                        saveData.course_id = courseId;
                        saveData.is_qualified = 'Y';
                        saveData.is_entry_test_required = 'N';
                        var d = new Date();
                        var m1 = d.getMonth() + 1;
                        var datetime = d.getFullYear() + "-" + m1 + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
                        saveData.datetime = datetime;
                        var param = {
                            tablename: 'mdl_student_entry_test_master',
                            data: [saveData]
                        };
                        Mastermodel.commonInsert(param, function (err) {
                            res.status(200).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
                        }, function (data) {

                            var param = {
                                tablename: 'mdl_student_wishlist',
                                condition: {
                                    'courseid': courseId,
                                    'studentid': studentId
                                },
                            };
                            Mastermodel.commonDelete(param, function (err) {
                                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                            }, function (success) {
                                res.status(200).send(RESPONSEMESSAGE.WISHLIST.STUDENTMYLISTADDEDSUCCESSFULLY);
                            });

                        });


                    } else {
                        res.status(200).send(RESPONSEMESSAGE.WISHLIST.INVALIDCOURSEIDFOUND);
                    }


                });;
            }
        });
    }
});


/*
 * Used to get entry test question for student for course wise
 * Created On : 08-09-2017
 * Created By : Monalisa
 * Input      : courseId(int), unitId(int), studentId(int)
 * Modified On
 */
router.post('/getStudentEntryTestDetails', customodule.verifyapicalltoken, function (req, res) {
    var questionArr = [];
    var course_id = req.body.courseId;
    var unit_id = req.body.unitId;
    var student_id = req.body.studentId;

    if (!course_id) {
        res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NOCOURSEFOUND);
    } else if (isNaN(course_id)) {
        res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.INVALIDCOURSE);
    } else {
        /**** Used to get student entry test question list ****/
        var fields = " met.course_id, met.unit_id, met.id, met.question , met.question_type ";
        var condition = " where met.course_id = '" + course_id + "' ";
        var param = {
            from: 'from mdl_entry_test_question as met',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (entrytestRes) {
            var entryTestquestionLen = entrytestRes.length;
            var quesLen = entrytestRes.length;
            var queaArr = new Array();
            if (entrytestRes.length > 0) {
                /**** Used to get student entry test answer list ****/
                asyncForEach(entrytestRes, function (positionitem, positionindex, positionarr) {
                    var cnt = positionindex;
                    console.log(positionitem.id);
                    var fields = " met.* ";
                    var condition = " where met.question_id = '" + positionitem.id + "' ";
                    var param = {
                        from: 'from mdl_entry_test_answer as met',
                        condition: condition,
                        fields: fields
                    };

                    Mastermodel.commonSelect(param, function (err) {
                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                    }, function (ansRes) {
                        questionArr.push(positionindex);
                        var questionLen = questionArr.length;

                        queaArr.push({
                            "questionId": positionitem.id
                        });
                        entrytestRes[cnt].answer = ansRes;
                        cnt++;
                        if (quesLen == queaArr.length) {

                            var response = {
                                resCode: 200,
                                response: entrytestRes
                            };
                            res.status(200).send(response);
                        }
                    });
                });
            } else {
                res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NORECORDSFOUND);
            }
        });
    }
});




/*
 * Used to get RPL entry test question for student  for course wise
 * Created On : 08-09-2017
 * Created By : Monalisa
 * Input      : courseId(int)
 * Modified On
 */
router.post('/getRPLEntryTestDetails', customodule.verifyapicalltoken, function (req, res) {
    var questionArr = [];
    var course_id = req.body.courseId;
    var unit_id = req.body.unitId;
    var student_id = req.body.studentId;
    if (!course_id) {
        res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NOCOURSEFOUND);
    } else if (isNaN(course_id)) {
        res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.INVALIDCOURSE);
    } else {

        var fields = " mrsm.id  as rplId";
        var condition = " where mrsm.course_id = '" + course_id + "' AND  mrsm.unit_id = '" + unit_id + "' AND  mrsm.student_id = '" + student_id + "' AND `is_saved` = 'Y'  AND `is_submitted` = 'N'  AND is_valid = 'Y'  ";
        var orderby = " order by attempt desc";
        var limit = " 0,1 ";
        var param = {
            from: 'from mdl_rpl_process_submission_master as mrsm',
            condition: condition,
            fields: fields,
            orderby: orderby,
            limit: limit
        };

        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (stuRes) {

            if (stuRes.length > 0) {

                var rplId = stuRes[0].rplId;
                console.log("rplId", rplId);
                if (rplId > 0) {

                    /**** Used to get student entry test question list ****/
                    var fields = "met.id, met.question, met.question_type, me.student_answer, me.masterid, me.additional_comments ";
                    var condition = "where met.course_id = '" + course_id + "'       AND  met.unit_id = '" + unit_id + "'";
                    var join = "LEFT JOIN mdl_rpl_process_question_evidence me  ON me.question_id = met.id   AND  me.masterid = '" + rplId + "'  ";
                    var orderby = "order by met.id asc";
                    var param = {
                        from: 'from mdl_rpl_question as met',
                        condition: condition,
                        fields: fields,
                        join: join,
                        orderby: orderby
                    };
                    Mastermodel.commonSelect(param, function (err) {
                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                    }, function (entrytestRes) {
                        var entryTestquestionLen = entrytestRes.length;
                        if (entrytestRes.length > 0) {
                            /**** Used to get student entry test answer list ****/
                            asyncForEach(entrytestRes, function (positionitem, positionindex, positionarr) {
                                var cnt = positionindex;
                                console.log(positionitem.id);
                                var fields = " met.* ";
                                var condition = " where met.question_id = '" + positionitem.id + "' ";
                                var param = {
                                    from: 'from mdl_rpl_answer as met',
                                    condition: condition,
                                    fields: fields
                                };
                                Mastermodel.commonSelect(param, function (err) {
                                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                                }, function (ansRes) {

                                    entrytestRes[cnt].answer = ansRes;

                                    /***** additional file  ******/
                                    var fields = " mf.* ";
                                    var condition = " where mf.question_id = '" + positionitem.id + "' AND masterid = '" + rplId + "' ";
                                    var param = {
                                        from: 'from mdl_rpl_question_additional_file as mf',
                                        condition: condition,
                                        fields: fields
                                    };
                                    Mastermodel.commonSelect(param, function (err) {
                                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                                    }, function (additionalRes) {
                                        entrytestRes[cnt].additionalFiles = additionalRes;
                                        cnt++;
                                        questionArr.push(positionindex);
                                        var questionLen = questionArr.length;
                                        console.log(entryTestquestionLen, questionLen);

                                        if (entryTestquestionLen == questionLen) {

                                            var response = {
                                                resCode: 200,
                                                response: entrytestRes
                                            };
                                            res.status(200).send(response);
                                        }
                                    });
                                    /***** additional file  ******/

                                });
                            });




                        } else {
                            res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NORECORDSFOUND);
                        }
                    });

                }

            } else { // no saved data

                /**** Used to get student entry test question list ****/
                var fields = " met.id, met.question , met.question_type";
                var condition = " where met.course_id = '" + course_id + "' AND  met.unit_id = '" + unit_id + "'   ";
                var orderby = " order by met.id asc";
                var param = {
                    from: 'from mdl_rpl_question as met',
                    condition: condition,
                    fields: fields,
                    orderby: orderby
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (entrytestRes) {
                    console.log(entrytestRes);
                    var entryTestquestionLen = entrytestRes.length;
                    if (entrytestRes.length > 0) {
                        /**** Used to get student entry test answer list ****/
                        asyncForEach(entrytestRes, function (positionitem, positionindex, positionarr) {
                            var cnt = positionindex;
                            console.log(positionitem.id);
                            var fields = " met.* ";
                            var condition = " where met.question_id = '" + positionitem.id + "' ";
                            var param = {
                                from: 'from mdl_rpl_answer as met',
                                condition: condition,
                                fields: fields
                            };
                            Mastermodel.commonSelect(param, function (err) {
                                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                            }, function (ansRes) {
                                questionArr.push(positionindex);
                                var questionLen = questionArr.length;
                                // console.log(entrytestRes[positionindex]);
                                //  console.log(entrytestRes);
                                entrytestRes[cnt].answer = ansRes;
                                cnt++;
                                console.log(entryTestquestionLen, questionLen);
                                if (entryTestquestionLen == questionLen) {

                                    var response = {
                                        resCode: 200,
                                        response: entrytestRes
                                    };
                                    res.status(200).send(response);
                                }
                            });
                        });
                    } else {
                        res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NORECORDSFOUND);
                    }
                });

            }
        });



    }


});


/*
 * Used to post RPL entry test question from student for course wise
 * Created On : 08-09-2017
 * Created By : Monalisa
 * Input      : courseId(int)
 * Modified On
 */
router.post('/postStudentEntryTestResult', customodule.verifyapicalltoken, function (req, res) {
    var course_id = req.body.courseId;
    var total = req.body.total;
    var correct = req.body.correct;
    var student_id = req.body.studentId;
    var questionArr = req.body.questionArr;
    var questionidArr = req.body.questionidArr;
    var correctansArr = req.body.correctansArr;
    var studentansArr = req.body.studentansArr;


    if (!course_id) {
        res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.NOCOURSEFOUND);
    } else if (isNaN(course_id)) {
        res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.INVALIDCOURSE);
    } else {

        var fields = "id, is_qualified";
        var condition = " where student_id ='" + student_id + "' AND course_id = '" + course_id + "'  ";
        var param = {
            from: 'from mdl_student_entry_test_master ',
            condition: condition,
            fields: fields
        };


        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (ansRes) {
            if (ansRes.length > 0) {
                res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.STUDENTALREADYQUALIFIED);
            } else {

                var fields = "id";
                var condition = "where id='" + course_id + "'";
                var param = {
                    from: 'from mdl_course_categories ',
                    condition: condition,
                    fields: fields
                };

                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (ansRes) {

                    if (ansRes.length > 0) {


                        var percentagecalc = Math.round((correct / total) * 100);
                        if (percentagecalc >= 70) {

                            /************* Delete From wishlist this course *******************/

                            var param = {
                                tablename: 'mdl_student_wishlist',
                                condition: {
                                    'courseid': req.body.courseId,
                                    'studentid': req.body.studentId
                                },
                            };

                            Mastermodel.commonDelete(param, function (err) {
                                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                            }, function (success) {

                                /*******************************************************************/


                                var saveData = {};
                                saveData.course_id = req.body.courseId;
                                saveData.student_id = req.body.studentId;
                                saveData.is_qualified = 'Y';
                                var d = new Date();

                                var m1 = d.getMonth();
                                var month = m1 + 1;
                                var datetime = d.getFullYear() + "-" + month + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
                                saveData.datetime = datetime;
                                var param = {
                                    tablename: 'mdl_student_entry_test_master',
                                    data: [saveData]
                                };
                                Mastermodel.commonInsert(param, function (err) {
                                    res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                                }, function (data) {
                                    var total = questionArr.length;
                                    var qcountArr = [];

                                    asyncForEach(questionArr, function (positionitem, positionindex, positionarr) {

                                        var j = positionindex;
                                        // for (var j = 0; j < total; j++) {
                                        var saveData = {};
                                        saveData.master_id = data.insertId;
                                        saveData.question_id = questionidArr[j];
                                        saveData.question = questionArr[j];
                                        saveData.student_ans = studentansArr[j];
                                        saveData.correct_ans = correctansArr[j];
                                        saveData.datetime = datetime;

                                        var param = {
                                            tablename: 'mdl_student_entry_test_details',
                                            data: [saveData]
                                        };
                                        Mastermodel.commonInsert(param, function (err) {
                                            res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                                        }, function (data) {
                                            qcountArr.push(positionindex);
                                            var questionLen = qcountArr.length;

                                            if (total == questionLen) {
                                                var notificationparam = {};
                                                notificationparam.userId = student_id;
                                                notificationparam.courseId = course_id;
                                                notificationparam.unitId = 0;
                                                notificationparam.userType = 'Student';
                                                notificationparam.pushNotificationCode = NOTIFICATIONMESSAGE.STUDENT.ENTRYTESTQUALIFIEDSUCCESS;
                                                Masternotificationmodel.sendPushNotification(notificationparam, function (err) {
                                                    var response = {
                                                        resCode: 201,
                                                        response: error
                                                    }
                                                    res.status(200).send(response);
                                                }, function (succNotificationRes) {
                                                    var response = {
                                                        resCode: 200,
                                                        resMessage: "You have qualified in entry test successfully",
                                                        response: "You have qualified in entry test successfully"
                                                    };
                                                    res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.STUDENTQUALIFIED);
                                                });
                                            }

                                        });

                                    });
                                });
                                /*******************************************************************/

                            });


                        } else {
                            var notificationparam = {};
                            notificationparam.userId = student_id;
                            notificationparam.courseId = course_id;
                            notificationparam.unitId = 0;
                            notificationparam.userType = 'Student';
                            notificationparam.pushNotificationCode = NOTIFICATIONMESSAGE.STUDENT.ENTRYTESTQUALIFIEDFAILURE;
                            Masternotificationmodel.sendPushNotification(notificationparam, function (err) {
                                var response = {
                                    resCode: 201,
                                    response: error
                                }
                                res.status(200).send(response);
                            }, function (succNotificationRes) {
                                var response = {
                                    resCode: 202,
                                    response: "Sorry, You are not qualified. Please try again!!"
                                };
                                res.status(202).send(response);
                            });

                        }

                    } else {

                        res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.NOCOURSEFOUND);

                    } //end of else


                }); // end of course id select

            } //end of else

        }); //end of select

    } //end of else
}); //end of post



/*
 * Used to get RPL Question
 * Created On : 08-09-2017
 * Created By : Monalisa
 * Input      : courseId(int)
 * Modified On
 */
router.post('/getRPLQuestionList', customodule.verifyapicalltoken, function (req, res) {


    /**** Used to get RPL question list ****/
    var fields = " mrq.* ";
    var condition = " where 1 ";
    var param = {

        from: 'from mdl_rpl_question as mrq',
        condition: condition,
        fields: fields
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (rplRes) {
        if (rplRes.length > 0) {
            res.status(200).send(rplRes);
        } else {
            res.status(200).send(RESPONSEMESSAGE.RPL.NORECORDSFOUND);
        }
    });

});



/*
 * Used to get my course details
 * Created On : 15-09-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
router.post('/getMyStudentCourseList', customodule.verifyapicalltoken, function (req, res) {

    var searchText = req.body.searchText;
    var isAccrediated = req.body.isAccrediated;
    var studentId = req.body.studentId;
    var sortByParam = req.body.sortByParam;

    if (node_module.validator.matches(searchText, SETTING.specialcharpattern)) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.INVALIDREQUESTSPECIALCHAR);
    } else {

        /**** Course Search By search Text and isAccrediated parameter  ****/
        var fields = " mcc.id,  mcc.name,  mcc.idnumber, mcc.description, mcc.isaccrediated , mcc.isentryteststudent , mcc.course_image as cimage,  (case when mcc.course_image=null then mcc.course_image else concat('" + base_url_course_image + "',mcc.course_image) end) as courseimage, msetm.is_qualified, msw.iswishlistadd, mslm.learning_mode, mslm.unit_id, mslm.course_id ";
        // /*, mrpsm.is_submitted,  mrpsm.is_actiontaken, mrpsm.is_approved , mrpsm.final_submitted , mrpsm.id as rpl_submission_id,  mrpsm.trainer_status , mrpsm.trainer_id
        var condition = "where 1";
        if (searchText != "") {
            condition += " AND ( mcc.name LIKE '%" + searchText + "%' OR  mcc.idnumber LIKE '%" + searchText + "%' ) ";
        }
        if (isAccrediated != "") {
            condition += " AND  mcc.isaccrediated LIKE '%" + isAccrediated + "%'   ";
        }
        var join = " INNER JOIN mdl_student_entry_test_master msetm      ON mcc.id = msetm.course_id  AND msetm.student_id = '" + studentId + "' ";
        join += " LEFT JOIN mdl_student_wishlist msw                 ON mcc.id = msw.courseid     AND msw.studentid    = '" + studentId + "' ";
        join += " LEFT JOIN mdl_student_learning_mode mslm           ON mcc.id = mslm.course_id   AND mslm.student_id  = '" + studentId + "'  AND ( mslm.unit_id = '' || mslm.unit_id IS NULL) ";
        if (sortByParam != undefined && sortByParam != '') {
            if (sortByParam == 'ascCourse') {
                var orderby = " order by mcc.name asc ";
            }
            if (sortByParam == 'descCourse') {
                var orderby = " order by mcc.name desc ";
            }
            if (sortByParam == 'ascPrice') {
                var orderby = " order by mcc.cprice asc ";
            }
            if (sortByParam == 'descPrice') {
                var orderby = " order by mcc.cprice desc ";
            }
        } else {
            var orderby = " order by mcc.id desc ";
        }
        var param = {
            from: 'from mdl_course_categories as mcc',
            condition: condition,
            fields: fields,
            join: join,
            orderby: orderby
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (courseRes) { //console.log(courseRes);
            if (courseRes.length > 0) {
                var response = {
                    resCode: 200,
                    response: courseRes
                };
                res.status(200).send(response);

            } else {
                res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
            }
        });

    }

});


/*
 * Used to upload evidence for RPL questions
 * Created On : 20-09-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
var work_evidence = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, pathEvidenceUpload + 'all_evidence')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
})
router.post('/uploadStudentAllEvidence', customodule.verifyapicalltoken, multer({
    storage: work_evidence
}).single('img'), function (req, res) {
    if (req.body.title == '' || req.body.title == undefined) {
        res.status(200).send(RESPONSEMESSAGE.EVIDENCE.TITLEEMPTY);
    } else if (req.body.description == '' || req.body.description == undefined) {
        res.status(200).send(RESPONSEMESSAGE.EVIDENCE.DESCRIPTIONEMPTY);
    } else if (req.file.filename == '' || req.file.filename == undefined) {
        res.status(200).send(RESPONSEMESSAGE.EVIDENCE.FILEEMPTY);
    } else {

        var saveData = {};
        saveData.studentid = req.body.studentId;
        saveData.courseid = req.body.courseId;
        saveData.unitid = req.body.unitId;
        saveData.questionid = req.body.questionId;
        saveData.title = req.body.title;
        saveData.description = req.body.description;
        saveData.file = req.file.filename;
        saveData.issubmitted = 'N';
        saveData.rplsubmissionid = '0'; //need to check why this field value added


        var fields = " mrpsm.id";
        var condition = " where 1 AND studentid = '" + studentId + "' AND courseid = '" + courseid + "' AND unitid = '" + unitid + "'  AND  questionid = '" + questionid + "' AND issubmitted  = 'N' ";
        var param = {
            from: 'from mdl_rpl_process_submission_master as mrpsm',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (rplRes) {

            if (rplRes.length > 0) { // already inserted, need masterid
                var masterid = rplRes[0].id;
                /* insert data into child table */
                var saveData = {};
                saveData.masterid = masterid;
                saveData.title = title;
                saveData.description = description;
                saveData.statusid = 2;
                saveData.file = file;
                var param = {
                    tablename: 'mdl_rpl_process_question_evidence',
                    data: [saveData]
                };
                Mastermodel.commonInsert(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                }, function (data) {
                    //echo success message  here
                });

            } else { // we need to insert in master table

                var fields = " mrpsm.attempt";
                var condition = " where 1 AND studentid = '" + studentId + "' AND courseid = '" + courseid + "' AND unitid = '" + unitid + "'  AND  questionid = '" + questionid + "' AND issubmitted  = 'Y' AND isactiontaken = 'Y' AND  isapproved    = 'N' ";
                var param = {
                    from: 'from mdl_rpl_process_submission_master as mrpsm',
                    condition: condition,
                    fields: fields
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (attemptRes) {

                    if (attemptRes.length > 0) { // already inserted, need masterid
                        var saveData = {};
                        var attempt = attemptRes[0].attempt + 1;
                        var d = new Date();
                        var m1 = d.getMonth() + 1;
                        var datetime = d.getFullYear() + "-" + m1 + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
                        //insert in master table...
                        saveData.attempt = attempt;
                        saveData.studentid = studentid;
                        saveData.courseid = courseid;
                        saveData.unitid = unitid;
                        saveData.rplsubmissionid = 0;
                        saveData.statusid = 2;
                        saveData.submissiondatetime = datetime;
                        saveData.issubmitted = 'N';
                        var param = {
                            tablename: 'mdl_rpl_process_submission_master',
                            data: [saveData]
                        }
                        Mastermodel.commonInsert(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                        }, function (data) {

                            var masterid = data.insertId;
                            /* insert data into child table */
                            var saveData = {};
                            saveData.masterid = masterid;
                            saveData.title = title;
                            saveData.description = description;
                            saveData.statusid = 2;
                            saveData.file = file;
                            var param = {
                                tablename: 'mdl_rpl_process_question_evidence',
                                data: [saveData]
                            }
                            Mastermodel.commonInsert(param, function (err) {
                                res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                            }, function (data) {
                                //echo success message  here
                            })

                        })


                    } else { // we need to insert in master table

                        var d = new Date();
                        var m1 = d.getMonth() + 1;
                        var datetime = d.getFullYear() + "-" + m1 + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
                        var attempt = 1;

                        var saveData = {};
                        saveData.attempt = attempt;
                        saveData.studentid = studentid;
                        saveData.courseid = courseid;
                        saveData.unitid = unitid;
                        saveData.rplsubmissionid = 0;
                        saveData.statusid = 2;
                        saveData.submissiondatetime = datetime;
                        saveData.issubmitted = 'N';
                        var param = {
                            tablename: 'mdl_rpl_process_submission_master',
                            data: [saveData]
                        };
                        Mastermodel.commonInsert(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                        }, function (data) {;
                            var masterid = data.insertId;
                            /* insert data into child table */
                            var saveData = {};
                            saveData.masterid = masterid;
                            saveData.title = title;
                            saveData.description = description;
                            saveData.statusid = 2;
                            saveData.file = file;

                            var param = {
                                tablename: 'mdl_rpl_process_question_evidence',
                                data: [saveData]
                            };
                            Mastermodel.commonInsert(param, function (err) {
                                res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                            }, function (data) {
                                //echo success message  here
                            });

                        });

                    }

                });
            }
        });

    }

});


router.post('/testapi', customodule.verifyapicalltoken, function (req, res) {

    /*** This is used for add data to trainer wishlist ***/
    var saveData = {};
    saveData.firstname = req.body.firstname;
    saveData.lastname = req.body.lastname;
    saveData.email = req.body.email;
    var param = {
        tablename: 'test',
        data: [saveData]
    };
    Mastermodel.commonInsert(param, function (err) {
        res.status(200).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
    }, function (data) {
        res.status(200).send(RESPONSEMESSAGE.WISHLIST.STUDENTWISHLISTADDEDSUCCESSFULLY);
    });


});
var test_file = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, pathTestUpload + 'test_file')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname)); //Appending extension
    }
})
//multer({ storage: test_file }).single('image'),
router.post('/testfileuploadapi', customodule.verifyapicalltoken, multer({
    storage: test_file
}).single('image'), function (req, res) {

    /*** This is used for add data to trainer wishlist ***/
    /*var saveData           = {};
    saveData.firstname     = req.body.firstname;
    saveData.lastname      = req.body.lastname;
    saveData.email         = req.body.email;
    var param = {
          dbsetting  : dbsetting,
          tablename  :'test',
          data       : [saveData]
      }
     Mastermodel.commonInsert(param,function(err){
      res.status(200).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
     },function(data){
      res.status(200).send(RESPONSEMESSAGE.WISHLIST.STUDENTWISHLISTADDEDSUCCESSFULLY);
    })*/


});

/*
router.post('/testlogin',customodule.verifyapicalltoken, function(req, res, next){

   var c        = client.create({
       wwwroot: "http://localhost/moodlev2/"
   });

   c.authenticate({
       username: "student1",
       password: "Student1@123"
   }, function (error,data){

       console.log(data);
       /*if (!error) {
           // Client is authenticated and ready to be used.
       }*/
/*    });

});*/


/*
 * Used to get student qualified course list
 * Created On : 12-09-2017
 * Created By : Monalisa
 * Input      : studentId(int)
 * Modified On
 */

/*************** First Milestone ****************************/
/*
 * Used for user registration
 * Created On : 24-10-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/userRegistration', function (req, res) {

    var fname1 = req.body.fname;
    var lname1 = req.body.lname;
    var email1 = req.body.email;
    var phone1 = req.body.phone;
    var username1 = req.body.username;
    var password1 = req.body.password;
    var imageUrl = req.body.imageUrl;
    var socialUserId = req.body.socialUserId.toString();
    var regType = req.body.regType;
    var userType = req.body.userType;
    var city = req.body.city;
    var country = req.body.country;
    var state = req.body.state;
    var address1 = req.body.address1;
    var address2 = req.body.address2;
    var zip = req.body.zip;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;

    var fname = fname1.trim();
    var lname = lname1.trim();
    var email = email1.trim();
    var username = username1.trim();
    var password = password1; //.trim();
    var phone = phone1.trim();


    if (!fname1 || !lname1 || !email1 || !phone1 || !username1 || !password1) {
        res.status(201).send(RESPONSEMESSAGE.USER.REQUIREDFIELDERROR);
    } else if (fname == "" || lname == "" || email == "" || username == "" || password == "" || phone == "") {
        res.status(201).send(RESPONSEMESSAGE.USER.REQUIREDFIELDERROR);
    } else {
        /**** Check for username ******/
        var fields = " mu.id ";
        var condition = " where mu.username = '" + username + "' ";
        var param = {
            from: 'from mdl_user as mu',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            res.status(201).send(DATABASECONNECTIONERROR);
        }, function (userRes) {

            if (userRes.length > 0) {
                res.status(201).send(RESPONSEMESSAGE.USER.USERNAMEEXISTS);
            } else {
                /**** Check for email ***********/
                var fields = " mu.id ";
                var condition = " where mu.email = '" + email + "' ";
                var param = {
                    from: 'from mdl_user as mu',
                    condition: condition,
                    fields: fields
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                    res.status(201).send(DATABASECONNECTIONERROR);
                }, function (userRes) {

                    if (userRes.length > 0) {
                        res.status(201).send(RESPONSEMESSAGE.USER.USERALREADYEXIST);
                    } else {
                        var encryptedPass = node_module.crypto.createHash('md5').update(customodule.setting.passprefix + password).digest("hex");
                        console.log("ssss" + socialUserId);
                        var socialUserIdVal = socialUserId;
                        var saveData = {};
                        saveData.auth = 'manual';
                        saveData.username = username;
                        saveData.password = encryptedPass;
                        saveData.email = email;
                        saveData.firstname = fname;
                        saveData.lastname = lname;
                        saveData.phone1 = phone;
                        saveData.city = city;
                        saveData.country = country;
                        saveData.socialimageurl = imageUrl;
                        saveData.socialuserid = socialUserIdVal.toString();
                        saveData.regtype = regType;
                        saveData.state = state;
                        saveData.address1 = address1;
                        saveData.address2 = address2;
                        saveData.zip = zip;
                        saveData.userType = userType;
                        saveData.latitude = latitude;
                        saveData.longitude = longitude;
                        var param = {
                            tablename: 'mdl_user',
                            data: [saveData]
                        }
                        //Mastermodel.commonInsert(param,function(err){
                        var insertUserSql = "INSERT INTO `mdl_user` (`auth`, `username`, `password`, `email`, `firstname`, `lastname`, `phone1`, `city`, `country`, `socialimageurl`, `socialuserid`, `regtype`, `state`, `address1`, `address2`, `zip`, `userType`) VALUES ('manual', '" + username + "', '" + encryptedPass + "', '" + email + "', '" + fname + "', '" + lname + "', '" + phone + "', '" + city + "', '" + country + "', '" + imageUrl + "', '" + socialUserIdVal + "', '" + regType + "', '" + state + "', '" + address1 + "', '" + address2 + "', '" + zip + "', '" + userType + "')";
                        connection.query(insertUserSql, function (insertUserError, insertUserData, insertUserFields) {

                            if (insertUserError == null) {
                                var userid = insertUserData.insertId;
                                /****** insert into user otp verification table ***********************/
                                //var otp = Math.floor(Math.random() * 10000) + 1;
                                var otp = commonFunction.randomFixedInteger(4);
                                var saveData = {};
                                saveData.user_id = userid;
                                saveData.type = 'registration';
                                saveData.otp = otp;
                                var param = {
                                    tablename: 'mdl_user_opt_verification',
                                    data: [saveData]
                                };
                                Mastermodel.commonInsert(param, function (err) {
                                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                                    res.status(201).send(RESPONSEMESSAGE.USER.OTPWRONGUSER);
                                }, function (data) {
                                    // mail send for otp will call here
                                    var emailObj = {
                                        username: username,
                                        email: email,
                                        otp: otp
                                    };
                                    customodule.mail('otpverification', emailObj);
                                    var userObj = {
                                        userid: userid,
                                        usertype: userType
                                    };
                                    var response = {
                                        resCode: 200,
                                        response: userObj
                                    };
                                    res.status(200).send(response);
                                });
                            } else {
                                res.status(201).send(RESPONSEMESSAGE.USER.DATABASECONNECTIONERROR);
                            }
                        });
                    }
                });
            }
        });
    }
});


/*
 * Check user OTP
 * Created On : 24-10-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/checkUserOtp', function (req, res) {

    var userId = req.body.userId;
    var otp = req.body.otp;
    var otptype = req.body.otptype;

    /**** Used to get RPL question list ****/
    var fields = " muov.id,muov.otp";
    var condition = " where 1 AND muov.user_id = '" + userId + "' and isused = '0' and type = '" + otptype + "' ";
    var orderby = " order by id desc ";
    var param = {
        from: 'from mdl_user_opt_verification as muov',
        condition: condition,
        fields: fields,
        orderby: orderby
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (rplRes) {
        if (rplRes.length > 0) {
            if (rplRes[0].otp == otp) {

                var dataupdate = {
                    isverified: 1
                }
                var updatecondition = {
                    id: userId
                }
                var param3 = {
                    tablename: 'mdl_user',
                    condition: updatecondition,
                    data: dataupdate
                }
                Mastermodel.commonUpdate(param3, function (error) {
                    res.status(201).send(RESPONSEMESSAGE.USER.OTPNOTVALID);
                }, function (data4) {

                    var dataupdate = {
                        isused: 1
                    };
                    var updatecondition = {
                        user_id: userId,
                        type: 'registration',
                        otp: otp
                    };
                    var param3 = {
                        tablename: 'mdl_user_opt_verification',
                        condition: updatecondition,
                        data: dataupdate
                    };
                    Mastermodel.commonUpdate(param3, function (error) {
                        res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONGUSER);
                    }, function (data4) {

                        var fields = " username,email,id";
                        var condition = " where id = '" + userId + "' ";
                        var param = {
                            from: 'from mdl_user',
                            condition: condition,
                            fields: fields
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (userRes) {

                            var username = userRes[0].username;
                            var email = userRes[0].email;

                            // mail send for account verification
                            var emailObj = {
                                username: username,
                                email: email
                            };
                            customodule.mail('accountverification', emailObj);

                            /**********************For Token ***************************/
                            /* Create Expiry time of access_token for a logged in user */
                            var ret = new Date();
                            var exp_time = ret.setTime(ret.getTime() + 60 * 24 * 60000);
                            //var exp_time = ret.setTime(ret.getTime() +  60000);
                            var expiredate = node_module.datetime.create(new Date(exp_time));
                            var expire = expiredate.format("Y-m-d H:M:S");
                            var access_token = node_module.jwt.sign({
                                time: new Date().getTime()
                            }, customodule.setting.jwtsecret);
                            /* End */
                            var saveData = {};
                            saveData.user_id = userRes[0].id;
                            saveData.token = access_token;
                            saveData.expire_time = expire;
                            var param = {
                                tablename: 'mdl_token_secret',
                                data: [saveData]
                            };
                            Mastermodel.commonInsert(param, function (err) {
                                res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONG);
                            }, function (data) {
                                var response = {
                                    resCode: 200,
                                    response: {
                                        userid: userRes[0].id,
                                        accessToken: access_token,
                                        response: "Otp Valid"
                                    }
                                };
                                res.status(200).send(response);
                            });
                            /**********************For Token ***************************/
                            //res.status(200).send(RESPONSEMESSAGE.USER.OTPVALID);

                        });

                    });

                });

            } else {
                res.status(201).send(RESPONSEMESSAGE.USER.OTPNOTVALID);
            }
        } else {
            res.status(201).send(RESPONSEMESSAGE.USER.OTPNOTVALID);
        }
    });
});


/*
 * Login
 * Created On : 24-10-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/userLogin', customodule.verifyapicalltoken, function (req, res) {

    var username = req.body.username;
    var password = req.body.password;
    var userType = req.body.userType;
    if (!username && !password && !userType) {
        res.status(201).send(RESPONSEMESSAGE.USER.REQUIREDFIELDERROR);
    } else {
        var encryptedPass = node_module.crypto.createHash('md5').update(customodule.setting.passprefix + password).digest("hex");
        var fields = " mu.id, mu.isverified, mu.username, mu.email";
        var condition = " where 1 AND mu.username = '" + username + "' AND mu.password = '" + encryptedPass + "' AND mu.usertype = '" + userType + "' ";
        var param = {
            from: 'from mdl_user as mu',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {
            if (userRes.length > 0) {
                var isverified = userRes[0].isverified;
                if (isverified == 1) {
                    var username = userRes[0].username;
                    var email = userRes[0].email;
                    var emailObj = {
                        name: username,
                        email: email
                    }
                    /** send a confirmation mail to user */
                    customodule.mail('loginEmail', emailObj);
                    var response = {
                        resCode: 200,
                        response: {
                            userid: userRes[0].id
                        }
                    }
                    res.status(200).send(response);
                } else {
                    var response = {
                        resCode: 202,
                        response: {
                            userid: userRes[0].id
                        }
                    }
                    res.status(202).send(response);
                }
            } else {
                res.status(201).send(RESPONSEMESSAGE.USER.USERNOTLOGGEDIN);
            }
        });
    }
});


/*
 * check valid user
 * Created On : 24-10-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/checkUser', customodule.verifyapicalltoken, function (req, res) {
    var socialUserId = req.body.socialUserId;
    var regType = req.body.regType;
    var userType = req.body.userType;
    //var username        = req.body.username;

    /**** Used to get RPL question list ****/
    var fields = " mu.id, mu.isverified";
    var condition = " where mu.socialuserid = '" + socialUserId + "'  AND mu.regtype = '" + regType + "' AND usertype = '" + userType + "' ";
    var param = {
        from: 'from mdl_user as mu',
        condition: condition,
        fields: fields
    };


    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (rplRes) {
        if (rplRes.length > 0) {
            var response = {
                resCode: 200,
                response: rplRes[0]
            }
            res.status(200).send(response);
        } else {
            res.status(201).send(RESPONSEMESSAGE.USER.USERNOTEXISTS);
        }
    });
});



/*
 * Send user OTP
 * Created On : 25-10-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/sendUserOtp', function (req, res) {
    var userId = req.body.userId;
    var otptype = req.body.otptype;


    /**** Used to get RPL question list ****/
    var fields = " mu.id, mu.username, mu.email ";
    var condition = " where 1 AND mu.id = '" + userId + "' ";
    var param = {
        from: 'from mdl_user as mu',
        condition: condition,
        fields: fields
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (rplRes) {
        if (rplRes.length > 0) {

            var username = rplRes[0].username;
            var email = rplRes[0].email;
            /****** insert into user otp verification table ***********************/

            var otp = commonFunction.randomFixedInteger(4);
            var saveData = {};
            saveData.user_id = userId;
            saveData.type = otptype;
            saveData.otp = otp;
            var param = {
                tablename: 'mdl_user_opt_verification',
                data: [saveData]
            };
            Mastermodel.commonInsert(param, function (err) {
                res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONG);
            }, function (data) {
                // mail send for otp will call here

                var emailObj = {
                    username: username,
                    email: email,
                    otp: otp
                };

                if (otptype == 'registration') {
                    customodule.mail('otpverification', emailObj);
                } else {
                    customodule.mail('forgotPass', emailObj);
                }
                res.status(200).send(RESPONSEMESSAGE.USER.OTPSENDSUCCESSFULLY);
            });


        } else {
            res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONGUSER);
        }
    });
});



/*
 * Send user OTP
 * Created On : 25-10-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/sendUserOtpNew', function (req, res) {
    var userId = req.body.userId;
    var otptype = req.body.otptype;
    var email = req.body.email;


    /**** Used to get RPL question list ****/
    var fields = " mu.id, mu.username, mu.email ";
    var condition = " where 1 AND mu.id = '" + userId + "' ";
    var param = {
        from: 'from mdl_user as mu',
        condition: condition,
        fields: fields
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (rplRes) {
        if (rplRes.length > 0) {

            var username = email;

            /****** insert into user otp verification table ***********************/
            //var otp = Math.floor(Math.random() * 10000) + 1;
            var otp = commonFunction.randomFixedInteger(4);
            var saveData = {};
            saveData.user_id = userId;
            saveData.type = otptype;
            saveData.otp = otp;
            var param = {
                tablename: 'mdl_user_opt_verification',
                data: [saveData]
            }

            Mastermodel.commonInsert(param, function (err) {
                res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONG);
            }, function (data) {
                // mail send for otp will call here

                var emailObj = {
                    username: username,
                    email: email,
                    otp: otp
                };

                if (otptype == 'registration') {
                    customodule.mail('otpverification', emailObj);
                } else {
                    customodule.mail('forgotPass', emailObj);
                }
                res.status(200).send(RESPONSEMESSAGE.USER.OTPSENDSUCCESSFULLY);
            });

        } else {
            res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONGUSER);
        }
    });
});




/*
 * check user email
 * Created On : 13-11-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/checkUserEmail', function (req, res) {
    var email = req.body.email;
    var usertype = req.body.usertype;
    /**** Used for check user  ****/
    var fields = " mu.id, mu.isverified, mu.username, mu.email";
    var condition = " where mu.email = '" + email + "' ";
    if (usertype != "")
        condition += " AND mu.usertype = '" + usertype + "' ";
    var param = {
        from: 'from mdl_user as mu',
        condition: condition,
        fields: fields
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (userRes) {
        if (userRes.length > 0) {
            var userId = userRes[0].id;
            /****** insert into user otp verification table ***********************/
            var otp = commonFunction.randomFixedInteger(4);
            var saveData = {};
            saveData.user_id = userId;
            saveData.type = 'forgotpass';
            saveData.otp = otp;
            var param = {
                tablename: 'mdl_user_opt_verification',
                data: [saveData]
            };
            Mastermodel.commonInsert(param, function (err) {
                res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONG);
            }, function (data) {
                // mail send for otp will call here

                /**************** mail ************************/
                var username = userRes[0].username;
                var email = userRes[0].email;

                // mail send for account verification
                var emailObj = {
                    username: username,
                    email: email,
                    otp: otp
                }
                customodule.mail('forgotPass', emailObj);
                /****************************************/

                var response = {
                    resCode: 200,
                    response: userRes[0]
                }
                res.status(200).send(response);

            });
        } else {
            res.status(201).send(RESPONSEMESSAGE.USER.USERNOTEXISTS);
        }
    });
});

/*
 * Check user OTP for forgot password
 * Created On : 13-11-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/checkUserForgotPassOtp', function (req, res) {
    var userId = req.body.userId;
    var otp = req.body.otp;
    var otptype = req.body.otptype;

    /**** Used to get RPL question list ****/
    var fields = " muov.id,muov.otp";
    var condition = " where 1 AND muov.user_id = '" + userId + "' and isused = '0' and type = '" + otptype + "' ";
    var orderby = " order by id desc ";
    var param = {
        from: 'from mdl_user_opt_verification as muov',
        condition: condition,
        fields: fields,
        orderby: orderby
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (rplRes) {
        if (rplRes.length > 0) {
            if (rplRes[0].otp == otp) {

                var dataupdate = {
                    isused: 1
                };
                var updatecondition = {
                    user_id: userId,
                    type: 'forgotpass',
                    otp: otp
                };
                var param3 = {
                    tablename: 'mdl_user_opt_verification',
                    condition: updatecondition,
                    data: dataupdate
                };
                Mastermodel.commonUpdate(param3, function (error) {
                    res.status(201).send(RESPONSEMESSAGE.USER.OTPNOTVALID);
                }, function (data4) {
                    res.status(200).send(RESPONSEMESSAGE.USER.OTPVALID);
                });

            } else {
                res.status(201).send(RESPONSEMESSAGE.USER.OTPNOTVALID);
            }
        } else {
            res.status(201).send(RESPONSEMESSAGE.USER.OTPNOTVALID);
        }
    });
});



/*
 * update user password
 * Created On : 13-11-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
router.post('/updateUserPassword', function (req, res) {
    var password = req.body.password;
    var confirm_password = req.body.confirm_password;
    var uid = req.body.uid;
    /**** Used to get RPL question list ****/
    var fields = " mu.id";
    var condition = " where 1 AND mu.id = '" + uid + "'";
    var param = {
        from: 'from mdl_user as mu',
        condition: condition,
        fields: fields
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (rplRes) {
        if (rplRes.length > 0) {
            if (password == confirm_password) {
                var encryptedPass = node_module.crypto.createHash('md5').update(customodule.setting.passprefix + password).digest("hex");
                var dataupdate = {
                    password: encryptedPass
                };
                var updatecondition = {
                    id: uid
                };
                var param3 = {
                    tablename: 'mdl_user',
                    condition: updatecondition,
                    data: dataupdate
                }
                Mastermodel.commonUpdate(param3, function (error) {

                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (data4) {
                    var fields = " username,email";
                    var condition = " where id = '" + uid + "' ";
                    var param = {
                        from: 'from mdl_user',
                        condition: condition,
                        fields: fields
                    };
                    Mastermodel.commonSelect(param, function (err) {
                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                    }, function (userRes) {
                        var username = userRes[0].username;
                        var email = userRes[0].email;
                        // mail send for account verification
                        var emailObj = {
                            username: username,
                            email: email
                        };
                        customodule.mail('passChange', emailObj);
                        res.status(200).send(RESPONSEMESSAGE.USER.PASSWORDUPDATE);
                    });
                });
            } else {
                res.status(201).send(RESPONSEMESSAGE.USER.PASSWORDMISMATCH);
            }
        } else {
            res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONGUSER);
        }
    });
});


/*
 * Used for update profile
 * Created On : 15-11-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
/* TEST FILE UPLOAD */

var storage = node_module.multer.diskStorage({
    destination: function (req, file, cb) {
        //var code = JSON.parse(req.body.model).empCode;
        var dest = 'public/userimage';
        node_module.mkdirp(dest, function (err) {
            if (err) cb(err, dest);
            else cb(null, dest);
        });
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + node_module.path.extname(file.originalname));
    }
});
var upload = node_module.multer({
    storage: storage
});
router.post('/updateProfile', customodule.verifyapicalltoken, upload.any(), function (req, res) {

    var id = req.body.id;
    var fname = req.body.fname;
    var lname = req.body.lname;
    var phone = req.body.phone;
    var city = req.body.city;
    var country = req.body.country;
    var state = req.body.state;
    var address1 = req.body.address1;
    var address2 = req.body.address2;
    var zip = req.body.zip;
    var date_of_birth = req.body.date_of_birth;
    var gender = req.body.gender;
    var istrusted = req.body.istrusted;
    var profileVideo = req.body.profileVideo;
    var selfDescription = req.body.selfDescription;
    //var profile_image = req.body.profile_image;

    var preferredgender = req.body.preferredgender;
    var trainingtypeid = req.body.trainingtypeid;
    var preferedgroup = req.body.preferedgroup;
    var preferedlanguage = req.body.preferedlanguage;
    var preferedtimeslot = req.body.preferedtimeslot;
    var learningtypeid = req.body.learningtypeid;

    if (req.files != undefined) {
        if (req.files.length > 0) {
            if (req.files[0].filename) {
                var profile_image = req.files[0].filename;
            } else
                var profile_image = "";
        } else {
            var profile_image = "";
        }
    } else {
        var profile_image = "";
    }
    if (!id) {
        //console.log(error);
        res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONGUSER);
    } else if (isNaN(id)) {
        //console.log(error);
        res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONGUSER);
    } else if (!fname || !lname || !phone || !city || !country || !state || !address1 || !zip) {
        res.status(201).send(RESPONSEMESSAGE.USER.REQUIREDFIELDERROR);
    } else {
        if (profile_image != "") {
            var dataupdate = {
                firstname: fname,
                lastname: lname,
                phone1: phone,
                city: city,
                country: country,
                state: state,
                address1: address1,
                address2: address2,
                zip: zip,
                date_of_birth: date_of_birth,
                gender: gender,
                istrusted: istrusted,
                profile_image: profile_image,
                profile_video: profileVideo,
                self_description: selfDescription,
                preferedgender: preferredgender,
                trainingtypeid: trainingtypeid,
                preferedgroup: preferedgroup,
                preferedlanguage: preferedlanguage,
                preferedtimeslot: preferedtimeslot,
                learningtypeid: learningtypeid
            }

        } else {
            var dataupdate = {
                firstname: fname,
                lastname: lname,
                phone1: phone,
                city: city,
                country: country,
                state: state,
                address1: address1,
                address2: address2,
                zip: zip,
                date_of_birth: date_of_birth,
                istrusted: istrusted,
                gender: gender,
                profile_video: profileVideo,
                self_description: selfDescription,
                preferedgender: preferredgender,
                trainingtypeid: trainingtypeid,
                preferedgroup: preferedgroup,
                preferedlanguage: preferedlanguage,
                preferedtimeslot: preferedtimeslot,
                learningtypeid: learningtypeid
            };

        }
        var updatecondition = {
            id: id
        };
        var param3 = {
            tablename: 'mdl_user',
            condition: updatecondition,
            data: dataupdate
        };
        Mastermodel.commonUpdate(param3, function (error) {
            res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONGUSER);
        }, function (data4) {

            if (profile_image != "") {

                var Jimp = require("jimp");
                var newPath = "public/userimage/" + profile_image;
                var thumbPath = "public/userimage/thumbs/" + profile_image;

                // .greyscale() // set greyscale

                Jimp.read(newPath).then(function (lenna) {
                    lenna.resize(256, 256) // resize
                        .quality(60) // set JPEG quality                       
                        .write(thumbPath); // save

                    res.status(200).send(RESPONSEMESSAGE.USER.PROFILEUPDATESUCCESSFULLY);

                });

            } else {
                res.status(200).send(RESPONSEMESSAGE.USER.PROFILEUPDATESUCCESSFULLY);
            }
        });
    }
});



router.post('/updateProfileWeb', customodule.verifyapicalltoken, function (req, res) {

    var id = req.body.id;
    var fname = req.body.fname;
    var lname = req.body.lname;
    var phone = req.body.phone;
    var city = req.body.city;
    var country = req.body.country;
    var state = req.body.state;
    var address1 = req.body.address1;
    var address2 = req.body.address2;
    var zip = req.body.zip;
    var date_of_birth = req.body.date_of_birth;
    var gender = req.body.gender;
    var istrusted = req.body.istrusted;
    var profileVideo = req.body.profileVideo;
    var selfDescription = req.body.selfDescription;
    var profile_image = req.body.profile_image;

    var preferredgender = req.body.preferredgender;
    var trainingtypeid = req.body.trainingtypeid;
    var preferedgroup = req.body.preferedgroup;
    var preferedlanguage = req.body.preferedlanguage;
    var preferedtimeslot = req.body.preferedtimeslot;
    var learningtypeid = req.body.learningtypeid;


    if (!id) {
        //console.log(error);
        res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONGUSER);
    } else if (isNaN(id)) {
        //console.log(error);
        res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONGUSER);
    } else if (!fname || !lname || !phone || !city || !country || !state || !address1 || !zip) {
        res.status(201).send(RESPONSEMESSAGE.USER.REQUIREDFIELDERROR);
    } else {
        if (profile_image != "") {
            var dataupdate = {
                firstname: fname,
                lastname: lname,
                phone1: phone,
                city: city,
                country: country,
                state: state,
                address1: address1,
                address2: address2,
                zip: zip,
                date_of_birth: date_of_birth,
                gender: gender,
                istrusted: istrusted,
                profile_image: profile_image,
                profile_video: profileVideo,
                self_description: selfDescription,
                preferedgender: preferredgender,
                trainingtypeid: trainingtypeid,
                preferedgroup: preferedgroup,
                preferedlanguage: preferedlanguage,
                preferedtimeslot: preferedtimeslot,
                learningtypeid: learningtypeid
            }

        } else {
            var dataupdate = {
                firstname: fname,
                lastname: lname,
                phone1: phone,
                city: city,
                country: country,
                state: state,
                address1: address1,
                address2: address2,
                zip: zip,
                date_of_birth: date_of_birth,
                istrusted: istrusted,
                gender: gender,
                profile_video: profileVideo,
                self_description: selfDescription,
                preferedgender: preferredgender,
                trainingtypeid: trainingtypeid,
                preferedgroup: preferedgroup,
                preferedlanguage: preferedlanguage,
                preferedtimeslot: preferedtimeslot,
                learningtypeid: learningtypeid
            };

        }
        var updatecondition = {
            id: id
        };
        var param3 = {
            tablename: 'mdl_user',
            condition: updatecondition,
            data: dataupdate
        };
        Mastermodel.commonUpdate(param3, function (error) {
            res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONGUSER);
        }, function (data4) {
            res.status(200).send(RESPONSEMESSAGE.USER.PROFILEUPDATESUCCESSFULLY);
        });
    }
});


/**
 * Upload profile video
 */
var storage = node_module.multer.diskStorage({
    destination: function (req, file, cb) {
        var dest = 'public/trainerProfileVideo';
        node_module.mkdirp(dest, function (err) {
            if (err) cb(err, dest);
            else cb(null, dest);
        });
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + node_module.path.extname(file.originalname));
    }
});
var profileVideoUpload = node_module.multer({
    storage: storage
});
router.post('/uploadProfileVideo', customodule.verifyapicalltoken, profileVideoUpload.any(), function (req, res) {

    if (req.files != undefined) {
        if (req.files.length > 0) {
            var filename = req.files[0].filename;
            var fileObj = {};
            fileObj.url = customodule.setting.hetc_base_url + 'trainerProfileVideo/' + filename;
            fileObj.name = filename;
            var success = {
                "resCode": 200,
                "response": fileObj
            };
            res.status(200).send(success);
        } else {
            res.status(201).send(RESPONSEMESSAGE.USER.PROFILEVIEDOREQUIRED);
        }
    } else {
        res.status(201).send(RESPONSEMESSAGE.USER.PROFILEVIEDOREQUIRED);
    }
});


/*
 * Used for test mail functionality
 * Created On : 24-10-2017
 * Created By : Dipti
 * Input      : NA
 * Modified On
 */
router.post('/checkMail', function (req, res, next) {
    var emailObj = {
        name: 'Monalisa',
        email: 'monalisa@indusnet.co.in',
        password: 'password'
    }
    customodule.mail('create', emailObj);
});

/*
 * Used for test mail functionality
 * Created On : 24-10-2017
 * Created By : Dipti
 * Input      : NA
 * Modified On
 */
router.post('/checkNotification', function () {
    var notificationTitle = 'Title';
    var notificationDesc = 'Description';
    var notificationDate = '2017-10-01';
    var pushNotificationTitle = notificationTitle;
    var pushNotificationDesc = notificationDesc;
    var pushNotificationDate = notificationDate;
    var pushNotificationTag = 'Result';
    var pushNotificationCode = 'result';
    var data = [{
        "device_type": "android",
        "device_id": "dfgdggdfdfgdf22",
        "device_token": "cE06nkIYtR4:APA91bG5i8O6-to-H10GUem0jo2MujriTKuQOsNDkw4vPVADMpfvWwpkfxJ9NeT3XxMriHCra_Lfa0OEL3s4wU9Z4B8217vuY-dSqYWYdukhoT22DkXsvrXuhkfO3la90-gYzvQC5ESX",
    }];
    sendGroupNotification(pushNotificationTitle, data, pushNotificationTag, pushNotificationDesc, pushNotificationCode, pushNotificationDate);
});

/*
 * GET LEARNING STYLE
 */
router.post('/getLearningStyle', function (req, res) {
    // Create a relative path URL
    var reqPath = node_module.path.join(__dirname, 'learningStyle.json');
    //Read JSON from relative path of this file
    node_module.fs.readFile(reqPath, 'utf8', function (err, data) {
        //Handle Error
        if (!err) {
            //Handle Success
            console.log("Success" + data);
            // Parse Data to JSON OR
            var jsonObj = JSON.parse(data);
            //Send back as Response
            res.end(data);
        } else {
            //Handle Error
            res.end("Error: " + err);
        }
    });
});

/*
 * Used to upload files for RPL submission
 * Created On : 12-12-2017
 * Modified On
 */
var mkdirp = require('mkdirp');
var rpl_documents = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/rplsubmissiondocuments');
    },
    filename: function (req, file, cb) {
        //var fileName = Date.now() + path.extname(file.originalname);        
        var fileName = file.originalname;
        cb(null, fileName) //Appending extension
    }
});

var uploadOption = multer({
    storage: rpl_documents
});
//customodule.verifyapicalltoken,
router.post('/uploadRPLDocument', customodule.verifyapicalltoken, uploadOption.any(), function (req, res) {
    var documents = req.files;
    if (typeof documents == 'undefined' || documents == '' || documents == null) {
        res.status(201).send(RESPONSEMESSAGE.RPL.DOCUMENTREQUIRED);
    } else if (documents.length == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.DOCUMENTREQUIRED);
    } else {
        var success = {
            "resCode": 200,
            "resData": req.files[0].filename,
            "response": "document successfully uploaded."
        };
        res.status(200).send(success);
    }
});

/*
 * Used to upload files for RPL submission
 * Created On : 12-12-2017
 * Modified On
 */
var mkdirp = require('mkdirp');
var rpl_documents = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/rpladditionalfiles')
    },
    filename: function (req, file, cb) {
        //var fileName = Date.now() + path.extname(file.originalname);
        var fileName = file.originalname.replace(/ /g, "_");
        cb(null, fileName); //Appending extension
    }
});

var uploadOption = multer({
    storage: rpl_documents
});
//customodule.verifyapicalltoken,
router.post('/uploadRPLAdditionalFiles', customodule.verifyapicalltoken, uploadOption.any(), function (req, res) {
    var documents = req.files;
    if (typeof documents == 'undefined' || documents == '' || documents == null) {
        res.status(201).send(RESPONSEMESSAGE.RPL.DOCUMENTREQUIRED);
    } else if (documents.length == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.DOCUMENTREQUIRED);
    } else {
        var success = {
            "resCode": 200,
            "resData": req.files[0].filename,
            "response": "document successfully uploaded."
        };
        res.status(200).send(success);
    }
});


/*
 * Used to get course details for APP
 * Created On : 05-09-2017
 * Created By : Monalisa
 * Input      : searchText(char)
 * Modified On
 */
router.post('/getCourseOverviewForApp', customodule.verifyapicalltoken, function (req, res) {
    var studentId = req.body.studentId;
    var courseId = req.body.courseId;

    /**** Course Search By search Text and isAccrediated parameter  ****/
    var fields = " mcc.id,  mcc.name,  mcc.idnumber, mcc.description,mcc.isentryteststudent, mcc.isaccrediated , mcc.course_image, msetm.is_qualified, msw.iswishlistadd , mrpsm.is_submitted,  mrpsm.is_actiontaken, mrpsm.is_approved , mrpsm.final_submitted , mrpsm.id as rpl_submission_id,  mrpsm.trainer_status , mrpsm.trainer_id , mslm.learning_mode";
    var condition = " where 1 and mcc.id = '" + courseId + "' ";
    var join = " LEFT JOIN mdl_student_entry_test_master msetm ON mcc.id = msetm.course_id       AND msetm.student_id = '" + studentId + "'  AND  msetm.is_qualified = 'Y' ";
    join += " LEFT JOIN mdl_student_wishlist msw            ON mcc.id = msw.courseid          AND msw.studentid =  '" + studentId + "'  ";
    join += " LEFT JOIN mdl_rpl_process_submission_master mrpsm  ON mcc.id = mrpsm.course_id  AND mrpsm.student_id = '" + studentId + "' ";
    join += " LEFT JOIN mdl_student_learning_mode mslm     ON mcc.id = mslm.course_id         AND mslm.student_id = '" + studentId + "' AND ( mslm.unit_id = '' || mslm.unit_id IS NULL) ";
    var param = {
        from: 'from mdl_course_categories as mcc',
        condition: condition,
        fields: fields,
        join: join
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (courseRes) {
        if (courseRes.length > 0) {
            var response = {
                resCode: 200,
                response: courseRes[0]
            };
            res.status(200).send(response);

        } else {
            res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
        }
    });
});



/*
 * Used to get student wishlist
 * Created On : 14-12-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
router.post('/getStudentWishList', customodule.verifyapicalltoken, function (req, res) {
    var studentId = req.body.studentId;
    var searchText = req.body.searchText;
    var isAccrediated = req.body.isAccrediated;



    if (node_module.validator.matches(searchText, SETTING.specialcharpattern)) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.INVALIDREQUESTSPECIALCHAR);
    } else {


        /**** Used to get RPL question list ****/
        var fields = " msw.courseid, msw.studentid, mcc.name, mcc.idnumber, mcc.isaccrediated , msetm.is_qualified, mcc.isentryteststudent";
        var condition = " where 1 AND studentid = '" + studentId + "' ";
        if (searchText != "") {
            condition += " AND ( mcc.name LIKE '%" + searchText + "%' OR  mcc.idnumber LIKE '%" + searchText + "%' ) ";
        }
        if (isAccrediated != "") {
            condition += " AND  mcc.isaccrediated LIKE '%" + isAccrediated + "%'   ";
        }
        var join = " LEFT JOIN mdl_course_categories as mcc           ON msw.courseid = mcc.id";
        join += " LEFT JOIN mdl_student_entry_test_master as msetm ON msw.courseid = msetm.course_id AND msetm.student_id = '" + studentId + "' AND msetm.is_qualified != 'Y' ";
        var param = {
            from: 'from mdl_student_wishlist as msw',
            condition: condition,
            fields: fields,
            join: join
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (rplRes) {
            if (rplRes.length > 0) {

                var response = {
                    resCode: 200,
                    response: rplRes
                }
                res.status(200).send(response);
            } else {
                res.status(201).send(RESPONSEMESSAGE.RPL.NORECORDSFOUND);
            }
        });

    }
});

/*
 * Used to upload files for RPL submission
 * Created On : 12-12-2017
 * Modified On
 */
var mkdirp = require('mkdirp');
var rpl_documents = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/rplsubmissiondocuments')
    },
    filename: function (req, file, cb) {
        //var fileName = Date.now() + path.extname(file.originalname);
        var fileName = file.originalname.replace(/ /g, "_");
        cb(null, fileName); //Appending extension
    }
})

var uploadOption = multer({
    storage: rpl_documents
});
router.post('/uploadRPLDocument', customodule.verifyapicalltoken, uploadOption.any(), function (req, res) {
    var documents = req.files;
    if (typeof documents == 'undefined' || documents == '' || documents == null) {
        res.status(201).send(RESPONSEMESSAGE.RPL.DOCUMENTREQUIRED);
    } else if (documents.length == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.DOCUMENTREQUIRED);
    } else {
        var success = {
            "resCode": 200,
            "resData": req.files[0].filename,
            "response": "document successfully uploaded."
        };
        res.status(200).send(success);
    }
});



/*
 * Used to upload video for trainer profile
 * Created On : 12-12-2017
 * Modified On
 */
var mkdirp = require('mkdirp');
var profile_video = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/trainerProfileVideo')
    },
    filename: function (req, file, cb) {
        var fileName = Date.now() + path.extname(file.originalname);
        cb(null, fileName); //Appending extension
    }
})

var uploadOption = multer({
    storage: profile_video
});
//customodule.verifyapicalltoken,
router.post('/uploadTrainerProfileVideo', customodule.verifyapicalltoken, uploadOption.any(), function (req, res) {
    var documents = req.files;
    if (typeof documents == 'undefined' || documents == '' || documents == null) {
        res.status(201).send(RESPONSEMESSAGE.RPL.PROFILEVIDEOREQUIRED);
    } else if (documents.length == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.PROFILEVIDEOREQUIRED);
    } else {
        var success = {
            "resCode": 200,
            "resData": req.files[0].filename,
            "response": "document successfully uploaded."
        };
        res.status(200).send(success);
    }
});

/*
 * Used to upload profile image for trainer profile
 * Created On : 12-12-2017
 * Modified On
 */
var mkdirp = require('mkdirp');
var profile_video = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/userimage');
    },
    filename: function (req, file, cb) {
        // var fileName = Date.now() + path.extname(file.originalname);
        var fileName = file.originalname.replace(/ /g, "_");
        cb(null, fileName); //Appending extension
    }
})

var uploadOption = multer({
    storage: profile_video
});
//customodule.verifyapicalltoken,
router.post('/uploadTrainerProfileImage', customodule.verifyapicalltoken, uploadOption.any(), function (req, res) {
    var documents = req.files;
    if (typeof documents == 'undefined' || documents == '' || documents == null) {
        res.status(201).send("Profile Image Required");
    } else if (documents.length == 0) {
        res.status(201).send("Profile Image Required");
    } else {

        var Jimp = require("jimp");
        var imageName = req.files[0].filename;
        var newPath = "public/userimage/" + imageName;
        var thumbPath = "public/userimage/thumbs/" + imageName;

        // .greyscale() // set greyscale

        Jimp.read(newPath).then(function (lenna) {
            lenna.resize(256, 256) // resize
                .quality(60) // set JPEG quality               
                .write(thumbPath); // save

            var success = {
                "resCode": 200,
                "resData": req.files[0].filename,
                "response": "Image successfully uploaded."
            };
            res.status(200).send(success);
        });
    }

});


/*
 * Used to upload files for gap traning video
 * Created On : 30-01-2018
 * Modified On
 */
var mkdirp = require('mkdirp');
var profile_video = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/gaptraningfile');
    },
    filename: function (req, file, cb) {
        var fileName = Date.now() + path.extname(file.originalname);
        cb(null, fileName); //Appending extension
    }
});

var uploadOption = multer({
    storage: profile_video
});
router.post('/uploadGapTraningMaterialVideo', customodule.verifyapicalltoken, uploadOption.any(), function (req, res) {
    var documents = req.files;
    if (typeof documents == 'undefined' || documents == '' || documents == null) {
        res.status(201).send(RESPONSEMESSAGE.RPL.GAPTRANINGVIDEOREQUIRED);
    } else if (documents.length == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.GAPTRANINGVIDEOREQUIRED);
    } else {
        var success = {
            "resCode": 200,
            "resData": req.files[0].filename,
            "response": "document successfully uploaded."
        };
        res.status(200).send(success);
    }
});


/*
 * Used to get trainer list
 * Created On : 30-01-2018
 * Modified On
 */
router.post('/getTrainerList', customodule.verifyapicalltoken, function (req, res) {

    var rpl_process_submission_id = req.body.rpl_process_submission_id;
    var fields = "m1.course_id, m1.unit_id";
    var condition = "where m1.id = '" + rpl_process_submission_id + "'";
    var param = {
        from: 'from mdl_rpl_process_submission_master m1 ',
        condition: condition,
        fields: fields
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (userRes) {


        var course_id = userRes[0].course_id;
        var unit_id = userRes[0].unit_id;
        var fields = "(select AVG(rating) from mdl_trainer_rating as mtrt where m1.trainer_id = mtrt.trainer_id and m1.course_id = mtrt.course_id and m1.unit_id = mtrt.unit_id ) as avgrating, m1.trainer_id, mu.id, mu.firstname, mu.lastname, mu.profile_image, mrptr.is_accepted";
        var condition = "where m1.course_id  = " + course_id + " AND m1.unit_id  = " + unit_id + "  ";
        var join = "JOIN mdl_user as mu    ON m1.trainer_id = mu.id     ";
        join += "LEFT JOIN mdl_rpl_process_trainer_request as mrptr  ON mrptr.trainer_id = mu.id AND rpl_process_submission_id = '" + rpl_process_submission_id + "'    ";

        var orderby = "order by mu.firstname asc";
        var param = {
            from: 'from mdl_trainer_approved_units m1 ',
            condition: condition,
            fields: fields,
            join: join,
            orderby: orderby
        };

        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {

            if (userRes.length > 0) {
                var response = {
                    resCode: 200,
                    response: userRes,
                    resData: userRes
                };
                res.status(201).send(response);
            } else {
                res.status(201).send(RESPONSEMESSAGE.RPL.NORECORDSFOUND);
            }

        }); //end of select

    }); //end of select

}); //e

/*
 * Used to user login action
 * Created On : 20-10-2018
 * Modified On
 */

router.post('/doUserLogin', function (req, res) {
    var email = req.body.email;
    var password = req.body.password;
    var userType = req.body.userType;
    var userLoginType = req.body.userLoginType;
    if (userLoginType == 'commonLogin') {


        if (!email || !password || !userLoginType) {
            res.status(201).send(RESPONSEMESSAGE.USER.REQUIREDFIELDERROR);
        } else {

            var encryptedPass = node_module.crypto.createHash('md5').update(customodule.setting.passprefix + password).digest("hex");
            var fields = " mu.id, mu.isverified, mu.username, mu.email";
            var condition = " where 1 AND mu.email = '" + email + "' AND mu.password = '" + encryptedPass + "' AND mu.usertype = '" + userType + "' ";
            var param = {
                from: 'from mdl_user as mu',
                condition: condition,
                fields: fields
            };

            Mastermodel.commonSelect(param, function (err) {
                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            }, function (userRes) {

                if (userRes.length > 0) {
                    var isverified = userRes[0].isverified;
                    if (isverified == 1) {
                        /****** Login Email ********************/
                        var username = userRes[0].username;
                        var email = userRes[0].email;
                        var emailObj = {
                            name: username,
                            email: email
                        };
                        customodule.mail('loginEmail', emailObj);

                        /* Create Expiry time of access_token for a logged in user */
                        var ret = new Date();
                        var exp_time = ret.setTime(ret.getTime() + 60 * 24 * 60000 * 10);
                        //var exp_time = ret.setTime(ret.getTime() +  60000);
                        var expiredate = node_module.datetime.create(new Date(exp_time));
                        var expire = expiredate.format("Y-m-d H:M:S");
                        var access_token = node_module.jwt.sign({
                            time: new Date().getTime()
                        }, customodule.setting.jwtsecret);
                        /* End */
                        var saveData = {};
                        saveData.user_id = userRes[0].id;
                        saveData.token = access_token;
                        saveData.expire_time = expire;
                        var param = {
                            tablename: 'mdl_token_secret',
                            data: [saveData]
                        };
                        Mastermodel.commonInsert(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONG);
                        }, function (data) {
                            var response = {
                                resCode: 200,
                                response: {
                                    userid: userRes[0].id,
                                    accessToken: access_token
                                }
                            };
                            res.status(200).send(response);
                        });
                    } else {
                        var response = {
                            resCode: 202,
                            response: {
                                userid: userRes[0].id
                            }
                        };
                        res.status(202).send(response);
                    }
                } else {
                    res.status(201).send(RESPONSEMESSAGE.USER.USERNOTLOGGEDIN);
                }
            });
        }

    }
    if (userLoginType == 'socialLogin') {

        var socialUserId = req.body.socialUserId;
        var regType = req.body.regType;
        var imageUrl = req.body.imageUrl;
        var email = req.body.userEmail;

        var firstName = req.body.firstName;
        var lastName = req.body.lastName;
        if (!socialUserId || !regType) {
            res.status(201).send(RESPONSEMESSAGE.USER.REQUIREDFIELDERROR);
        } else {
            /**** find the social user from database ****/
            var fields = " mu.id, mu.id as userid, mu.isverified";
            if (email == '') {
                var condition = " where mu.socialuserid = '" + socialUserId + "'  AND mu.regtype = '" + regType + "' AND usertype = '" + userType + "' ";
            } else {
                var condition = " where mu.email = '" + email + "'  AND usertype = '" + userType + "'  AND mu.regtype = '" + regType + "' ";
            }
            var param = {
                from: 'from mdl_user as mu',
                condition: condition,
                fields: fields
            };
            Mastermodel.commonSelect(param, function (err) {
                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            }, function (userRes) {
                if (userRes.length > 0) {

                    if (userRes[0].isverified > 0) {


                        /* Create Expiry time of access_token for a logged in user */
                        var ret = new Date();
                        var exp_time = ret.setTime(ret.getTime() + 60 * 24 * 60000 * 10);
                        //var exp_time = ret.setTime(ret.getTime() +  60000);
                        var expiredate = node_module.datetime.create(new Date(exp_time));
                        var expire = expiredate.format("Y-m-d H:M:S");
                        var access_token = node_module.jwt.sign({
                            time: new Date().getTime()
                        }, customodule.setting.jwtsecret);
                        /* End */
                        var saveData = {};
                        saveData.user_id = userRes[0].id;
                        saveData.token = access_token;
                        saveData.expire_time = expire;
                        var param = {
                            tablename: 'mdl_token_secret',
                            data: [saveData]
                        };
                        Mastermodel.commonInsert(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONG);
                        }, function (data) {
                            var response = {
                                resCode: 200,
                                response: {
                                    userid: userRes[0].id,
                                    isverified: userRes[0].isverified,
                                    accessToken: access_token
                                }
                            };
                            res.status(200).send(response);
                        });

                    } else {
                        var response = {
                            resCode: 200,
                            response: {
                                userid: userRes[0].id,
                                isverified: userRes[0].isverified
                            }
                        };
                        res.status(200).send(response);
                    }

                } else {


                    if (email == "") {

                        var isVerified = 0;

                        /**** No Email So insert ***/
                        //if the user is not exist then insert the user record in to the database
                        var insertUserSql = "INSERT INTO `mdl_user` (`auth`, `username`, `password`, `email`, `firstname`, `lastname`, `phone1`, `city`, `country`, `socialimageurl`, `socialuserid`, `regtype`, `state`, `address1`, `address2`, `zip`, `userType`, `isverified`) VALUES ('manual', '', '', '" + email + "', '" + firstName + "', '" + lastName + "', '', '', '', '" + imageUrl + "', '" + socialUserId + "', '" + regType + "', '', '', '', '', '" + userType + "', " + isVerified + ")";
                        connection.query(insertUserSql, function (insertUserError, insertUserData, insertUserFields) {
                            if (insertUserError != null) {
                                res.status(201).send(RESPONSEMESSAGE.USER.REQUIREDFIELDERROR);
                            } else {
                                var userid = insertUserData.insertId;
                                var userObj = {
                                    userid: userid,
                                    isverified: 0
                                };
                                var response = {
                                    resCode: 200,
                                    response: userObj
                                };
                                res.status(200).send(response);
                            }
                        });

                    } else {

                        /**** Check database for record exists for that email ***/
                        var fields = " mu.id, mu.regtype, mu.usertype";
                        var condition = " where 1 AND mu.email = '" + email + "'  ";
                        var param = {
                            from: 'from mdl_user as mu',
                            condition: condition,
                            fields: fields
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (userRes) {

                            if (userRes.length > 0) {

                                /***** If email exists then error message *****/

                                //console.log(userRes[0].regType);
                                var regType1 = userRes[0].regtype;
                                var type = userRes[0].usertype;
                                if (type == userType) {
                                    if (regType1 == 'App' || regType1 == 'web') {
                                        var msg = "You have already logged in into the application through normal login.";
                                    } else {
                                        var msg = "You have already logged in into the application through other social login type.";
                                    }
                                } else {
                                    var msg = "You have already logged in into the application as " + type;
                                }
                                var response = {
                                    resCode: 201,
                                    response: msg
                                };
                                res.status(201).send(response);
                            } else {

                                var isVerified = 1;
                                //if the user is not exist then insert the user record in to the database
                                var insertUserSql = "INSERT INTO `mdl_user` (`auth`, `username`, `password`, `email`, `firstname`, `lastname`, `phone1`, `city`, `country`, `socialimageurl`, `socialuserid`, `regtype`, `state`, `address1`, `address2`, `zip`, `userType`, `isverified`) VALUES ('manual', '', '', '" + email + "', '" + firstName + "', '" + lastName + "', '', '', '', '" + imageUrl + "', '" + socialUserId + "', '" + req.body.regType + "', '', '', '', '', '" + userType + "', " + isVerified + ")";
                                connection.query(insertUserSql, function (insertUserError, insertUserData, insertUserFields) {
                                    if (insertUserError != null) {
                                        res.status(201).send(RESPONSEMESSAGE.USER.REQUIREDFIELDERROR);
                                    } else {
                                        var ret = new Date();
                                        var exp_time = ret.setTime(ret.getTime() + 60 * 24 * 60000 * 10);
                                        //var exp_time = ret.setTime(ret.getTime() +  60000);
                                        var expiredate = node_module.datetime.create(new Date(exp_time));
                                        var expire = expiredate.format("Y-m-d H:M:S");
                                        var access_token = node_module.jwt.sign({
                                            time: new Date().getTime()
                                        }, customodule.setting.jwtsecret);
                                        /* End */
                                        var saveData = {};
                                        saveData.user_id = insertUserData.insertId;
                                        saveData.token = access_token;
                                        saveData.expire_time = expire;
                                        var param = {
                                            tablename: 'mdl_token_secret',
                                            data: [saveData]
                                        };
                                        Mastermodel.commonInsert(param, function (err) {
                                            res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONG);
                                        }, function (data) {
                                            var response = {
                                                resCode: 200,
                                                response: {
                                                    userid: insertUserData.insertId,
                                                    response: userRes[0],
                                                    accessToken: access_token,
                                                    isverified: isVerified
                                                }
                                            };
                                            res.status(200).send(response);
                                        });

                                    }
                                });

                            }


                        });


                    }



                }
            });
        }
    }

});


/*
 * Used to verify user email
 * Created On : 20-10-2018
 * Modified On
 */
//customodule.verifyapicalltoken,
router.post('/verifyEmailOtp', function (req, res) {
    var username = req.body.username;
    var userid = req.body.userid;
    var email = req.body.email;



    /****** insert into user otp verification table ***********************/


    var fields = " mu.id, mu.regtype";
    var condition = " where 1 AND mu.email = '" + email + "'  ";
    var param = {
        from: 'from mdl_user as mu',
        condition: condition,
        fields: fields
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
    }, function (userRes) {

        if (userRes.length > 0) {

            /***** If email exists then error message *****/

            var msg = "You have already logged in into the application through other social login type.";
            var response = {
                resCode: 201,
                response: msg
            };
            res.status(201).send(response);


        } else {

            var otp = commonFunction.randomFixedInteger(4);
            var saveData = {};
            saveData.user_id = userid;
            saveData.type = 'registration';
            saveData.otp = otp;
            var param = {
                tablename: 'mdl_user_opt_verification',
                data: [saveData]
            };
            Mastermodel.commonInsert(param, function (err) {
                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                res.status(201).send(RESPONSEMESSAGE.USER.OTPWRONGUSER);
            }, function (data) {


                var dataupdate = {
                    email: email
                }
                var updatecondition = {
                    id: userid

                }
                var param3 = {
                    tablename: 'mdl_user',
                    condition: updatecondition,
                    data: dataupdate
                }
                Mastermodel.commonUpdate(param3, function (error) {
                    res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
                }, function (data4) {

                    // mail send for otp will call here
                    var emailObj = {
                        username: username,
                        email: email,
                        otp: otp
                    };
                    customodule.mail('otpverification', emailObj);


                    var userObj = {
                        userid: userid
                    };
                    var response = {
                        resCode: 200,
                        response: userObj
                    };
                    res.status(200).send(response);

                });


            });

        }
    });




});

/*
 * Used for Trainer request for RPL
 * Created On : 20-10-2018
 * Modified On
 */
router.post('/postRPLTrainerRequest', customodule.verifyapicalltoken, function (req, res) {
    var rpl_process_submission_id = req.body.rpl_process_submission_id;
    var trainer_id = req.body.trainer_id;
    var student_id = req.body.student_id;
    if (!rpl_process_submission_id) {
        res.status(200).send(RESPONSEMESSAGE.RPL.RPLPROCESSSUBMISSIONIDREQUIRED);
    } else if (!trainer_id) {
        res.status(200).send(RESPONSEMESSAGE.RPL.TRAINERREQUIRED);
    } else if (!student_id) {
        res.status(200).send(RESPONSEMESSAGE.RPL.STUDENTREQUIRED);
    } else {
        var fields = "id,course_id,unit_id";
        var condition = " where id ='" + rpl_process_submission_id + "' AND student_id = '" + student_id + "' AND      is_submitted = 'Y'";
        var param = {
            from: 'from mdl_rpl_process_submission_master ',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (ansRes) {
            if (ansRes.length > 0) {
                var saveData = {};
                saveData.rpl_process_submission_id = rpl_process_submission_id;
                saveData.trainer_id = trainer_id;
                var param = {
                    tablename: 'mdl_rpl_process_trainer_request',
                    data: [saveData]
                }
                Mastermodel.commonInsert(param, function (err) {
                    res.status(200).send(err);
                }, function (data) {
                    var dataupdate = {
                        final_submitted: 'Y',
                        trainer_status: 0
                    };
                    var updatecondition = {
                        id: rpl_process_submission_id
                    };
                    var param3 = {
                        tablename: 'mdl_rpl_process_submission_master',
                        condition: updatecondition,
                        data: dataupdate
                    };
                    Mastermodel.commonUpdate(param3, function (error) {
                        res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
                    }, function (data4) {
                        var notificationparam = {};
                        notificationparam.userId = trainer_id;
                        notificationparam.courseId = ansRes[0]['course_id'];
                        notificationparam.unitId = ansRes[0]['unit_id'];
                        notificationparam.userType = 'Trainer';
                        notificationparam.pushNotificationCode = NOTIFICATIONMESSAGE.TRAINER.PENDINGRPLREQUEST;
                        Masternotificationmodel.sendPushNotification(notificationparam, function (err) {
                            var response = {
                                resCode: 201,
                                response: error
                            };
                            res.status(200).send(response);
                        }, function (succNotificationRes) {
                            var response = {
                                resCode: 200,
                                resMessage: "Trainer selected successfully",
                                response: "Trainer selected successfully"
                            };
                            res.status(200).send(response);
                        });

                    });
                });
            } else {
                res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);

            }
        }); //end of select

    } //end of else
}); //e

/*
 * Used for RPL request list from student for trainer
 * Created On : 20-10-2018
 * Modified On
 */

router.post('/getUnitRequestList', customodule.verifyapicalltoken, function (req, res) {
    var trainer_id = req.body.trainer_id;
    if (!trainer_id) {
        res.status(200).send(RESPONSEMESSAGE.COMMONALL.TRAINERREQUIRED);
    } else {
        var fields = "tr.id,tr.rpl_process_submission_id,tr.is_accepted,tr.is_action_taken,u.firstname,u.lastname,cc.name,cc.idnumber,c.fullname,c.idnumber as unitcode,sm.attempt, sm.is_paid, sm.paid_amount";
        var join = " LEFT JOIN mdl_rpl_process_submission_master as sm ON sm.id = tr.rpl_process_submission_id  AND  sm.is_actiontaken != 'Y' ";
        join += "LEFT  JOIN mdl_user as u ON u.id                            = sm.student_id ";
        join += "LEFT  JOIN mdl_course_categories as cc ON cc.id             = sm.course_id ";
        join += "LEFT  JOIN mdl_course as c ON c.id                          = sm.unit_id ";

        var condition = " where tr.trainer_id = '" + trainer_id + "'  AND sm.final_submitted='Y' ";
        var orderby = " order by tr.id DESC ";
        var param = {
            from: 'from mdl_rpl_process_trainer_request as tr',
            condition: condition,
            fields: fields,
            join: join,
            orderby: orderby
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {
            if (userRes.length > 0) {
                var response = {
                    resCode: 200,
                    resData: userRes,
                    response: userRes
                };
                res.status(200).send(response);
            } else {
                res.status(201).send(RESPONSEMESSAGE.RPL.NORECORDSFOUND);
            }
        }); //end of select
    }

}); //e



/*
 * Used for accept/reject RPL request from student for trainer
 * Created On : 20-10-2018
 * Modified On
 */
router.post('/approveRejectRPLUnitRequest', customodule.verifyapicalltoken, function (req, res) {
    var id = req.body.id;
    var comment = req.body.comment;
    var is_accepted = req.body.is_accepted;
    var unit_id = req.body.unit_id;
    var course_id = req.body.course_id;
    var student_id = req.body.student_id;
    if (!id) {
        res.status(201).send(RESPONSEMESSAGE.COMMONALL.IDREQUIRED);
    } else if (!comment) {
        res.status(201).send(RESPONSEMESSAGE.COMMONALL.COMMENTREQUIRED);
    } else if (!is_accepted) {
        res.status(201).send(RESPONSEMESSAGE.COMMONALL.APPROVEREJECTREQUIRED);
    } else {
        var fields = "id,rpl_process_submission_id,trainer_id";
        var condition = " where id ='" + id + "'";
        var param = {
            from: 'from mdl_rpl_process_trainer_request',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (ansRes) {
            if (ansRes.length == 0) {
                res.status(200).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
            } else {
                var rpl_process_submission_id = ansRes[0].rpl_process_submission_id;
                var trainer_id = ansRes[0].trainer_id;
                var saveData = {};
                var dataupdate = {
                    is_accepted: is_accepted,
                    trainer_comment: comment,
                    is_action_taken: 'Y'
                };
                var updatecondition = {
                    id: id
                };
                var param3 = {
                    tablename: 'mdl_rpl_process_trainer_request',
                    condition: updatecondition,
                    data: dataupdate
                };
                Mastermodel.commonUpdate(param3, function (error) {
                    res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
                }, function (data4) {
                    var dataupdate = {
                        trainer_status: is_accepted == 'Y' ? 1 : 2,
                        trainer_id: is_accepted == 'Y' ? trainer_id : 0
                    };
                    var updatecondition = {
                        id: rpl_process_submission_id
                    };
                    var param3 = {
                        tablename: 'mdl_rpl_process_submission_master',
                        condition: updatecondition,
                        data: dataupdate
                    };
                    Mastermodel.commonUpdate(param3, function (error) {
                        res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
                    }, function (data4) {
                        if (is_accepted == 'Y') {
                            var d = new Date();
                            var m1 = d.getMonth();
                            var month = m1 + 1;
                            var date = d.getFullYear() + "-" + month + "-" + d.getDate();
                            var saveData = {};
                            saveData.course_id = req.body.course_id;
                            saveData.unit_id = req.body.unit_id;
                            saveData.student_id = req.body.student_id;
                            saveData.trainer_id = req.body.trainer_id;
                            saveData.learning_mode_type_id = 1;
                            saveData.type = req.body.type;
                            saveData.date = date;
                            var param = {
                                tablename: 'mdl_student_trainer_list',
                                data: [saveData]
                            };
                            Mastermodel.commonInsert(param, function (err) {
                                console.log('Insert', err);
                                res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
                            }, function (data) {


                                var notificationparam = {};
                                notificationparam.userId = student_id;
                                notificationparam.courseId = course_id;
                                notificationparam.unitId = unit_id;
                                notificationparam.userType = 'Student';
                                notificationparam.pushNotificationCode = NOTIFICATIONMESSAGE.STUDENT.APPREQUEST;
                                Masternotificationmodel.sendPushNotification(notificationparam, function (err) {
                                    var response = {
                                        resCode: 201,
                                        response: error
                                    };
                                    res.status(200).send(response);
                                }, function (succNotificationRes) {
                                    var response = {
                                        resCode: 200,
                                        resMessage: "Trainer Accepted successfully",
                                        response: "Trainer Accepted successfully"
                                    };
                                    res.status(200).send(response);
                                });
                            });
                        } else {

                            var notificationparam = {};
                            notificationparam.userId = student_id;
                            notificationparam.courseId = course_id;
                            notificationparam.unitId = unit_id;
                            notificationparam.userType = 'Student';
                            notificationparam.pushNotificationCode = NOTIFICATIONMESSAGE.STUDENT.REJREQUEST;
                            Masternotificationmodel.sendPushNotification(notificationparam, function (err) {
                                var response = {
                                    resCode: 201,
                                    response: error
                                };
                                res.status(200).send(response);
                            }, function (succNotificationRes) {
                                var response = {
                                    resCode: 200,
                                    resMessage: "Trainer Rejected ",
                                    response: "Trainer Rejected "
                                };
                                res.status(200).send(response);
                            });
                        }
                    });
                });
            }
        }); //end of select

    } //end of else
}); //e


/*
 * Used for display student request details
 * Created On : 20-10-2018
 * Modified On
 */
router.post('/getUnitRequestDetails', customodule.verifyapicalltoken, function (req, res) {
    var id = req.body.id;
    if (!id) {
        res.status(200).send(RESPONSEMESSAGE.COMMONALL.IDREQUIRED);
    } else {

        var fields = "tr.id, sm.student_id,  sm.course_id, sm.unit_id,  tr.rpl_process_submission_id,tr.is_accepted,tr.is_action_taken,u.firstname,u.lastname,cc.name,cc.idnumber,c.fullname,c.idnumber as unitcode,sm.attempt,sm.is_paid, sm.paid_amount ";
        //fields    += "qe.question_id,qe.question_type,qe.student_answer,qe.correct_answer,qe.file,qe.statusid";
        var join = " JOIN mdl_rpl_process_submission_master as sm ON sm.id = tr.rpl_process_submission_id ";
        join += "LEFT  JOIN mdl_user as u ON u.id = sm.student_id ";
        join += "LEFT  JOIN mdl_course_categories as cc ON cc.id = sm.course_id ";
        join += "LEFT  JOIN mdl_course as c ON c.id = sm.unit_id ";
        //join += "LEFT  JOIN mdl_rpl_process_question_evidence as qe ON qe.masterid = sm.id ";

        var condition = " where tr.id = '" + id + "' AND sm.is_valid != 'N' ";
        var param = {
            from: 'from mdl_rpl_process_trainer_request as tr',
            condition: condition,
            fields: fields,
            join: join
        };

        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {


            if (userRes.length > 0) {

                var fields = " qe.id,qe.question_id,qe.question_type, qe.student_answer, qe.correct_answer, qe.file, qe.statusid, q.question, qe.is_accepted, qe.is_action_taken, qe.comment, qe.additional_comments";
                var join = " LEFT JOIN mdl_rpl_question as q ON q.id = qe.question_id ";
                var orderby = " order by qe.id ASC ";
                var condition = " where qe.masterid = '" + userRes[0].rpl_process_submission_id + "'";
                var param = {
                    from: 'from mdl_rpl_process_question_evidence as qe',
                    condition: condition,
                    fields: fields,
                    orderby: orderby,
                    join: join
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (quesRes) {
                    if (quesRes.length > 0) {
                        var total = quesRes.length;
                        var qcountArr = [];
                        asyncForEach(quesRes, function (positionitem, positionindex, positionarr) {
                            var evidenceId = positionitem.id;
                            var cnt = positionindex;

                            /***** additional file  ******/

                            var fields = " mf.* ";
                            var condition = " where mf.evedence_id = '" + evidenceId + "' ";
                            var param = {
                                from: 'from mdl_rpl_question_additional_file as mf',
                                condition: condition,
                                fields: fields
                            };
                            Mastermodel.commonSelect(param, function (err) {
                                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                            }, function (additionalRes) {

                                quesRes[cnt].additionalFiles = additionalRes;
                                cnt++;
                                qcountArr.push(positionindex);
                                var questionLen = qcountArr.length;

                                if (total == questionLen) {

                                    var response = {
                                        resCode: 200,
                                        response: {
                                            userRes: userRes,
                                            quesRes: quesRes
                                        }
                                    };
                                    res.status(200).send(response);
                                }

                            });

                            /***** additional file  ******/
                        });

                    } else {
                        res.status(201).send(RESPONSEMESSAGE.RPL.NORECORDSFOUND);
                    }
                });

            } else {
                res.status(201).send(RESPONSEMESSAGE.RPL.NORECORDSFOUND);
            }
        }); //end of select
    }

});

/*
 * Used to get student course unit details
 * Created On : 29-12-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/getStudentCourseUnitDetails', customodule.verifyapicalltoken, function (req, res) {
    var courseId = req.body.courseId;
    var studentId = req.body.studentId;

    /**** Course Search By search Text and isAccrediated parameter  ****/
    var fields = " mc.id, mc.fullname, mc.idnumber, mc.summary, mc.unit_image,   mrpsm.is_submitted,  mrpsm.is_actiontaken, mrpsm.is_approved , mrpsm.final_submitted , mrpsm.id as rpl_submission_id,  mrpsm.trainer_status , mrpsm.trainer_id , mrpsm.is_paid, mrpsm.paid_amount, mrpsm.tcomment";
    var condition = " where 1 and mc.category=" + courseId;
    var join = " LEFT JOIN mdl_rpl_process_submission_master mrpsm  ON mc.id = mrpsm.unit_id  AND mrpsm.student_id = '" + studentId + "'  ";

    var param = {
        from: 'from mdl_course as mc',
        condition: condition,
        fields: fields,
        join: join
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (courseRes) { //console.log(courseRes);
        if (courseRes.length > 0) {
            var response = {
                resCode: 200,
                response: courseRes
            };
            res.status(200).send(response);

        } else {
            res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
        }
    });
});




/*
 * Used to final approve or reject RP, request for student from trainer
 * Created On : 29-12-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/finalapproveRejectRPLUnit', customodule.verifyapicalltoken, function (req, res) {
    var id = req.body.id;
    var comment = req.body.comment;
    var is_accepted = req.body.is_accepted;
    var is_gap_traning_required = req.body.is_gap_traning_required;

    if (!id) {
        res.status(201).send(RESPONSEMESSAGE.COMMONALL.IDREQUIRED);
    } else if (!comment) {
        res.status(201).send(RESPONSEMESSAGE.COMMONALL.COMMENTREQUIRED);
    } else if (!is_accepted) {
        res.status(201).send(RESPONSEMESSAGE.COMMONALL.APPROVEREJECTREQUIRED);
    } else {
        var fields = "id,student_id,course_id,unit_id ";
        var condition = " where id ='" + id + "'";
        var param = {
            from: 'from mdl_rpl_process_submission_master',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (ansRes) {
            if (ansRes.length == 0) {
                res.status(200).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
            } else {
                if (is_accepted == 'Y') {
                    var dataupdate = {
                        is_approved: is_accepted,
                        tcomment: comment,
                        is_actiontaken: 'Y'
                    };
                } else {
                    var dataupdate = {
                        is_approved: is_accepted,
                        tcomment: comment,
                        is_actiontaken: 'Y',
                        total_acnt: '0',
                        is_gap_traning_required: is_gap_traning_required
                    };
                }
                var updatecondition = {
                    id: id
                };
                var param3 = {
                    tablename: 'mdl_rpl_process_submission_master',
                    condition: updatecondition,
                    data: dataupdate
                };
                Mastermodel.commonUpdate(param3, function (error) {
                    res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
                }, function (data4) {
                    var notificationparam = {};
                    notificationparam.userId = ansRes[0]['student_id'];
                    notificationparam.courseId = ansRes[0]['course_id'];
                    notificationparam.unitId = ansRes[0]['unit_id'];
                    notificationparam.userType = 'Student';
                    notificationparam.pushNotificationCode = NOTIFICATIONMESSAGE.TRAINER.APPROVEDRPLDCUMENT;
                    Masternotificationmodel.sendPushNotification(notificationparam, function (err) {
                        var response = {
                            resCode: 201,
                            response: error
                        };
                        res.status(200).send(response);
                    }, function (succNotificationRes) {
                        var response = {
                            resCode: 200,
                            resMessage: "Trainer verified document successfully",
                            response: "Trainer verified document successfully"
                        };
                        res.status(200).send(RESPONSEMESSAGE.RPL.FINALSUBMISSION);
                    });
                });
            }
        });
    }
});
/*
 * Used to get RPL entry test question for resubmit
 * Created On : 03-01-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/getResubmitRPLEntryTestDetails', customodule.verifyapicalltoken, function (req, res) {
    var questionArr = [];
    var rplresubmissionId = req.body.rplresubmissionId;
    var unitId = req.body.unitId;
    var courseId = req.body.courseId;
    var studentId = req.body.studentId;


    if (!rplresubmissionId) {
        res.status(201).send(RESPONSEMESSAGE.COMMONALL.IDREQUIRED);
    } else {

        var fields = " mrsm.id  as rplId";
        var condition = " where mrsm.course_id = '" + courseId + "' AND  mrsm.unit_id = '" + unitId + "' AND  mrsm.student_id = '" + studentId + "' AND `is_saved` = 'Y'  AND `is_submitted` = 'N'  AND is_valid != 'N'  ";
        var orderby = " order by attempt desc";
        var limit = " 0,1 ";
        var param = {
            from: 'from mdl_rpl_process_submission_master as mrsm',
            condition: condition,
            fields: fields,
            orderby: orderby,
            limit: limit
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (stuRes) {


            if (stuRes.length > 0) {
                var rplId = stuRes[0].rplId;
                if (rplId > 0) {

                    /**** Used to get student entry test question list ****/
                    var fields = " mrpqe.question_id, mrq.id, mrq.question, mrq.question_type , mrq.price, me.student_answer, me.masterid, me.additional_comments";
                    var condition = " where mrpqe.masterid = '" + rplresubmissionId + "' AND mrpqe.is_accepted = 'N' AND mrpqe.is_action_taken = 'Y' ";
                    var join = " LEFT JOIN mdl_rpl_question mrq  ON mrpqe.question_id = mrq.id   ";
                    join += " LEFT JOIN mdl_rpl_process_question_evidence me  ON me.question_id = mrq.id   AND  me.masterid = '" + rplId + "'  ";
                    var param = {
                        from: 'from mdl_rpl_process_question_evidence as mrpqe',
                        condition: condition,
                        fields: fields,
                        join: join
                    };
                    Mastermodel.commonSelect(param, function (err) {
                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                    }, function (entrytestRes) {
                        var entryTestquestionLen = entrytestRes.length;
                        if (entrytestRes.length > 0) {


                            /**** Used to get student entry test answer list ****/
                            asyncForEach(entrytestRes, function (positionitem, positionindex, positionarr) {
                                var cnt = positionindex;
                                var fields = " met.* ";
                                var condition = " where met.question_id = '" + positionitem.id + "' ";
                                var param = {
                                    from: 'from mdl_rpl_answer as met',
                                    condition: condition,
                                    fields: fields
                                };

                                // entrytestRes[positionindex].hii="12345" ;
                                Mastermodel.commonSelect(param, function (err) {
                                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                                }, function (ansRes) {

                                    entrytestRes[cnt].answer = ansRes;

                                    /***** additional file  ******/
                                    var fields = " mf.* ";
                                    var condition = " where mf.question_id = '" + positionitem.id + "' AND masterid = '" + rplId + "' ";
                                    var param = {
                                        from: 'from mdl_rpl_question_additional_file as mf',
                                        condition: condition,
                                        fields: fields
                                    };
                                    Mastermodel.commonSelect(param, function (err) {
                                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                                    }, function (additionalRes) {
                                        entrytestRes[cnt].additionalFiles = additionalRes;
                                        cnt++;
                                        questionArr.push(positionindex);
                                        var questionLen = questionArr.length;
                                        if (entryTestquestionLen == questionLen) {
                                            var response = {
                                                resCode: 200,
                                                response: entrytestRes
                                            };
                                            res.status(200).send(response);
                                        }
                                    });
                                    /***** additional file  ******/

                                });


                            });



                        } else {
                            res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NORECORDSFOUND);
                        }
                    });

                }

            } else { // no saved data

                /**** Used to get student entry test question list ****/
                var fields = " mrpqe.question_id, mrq.id, mrq.question, mrq.question_type , mrq.price ";
                var condition = " where mrpqe.masterid = '" + rplresubmissionId + "' AND is_accepted = 'N' AND is_action_taken = 'Y' ";
                var join = " LEFT JOIN mdl_rpl_question mrq  ON mrpqe.question_id = mrq.id   ";
                var param = {
                    from: 'from mdl_rpl_process_question_evidence as mrpqe',
                    condition: condition,
                    fields: fields,
                    join: join
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (entrytestRes) {
                    var entryTestquestionLen = entrytestRes.length;
                    if (entrytestRes.length > 0) {

                        /**** Used to get student entry test answer list ****/
                        asyncForEach(entrytestRes, function (positionitem, positionindex, positionarr) {
                            var cnt = positionindex;
                            var fields = " met.* ";
                            var condition = " where met.question_id = '" + positionitem.id + "' ";
                            var param = {
                                from: 'from mdl_rpl_answer as met',
                                condition: condition,
                                fields: fields
                            };
                            Mastermodel.commonSelect(param, function (err) {
                                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                            }, function (ansRes) {
                                questionArr.push(positionindex);
                                var questionLen = questionArr.length;
                                entrytestRes[cnt].answer = ansRes;
                                cnt++;
                                if (entryTestquestionLen == questionLen) {

                                    var response = {
                                        resCode: 200,
                                        response: entrytestRes
                                    }
                                    res.status(200).send(response);
                                }
                            });
                        });

                    } else {
                        res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NORECORDSFOUND);
                    }
                });

            }

        });
    }
});
/*
 * Used for make payment from student
 * Created On : 03-01-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/demoMakePaymentYes', customodule.verifyapicalltoken, function (req, res, next) {
    var dbsetting = req.db_setting;
    var rplprocesssubmissionid = req.body.rplprocesssubmissionid;
    if (!rplprocesssubmissionid) {
        res.status(201).send(RESPONSEMESSAGE.COMMONALL.IDREQUIRED);
    } else {
        var fields = "id";
        var condition = " where id ='" + rplprocesssubmissionid + "' AND  is_valid != 'N' ";
        var param = {
            from: 'from mdl_rpl_process_submission_master',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (ansRes) {
            if (ansRes.length == 0) {
                res.status(200).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
            } else {

                var dataupdate = {
                    is_paid: 'Yes',
                    paid_amount: 20
                }
                var updatecondition = {
                    id: rplprocesssubmissionid
                }
                var param3 = {
                    tablename: 'mdl_rpl_process_submission_master',
                    condition: updatecondition,
                    data: dataupdate
                }
                Mastermodel.commonUpdate(param3, function (error) {
                    res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
                }, function (data4) {
                    res.status(200).send(RESPONSEMESSAGE.RPL.PAYMENTSUCCESS);

                });

            }
        }); //end of select

    } //end of else
}); //e

/*
 * Used to get course unit details
 * Created On : 04-01-2018
 * Created By : Monalisa
 * Input      : courseId, studentId
 * Modified On
 */
router.post('/getStudentCourseUnitDetailsNew', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    //console.log(req.db_setting);
    var questionArr = [];
    var dbsetting = req.db_setting;
    var courseId = req.body.courseId;
    var studentId = req.body.studentId;


    /**** Course Search By search Text and isAccrediated parameter  ****/

    var fields = " mc.id, mc.fullname, mc.idnumber, mc.summary, mc.unit_image as uimage, (case when mc.unit_image=null then mc.unit_image else concat('" + base_url_unit_image + "',mc.unit_image) end) as unitimage , mc.uprice, mslm.learning_mode";
    var condition = " where 1 and mc.category=" + courseId + "  ";
    var join = " LEFT JOIN mdl_student_learning_mode mslm  ON mc.id = mslm.unit_id  AND mslm.student_id  = '" + studentId + "' AND mslm.course_id  = '" + courseId + "'   ";

    var param = {
        from: 'from mdl_course as mc',
        condition: condition,
        fields: fields,
        join: join
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (courseRes) { //console.log(courseRes);
        var courseResLen = courseRes.length;

        if (courseRes.length > 0) {
            /**** Used to get student entry test answer list ****/
            asyncForEach(courseRes, function (positionitem, positionindex, positionarr) {
                var cnt = positionindex;
                console.log(positionitem.id);
                var fields = " mrpsm.* , mp.id as gap_traning_is_taken,mu.firstname,mu.lastname,mu1.firstname as approvedfirstname,mu1.lastname as approvedlast";
                var condition = " where mrpsm.student_id = '" + studentId + "' AND  mrpsm.course_id = '" + courseId + "'      AND  mrpsm.unit_id = '" + positionitem.id + "' AND is_active = 'Y'   "; //AND is_valid != 'N'
                var join = " LEFT JOIN mdl_gap_traning_payment mp         ON mrpsm.id = mp.rpl_process_submission_id	 AND mp.student_id = '" + studentId + "' ";
                join += " LEFT JOIN mdl_rpl_process_trainer_request mrptr ON mrptr.rpl_process_submission_id = mrpsm.id and mrptr.is_action_taken = 'N' ";
                join += " LEFT JOIN mdl_user mu ON mu.id = mrptr.trainer_id ";
                join += " LEFT JOIN mdl_user mu1 ON mu1.id = mrpsm.trainer_id ";


                var orderby = " order by mrpsm.id DESC limit 0,1 ";
                var param = {
                    from: 'from mdl_rpl_process_submission_master as mrpsm',
                    condition: condition,
                    fields: fields,
                    join: join,
                    orderby: orderby
                };
                // entrytestRes[positionindex].hii="12345" ;
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (ansRes) {



                    // console.log(entrytestRes[positionindex]);
                    //  console.log(entrytestRes);

                    if (ansRes.length > 0) {

                        var rplId = ansRes[0].id;
                        var attempt = ansRes[0].attempt;
                        courseRes[cnt].rpl = ansRes[0];

                        //console.log(courseResLen, questionLen);



                        /********** Previous attempt code***************/
                        var previousattempt = attempt - 1;
                        console.log("previousattempt", previousattempt);
                        //if (previousattempt > 0) {

                        var fields = "m.id";
                        var condition = " where m.course_id = '" + courseId + "' AND m.unit_id = '" + positionitem.id + "' AND m.student_id = '" + studentId + "' and attempt = '" + previousattempt + "' AND is_valid != 'N'  ";
                        var param = {
                            from: 'from mdl_rpl_process_submission_master as m',
                            condition: condition,
                            fields: fields
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (previousattemptRes) {
                            questionArr.push(positionindex);
                            var questionLen = questionArr.length;

                            courseRes[cnt].previousattempt = previousattemptRes[0];
                            cnt++;

                            if (courseResLen == questionLen) {
                                var response = {
                                    resCode: 200,
                                    response: courseRes
                                }
                                res.status(200).send(response);
                            }

                        });


                        /*} else {
                            cnt++;
                            if (courseResLen == questionLen) {
                                var response = {
                                    resCode: 200,
                                    response: courseRes
                                }
                                res.status(200).send(response);
                            }
                        }*/



                        //    }); //end of inner select


                    } else {
                        questionArr.push(positionindex);
                        var questionLen = questionArr.length;

                        if (courseResLen == questionLen) {
                            var response = {
                                resCode: 200,
                                response: courseRes
                            }
                            res.status(200).send(response);
                        }


                    }



                }); ////end of middle select


            }); //end of first loop





        } else {
            res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
        }
    });
});

/*
 * Used to get approved unit request list for trainer
 * Created On : 04-01-2018
 * Created By : Monalisa
 * Input      : trainer_id
 * Modified On
 */
router.post('/getApprovedUnitRequestList', customodule.verifyapicalltoken, function (req, res, next) {
    var trainer_id = req.body.trainer_id;
    var searchTextval = req.body.searchTextval;
    var searchStudentName = req.body.searchStudentName;
    var isQualified = req.body.isQualified;


    var dbsetting = req.db_setting;
    if (!trainer_id) {
        res.status(200).send(RESPONSEMESSAGE.COMMONALL.TRAINERREQUIRED);
    } else {

        var fields = "tr.id,mpd.amount, mpd.date as payment_date, tr.rpl_process_submission_id,tr.is_accepted,tr.is_action_taken,u.firstname,u.lastname,cc.name,cc.idnumber,c.fullname,c.idnumber as unitcode,sm.attempt, sm.is_paid, sm.paid_amount,sm.is_approved, sm.student_id, sm.course_id, sm.unit_id";
        var join = "  JOIN mdl_rpl_process_submission_master as sm ON sm.id = tr.rpl_process_submission_id   "; //AND
        join += "LEFT  JOIN mdl_payment_details as mpd ON mpd.id             = sm.payment_id ";
        join += "LEFT  JOIN mdl_user as u ON u.id                            = sm.student_id ";
        join += "LEFT  JOIN mdl_course_categories as cc ON cc.id             = sm.course_id ";
        join += "LEFT  JOIN mdl_course as c ON c.id                          = sm.unit_id ";

        var condition = " where tr.trainer_id = '" + trainer_id + "'  AND sm.final_submitted='Y'  "; //AND sm.is_actiontaken='Y'
        if (searchTextval != "")
            condition += " AND ( cc.name LIKE '%" + searchTextval + "%' OR  cc.idnumber LIKE '%" + searchTextval + "%' ) ";
        if (searchStudentName != "")
            condition += " AND ( u.firstname LIKE '%" + searchStudentName + "%' OR  u.lastname LIKE '%" + searchStudentName + "%' ) ";
        if (isQualified != "")
            condition += " AND  sm.is_approved = 'Y' AND  sm.is_valid = 'Y' ";

        var orderby = " order by tr.id DESC ";
        var param = {

            from: 'from mdl_rpl_process_trainer_request as tr',
            condition: condition,
            fields: fields,
            join: join,
            orderby: orderby
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {
            if (userRes.length > 0) {
                var response = {
                    resCode: 200,
                    resData: userRes,
                    response: userRes
                }
                res.status(200).send(response);
            } else {
                res.status(201).send(RESPONSEMESSAGE.RPL.NORECORDSFOUND);
            }
        }); //end of select
    }

}); //e


/*
 * Used to get unit request list for trainer modified
 * Created On : 05-01-2018
 * Created By : Monalisa
 * Input      : trainer_id
 * Modified On
 */
router.post('/getUnitRequestListNew', customodule.verifyapicalltoken, function (req, res, next) {

    var questionArr = [];
    var trainer_id = req.body.trainer_id;
    var searchTextval = req.body.searchTextval;
    if (!trainer_id) {
        res.status(200).send(RESPONSEMESSAGE.COMMONALL.TRAINERREQUIRED);
    } else {

        var fields = "tr.id, mpd.amount, mpd.date as payment_date, sm.course_id, sm.unit_id, sm.student_id, tr.rpl_process_submission_id,tr.is_accepted,tr.is_action_taken,u.firstname,u.lastname,cc.name,cc.idnumber,c.fullname,c.idnumber as unitcode,sm.attempt, sm.is_paid, sm.paid_amount, mrrl.is_action as refund_action, mrrl.is_approved as refund_approved";
        var join = "  JOIN mdl_rpl_process_submission_master as sm ON sm.id = tr.rpl_process_submission_id  AND  sm.is_actiontaken != 'Y' AND  sm.is_valid != 'N' ";
        join += "LEFT  JOIN mdl_payment_details as mpd ON mpd.id             = sm.payment_id ";
        join += "LEFT  JOIN mdl_user as u ON u.id                            = sm.student_id ";
        join += "LEFT  JOIN mdl_course_categories as cc ON cc.id             = sm.course_id ";
        join += "LEFT  JOIN mdl_course as c ON c.id                          = sm.unit_id ";
        join += "LEFT  JOIN mdl_refund_request_log as mrrl ON mrrl.rpl_process_submission_id    = sm.id ";

        var condition = " where tr.trainer_id = '" + trainer_id + "' AND sm.is_valid != 'N' AND ( (tr.is_action_taken = 'N' && tr.is_accepted = 'N') ||  (tr.is_action_taken = 'Y' && tr.is_accepted = 'Y') ) AND sm.final_submitted='Y' ";
        if (searchTextval != "") {
            condition += " AND (u.firstname LIKE '%" + searchTextval + "%' OR  cc.name LIKE '%" + searchTextval + "%' ) ";
        }
        var orderby = " order by tr.id DESC ";
        var param = {

            from: 'from mdl_rpl_process_trainer_request as tr',
            condition: condition,
            fields: fields,
            join: join,
            orderby: orderby
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {
            var courseResLen = userRes.length;
            if (userRes.length > 0) {
                /**** Used to get student entry test answer list ****/
                asyncForEach(userRes, function (positionitem, positionindex, positionarr) {
                    var cnt = positionindex;
                    var fields = " mr.id, mr.is_accepted, mr.is_action_taken ";
                    var condition = " where mr.masterid = '" + positionitem.rpl_process_submission_id + "'   ";

                    var param = {

                        from: 'from mdl_rpl_process_question_evidence as mr',
                        condition: condition,
                        fields: fields
                    };
                    // entrytestRes[positionindex].hii="12345" ;
                    Mastermodel.commonSelect(param, function (err) {
                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                    }, function (ansRes) {
                        questionArr.push(positionindex);
                        var questionLen = questionArr.length;
                        // console.log(entrytestRes[positionindex]);
                        //  console.log(entrytestRes);
                        userRes[cnt].count = ansRes;
                        cnt++;
                        if (courseResLen == questionLen) {

                            var response = {
                                resCode: 200,
                                resData: userRes,
                                response: userRes
                            }
                            res.status(200).send(response);
                        }
                    })
                })

            } else {
                res.status(201).send(RESPONSEMESSAGE.RPL.NORECORDSFOUND);
            }
        }); //end of select
    }

}); //e


/*
 * Used for add learning mode
 * Created On : 05-01-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/addStudentLearningMode', customodule.verifyapicalltoken, function (req, res, next) {


    var dbsetting = req.db_setting;
    var studentId = req.body.studentId;
    var courseId = req.body.courseId;
    var unitId = req.body.unitId;
    var learningmode = req.body.learningmode;

    // console.log(is_accepted);

    if (!studentId) {
        res.status(201).send(RESPONSEMESSAGE.RPL.STUDENTIDREQUIRED);
    } else if (!courseId) {
        res.status(201).send(RESPONSEMESSAGE.RPL.COURSEIDREQUIRED);
    } else if (!learningmode) {
        res.status(201).send(RESPONSEMESSAGE.RPL.LEARNINGMODEREQUIRED);
    } else {

        var fields = "id";
        var condition = " where student_id ='" + studentId + "' AND course_id = '" + courseId + "' AND unit_id = '" + unitId + "'  ";

        var param = {

            from: 'from mdl_student_learning_mode',
            condition: condition,
            fields: fields
        };

        console.log("param222", param);


        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (ansRes) {
            if (ansRes.length > 0) {
                res.status(200).send(RESPONSEMESSAGE.COMMON.LEARNINGMODEALREADYEXISTS);
            } else {

                var saveData = {};
                saveData.student_id = studentId;
                saveData.course_id = courseId;
                saveData.unit_id = unitId;
                saveData.learning_mode = learningmode;

                var param = {

                    tablename: 'mdl_student_learning_mode',
                    data: [saveData]
                }

                Mastermodel.commonInsert(param, function (err) {
                    console.log("++++++++++++++++++++++++++++++++++++", err);
                    res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                }, function (data) {

                    var response = {
                        resCode: 200,
                        response: "Learning Mode Selected Successfully"
                    }
                    res.status(200).send(response);
                    //res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.RPLTESTSUBMITTED);


                });

            }
        }); //end of select

    } //end of else
}); //e

/*
 * Used to get student virtual classroom schdule
 * Created On : 11-01-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/getStudentVCSelectTrainerPage', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    //console.log(req.db_setting);
    var dbsetting = req.db_setting;
    var searchText = req.body.searchText;
    var isAccrediated = req.body.isAccrediated;
    var studentId = req.body.studentId;


    if (!studentId || studentId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.STUDENTIDREQUIRED);
    } else {

        /**** Course Search By search Text and isAccrediated parameter  ****/
        var fields = " mslm.*,   mcc.name as cname,  mcc.idnumber as ccode,  mc.fullname as uname,  mc.idnumber as ucode  ";
        // /*, mrpsm.is_submitted,  mrpsm.is_actiontaken, mrpsm.is_approved , mrpsm.final_submitted , mrpsm.id as rpl_submission_id,  mrpsm.trainer_status , mrpsm.trainer_id
        var condition = " where 1 AND mslm.student_id = '" + studentId + "'  AND mslm.learning_mode= 'vc'  ";
        var join = " LEFT JOIN mdl_course_categories mcc ON mslm.course_id = mcc.id ";
        join += " LEFT JOIN mdl_course mc             ON mslm.unit_id = mc.id  ";
        //join     += " LEFT JOIN mdl_course mc             ON mslm.unit_id = mc.id  ";

        var orderby = " order by mslm.id desc ";

        var param = {

            from: 'from mdl_student_learning_mode as mslm',
            condition: condition,
            fields: fields,
            join: join,
            orderby: orderby
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (courseRes) { //console.log(courseRes);
            if (courseRes.length > 0) {
                var response = {
                    resCode: 200,
                    response: courseRes
                }
                res.status(200).send(response);

            } else {
                res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
            }
        });

    }

});



/*
 * Used to get student face to face  schdule
 * Created On : 11-01-2018
 * Created By : Monalisa
 * Input      : searchText(char)
 * Modified On
 */
router.post('/getStudentf2fSelectTrainerPage', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    //console.log(req.db_setting);
    var dbsetting = req.db_setting;
    var searchText = req.body.searchText;
    var isAccrediated = req.body.isAccrediated;
    var studentId = req.body.studentId;


    if (!studentId || studentId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.STUDENTIDREQUIRED);
    } else {

        /**** Course Search By search Text and isAccrediated parameter  ****/
        var fields = " mslm.*,   mcc.name as cname,  mcc.idnumber as ccode,  mc.fullname as uname,  mc.idnumber as ucode  ";
        // /*, mrpsm.is_submitted,  mrpsm.is_actiontaken, mrpsm.is_approved , mrpsm.final_submitted , mrpsm.id as rpl_submission_id,  mrpsm.trainer_status , mrpsm.trainer_id
        var condition = " where 1 AND mslm.student_id = '" + studentId + "'  AND mslm.learning_mode= 'f2f'  ";
        var join = " LEFT JOIN mdl_course_categories mcc ON mslm.course_id = mcc.id ";
        join += " LEFT JOIN mdl_course mc             ON mslm.unit_id = mc.id  ";

        var orderby = " order by mslm.id desc ";

        var param = {

            from: 'from mdl_student_learning_mode as mslm',
            condition: condition,
            fields: fields,
            join: join,
            orderby: orderby
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (courseRes) { //console.log(courseRes);
            if (courseRes.length > 0) {
                var response = {
                    resCode: 200,
                    response: courseRes
                }
                res.status(200).send(response);

            } else {
                res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
            }
        });

    }

});
/*
 * Used to get student RPL Request List
 * Created On : 15-01-2018
 * Created By : Monalisa
 * Input      : searchText(char)
 * Modified On
 */
router.post('/getStudentRPLRequestList', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    //console.log(req.db_setting);
    var dbsetting = req.db_setting;
    var searchText = req.body.searchText;
    var action = req.body.action;
    var studentId = req.body.studentId;

    if (!studentId || studentId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.STUDENTIDREQUIRED);
    } else {

        if (node_module.validator.matches(searchText, SETTING.specialcharpattern)) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.INVALIDREQUESTSPECIALCHAR);
        } else {

            /**** Course Search By search Text and isAccrediated parameter  ****/
            var fields = " mt.*, ms.attempt, ms.course_id, ms.unit_id, ms.student_id, mcc.name as cname, mcc.idnumber as ccode, mc.fullname as uname, mc.idnumber as ucode, mu.firstname, mu.lastname  ";
            var condition = " where 1 AND  ms.student_id = '" + studentId + "' AND  mt.is_action_taken = 'Y'  AND  mt.is_resubmit = 'N'  ";
            if (searchText != "")
                condition += " AND ( mcc.name LIKE '%" + searchText + "%' OR  mcc.idnumber LIKE '%" + searchText + "%' ) ";
            if (action != "")
                condition += " AND  mt.is_accepted LIKE '%" + action + "%'   ";
            var join = " LEFT JOIN mdl_rpl_process_submission_master ms ON mt.rpl_process_submission_id = ms.id  AND ms.is_valid != 'N' ";
            join += " LEFT JOIN mdl_course_categories mcc            ON ms.course_id                 = mcc.id  ";
            join += " LEFT JOIN mdl_course                    mc     ON ms.unit_id                   = mc.id  ";
            join += " LEFT JOIN mdl_user                      mu     ON mt.trainer_id                = mu.id ";
            var param = {

                from: 'from mdl_rpl_process_trainer_request as mt',
                condition: condition,
                fields: fields,
                join: join
                //,orderby: orderby
            };
            Mastermodel.commonSelect(param, function (err) {
                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            }, function (courseRes) { //console.log(courseRes);
                if (courseRes.length > 0) {
                    var response = {
                        resCode: 200,
                        response: courseRes
                    }
                    res.status(200).send(response);

                } else {
                    res.status(200).send(RESPONSEMESSAGE.COMMON.NORECORDS);
                }
            });

        }

    }

});



/*
 * Used to get student VC Trainer List
 * Created On : 17-01-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/getVcTrainerList', customodule.verifyapicalltoken, function (req, res, next) {
    var studentId = req.body.studentId;
    var courseId = req.body.courseId;
    var unitId = req.body.unitId;
    var learningmode = req.body.learningmode;
    var dbsetting = req.db_setting;


    if (!studentId || studentId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.STUDENTIDREQUIRED);
    } else if (!courseId || courseId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.COURSEIDREQUIRED);
    } else {

        if (unitId == undefined || unitId == null)
            unitId = 0;

        var d = new Date();
        var m1 = d.getMonth() + 1;
        var datetime = d.getFullYear() + "-" + m1 + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();


        var fields = " (select AVG(rating) from mdl_trainer_rating as mtrt where mt.trainerid = mtrt.trainer_id and mt.course_id = mtrt.course_id and mt.unit_id = mtrt.unit_id ) as avgrating, mt.id, mt.trainerid,  mt.price, mt.date,  mt.timezone,  mu.firstname, mu.lastname, mu.profile_image,mv.id as bookedId, mv.is_valid";
        var condition = " where mt.course_id = " + courseId + " AND  mt.unit_id = " + unitId + "  AND mt.learning_mode_type_id = " + learningmode + "";
        condition += " AND  mt.date >= '" + datetime + "' ";
        var join = " LEFT JOIN mdl_user as mu           ON mt.trainerid = mu.id ";
        join += " LEFT JOIN mdl_student_booked_vc mv ON mt.id        = mv.schdule_id AND mv.student_id = " + studentId + " ";
        var param = {

            from: 'from mdl_trainer_schdule mt ',
            condition: condition,
            fields: fields,
            join: join
        };

        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {
            if (userRes.length > 0) {
                var response = {
                    resCode: 200,
                    response: userRes,
                    resData: userRes
                }
                res.status(200).send(response);
            } else {
                res.status(201).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
            }
        }); //end of select


    }


}); //e



/*
 * Used to get student cart details
 * Created On : 02-03-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/getCartDetails', customodule.verifyapicalltoken, function (req, res, next) {
    var schdule_id = req.body.schdule_id;
    var dbsetting = req.db_setting;


    if (!schdule_id || schdule_id == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.STUDENTIDREQUIRED);
    } else {


        var fields = " mt.*, mlmt .name as learning_mode,  mu.firstname, mu.lastname, mu.profile_image, mcc.name as cname, mcc.idnumber as ccode, mc.fullname as uname, mc.idnumber as ucode";
        var condition = " where mt.id  = " + schdule_id + "  ";
        var join = "  LEFT JOIN mdl_user as mu             ON mt.trainerid = mu.id ";
        join += " LEFT JOIN mdl_course_categories mcc  ON mt.course_id = mcc.id  ";
        join += " LEFT JOIN mdl_course          mc     ON mt.unit_id   = mc.id  ";
        join += " LEFT JOIN mdl_learning_mode_type  mlmt    ON mlmt.id   = mt.learning_mode_type_id  ";


        var param = {

            from: 'from mdl_trainer_schdule mt ',
            condition: condition,
            fields: fields,
            join: join
        };

        console.log("param222", param);
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {
            if (userRes.length > 0) {
                var response = {
                    resCode: 200,
                    response: userRes[0]
                }
                res.status(200).send(response);
            } else {
                res.status(201).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
            }
        }); //end of select


    }


}); //e

/*
 * Used to get add student booked VC
 * Created On : 18-01-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/addStudentBookedVC', customodule.verifyapicalltoken, function (req, res, next) {


    var dbsetting = req.db_setting;
    var studentId = req.body.studentId;
    var courseId = req.body.courseId;
    var unitId = req.body.unitId;
    var trainerId = req.body.trainerId;
    var schduleId = req.body.schduleId;

    var d = new Date();
    var m1 = d.getMonth() + 1;
    var datetime = d.getFullYear() + "-" + m1 + "-" + d.getDate();

    if (!studentId || studentId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.STUDENTIDREQUIRED);
    } else if (!courseId || courseId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.COURSEIDREQUIRED);
    } else if (!trainerId || trainerId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.STUDENTIDREQUIRED);
    } else if (!schduleId || schduleId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.SCHDULEIDREQUIRED);
    } else {

        var fields = "id,price";
        var condition = " where id ='" + schduleId + "'   ";

        var param = {

            from: 'from mdl_trainer_schdule',
            condition: condition,
            fields: fields
        };

        console.log("param222", param);


        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (ansRes) {
            if (ansRes.length > 0) {

                var price = ansRes[0].price;

                var saveData = {};
                saveData.student_id = studentId;
                saveData.course_id = courseId;
                saveData.unit_id = unitId;
                saveData.trainer_id = trainerId;
                saveData.schdule_id = schduleId;
                saveData.payment_amount = price;
                saveData.payment_date = datetime;

                var param = {

                    tablename: 'mdl_student_booked_vc',
                    data: [saveData]
                }

                Mastermodel.commonInsert(param, function (err) {
                    console.log("++++++++++++++++++++++++++++++++++++", err);
                    res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                }, function (data) {

                    var response = {
                        resCode: 200,
                        response: "VC Schdule Booked Successfully"
                    }
                    res.status(200).send(response);
                    //res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.RPLTESTSUBMITTED);
                });

            } else {

                res.status(200).send(RESPONSEMESSAGE.COMMON.NODATAEXISTS);
            }
        }); //end of select

    } //end of else
}); //e

/*
 * Used to get student booked VC Liist
 * Created On : 18-01-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/studentBookedVCList', function (req, res, next) {
    var studentId = req.body.studentId;
    var dbsetting = '';
    var questionArr = [];


    if (!studentId || studentId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.STUDENTIDREQUIRED);
    } else {

        var base_url1 = "http://52.63.96.113:4444/share/courseimage/";
        var base_url2 = "http://52.63.96.113:4444/share/unitimage/";

        var d = new Date();
        var month = d.getMonth() + 1;
        var datetime = d.getFullYear() + "-" + month + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();


        var fields = " msbv.*, ml.name as learning_mode, mts.date, mts.price, mts.duration, DATE_FORMAT(mts.date, '%a %M %d %Y %H %i %p') as dateformat,  mu.firstname, mu.lastname, mu.profile_image, mcc.name as cname, mcc.description as course_description,  mcc.idnumber as ccode, (case when mcc.course_image=null then mcc.course_image else concat('" + base_url1 + "',mcc.course_image) end) as courseimage, mc.summary as unit_description,  mc.fullname as uname, mc.idnumber as ucode";
        fields += ",  (case when mc.unit_image=null then mc.unit_image else concat('" + base_url2 + "',mc.unit_image) end) as unitimage";
        var condition = " where msbv.student_id = " + studentId + "  AND  msbv.learning_mode_type_id in (2,4) AND msbv.is_valid = 'Y' ";
        condition += " AND  mts.date >= '" + datetime + "' ";
        var join = " LEFT JOIN mdl_user   as mu               ON mu.id  = msbv.trainer_id ";
        join += " LEFT JOIN mdl_course as mc              ON mc.id  = msbv.unit_id ";
        join += " LEFT JOIN mdl_course_categories as mcc  ON mcc.id = msbv.course_id ";
        join += " LEFT JOIN mdl_trainer_schdule as   mts  ON mts.id = msbv.schdule_id ";
        join += " LEFT JOIN mdl_learning_mode_type as ml  ON ml.id  = msbv.learning_mode_type_id ";

        var orderby = "order by mts.date asc";
        var param = {

            from: 'from mdl_student_booked_vc msbv ',
            condition: condition,
            fields: fields,
            join: join,
            orderby: orderby
        };

        console.log("param222", param);


        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (qualificationRes) {

            var quesLen = qualificationRes.length;
            var queaArr = new Array();
            if (qualificationRes.length > 0) {


                /*var response = {
                    resCode: 200,
                    response: userRes,
                    resData: userRes
                }
                 res.status(201).send(response);*/


                asyncForEach(qualificationRes, function (positionitem, positionindex, positionarr) {
                    var cnt = positionindex;
                    console.log(positionitem.id);
                    var schduleid = positionitem.schdule_id;

                    var fields = " count(id) as no_of_student ";
                    var condition = " where schdule_id = '" + schduleid + "'   ";
                    var param = {

                        from: 'from mdl_student_booked_vc ',
                        condition: condition,
                        fields: fields
                    };

                    Mastermodel.commonSelect(param, function (err) {
                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                    }, function (courseRes) {
                        if (courseRes.length > 0) {
                            var no_of_student = courseRes[0].no_of_student;

                            qualificationRes[cnt].no_of_student = no_of_student;

                            cnt++;
                            questionArr.push(positionindex);
                            var questionLen = questionArr.length;
                            queaArr.push({
                                "questionId": positionitem.id
                            })

                            if (quesLen == queaArr.length) {

                                var response = {
                                    resCode: 200,
                                    response: qualificationRes
                                }
                                res.status(200).send(response);
                            }

                        }

                    });

                })






            } else {
                res.status(201).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
            }
        }); //end of select


    }


}); //e




/*
 * Used to get student booked VC Liist
 * Created On : 18-01-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/studentBookedVCListWeb', customodule.verifyapicalltoken, function (req, res, next) {
    var studentId = req.body.studentId;
    var dbsetting = req.db_setting;

    var fromDate = req.body.fromDate;
    var toDate = req.body.toDate;
    var searchTextval = req.body.searchTextval;
    var learningMode = req.body.learningMode;


    var questionArr = [];


    if (!studentId || studentId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.STUDENTIDREQUIRED);
    } else {

        if (node_module.validator.matches(searchTextval, SETTING.specialcharpattern)) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.INVALIDREQUESTSPECIALCHAR);
        } else {


            var base_url1 = "http://52.63.96.113:4444/share/courseimage/";
            var base_url2 = "http://52.63.96.113:4444/share/unitimage/";

            var d = new Date();
            var month = d.getMonth() + 1;
            var datetime = d.getFullYear() + "-" + month + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
            var fields = " msbv.*, ml.name as learning_mode, mts.date, mts.todate, mts.price, mts.duration, DATE_FORMAT(mts.date, '%a %M %d %Y %H %i %p') as dateformat,  mu.firstname, mu.lastname, mu.profile_image, mcc.name as cname, mcc.description as course_description,  mcc.idnumber as ccode, (case when mcc.course_image=null then mcc.course_image else concat('" + base_url1 + "',mcc.course_image) end) as courseimage, mc.summary as unit_description,  mc.fullname as uname, mc.idnumber as ucode";
            fields += ",  (case when mc.unit_image=null then mc.unit_image else concat('" + base_url2 + "',mc.unit_image) end) as unitimage";


            var condition = " where msbv.student_id = " + studentId + "   ";
            condition += " AND  mts.date >= '" + datetime + "' ";
            if (searchTextval != "")
                condition += " AND ( mcc.name LIKE '%" + searchTextval + "%' OR  mcc.idnumber LIKE '%" + searchTextval + "%' ) ";
            if (learningMode != "")
                condition += " AND ( msbv.learning_mode_type_id = '" + learningMode + "' ) ";
            /* if (fromDate != "" && toDate != "")
                 condition += " AND ( mts.date >= '" + fromDate + "' &&  mts.todate <= '" + toDate + "' ) ";*/

            if (fromDate != "" && (toDate == "" || toDate == undefined))
                condition += " AND  mts.date >= '" + fromDate + "' ";
            if (toDate != "" && (fromDate == "" || fromDate == undefined))
                condition += " AND  mts.date <= '" + toDate + "' ";
            if (fromDate != "" && toDate != "")
                condition += " AND  mts.date BETWEEN '" + fromDate + "' AND '" + toDate + "'   ";



            var join = " LEFT JOIN mdl_user   as mu              ON mu.id  = msbv.trainer_id ";
            join += " LEFT JOIN mdl_course as mc              ON mc.id  = msbv.unit_id ";
            join += " LEFT JOIN mdl_course_categories as mcc  ON mcc.id = msbv.course_id ";
            join += " LEFT JOIN mdl_trainer_schdule as   mts  ON mts.id = msbv.schdule_id ";
            join += " LEFT JOIN mdl_learning_mode_type as ml  ON ml.id  = msbv.learning_mode_type_id ";

            var orderby = "order by mts.date asc";
            var param = {
                from: 'from mdl_student_booked_vc msbv ',
                condition: condition,
                fields: fields,
                join: join,
                orderby: orderby
            };

            Mastermodel.commonSelect(param, function (err) {
                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            }, function (qualificationRes) {

                var quesLen = qualificationRes.length;
                var queaArr = new Array();
                if (qualificationRes.length > 0) {


                    /*var response = {
                        resCode: 200,
                        response: userRes,
                        resData: userRes
                    }
                    res.status(201).send(response);*/


                    asyncForEach(qualificationRes, function (positionitem, positionindex, positionarr) {
                        var cnt = positionindex;
                        console.log(positionitem.id);
                        var schduleid = positionitem.schdule_id;

                        var fields = " count(id) as no_of_student ";
                        var condition = " where schdule_id = '" + schduleid + "'   ";
                        var param = {

                            from: 'from mdl_student_booked_vc ',
                            condition: condition,
                            fields: fields
                        };

                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (courseRes) {
                            if (courseRes.length > 0) {
                                var no_of_student = courseRes[0].no_of_student;

                                qualificationRes[cnt].no_of_student = no_of_student;

                                cnt++;
                                questionArr.push(positionindex);
                                var questionLen = questionArr.length;
                                queaArr.push({
                                    "questionId": positionitem.id
                                })

                                if (quesLen == queaArr.length) {

                                    var response = {
                                        resCode: 200,
                                        response: qualificationRes
                                    }
                                    res.status(200).send(response);
                                }

                            }

                        });

                    })






                } else {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
                }
            }); //end of select

        }

    }


}); //e





/*
 * Used to get student completed booked VC Liist
 * Created On : 18-01-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/studentBookedVCLogList', function (req, res, next) {
    var studentId = req.body.studentId;
    var dbsetting = '';
    var questionArr = [];


    if (!studentId || studentId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.STUDENTIDREQUIRED);
    } else {

        var base_url1 = "http://52.63.96.113:4444/share/courseimage/";
        var base_url2 = "http://52.63.96.113:4444/share/unitimage/";

        var d = new Date();
        var month = d.getMonth() + 1;
        var datetime = d.getFullYear() + "-" + month + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();

        var fields = " msbv.*, ml.name as learning_mode, mts.date, mts.price, mts.duration, DATE_FORMAT(mts.date, '%a %M %d %Y %H %i %p') as dateformat,  mu.firstname, mu.lastname, mu.profile_image, mcc.name as cname, mcc.description as course_description,  mcc.idnumber as ccode, (case when mcc.course_image=null then mcc.course_image else concat('" + base_url1 + "',mcc.course_image) end) as courseimage, mc.summary as unit_description,  mc.fullname as uname, mc.idnumber as ucode";
        fields += ",  (case when mc.unit_image=null then mc.unit_image else concat('" + base_url2 + "',mc.unit_image) end) as unitimage";
        var condition = " where msbv.student_id = " + studentId + "  AND  msbv.learning_mode_type_id in (2,4) AND msbv.is_valid = 'Y' ";
        condition += " AND  mts.date < '" + datetime + "' ";
        var join = " LEFT JOIN mdl_user   as mu              ON mu.id  = msbv.trainer_id ";
        join += " LEFT JOIN mdl_course as mc                 ON mc.id  = msbv.unit_id ";
        join += " LEFT JOIN mdl_course_categories as mcc     ON mcc.id = msbv.course_id ";
        join += " LEFT JOIN mdl_trainer_schdule as   mts     ON mts.id = msbv.schdule_id ";
        join += " LEFT JOIN mdl_learning_mode_type as ml    ON ml.id  = msbv.learning_mode_type_id ";

        var orderby = "order by mts.date asc";
        var param = {

            from: 'from mdl_student_booked_vc msbv ',
            condition: condition,
            fields: fields,
            join: join,
            orderby: orderby
        };

        console.log("param222", param);


        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (qualificationRes) {

            var quesLen = qualificationRes.length;
            var queaArr = new Array();
            if (qualificationRes.length > 0) {


                /*var response = {
                    resCode: 200,
                    response: userRes,
                    resData: userRes
                }
                 res.status(201).send(response);*/


                asyncForEach(qualificationRes, function (positionitem, positionindex, positionarr) {
                    var cnt = positionindex;
                    console.log(positionitem.id);
                    var schduleid = positionitem.schdule_id;

                    var fields = " count(id) as no_of_student ";
                    var condition = " where schdule_id = '" + schduleid + "'  ";
                    var param = {

                        from: 'from mdl_student_booked_vc ',
                        condition: condition,
                        fields: fields
                    };

                    Mastermodel.commonSelect(param, function (err) {
                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                    }, function (courseRes) {
                        if (courseRes.length > 0) {
                            var no_of_student = courseRes[0].no_of_student;

                            qualificationRes[cnt].no_of_student = no_of_student;

                            cnt++;
                            questionArr.push(positionindex);
                            var questionLen = questionArr.length;
                            queaArr.push({
                                "questionId": positionitem.id
                            })

                            if (quesLen == queaArr.length) {

                                var response = {
                                    resCode: 200,
                                    response: qualificationRes
                                }
                                res.status(200).send(response);
                            }

                        }

                    });

                })






            } else {
                res.status(201).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
            }
        }); //end of select


    }


}); //e




/*
 * Used to get student completed booked VC Liist
 * Created On : 18-01-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/studentBookedVCLogListWeb', customodule.verifyapicalltoken, function (req, res, next) {
    var studentId = req.body.studentId;
    var dbsetting = req.db_setting;
    var questionArr = [];
    var fromDate = req.body.fromDate;
    var toDate = req.body.toDate;
    var searchTextval = req.body.searchTextval;
    var learningMode = req.body.learningMode;
    if (!studentId || studentId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.STUDENTIDREQUIRED);
    } else {

        if (node_module.validator.matches(searchTextval, SETTING.specialcharpattern)) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.INVALIDREQUESTSPECIALCHAR);
        } else {
            var base_url1 = "http://52.63.96.113:4444/share/courseimage/";
            var base_url2 = "http://52.63.96.113:4444/share/unitimage/";
            var d = new Date();
            var month = d.getMonth() + 1;
            var datetime = d.getFullYear() + "-" + month + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();

            var fields = " msbv.*, ml.name as learning_mode, mts.date, mts.todate, mts.price, mts.duration, DATE_FORMAT(mts.date, '%a %M %d %Y %H %i %p') as dateformat,  mu.firstname, mu.lastname, mu.profile_image, mcc.name as cname, mcc.description as course_description,  mcc.idnumber as ccode, (case when mcc.course_image=null then mcc.course_image else concat('" + base_url1 + "',mcc.course_image) end) as courseimage, mc.summary as unit_description,  mc.fullname as uname, mc.idnumber as ucode";
            fields += ",  (case when mc.unit_image=null then mc.unit_image else concat('" + base_url2 + "',mc.unit_image) end) as unitimage";
            var condition = " where msbv.student_id = " + studentId + "  ";
            condition += " AND  mts.date < '" + datetime + "' ";

            if (searchTextval != "")
                condition += " AND ( mcc.name LIKE '%" + searchTextval + "%' OR  mcc.idnumber LIKE '%" + searchTextval + "%' ) ";
            if (learningMode != "")
                condition += " AND ( msbv.learning_mode_type_id = '" + learningMode + "' ) ";
            /* if (fromDate != "" && toDate != "")
                 condition += " AND ( mts.date >= '" + fromDate + "' &&  mts.todate <= '" + toDate + "' ) ";*/

            if (fromDate != "" && (toDate == "" || toDate == undefined))
                condition += " AND  mts.date >= '" + fromDate + "' ";
            if (toDate != "" && (fromDate == "" || fromDate == undefined))
                condition += " AND  mts.date <= '" + toDate + "' ";
            if (fromDate != "" && toDate != "")
                condition += " AND  mts.date BETWEEN '" + fromDate + "' AND '" + toDate + "'   ";




            var join = " LEFT JOIN mdl_user   as mu              ON mu.id  = msbv.trainer_id ";
            join += " LEFT JOIN mdl_course as mc                 ON mc.id  = msbv.unit_id ";
            join += " LEFT JOIN mdl_course_categories as mcc     ON mcc.id = msbv.course_id ";
            join += " LEFT JOIN mdl_trainer_schdule as   mts     ON mts.id = msbv.schdule_id ";
            join += " LEFT JOIN mdl_learning_mode_type as ml    ON ml.id  = msbv.learning_mode_type_id ";

            var orderby = "order by mts.date asc";
            var param = {

                from: 'from mdl_student_booked_vc msbv ',
                condition: condition,
                fields: fields,
                join: join,
                orderby: orderby
            };

            Mastermodel.commonSelect(param, function (err) {
                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            }, function (qualificationRes) {

                var quesLen = qualificationRes.length;
                var queaArr = new Array();
                if (qualificationRes.length > 0) {


                    /*var response = {
                        resCode: 200,
                        response: userRes,
                        resData: userRes
                    }
                    res.status(201).send(response);*/


                    asyncForEach(qualificationRes, function (positionitem, positionindex, positionarr) {
                        var cnt = positionindex;
                        console.log(positionitem.id);
                        var schduleid = positionitem.schdule_id;

                        var fields = " count(id) as no_of_student ";
                        var condition = " where schdule_id = '" + schduleid + "'  ";
                        var param = {

                            from: 'from mdl_student_booked_vc ',
                            condition: condition,
                            fields: fields
                        };

                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (courseRes) {
                            if (courseRes.length > 0) {
                                var no_of_student = courseRes[0].no_of_student;

                                qualificationRes[cnt].no_of_student = no_of_student;

                                cnt++;
                                questionArr.push(positionindex);
                                var questionLen = questionArr.length;
                                queaArr.push({
                                    "questionId": positionitem.id
                                })

                                if (quesLen == queaArr.length) {

                                    var response = {
                                        resCode: 200,
                                        response: qualificationRes
                                    }
                                    res.status(200).send(response);
                                }

                            }

                        });

                    })






                } else {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
                }
            }); //end of select

        }


    }


}); //e



/*
 * Used to get student f2f Trainer List
 * Created On : 18-01-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/getf2fTrainerList', customodule.verifyapicalltoken, function (req, res, next) {
    var studentId = req.body.studentId;
    var courseId = req.body.courseId;
    var unitId = req.body.unitId;
    var dbsetting = req.db_setting;


    if (!studentId || studentId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.STUDENTIDREQUIRED);
    } else if (!courseId || courseId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.COURSEIDREQUIRED);
    } else {


        var fields = " mt.id, mt.trainerid,  mt.price, mt.date, mu.firstname, mu.lastname, mu.profile_image";
        var condition = " where mt.course_id = " + courseId + " AND  mt.unit_id = " + unitId + " ";
        var join = " LEFT JOIN mdl_user as mu  ON mt.trainerid = mu.id ";
        var param = {

            from: 'from mdl_trainer_f2f_schdule mt ',
            condition: condition,
            fields: fields,
            join: join
        };

        console.log("param222", param);


        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {
            if (userRes.length > 0) {
                var response = {
                    resCode: 200,
                    response: userRes,
                    resData: userRes
                }
                res.status(201).send(response);
            } else {
                res.status(201).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
            }
        }); //end of select


    }


}); //e

/*
 * Used to get add student booked F2F
 * Created On : 18-01-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/addStudentBookedF2F', customodule.verifyapicalltoken, function (req, res, next) {

    var dbsetting = req.db_setting;
    var studentId = req.body.studentId;
    var courseId = req.body.courseId;
    var unitId = req.body.unitId;
    var trainerId = req.body.trainerId;
    var schduleId = req.body.schduleId;

    var d = new Date();
    var m1 = d.getMonth() + 1;
    var datetime = d.getFullYear() + "-" + m1 + "-" + d.getDate();

    if (!studentId || studentId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.STUDENTIDREQUIRED);
    } else if (!courseId || courseId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.COURSEIDREQUIRED);
    } else if (!trainerId || trainerId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.STUDENTIDREQUIRED);
    } else if (!schduleId || schduleId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.SCHDULEIDREQUIRED);
    } else {

        var fields = "id,price";
        var condition = " where id ='" + schduleId + "'   ";

        var param = {

            from: 'from mdl_trainer_f2f_schdule',
            condition: condition,
            fields: fields
        };

        console.log("param222", param);


        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (ansRes) {
            if (ansRes.length > 0) {

                var price = ansRes[0].price;

                var saveData = {};
                saveData.student_id = studentId;
                saveData.course_id = courseId;
                saveData.unit_id = unitId;
                saveData.trainer_id = trainerId;
                saveData.schdule_id = schduleId;
                saveData.payment_amount = price;
                saveData.payment_date = datetime;

                var param = {

                    tablename: 'mdl_student_booked_f2f',
                    data: [saveData]
                }

                Mastermodel.commonInsert(param, function (err) {
                    console.log("++++++++++++++++++++++++++++++++++++", err);
                    res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                }, function (data) {

                    var response = {
                        resCode: 200,
                        response: "F2F Schdule Booked Successfully"
                    }
                    res.status(200).send(response);
                    //res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.RPLTESTSUBMITTED);
                });

            } else {

                res.status(200).send(RESPONSEMESSAGE.COMMON.NODATAEXISTS);
            }
        }); //end of select

    } //end of else
}); //e



/*
 * Used to get student booked F2F Liist
 * Created On : 18-01-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/studentBookedF2FList', customodule.verifyapicalltoken, function (req, res, next) {
    var studentId = req.body.studentId;
    var dbsetting = req.db_setting;


    if (!studentId || studentId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.STUDENTIDREQUIRED);
    } else {


        var d = new Date();
        var month = d.getMonth() + 1;
        var datetime = d.getFullYear() + "-" + month + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();


        var fields = " msbv.*, mts.date, mts.price, mts.date,  mu.firstname, mu.lastname, mu.profile_image, mcc.name as cname, mcc.idnumber as ccode, mc.fullname as uname, mc.idnumber as ucode";
        var condition = " where msbv.student_id = " + studentId + "  ";
        condition += " AND  mts.date >= '" + datetime + "' ";
        var join = " LEFT JOIN mdl_user   as mu              ON mu.id  = msbv.trainer_id ";
        join += " LEFT JOIN mdl_course as mc              ON mc.id  = msbv.unit_id ";
        join += " LEFT JOIN mdl_course_categories as mcc  ON mcc.id = msbv.course_id ";
        join += " LEFT JOIN mdl_trainer_f2f_schdule as   mts  ON mts.id = msbv.schdule_id ";

        var orderby = "order by mts.date asc";

        var param = {

            from: 'from mdl_student_booked_f2f msbv ',
            condition: condition,
            fields: fields,
            join: join,
            orderby: orderby
        };

        console.log("param222", param);


        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {
            if (userRes.length > 0) {
                var response = {
                    resCode: 200,
                    response: userRes,
                    resData: userRes
                }
                res.status(201).send(response);
            } else {
                res.status(201).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
            }
        }); //end of select


    }


}); //e


/*
 * Used to get completed student booked F2F Log Liist
 * Created On : 18-01-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/studentBookedF2FLogList', customodule.verifyapicalltoken, function (req, res, next) {
    var studentId = req.body.studentId;
    var dbsetting = req.db_setting;


    if (!studentId || studentId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.STUDENTIDREQUIRED);
    } else {


        var d = new Date();
        var month = d.getMonth() + 1;
        var datetime = d.getFullYear() + "-" + month + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();


        var fields = " msbv.*, mts.date, mts.price, mts.date,  mu.firstname, mu.lastname, mu.profile_image, mcc.name as cname, mcc.idnumber as ccode, mc.fullname as uname, mc.idnumber as ucode";
        var condition = " where msbv.student_id = " + studentId + "  ";
        condition += " AND  mts.date < '" + datetime + "' ";
        var join = " LEFT JOIN mdl_user   as mu              ON mu.id  = msbv.trainer_id ";
        join += " LEFT JOIN mdl_course as mc              ON mc.id  = msbv.unit_id ";
        join += " LEFT JOIN mdl_course_categories as mcc  ON mcc.id = msbv.course_id ";
        join += " LEFT JOIN mdl_trainer_f2f_schdule as   mts  ON mts.id = msbv.schdule_id ";

        var orderby = "order by mts.date asc";

        var param = {

            from: 'from mdl_student_booked_f2f msbv ',
            condition: condition,
            fields: fields,
            join: join,
            orderby: orderby
        };

        console.log("param222", param);


        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {
            if (userRes.length > 0) {
                var response = {
                    resCode: 200,
                    response: userRes,
                    resData: userRes
                }
                res.status(201).send(response);
            } else {
                res.status(201).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
            }
        }); //end of select


    }


}); //e


/*
 * get user details
 * Created On : 27-10-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
router.post('/getUserDetails', function (req, res, next) {
    //console.log(req.body);
    var questionArr = [];
    var dbsetting = '';
    var userId = req.body.userId;
    /**** Used to get RPL question list ****/
    if (!userId) {
        res.status(201).send(RESPONSEMESSAGE.USER.NOUSERIDFOUND);
    } else if (isNaN(userId)) {
        res.status(201).send(RESPONSEMESSAGE.USER.INVALIDUSER);
    } else {

        var base_url = "http://52.63.96.113:4444/share/userimage/thumbs/";
        var base_url_big = "http://52.63.96.113:4444/share/userimage/";


        //IF(mu.usertype='Trainer', select avg(rating) from mdl_trainer_rating where trainer_id = mu.id, 0) as student_rating  , 

        var fields = " (select avg(rating) from mdl_trainer_rating where trainer_id = mu.id) as trainer_rating,  (select avg(rating) from mdl_student_rating where student_id = mu.id) as student_rating,  mu.id,  mu.username,  mu.firstname,  mu.lastname, mu.email, mu.phone1, mu.city, mu.country, mu.socialimageurl, mu.socialuserid, mu.regtype,  mu.isverified, mu.state, mu.address1, mu.address2, mu.zip, mu.usertype, mu.addeddate, (case when profile_image=null then profile_image else concat('" + base_url + "',profile_image) end) as profileimage ,mu.profile_image, (case when profile_image=null then profile_image else concat('" + base_url_big + "',profile_image) end) as bigprofileimage , mu.profile_video, mu.self_description, mu.date_of_birth, mu.gender, mu.istrusted, mu.learningtypeid, mu.trainingtypeid, mu.preferedgender, mu.preferedlanguage, mu.preferedtimeslot, mu.preferedgroup";
        var condition = " where 1 AND mu.id = '" + userId + "'  ";
        var param = {
            from: 'from mdl_user as mu',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (usrRes) {
            //console.log(rplRes);
            if (usrRes.length > 0) {

                var response = {
                    resCode: 200,
                    response: usrRes[0],
                }
                res.status(200).send(response);
            } else {
                res.status(201).send(RESPONSEMESSAGE.USER.USERNOTEXISTS);
            }
        });
    }

    /************* End ************************/


});




/*
 * Used to get student rpl submission log
 * Created On : 19-01-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/getStudentRPLsubmissionLog', customodule.verifyapicalltoken, function (req, res) {

    var questionArr = [];
    var student_id = req.body.studentId;

    if (!student_id) {
        res.status(200).send(RESPONSEMESSAGE.RPL.STUDENTIDREQUIRED);
    } else if (isNaN(student_id)) {
        res.status(200).send(RESPONSEMESSAGE.RPL.STUDENTIDREQUIRED);
    } else {
        /**** Used to get student entry test question list ****/
        var fields = " DISTINCT(unit_id) ";
        var condition = " where student_id = '" + student_id + "' AND final_submitted = 'Y' AND trainer_status = '1' AND is_paid = 'Yes' AND is_valid != 'N' ";
        var param = {
            from: 'from mdl_rpl_process_submission_master ',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (rplRes) {


            var quesLen = rplRes.length;
            var queaArr = new Array();

            if (rplRes.length > 0) {

                /**** Used to get student entry test answer list ****/
                var finalArr = new Array();
                asyncForEach(rplRes, function (positionitem, positionindex, positionarr) {
                    var cnt = positionindex;
                    console.log(positionitem.unit_id);
                    var unit_id = positionitem.unit_id;


                    /********************* get course id from unit id *********/
                    var fields = " category ";
                    var condition = " where id = '" + unit_id + "'  ";
                    var param = {
                        from: 'from mdl_course ',
                        condition: condition,
                        fields: fields
                    };
                    Mastermodel.commonSelect(param, function (err) {
                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                    }, function (courseRes) {
                        if (courseRes.length > 0) {
                            var course_id = courseRes[0].category;


                            var fields = " id ";
                            var condition = " where student_id = '" + student_id + "'AND course_id = '" + course_id + "' AND unit_id = '" + unit_id + "' AND final_submitted = 'Y' AND trainer_status = '1' AND is_paid = 'Yes' AND is_valid != 'N' ";
                            var orderby = " order by id desc";
                            var limit = " 0,1";
                            var param = {
                                from: 'from mdl_rpl_process_submission_master ',
                                condition: condition,
                                fields: fields,
                                orderby: orderby,
                                limit: limit
                            };

                            /*********************************************************/

                            Mastermodel.commonSelect(param, function (err) {
                                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                            }, function (submissionRes) {
                                if (submissionRes.length > 0) {
                                    var rpl_submission_id = submissionRes[0].id;
                                    /******************* Log Details **************************/

                                    var fields = " mt.* , mcc.name as cname, mcc.idnumber as ccode, mc.fullname as uname, mc.idnumber as ucode, mu.firstname, mu.lastname  ";
                                    var condition = " where mt.id = '" + rpl_submission_id + "'   ";
                                    var join = " LEFT JOIN mdl_course_categories mcc ON mt.course_id = mcc.id ";
                                    join += " LEFT JOIN mdl_course            mc  ON mt.unit_id   = mc.id ";
                                    join += " LEFT JOIN mdl_user              mu  ON mt.trainer_id   = mu.id ";
                                    var param = {
                                        from: 'from mdl_rpl_process_submission_master mt  ',
                                        condition: condition,
                                        fields: fields,
                                        join: join
                                    };
                                    Mastermodel.commonSelect(param, function (err) {
                                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                                    }, function (submissionRes) {

                                        finalArr[cnt] = submissionRes[0];
                                        cnt++;
                                        questionArr.push(positionindex);
                                        var questionLen = questionArr.length;

                                        queaArr.push({
                                            "questionId": positionitem.id
                                        });


                                        console.log(quesLen, queaArr.length);
                                        console.log("finalArr", finalArr);
                                        if (quesLen == queaArr.length) {



                                            var response = {
                                                resCode: 200,
                                                response: finalArr
                                            };
                                            res.status(200).send(response);
                                        }
                                    });
                                    /******************* Log Details **************************/
                                }
                            });
                            /*********************************************************/
                        }
                    });
                    /************************** Get RPL details ************************/
                })

            } else {
                res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.NORECORDSFOUND);
            }
        });
    }
});




/*
 * Used to get student rpl submission log details
 * Created On : 19-01-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
/*
router.post('/getStudentRPLsubmissionLogDetails', customodule.verifyapicalltoken, function(req, res, next) {
    //console.log(req.body);
    var questionArr = [];
    var dbsetting = req.db_setting;
    var course_id = req.body.courseId;
    var unit_id = req.body.unitId;
    var student_id = req.body.studentId;

    if (!course_id || course_id == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.COURSEIDREQUIRED);
    } else if (isNaN(course_id)) {
        res.status(201).send(RESPONSEMESSAGE.RPL.COURSEIDREQUIRED);
    } else if (!unit_id || unit_id == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.UNITIDREQUIRED);
    } else if (isNaN(unit_id)) {
        res.status(201).send(RESPONSEMESSAGE.RPL.UNITIDREQUIRED);
    } else {
        /**** Used to get student entry test question list ****
        var fields = " id ";
        var condition = " where course_id = '" + course_id + "' AND  unit_id = '" + unit_id + "' AND  student_id = '" + student_id + "' AND is_valid != 'N' ";
        var orderby = " order by attempt asc ";
        var param = {

            from: 'from mdl_rpl_process_submission_master ',
            condition: condition,
            fields: fields,
            orderby: orderby
        };
        Mastermodel.commonSelect(param, function(err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function(rplRes) {

            var quesLen = rplRes.length;
            var queaArr = new Array();

            /**** Used to get student entry test answer list ****

            if (rplRes.length > 0) {

                var finalArr = new Array();
                var i = 0;
                asyncForEach(rplRes, function(positionitem, positionindex, positionarr) {
                    var cnt = positionindex;
                    console.log(positionitem.id);
                    var rplsubmissionid = positionitem.id;
                    //i++;
                    //finalArr.push(i);



                    /********************* get course id from unit id *********
                    var fields = " attempt ";
                    var condition = " where id = '" + rplsubmissionid + "'  ";
                    var param = {

                        from: 'from mdl_rpl_process_submission_master ',
                        condition: condition,
                        fields: fields
                    };
                    Mastermodel.commonSelect(param, function(err) {
                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                    }, function(courseRes) {
                        if (courseRes.length > 0) {

                            var localvar = {};

                            console.log("attempt", courseRes[0].attempt);
                            console.log("cnt", cnt);


                            var attempt = courseRes[0].attempt;
                            localvar.attempt = attempt;

                            // console.log(finalArr[cnt]);


                            var fields = " mq.* , q.question  ";
                            var condition = " where mq.masterid   = '" + rplsubmissionid + "' ";
                            var join = " LEFT JOIN mdl_rpl_question as q ON q.id = mq.question_id  ";
                            var orderby = " order by id desc";
                            var limit = " 0,1";
                            var param = {

                                from: 'from mdl_rpl_process_question_evidence mq ',
                                condition: condition,
                                fields: fields,
                                join: join
                            };
                            Mastermodel.commonSelect(param, function(err) {
                                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                            }, function(submissionRes) {


                                /*
                                localvar.details = submissionRes;
                                finalArr.push(localvar);
                                cnt++;
                                questionArr.push(positionindex);
                                var questionLen = questionArr.length;

                                queaArr.push({
                                    "questionId": positionitem.id
                                })

                                console.log(quesLen, queaArr.length);
                                if (quesLen == queaArr.length) {

                                    var response = {
                                        resCode: 200,
                                        response: finalArr
                                    }
                                    res.status(200).send(response);
                                }


                              finalArr[cnt] = submissionRes;


                                    /***** additional file  ******
                                    var fields = " mf.* ";
                                    var condition = " where mf.question_id = '" + positionitem.id + "' AND masterid = '" + rplId + "' ";
                                    var param = {

                                        from: 'from mdl_rpl_question_additional_file as mf',
                                        condition: condition,
                                        fields: fields
                                    };
                                    Mastermodel.commonSelect(param, function(err) {
                                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                                    }, function(additionalRes) {
                                        entrytestRes[cnt].additionalFiles = additionalRes;
                                        cnt++;
                                        questionArr.push(positionindex);
                                        var questionLen = questionArr.length;
                                        console.log(quesLen, questionLen);





                                        if (entryTestquestionLen == questionLen) {
                                            var response = {
                                                resCode: 200,
                                                response: finalArr
                                            }
                                            res.status(200).send(response);
                                        }
                                    });
                                    /***** additional file  ******



                            });
                            /******************* Log Details **************************


                        }
                    });



                    /************************** Get RPL details ************************
                })
            } else {
                res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.NORECORDSFOUND);
            }




        });
        /*****************************************

    }

});*/




/*
 * Used to get student rpl submission log details
 * Created On : 19-01-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/getStudentRPLsubmissionLogDetails', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    var questionArr = [];
    var dbsetting = req.db_setting;
    var course_id = req.body.courseId;
    var unit_id = req.body.unitId;
    var student_id = req.body.studentId;

    if (!course_id || course_id == 0) {
        res.status(200).send(RESPONSEMESSAGE.RPL.COURSEIDREQUIRED);
    } else if (isNaN(course_id)) {
        res.status(200).send(RESPONSEMESSAGE.RPL.COURSEIDREQUIRED);
    } else if (!unit_id || unit_id == 0) {
        res.status(200).send(RESPONSEMESSAGE.RPL.UNITIDREQUIRED);
    } else if (isNaN(unit_id)) {
        res.status(200).send(RESPONSEMESSAGE.RPL.UNITIDREQUIRED);
    } else {
        /**** Used to get student entry test question list ****/
        var fields = " id ";
        var condition = " where course_id = '" + course_id + "' AND  unit_id = '" + unit_id + "' AND  student_id = '" + student_id + "' AND is_valid != 'N' ";
        var orderby = " order by attempt asc ";
        var param = {

            from: 'from mdl_rpl_process_submission_master ',
            condition: condition,
            fields: fields,
            orderby: orderby
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (rplRes) {

            var quesLen = rplRes.length;
            var queaArr = new Array();

            /**** Used to get student entry test answer list ****/

            if (rplRes.length > 0) {

                var finalArr = new Array();
                var i = 0;
                asyncForEach(rplRes, function (positionitem, positionindex, positionarr) {
                    var cnt = positionindex;
                    console.log(positionitem.id);
                    var rplsubmissionid = positionitem.id;
                    //i++;
                    //finalArr.push(i);



                    /********************* get course id from unit id *********/
                    var fields = " attempt ";
                    var condition = " where id = '" + rplsubmissionid + "'  ";
                    var param = {

                        from: 'from mdl_rpl_process_submission_master ',
                        condition: condition,
                        fields: fields
                    };
                    Mastermodel.commonSelect(param, function (err) {
                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                    }, function (courseRes) {
                        if (courseRes.length > 0) {

                            var localvar = {};

                            console.log("attempt", courseRes[0].attempt);
                            console.log("cnt", cnt);


                            var attempt = courseRes[0].attempt;
                            localvar.attempt = attempt;

                            // console.log(finalArr[cnt]);


                            var fields = " mq.* , q.question  ";
                            var condition = " where mq.masterid   = '" + rplsubmissionid + "' ";
                            var join = " LEFT JOIN mdl_rpl_question as q ON q.id = mq.question_id  ";
                            var orderby = " order by id desc";
                            var limit = " 0,1";
                            var param = {

                                from: 'from mdl_rpl_process_question_evidence mq ',
                                condition: condition,
                                fields: fields,
                                join: join
                            };
                            Mastermodel.commonSelect(param, function (err) {
                                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                            }, function (submissionRes) {

                                localvar.details = submissionRes;
                                finalArr.push(localvar);



                                cnt++;
                                questionArr.push(positionindex);
                                var questionLen = questionArr.length;

                                queaArr.push({
                                    "questionId": positionitem.id
                                })

                                console.log(quesLen, queaArr.length);
                                if (quesLen == queaArr.length) {

                                    var response = {
                                        resCode: 200,
                                        response: finalArr
                                    }
                                    res.status(200).send(response);
                                }
                            });
                            /******************* Log Details **************************/


                        }
                    });



                    /************************** Get RPL details ************************/
                })
            } else {
                res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NORECORDSFOUND);
            }




        });
        /*****************************************/

    }

});



/*
 * Used to get gap Traning add payment
 * Created On : 29-01-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/gapTraningMakePaymentPath', customodule.verifyapicalltoken, function (req, res, next) {

    var dbsetting = req.db_setting;
    var studentId = req.body.studentId;
    var courseId = req.body.courseId;
    var unitId = req.body.unitId;
    var trainerId = req.body.trainerId;
    var rpl_process_submission_id = req.body.rpl_process_submission_id;
    var tp = req.body.tp;
    var quid = req.body.quid;

    var d = new Date();
    var month = d.getMonth() + 1;
    var datetime = d.getFullYear() + "-" + month + "-" + d.getDate();

    if (!studentId || studentId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.STUDENTREQUIRED);
    } else if (isNaN(studentId)) {
        res.status(201).send(RESPONSEMESSAGE.RPL.STUDENTREQUIRED);
    } else if (!courseId || courseId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.COURSEIDREQUIRED);
    } else if (isNaN(courseId)) {
        res.status(201).send(RESPONSEMESSAGE.RPL.COURSEIDREQUIRED);
    } else if (!trainerId || trainerId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.TRAINERREQUIRED);
    } else if (isNaN(trainerId)) {
        res.status(201).send(RESPONSEMESSAGE.RPL.TRAINERREQUIRED);
    } else if (!unitId || unitId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.UNITIDREQUIRED);
    } else if (isNaN(unitId)) {
        res.status(201).send(RESPONSEMESSAGE.RPL.UNITIDREQUIRED);
    } else {

        var fields = "id";
        var condition = " where student_id ='" + studentId + "'  AND  course_id ='" + courseId + "' AND  unit_id ='" + unitId + "'   AND  rpl_process_submission_id ='" + rpl_process_submission_id + "' ";

        var param = {

            from: 'from mdl_gap_traning_payment',
            condition: condition,
            fields: fields
        };

        console.log("param222", param);


        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (ansRes) {
            if (ansRes.length > 0) {

                var response = {
                    resCode: 201,
                    response: "You have already select gap traning for this unit."
                }
                res.status(201).send(response);

            } else {

                //var price  = ansRes[0].price;

                var saveData = {};
                saveData.student_id = studentId;
                saveData.course_id = courseId;
                saveData.unit_id = unitId;
                saveData.trainer_id = trainerId;
                saveData.rpl_process_submission_id = rpl_process_submission_id;
                saveData.payment_id = 0;
                saveData.total_price = tp;
                saveData.qid = quid;
                saveData.payment_date = datetime;

                var param = {

                    tablename: 'mdl_gap_traning_payment',
                    data: [saveData]
                }

                Mastermodel.commonInsert(param, function (err) {
                    console.log("++++++++++++++++++++++++++++++++++++", err);
                    res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                }, function (data) {

                    var response = {
                        resCode: 200,
                        response: "Gap Traning Payment Successfully Done."
                    }
                    res.status(200).send(response);
                    //res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.RPLTESTSUBMITTED);
                });
            }
        }); //end of select

    } //end of else
}); //e


// POST /charge
router.post('/gapcharge', customodule.verifyapicalltoken, function (req, res, next) {

    var quid = req.body.quid;
    if (!quid) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.NOQUID);
    } else {

        var fields = " sum(price) as amount ";
        var condition = " where id in ( " + quid + " )  ";
        var param = {

            from: 'from mdl_rpl_question',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (quesRes) {

            var amount = quesRes[0].amount;
            if (amount > 0) {
                charge(req, amount).then(data => {
                    var dbsetting = req.db_setting;


                    var transaction_id = data.id;
                    var transaction_amount = data.amount;
                    var transaction_date = data.created;
                    var transaction_currency = data.currency;
                    var description = data.description;
                    var payer_id = data.source.id;
                    var payer_name = data.source.name;


                    var studentId = req.body.studentId;
                    var courseId = req.body.course_id;
                    var unitId = req.body.unit_id;
                    var trainerId = req.body.trainer_id;
                    var rpl_process_submission_id = req.body.rpl_process_submission_id;
                    var learning_id = req.body.learning_id;
                    var quid = req.body.quid;



                    var d = new Date();
                    var month = d.getMonth() + 1;
                    var datetime = d.getFullYear() + "-" + month + "-" + d.getDate();

                    if (!studentId || studentId == 0) {
                        res.status(201).send(RESPONSEMESSAGE.RPL.STUDENTREQUIRED);
                    } else if (isNaN(studentId)) {
                        res.status(201).send(RESPONSEMESSAGE.RPL.STUDENTREQUIRED);
                    } else if (!courseId || courseId == 0) {
                        res.status(201).send(RESPONSEMESSAGE.RPL.COURSEIDREQUIRED);
                    } else if (isNaN(courseId)) {
                        res.status(201).send(RESPONSEMESSAGE.RPL.COURSEIDREQUIRED);
                    } else if (!trainerId || trainerId == 0) {
                        res.status(201).send(RESPONSEMESSAGE.RPL.TRAINERREQUIRED);
                    } else if (isNaN(trainerId)) {
                        res.status(201).send(RESPONSEMESSAGE.RPL.TRAINERREQUIRED);
                    } else if (!unitId || unitId == 0) {
                        res.status(201).send(RESPONSEMESSAGE.RPL.UNITIDREQUIRED);
                    } else if (isNaN(unitId)) {
                        res.status(201).send(RESPONSEMESSAGE.RPL.UNITIDREQUIRED);
                    } else {

                        var fields = "id";
                        var condition = " where student_id ='" + studentId + "'  AND  course_id ='" + courseId + "' AND  unit_id ='" + unitId + "'   AND  rpl_process_submission_id ='" + rpl_process_submission_id + "' ";
                        var param = {
                            from: 'from mdl_gap_traning_payment',
                            condition: condition,
                            fields: fields
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (ansRes) {
                            if (ansRes.length > 0) {
                                var response = {
                                    resCode: 201,
                                    response: "You have already select gap traning for this unit."
                                }
                                res.status(201).send(response);
                            } else {
                                var fields = " email ";
                                var condition = " where id = '" + trainerId + "' ";
                                var param = {
                                    from: 'from mdl_user ',
                                    condition: condition,
                                    fields: fields
                                };
                                Mastermodel.commonSelect(param, function (err) {
                                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                                }, function (userRes) {


                                    payer_name = userRes[0].email;

                                    var d = new Date();
                                    var month = d.getMonth() + 1;
                                    var datetime = d.getFullYear() + "-" + month + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
                                    var saveData = {};
                                    saveData.transaction_id = transaction_id;
                                    saveData.amount = transaction_amount;
                                    saveData.date = datetime;
                                    saveData.currency = transaction_currency;
                                    saveData.description = description;
                                    saveData.payer_id = payer_id;
                                    saveData.payer_name = payer_name;
                                    saveData.student_id = studentId;
                                    saveData.learning_mode_type_id = 5;
                                    saveData.trainer_id = trainerId;
                                    saveData.course_id = courseId;
                                    saveData.unit_id = unitId;
                                    //saveData.stripe_details   = data;
                                    var param = {
                                        tablename: 'mdl_payment_details',
                                        data: [saveData]
                                    }
                                    Mastermodel.commonInsert(param, function (err) {
                                        console.log("++++++++++++++++++++++++++++++++++++", err);
                                        res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                                    }, function (data) {
                                        var paymentId = data.insertId;

                                        var saveData = {};
                                        saveData.student_id = studentId;
                                        saveData.course_id = courseId;
                                        saveData.unit_id = unitId;
                                        saveData.trainer_id = trainerId;
                                        saveData.rpl_process_submission_id = rpl_process_submission_id;
                                        saveData.payment_id = paymentId;
                                        saveData.total_price = transaction_amount;
                                        saveData.qid = quid;
                                        saveData.payment_date = datetime;

                                        var param = {

                                            tablename: 'mdl_gap_traning_payment',
                                            data: [saveData]
                                        }

                                        Mastermodel.commonInsert(param, function (err) {
                                            console.log("++++++++++++++++++++++++++++++++++++", err);
                                            res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                                        }, function (data) {

                                            var response = {
                                                resCode: 200,
                                                response: "Gap Traning Payment Successfully Done."
                                            }
                                            res.status(200).send(response);
                                        });
                                    });

                                });



                            }
                        }); //end of select

                    } //end of else

                }).catch(error => {
                    var stripeErrObj = {};
                    stripeErrObj.resCode = 201;
                    stripeErrObj.response = error.message;
                    res.status(201).send(stripeErrObj);
                    //res.render('error', error);
                });
            } else {
                res.status(201).send(RESPONSEMESSAGE.COMMON.NOPRICE);
            }
        });
    }

});




/*
 * Used to get gap Traning list for student
 * Created On : 29-01-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/studentGapTraningList', customodule.verifyapicalltoken, function (req, res, next) {
    var studentId = req.body.studentId;
    var dbsetting = req.db_setting;
    var searchText = req.body.searchText;

    var fields = " mp.*, mu.firstname, mu.lastname, mc.fullname as uname, mc.idnumber as ucode, mcc.name as cname, mcc.idnumber as ccode ";
    var condition = " where student_id = '" + studentId + "' ";
    /* if (searchText != "" || searchText != undefined || searchText != 'undefined'  || searchText != null )
         condition += " AND ( mcc.name LIKE '%" + searchText + "%' OR  mcc.idnumber LIKE '%" + searchText + "%' ) ";*/



    var join = " LEFT JOIN mdl_user   as mu              ON mp.trainer_id = mu.id ";
    join += " LEFT JOIN mdl_course as mc              ON mp.unit_id    = mc.id ";
    join += " LEFT JOIN mdl_course_categories as mcc  ON mp.course_id  = mcc.id ";
    var param = {

        from: 'from mdl_gap_traning_payment mp ',
        condition: condition,
        fields: fields,
        join: join
    };

    console.log("param222", param);


    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (userRes) {
        if (userRes.length > 0) {
            var response = {
                resCode: 200,
                response: userRes,
                resData: userRes
            }
            res.status(201).send(response);
        } else {
            res.status(201).send(RESPONSEMESSAGE.RPL.NORECORDSFOUND);
        }
    }); //end of select


}); //e
/*
 * Used to get RPL entry test question  with download materia;
 * Created On : 31-01-2018
 * Created By : Monalisa
 * Input      : courseId(int)
 * Modified On
 */
router.post('/getStudentGapDownloadMaterial', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    var questionArr = [];
    var dbsetting = req.db_setting;
    var rplresubmissionId = req.body.rplresubmissionId;
    var id = req.body.id;
    if (!rplresubmissionId) {
        res.status(201).send(RESPONSEMESSAGE.RPL.IDREQUIRED);
    } else if (!id) {
        res.status(201).send(RESPONSEMESSAGE.RPL.IDREQUIRED);
    } else {
        /**** Used to get student RPL test question list ****/

        var fields = " qid ";
        var condition = " where id = '" + id + "'  ";
        var param = {

            from: 'from mdl_gap_traning_payment',
            condition: condition,
            fields: fields
        };

        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (qRes) {

            if (qRes.length > 0) {
                var str = qRes[0].qid;
                var qidstring = str.split("*");

                /*var fields  = " mrpqe.question_id, mrq.id, mrq.question, mrq.question_type , mrq.price ";
                var condition = " where mrpqe.masterid = '" + rplresubmissionId + "' AND is_accepted = 'N' AND is_action_taken = 'Y' ";
                var join      = " LEFT JOIN mdl_rpl_question mrq  ON mrpqe.question_id = mrq.id   ";
                var param     = {

                    from     : 'from mdl_rpl_process_question_evidence as mrpqe',
                    condition: condition,
                    fields   : fields,
                    join     : join
                };*/

                var fields = "mrq.id, mrq.id as question_id , mrq.question, mrq.question_type , mrq.price ";
                var condition = "where mrq.id in ( " + qidstring + " ) ";
                var param = {

                    from: 'from mdl_rpl_question as mrq',
                    condition: condition,
                    fields: fields
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (entrytestRes) {
                    console.log(entrytestRes);
                    var entryTestquestionLen = entrytestRes.length;
                    if (entrytestRes.length > 0) {

                        /**** Used to get student gap traning documents ****/
                        asyncForEach(entrytestRes, function (positionitem, positionindex, positionarr) {
                            var cnt = positionindex;
                            console.log(positionitem.id);
                            var fields = " met.* ";
                            var condition = " where met.qid = '" + positionitem.id + "' ";
                            var param = {

                                from: 'from mdl_gap_traning_documents as met',
                                condition: condition,
                                fields: fields
                            };
                            // entrytestRes[positionindex].hii="12345" ;
                            Mastermodel.commonSelect(param, function (err) {
                                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                            }, function (ansRes) {
                                questionArr.push(positionindex);
                                var questionLen = questionArr.length;
                                // console.log(entrytestRes[positionindex]);
                                //  console.log(entrytestRes);
                                entrytestRes[cnt].material = ansRes;
                                cnt++;
                                console.log(entryTestquestionLen, questionLen);
                                if (entryTestquestionLen == questionLen) {

                                    var response = {
                                        resCode: 200,
                                        response: entrytestRes
                                    }
                                    res.status(200).send(response);
                                }
                            })
                        });

                    } else {
                        res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NORECORDSFOUND);
                    }
                });




            } else {
                res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NORECORDSFOUND);
            }

        });

    }
});


/*
 * Used to get dynamic learning style data;
 * Created On : 02-02-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/getDynamicStudentLearningStyle', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    var quizJSON = {};
    var questionArr = [];
    var dbsetting = req.db_setting;

    var fields = "mlsq.id, mlsq.question as q ";
    var condition = "where 1 ";
    var param = {

        from: 'from mdl_learning_style_question as mlsq',
        condition: condition,
        fields: fields
    };
    quizJSON.info = {
        "name": "Test Your Knowledge!!",
        "main": "<p class='mb-15'>Think you're smart enough to be on Jeopardy? Find out with this super crazy knowledge quiz!</p>",
        "results": "<h5>Learn More</h5><p>If you are an auditory learner, you learn by hearing and listening. You understand and remember things you have heard. You store information by the way it sounds, and you have an easier time understanding spoken instructions than written ones. You often learn by reading out loud because you have to hear it or speak it in order to know it.</p><p>As an auditory learner, you probably hum or talk to yourself or others if you become bored. People may think you are not paying attention, even though you may be hearing and understanding everything being said.</p>",
        "level1": "Jeopardy Ready",
        "level2": "Jeopardy Contender",
        "level3": "Jeopardy Amateur",
        "level4": "Jeopardy Newb",
        "level5": "Stay in school, kid..."
    }
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (entrytestRes) {
        console.log(entrytestRes);
        var entryTestquestionLen = entrytestRes.length;
        if (entrytestRes.length > 0) {

            /**** Used to get student gap traning documents ****/
            asyncForEach(entrytestRes, function (positionitem, positionindex, positionarr) {
                var cnt = positionindex;
                console.log(positionitem.id);
                var fields = " mlsd.id, mlsd.type as value, (case when mlsd.option_file=null then mlsd.option_file else concat('" + SETTING.learning_style_image_url + "',mlsd.option_file) end) as `option` ";
                var condition = " where mlsd.questionid = '" + positionitem.id + "' ";
                var param = {

                    from: 'from mdl_learning_style_details as mlsd',
                    condition: condition,
                    fields: fields
                };
                // entrytestRes[positionindex].hii="12345" ;
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (ansRes) {
                    questionArr.push(positionindex);
                    var questionLen = questionArr.length;
                    // console.log(entrytestRes[positionindex]);
                    //  console.log(entrytestRes);
                    entrytestRes[cnt].a = ansRes;
                    cnt++;
                    console.log(entryTestquestionLen, questionLen);
                    if (entryTestquestionLen == questionLen) {
                        quizJSON.questions = entrytestRes;
                        var response = {
                            resCode: 200,
                            quizJSON: quizJSON
                        }
                        res.status(200).send(response);
                    }
                })
            });

        } else {
            res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NORECORDSFOUND);
        }
    });



});


// POST /charge
router.post('/charge', customodule.verifyapicalltoken, function (req, res, next) {

    var unitId = req.body.unit_id;
    if (!unitId) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.NOUNIT);
    } else if (isNaN(unitId)) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.NOUNIT);
    } else {


        var fields = " uprice ";
        var condition = " where id = '" + req.body.unit_id + "'  ";
        var param = {
            from: 'from mdl_course',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (unitRes) {

            var amount = unitRes[0].uprice;

            console.log("amount", amount);
            if (amount > 0) {

                charge(req, amount).then(data => {


                    console.log(data);
                    // return false;


                    var dbsetting = req.db_setting;
                    var transaction_id = data.id;
                    var transaction_amount = data.amount;
                    var transaction_date = data.created;
                    var transaction_currency = data.currency;
                    var description = data.description;
                    var payer_id = data.source.id;
                    var payer_name = data.source.name;

                    var rplprocesssubmissionid = req.body.rplprocesssubmissionid;
                    var course_id = req.body.course_id;
                    var studentId = req.body.studentId;
                    var unit_id = req.body.unit_id;

                    if (!rplprocesssubmissionid) {
                        res.status(201).send(RESPONSEMESSAGE.COMMONALL.IDREQUIRED);
                    } else {
                        var fields = "mr.id, mr.payment_id, mr.trainer_id, ms.email";
                        var condition = " where mr.id ='" + rplprocesssubmissionid + "'";
                        var join = " LEFT JOIN mdl_user ms ON mr.trainer_id  = ms.id";
                        //console.log("inside sql" + rplprocesssubmissionid);
                        var param = {

                            from: 'from mdl_rpl_process_submission_master mr',
                            condition: condition,
                            fields: fields,
                            join: join
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (ansRes) {
                            if (ansRes.length == 0) {
                                res.status(201).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
                            } else {
                                var trainer_id = ansRes[0].trainer_id;
                                payer_name = ansRes[0].email;

                                /*if (payer_name == "" || payer_name == null)
                                    payer_name = trainer_email;*/

                                var d = new Date();
                                var month = d.getMonth() + 1;
                                var datetime = d.getFullYear() + "-" + month + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
                                var saveData = {};
                                saveData.transaction_id = transaction_id;
                                saveData.amount = transaction_amount;
                                saveData.date = datetime;
                                saveData.currency = transaction_currency;
                                saveData.description = description;
                                saveData.payer_id = payer_id;
                                saveData.payer_name = payer_name;
                                saveData.student_id = studentId;
                                saveData.trainer_id = trainer_id;
                                saveData.type = 'rpl';
                                saveData.learning_mode_type_id = 1;
                                saveData.course_id = course_id;
                                saveData.unit_id = unit_id;
                                var param = {

                                    tablename: 'mdl_payment_details',
                                    data: [saveData]
                                }
                                Mastermodel.commonInsert(param, function (err) {
                                    res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                                }, function (data) {
                                    var paymentId = data.insertId;
                                    var dataupdate = {
                                        is_paid: 'Yes',
                                        paid_amount: transaction_amount,
                                        payment_id: paymentId
                                    }
                                    var updatecondition = {
                                        id: rplprocesssubmissionid
                                    }
                                    var param3 = {

                                        tablename: 'mdl_rpl_process_submission_master',
                                        condition: updatecondition,
                                        data: dataupdate
                                    }
                                    Mastermodel.commonUpdate(param3, function (error) {
                                        res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
                                    }, function (data) {
                                        var notificationparam = {};
                                        notificationparam.userId = trainer_id;
                                        notificationparam.courseId = course_id;
                                        notificationparam.unitId = unit_id;
                                        notificationparam.userType = 'Trainer';
                                        notificationparam.pushNotificationCode = NOTIFICATIONMESSAGE.TRAINER.PAYMENTRECEIVED;
                                        Masternotificationmodel.sendPushNotification(notificationparam, function (err) {
                                            var response = {
                                                resCode: 201,
                                                response: error
                                            }
                                            res.status(200).send(response);
                                        }, function (succNotificationRes) {
                                            var response = {
                                                resCode: 200,
                                                resMessage: "Trainer selected successfully",
                                                response: "Trainer selected successfully"
                                            }
                                            res.status(200).send(RESPONSEMESSAGE.RPL.PAYMENTSUCCESS);
                                        });
                                    });

                                });
                            }
                        }); //end of select

                    } //end of else
                }).catch(error => {
                    var stripeErrObj = {};
                    stripeErrObj.resCode = 201;
                    stripeErrObj.response = error.message;
                    res.status(201).send(stripeErrObj);
                    //res.render('error', error);
                });

            } else {

                res.status(201).send(RESPONSEMESSAGE.COMMON.NOPRICE);
            }

        });

    }

});
/*
 * Used to get student Payment List
 * Created On : 6-02-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/studentPaymentList', customodule.verifyapicalltoken, function (req, res, next) {
    var studentId = req.body.studentId;
    var dbsetting = req.db_setting;
    var fromDate = req.body.fromDate;
    var toDate = req.body.toDate;
    var searchCourseName = req.body.searchCourseName;
    var searchTrainerName = req.body.searchTrainerName;
    var learningMode = req.body.learningMode;


    if (node_module.validator.matches(searchCourseName, SETTING.specialcharpattern) || node_module.validator.matches(searchTrainerName, SETTING.specialcharpattern)) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.INVALIDREQUESTSPECIALCHAR);
    } else {

        var fields = " mp.*, ml1.name as learning_mode,  mp.course_id,  mp.unit_id,  mp.trainer_id,   mr.id as rplid,mr.is_actiontaken,  mc.fullname as uname,  mc.idnumber as ucode,  mcc.name as cname,  mcc.idnumber as ccode, mu.firstname, mu.lastname , ml.id as refund_id, ml.is_action, ml.is_approved ";
        var condition = " where mp.student_id = '" + studentId + "' ";
        if (searchCourseName != "")
            condition += " AND ( mcc.name LIKE '%" + searchCourseName + "%' OR  mcc.idnumber LIKE '%" + searchCourseName + "%' ) ";
        if (searchTrainerName != "")
            condition += " AND ( mu.firstname LIKE '%" + searchTrainerName + "%' OR  mu.lastname LIKE '%" + searchTrainerName + "%' ) ";
        if (learningMode != "")
            condition += " AND ( mp.learning_mode_type_id = '" + learningMode + "'  ) ";
        if (fromDate != "" && (toDate == "" || toDate == undefined))
            condition += " AND  mp.date >= '" + fromDate + "' ";
        if (toDate != "" && (fromDate == "" || fromDate == undefined))
            condition += " AND  mp.date <= '" + toDate + "' ";
        if (fromDate != "" && toDate != "")
            condition += " AND  mp.date BETWEEN '" + fromDate + "' AND '" + toDate + "'   ";


        var join = "  LEFT JOIN mdl_rpl_process_submission_master as mr ON mp.id = mr.payment_id ";
        join += " LEFT JOIN mdl_course as mc               ON mp.unit_id    = mc.id ";
        join += " LEFT JOIN mdl_course_categories as mcc   ON mp.course_id  = mcc.id ";
        join += " LEFT JOIN mdl_user as mu                 ON mp.trainer_id = mu.id ";
        join += " LEFT JOIN mdl_refund_request_log as ml   ON mp.id = ml.payment_id ";
        join += " LEFT JOIN mdl_learning_mode_type as ml1  ON mp.learning_mode_type_id = ml1.id ";
        var param = {
            from: 'from mdl_payment_details mp ',
            condition: condition,
            fields: fields,
            join: join
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {
            if (userRes.length > 0) {
                var response = {
                    resCode: 200,
                    response: userRes
                }
                res.status(200).send(response);
            } else {
                res.status(201).send(RESPONSEMESSAGE.RPL.NORECORDSFOUND);
            }
        }); //end of select

    }


}); //e



/*
 * Used to get trainer Payment List
 * Created On : 6-02-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/trainerPaymentList', customodule.verifyapicalltoken, function (req, res, next) {
    var trainerId = req.body.trainerId;
    var dbsetting = req.db_setting;


    var fromDate = req.body.fromDate;
    var toDate = req.body.toDate;
    var searchCourseName = req.body.searchCourseName;
    var searchStudentName = req.body.searchStudentName;
    var learningMode = req.body.learningMode;


    var fields = " mp.*,  ml1.name as learning_mode, mp.course_id,  mp.unit_id,  mp.trainer_id,  mr.id as rplid,  mc.fullname as uname,  mc.idnumber as ucode,  mcc.name as cname,  mcc.idnumber as ccode, mu.firstname, mu.lastname , ml.id as refund_id, ml.is_action, ml.is_approved, ml.trainer_comment ";
    var condition = " where mp.trainer_id = '" + trainerId + "' ";


    if (searchCourseName != "")
        condition += " AND ( mcc.name LIKE '%" + searchCourseName + "%' OR  mcc.idnumber LIKE '%" + searchCourseName + "%' ) ";
    if (searchStudentName != "")
        condition += " AND ( mu.firstname LIKE '%" + searchStudentName + "%' OR  mu.lastname LIKE '%" + searchStudentName + "%' ) ";
    if (learningMode != "")
        condition += " AND ( mp.learning_mode_type_id = '" + learningMode + "'  ) ";
    if (fromDate != "" && (toDate == "" || toDate == undefined))
        condition += " AND  mp.date >= '" + fromDate + "' ";
    if (toDate != "" && (fromDate == "" || fromDate == undefined))
        condition += " AND  mp.date <= '" + toDate + "' ";
    if (fromDate != "" && toDate != "")
        condition += " AND  mp.date BETWEEN '" + fromDate + "' AND '" + toDate + "'   ";


    var join = " LEFT JOIN mdl_rpl_process_submission_master as mr ON mp.id = mr.payment_id ";
    join += " LEFT JOIN mdl_course as mc              ON mp.unit_id    = mc.id ";
    join += " LEFT JOIN mdl_course_categories as mcc  ON mp.course_id  = mcc.id ";
    join += " LEFT JOIN mdl_user as mu                ON mp.student_id = mu.id ";
    join += " LEFT JOIN mdl_refund_request_log as ml  ON mp.id = ml.payment_id ";
    join += " LEFT JOIN mdl_learning_mode_type as ml1 ON mp.learning_mode_type_id = ml1.id ";

    var param = {

        from: 'from mdl_payment_details mp ',
        condition: condition,
        fields: fields,
        join: join
    };


    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (userRes) {
        if (userRes.length > 0) {
            var response = {
                resCode: 200,
                response: userRes
            }
            res.status(200).send(response);
        } else {
            res.status(201).send(RESPONSEMESSAGE.RPL.NORECORDSFOUND);
        }
    }); //end of select


}); //e


/*
 * Used to get student refund List
 * Created On : 19-02-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/studentRefundList', customodule.verifyapicalltoken, function (req, res, next) {
    var studentId = req.body.studentId;
    var dbsetting = req.db_setting;

    var fromDate = req.body.fromDate;
    var toDate = req.body.toDate;
    var searchCourseName = req.body.searchCourseName;
    var searchTrainerName = req.body.searchTrainerName;
    var learningMode = req.body.learningMode;

    if (node_module.validator.matches(searchCourseName, SETTING.specialcharpattern) || node_module.validator.matches(searchTrainerName, SETTING.specialcharpattern)) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.INVALIDREQUESTSPECIALCHAR);
    } else {



        var fields = " mpd.*,  ml1.name as learning_mode, mp.transaction_id, mp.amount, mp.date, mp.currency, mp.date as payment_date,   mp.course_id,  mp.unit_id,  mp.trainer_id,   mpd.is_action as trainer_action, mpd.is_approved as trainer_approved, mr.id as rplid,  mc.fullname as uname,  mc.idnumber as ucode,  mcc.name as cname,  mcc.idnumber as ccode, mu.firstname, mu.lastname ";
        var condition = " where mpd.student_id = '" + studentId + "' ";
        if (searchCourseName != "")
            condition += " AND ( mcc.name LIKE '%" + searchCourseName + "%' OR  mcc.idnumber LIKE '%" + searchCourseName + "%' ) ";
        if (searchTrainerName != "")
            condition += " AND ( mu.firstname LIKE '%" + searchTrainerName + "%' OR  mu.lastname LIKE '%" + searchTrainerName + "%' ) ";
        if (learningMode != "")
            condition += " AND ( mp.learning_mode_type_id = '" + learningMode + "'  ) ";

        if (fromDate != "" && (toDate == "" || toDate == undefined))
            condition += " AND  mpd.date >= '" + fromDate + "' ";
        if (toDate != "" && (fromDate == "" || fromDate == undefined))
            condition += " AND  mpd.date <= '" + toDate + "' ";
        if (fromDate != "" && toDate != "")
            condition += " AND  mpd.date BETWEEN '" + fromDate + "' AND '" + toDate + "'   ";

        var join = " JOIN mdl_payment_details as mp      ON mpd.payment_id = mp.id ";
        join += " LEFT JOIN mdl_rpl_process_submission_master as mr ON mpd.payment_id = mr.payment_id ";
        join += " LEFT JOIN mdl_course as mc                    ON mp.unit_id     = mc.id ";
        join += " LEFT JOIN mdl_course_categories as mcc        ON mp.course_id   = mcc.id ";
        join += " LEFT JOIN mdl_user as mu                      ON mp.trainer_id  = mu.id ";
        join += " LEFT JOIN mdl_learning_mode_type as ml1       ON mp.learning_mode_type_id = ml1.id ";
        var param = {

            from: 'from mdl_refund_request_log mpd ',
            condition: condition,
            fields: fields,
            join: join
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {
            if (userRes.length > 0) {
                var response = {
                    resCode: 200,
                    response: userRes
                }
                res.status(200).send(response);
            } else {
                res.status(201).send(RESPONSEMESSAGE.RPL.NORECORDSFOUND);
            }
        }); //end of select

    }

}); //e


/*
 * Used to get Trainer refund List
 * Created On : 19-02-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/trainerRefundList', customodule.verifyapicalltoken, function (req, res, next) {
    var trainerId = req.body.trainerId;
    var dbsetting = req.db_setting;


    var fromDate = req.body.fromDate;
    var toDate = req.body.toDate;
    var searchCourseName = req.body.searchCourseName;
    var searchStudentName = req.body.searchStudentName;
    var learningMode = req.body.learningMode;


    var fields = " mpd.*, mp.learning_mode_type_id,  ml1.name as learning_mode,  mp.transaction_id, mp.amount, mp.date, mp.currency, mp.date as payment_date,mp.type,   mr.id as rplid,  mc.fullname as uname,  mc.idnumber as ucode,  mcc.name as cname,  mcc.idnumber as ccode, mu.firstname, mu.lastname, mr.is_approved as rpl_approved, mr.is_actiontaken as rpl_action_taken";
    var condition = " where mpd.trainer_id = '" + trainerId + "' ";
    var join = " JOIN mdl_payment_details as mp       ON mpd.payment_id = mp.id ";


    if (searchCourseName != "")
        condition += " AND ( mcc.name LIKE '%" + searchCourseName + "%' OR  mcc.idnumber LIKE '%" + searchCourseName + "%' ) ";
    if (searchStudentName != "")
        condition += " AND ( mu.firstname LIKE '%" + searchStudentName + "%' OR  mu.lastname LIKE '%" + searchStudentName + "%' ) ";
    if (learningMode != "")
        condition += " AND ( mp.learning_mode_type_id = '" + learningMode + "'  ) ";
    if (fromDate != "" && (toDate == "" || toDate == undefined))
        condition += " AND  mpd.date >= '" + fromDate + "' ";
    if (toDate != "" && (fromDate == "" || fromDate == undefined))
        condition += " AND  mpd.date <= '" + toDate + "' ";
    if (fromDate != "" && toDate != "")
        condition += " AND  mpd.date BETWEEN '" + fromDate + "' AND '" + toDate + "'   ";
    join += " LEFT JOIN mdl_rpl_process_submission_master as mr ON mpd.payment_id = mr.payment_id ";
    join += " LEFT JOIN mdl_course as mc                   ON mpd.unit_id     = mc.id ";
    join += " LEFT JOIN mdl_course_categories as mcc       ON mpd.course_id   = mcc.id ";
    join += " LEFT JOIN mdl_user as mu                     ON mpd.student_id  = mu.id ";
    join += " LEFT JOIN mdl_learning_mode_type as ml1      ON mp.learning_mode_type_id = ml1.id ";


    var param = {

        from: 'from mdl_refund_request_log mpd ',
        condition: condition,
        fields: fields,
        join: join
    };

    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (userRes) {
        if (userRes.length > 0) {
            var response = {
                resCode: 200,
                response: userRes
            }
            res.status(200).send(response);
        } else {
            res.status(201).send(RESPONSEMESSAGE.RPL.NORECORDSFOUND);
        }
    }); //end of select


}); //e

/*
 * Used to get student Payment List
 * Created On : 6-02-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/studentPaymentdetails', customodule.verifyapicalltoken, function (req, res, next) {
    var paymentId = req.body.paymentId;
    var dbsetting = req.db_setting;


    var fields = " mp.learning_mode_type_id";
    var condition = " where mp.id = '" + paymentId + "' ";
    var param = {

        from: 'from mdl_payment_details mp ',
        condition: condition,
        fields: fields
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (learningRes) {

        var learning_mode_type_id = learningRes[0].learning_mode_type_id;
        if (learning_mode_type_id == 1) {

            var fields = " mp.*,  mr.is_approved, mr.id as rplid,  mc.fullname as uname,  mc.idnumber as ucode,  mcc.name as cname,  mcc.idnumber as ccode, mu.firstname, mu.lastname , ml.id as refund_id";
            var condition = " where mp.id = '" + paymentId + "' ";
            var join = "  JOIN mdl_rpl_process_submission_master as mr ON mp.id = mr.payment_id ";
            join += " LEFT JOIN mdl_course as mc                   ON mp.unit_id    = mc.id ";
            join += " LEFT JOIN mdl_course_categories as mcc       ON mp.course_id  = mcc.id ";
            join += " LEFT JOIN mdl_user as mu                     ON mp.trainer_id = mu.id ";
            join += " LEFT JOIN mdl_refund_request_log as ml       ON mp.id         = ml.payment_id ";

        } else {

            var fields = " mp.*,  mr.id as rplid,  mc.fullname as uname,  mc.idnumber as ucode,  mcc.name as cname,  mcc.idnumber as ccode, mu.firstname, mu.lastname , ml.id as refund_id";
            var condition = " where mp.id = '" + paymentId + "' ";
            var join = "  JOIN mdl_student_booked_vc as mr         ON mp.id         = mr.payment_id ";
            join += " LEFT JOIN mdl_course as mc                   ON mp.unit_id    = mc.id ";
            join += " LEFT JOIN mdl_course_categories as mcc       ON mp.course_id  = mcc.id ";
            join += " LEFT JOIN mdl_user as mu                     ON mp.trainer_id = mu.id ";
            join += " LEFT JOIN mdl_refund_request_log as ml       ON mp.id         = ml.payment_id ";



        }

        var param = {

            from: 'from mdl_payment_details mp ',
            condition: condition,
            fields: fields,
            join: join
        };

        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {
            if (userRes.length > 0) {
                var response = {
                    resCode: 200,
                    response: userRes[0]
                }
                res.status(200).send(response);
            } else {
                res.status(201).send(RESPONSEMESSAGE.RPL.NORECORDSFOUND);
            }
        }); //end of select








    });




}); //e


/*
 * Used to get student Payment List
 * Created On : 6-02-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/trainerPaymentdetails', customodule.verifyapicalltoken, function (req, res, next) {
    var paymentId = req.body.paymentId;
    var dbsetting = req.db_setting;



    var fields = " mp.learning_mode_type_id";
    var condition = " where mp.id = '" + paymentId + "' ";
    var param = {

        from: 'from mdl_payment_details mp ',
        condition: condition,
        fields: fields
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (learningRes) {


        var learning_mode_type_id = learningRes[0].learning_mode_type_id;

        if (learning_mode_type_id == 1) {


            var fields = " mp.*, mr.is_approved, mr.id as rplid,  mc.fullname as uname,  mc.idnumber as ucode,  mcc.name as cname,  mcc.idnumber as ccode, mu.firstname, mu.lastname , ml.id as refund_id";
            var condition = " where mp.id = '" + paymentId + "' ";
            var join = "  JOIN mdl_rpl_process_submission_master as mr ON mp.id = mr.payment_id ";
            join += " LEFT JOIN mdl_course as mc              ON mp.unit_id     = mc.id ";
            join += " LEFT JOIN mdl_course_categories as mcc  ON mp.course_id   = mcc.id ";
            join += " LEFT JOIN mdl_user as mu                ON mp.student_id  = mu.id ";
            join += " LEFT JOIN mdl_refund_request_log as ml  ON mp.id          = ml.payment_id ";


        } else {


            var fields = " mp.*,  mr.id as rplid,  mc.fullname as uname,  mc.idnumber as ucode,  mcc.name as cname,  mcc.idnumber as ccode, mu.firstname, mu.lastname , ml.id as refund_id";
            var condition = " where mp.id = '" + paymentId + "' ";
            var join = "  JOIN mdl_student_booked_vc as mr    ON mp.id         = mr.payment_id ";
            join += " LEFT JOIN mdl_course as mc              ON mp.unit_id    = mc.id ";
            join += " LEFT JOIN mdl_course_categories as mcc  ON mp.course_id  = mcc.id ";
            join += " LEFT JOIN mdl_user as mu                ON mp.student_id = mu.id ";
            join += " LEFT JOIN mdl_refund_request_log as ml  ON mp.id         = ml.payment_id ";



        }

        var param = {

            from: 'from mdl_payment_details mp ',
            condition: condition,
            fields: fields,
            join: join
        };



        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {
            if (userRes.length > 0) {
                var response = {
                    resCode: 200,
                    response: userRes[0]
                }
                res.status(200).send(response);
            } else {
                res.status(201).send(RESPONSEMESSAGE.RPL.NORECORDSFOUND);
            }
        }); //end of select


    });



}); //e

/*
 * Used  for refund request
 * Created On : 07-02-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/addStudentRefundRequest', customodule.verifyapicalltoken, function (req, res, next) {

    var dbsetting = req.db_setting;
    var paymentId = req.body.paymentId;
    var rplId = req.body.rplId;
    var comment = req.body.comment;


    var d = new Date();
    var m = d.getMonth() + 1;
    var datetime = d.getFullYear() + "-" + m + "-" + d.getDate();


    if (!paymentId || paymentId == 0) {
        res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONGUSER);
    } else if (!rplId || rplId == 0) {
        res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONGUSER);
    } else {



        var fields = " student_id, course_id, unit_id, trainer_id ";
        var condition = " where mp.id = '" + paymentId + "' ";
        var param = {

            from: 'from mdl_payment_details mp ',
            condition: condition,
            fields: fields
        };

        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (ansRes) {


            if (ansRes.length > 0) {

                var studentId = ansRes[0].student_id;
                var courseId = ansRes[0].course_id;
                var unitId = ansRes[0].unit_id;
                var trainerId = ansRes[0].trainer_id;

                var saveData = {};
                saveData.student_id = studentId;
                saveData.course_id = courseId;
                saveData.unit_id = unitId;
                saveData.trainer_id = trainerId;
                saveData.rpl_process_submission_id = rplId;
                saveData.payment_id = paymentId;
                saveData.comment = comment;
                saveData.date = datetime;

                var param = {

                    tablename: 'mdl_refund_request_log',
                    data: [saveData]
                }

                Mastermodel.commonInsert(param, function (err) {
                    console.log("++++++++++++++++++++++++++++++++++++", err);
                    res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                }, function (data) {

                    var response = {
                        resCode: 200,
                        response: "Refund Request Sent Successfully"
                    }
                    res.status(200).send(response);
                    //res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.RPLTESTSUBMITTED);
                });

            } else {

                res.status(200).send(RESPONSEMESSAGE.COMMON.NODATAEXISTS);

            }
        });




    } //end of else
}); //e


/*
 * Used  for trainer responce request
 * Created On : 07-02-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/addTrainerResponceRequest', customodule.verifyapicalltoken, function (req, res, next) {

    var dbsetting = req.db_setting;
    var paymentId = req.body.paymentId;
    var rplId = req.body.rplId;
    var comment = req.body.comment;
    var refundId = req.body.refundId;
    var is_approved = req.body.is_approved;

    var learning_mode_type_id = req.body.learning_mode_type_id;

    var d = new Date();
    var datetime = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate();

    if (!paymentId || paymentId == 0) {
        res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONGUSER);
    } else if (!refundId || refundId == 0) {
        res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONGUSER);
    } else {


        var fields = "student_id, course_id, unit_id, trainer_id ";
        var condition = " where id ='" + paymentId + "'   ";

        var param = {

            from: 'from mdl_payment_details',
            condition: condition,
            fields: fields
        };



        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (ansRes) {
            if (ansRes.length > 0) {

                var studentId = ansRes[0].student_id;
                var courseId = ansRes[0].course_id;
                var unitId = ansRes[0].unit_id;
                var trainerId = ansRes[0].trainer_id;


                var dataupdate = {
                    is_action: 'Yes',
                    is_approved: is_approved,
                    trainer_comment: comment

                }
                var updatecondition = {
                    id: refundId
                }
                var param3 = {

                    tablename: 'mdl_refund_request_log',
                    condition: updatecondition,
                    data: dataupdate
                }

                Mastermodel.commonUpdate(param3, function (err) {
                    console.log("++++++++++++++++++++++++++++++++++++", err);
                    res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                }, function (data) {

                    if (is_approved == 'Yes') {

                        var dataupdate = {
                            is_refund: 'Y'
                        }
                        var updatecondition = {
                            id: paymentId
                        }
                        var param3 = {

                            tablename: 'mdl_payment_details',
                            condition: updatecondition,
                            data: dataupdate
                        }
                        Mastermodel.commonUpdate(param3, function (err) {
                            console.log("++++++++++++++++++++++++++++++++++++", err);
                            res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                        }, function (data) {

                            if (learning_mode_type_id == 1) {

                                var dataupdate = {
                                    is_valid: 'N',
                                    is_active: 'N'
                                }
                                var updatecondition = {
                                    id: rplId
                                }
                                var param3 = {

                                    tablename: 'mdl_rpl_process_submission_master',
                                    condition: updatecondition,
                                    data: dataupdate
                                }
                                Mastermodel.commonUpdate(param3, function (err) {
                                    console.log("++++++++++++++++++++++++++++++++++++", err);
                                    res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                                }, function (data) {

                                    var response = {
                                        resCode: 200,
                                        response: "Comment Sent Successfully"
                                    }
                                    res.status(200).send(response);

                                });


                            } else {

                                var dataupdate = {
                                    is_valid: 'N'
                                }
                                var updatecondition = {
                                    payment_id: paymentId
                                }
                                var param3 = {

                                    tablename: 'mdl_student_booked_vc',
                                    condition: updatecondition,
                                    data: dataupdate
                                }
                                Mastermodel.commonUpdate(param3, function (err) {
                                    console.log("++++++++++++++++++++++++++++++++++++", err);
                                    res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                                }, function (data) {

                                    var response = {
                                        resCode: 200,
                                        response: "Comment Sent Successfully"
                                    }
                                    res.status(200).send(response);

                                });




                            }




                        });


                    } else {

                        var response = {
                            resCode: 200,
                            response: "Comment Sent Successfully"
                        }
                        res.status(200).send(response);
                    }

                });

            } else {

                res.status(201).send(RESPONSEMESSAGE.COMMON.NODATAEXISTS);
            }
        }); //end of select

    } //end of else
}); //e




/*
 * Used to get student details by VC schdule id
 * Created On : 07-02-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/studentDetailsForVC', function (req, res, next) {
    var schduleId = req.body.schduleId;
    var dbsetting = '';
    var questionArr = [];


    if (!schduleId || schduleId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.IDREQUIRED);
    } else {

        var base_url1 = "http://52.63.96.113:4444/share/courseimage/";
        var base_url2 = "http://52.63.96.113:4444/share/unitimage/";
        var base_url3 = "http://52.63.96.113:4444/share/userimage/";



        var d = new Date();
        var month = d.getMonth() + 1;
        var datetime = d.getFullYear() + "-" + month + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();

        var fields = " msbv.*, mu.firstname, mu.lastname,  (case when mu.profile_image=null then mu.profile_image else concat('" + base_url3 + "',mu.profile_image) end) as profileimage ";
        //var fields = " msbv.*, mts.date, mts.price, mts.duration, DATE_FORMAT(mts.date, '%a %M %d %Y %H %i %p') as dateformat,  mu.firstname, mu.lastname, mu.profile_image, mcc.name as cname, mcc.description as course_description,  mcc.idnumber as ccode, (case when mcc.course_image=null then mcc.course_image else concat('" + base_url1 + "',mcc.course_image) end) as courseimage, mc.summary as unit_description,  mc.fullname as uname, mc.idnumber as ucode";
        //fields += ",  (case when mc.unit_image=null then mc.unit_image else concat('" + base_url2 + "',mc.unit_image) end) as unitimage";
        var condition = " where msbv.schdule_id = " + schduleId + "  AND msbv.is_valid = 'Y' ";
        //condition += " AND  mts.date >= '" + datetime + "' ";
        var join = " LEFT JOIN mdl_user   as mu              ON mu.id  = msbv.student_id ";
        //join += " LEFT JOIN mdl_course as mc              ON mc.id  = msbv.unit_id ";
        //join += " LEFT JOIN mdl_course_categories as mcc  ON mcc.id = msbv.course_id ";
        //join += " LEFT JOIN mdl_trainer_schdule as   mts  ON mts.id = msbv.schdule_id ";


        var param = {

            from: 'from mdl_student_booked_vc msbv ',
            condition: condition,
            fields: fields,
            join: join
        };

        console.log("param222", param);


        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (qualificationRes) {
            if (qualificationRes.length > 0) {

                var response = {
                    resCode: 200,
                    response: qualificationRes
                }
                res.status(201).send(response);

            } else {
                res.status(201).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
            }
        }); //end of select


    }


});

/*
 * Get only accrediated course list for student panel
 */
router.post('/studentDetailsForVC', customodule.verifyapicalltoken, function (req, res, next) {
    var dbsetting = req.db_setting;
    var searchText = req.body.searchText;
    var isAccrediated = req.body.isAccrediated;
    var studentId = req.body.studentId;
    var sortByParam = req.body.sortByParam;
    /**** Course Search By search Text and isAccrediated parameter  ****/
    var fields = " mcc.id,  mcc.name,  mcc.idnumber, mcc.description, mcc.isaccrediated , mcc.isentryteststudent , mcc.course_image, msetm.is_qualified, msw.iswishlistadd, mslm.learning_mode, mslm.unit_id, mslm.course_id ";
    // /*, mrpsm.is_submitted,  mrpsm.is_actiontaken, mrpsm.is_approved , mrpsm.final_submitted , mrpsm.id as rpl_submission_id,  mrpsm.trainer_status , mrpsm.trainer_id
    var condition = "where 1";
    if (searchText != "")
        condition += " AND ( mcc.name LIKE '%" + searchText + "%' OR  mcc.idnumber LIKE '%" + searchText + "%' ) ";
    if (isAccrediated != "")
        condition += " AND  mcc.isaccrediated LIKE '%" + isAccrediated + "%'   ";
    var join = " LEFT JOIN mdl_student_entry_test_master msetm      ON mcc.id = msetm.course_id  AND msetm.student_id = '" + studentId + "' ";
    join += " LEFT JOIN mdl_student_wishlist msw                 ON mcc.id = msw.courseid     AND msw.studentid    = '" + studentId + "' ";
    join += " LEFT JOIN mdl_student_learning_mode mslm           ON mcc.id = mslm.course_id   AND mslm.student_id  = '" + studentId + "'  AND ( mslm.unit_id = '' || mslm.unit_id IS NULL) ";
    if (sortByParam != undefined && sortByParam != '') {
        if (sortByParam == 'ascCourse') {
            var orderby = " order by mcc.name asc ";
        }
        if (sortByParam == 'descCourse') {
            var orderby = " order by mcc.name desc ";
        }
        if (sortByParam == 'ascPrice') {
            var orderby = " order by mcc.cprice asc ";
        }
        if (sortByParam == 'descPrice') {
            var orderby = " order by mcc.cprice desc ";
        }
    } else {
        var orderby = " order by mcc.id desc ";
    }
    var param = {

        from: 'from mdl_course_categories as mcc',
        condition: condition,
        fields: fields,
        join: join,
        orderby: orderby
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (courseRes) { //console.log(courseRes);
        if (courseRes.length > 0) {
            var response = {
                resCode: 200,
                response: courseRes
            }
            res.status(200).send(response);

        } else {
            res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
        }
    });
})

/*
 * API to get my course list
 * Created On : 05-09-2017 *
 * Input      :
 * Modified On
 */
router.post('/getMyCourseList', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    //console.log(req.db_setting);
    var dbsetting = req.db_setting;
    var searchText = req.body.searchText;
    var isAccrediated = req.body.isAccrediated;
    var studentId = req.body.studentId;
    var sortByParam = req.body.sortByParam;
    /**** Course Search By search Text and isAccrediated parameter  ****/
    var fields = " mcc.id,  mcc.name,  mcc.idnumber, mcc.description, mcc.isaccrediated , mcc.isentryteststudent , mcc.course_image, msetm.is_qualified, msw.iswishlistadd, mslm.learning_mode, mslm.unit_id, mslm.course_id ";
    // /*, mrpsm.is_submitted,  mrpsm.is_actiontaken, mrpsm.is_approved , mrpsm.final_submitted , mrpsm.id as rpl_submission_id,  mrpsm.trainer_status , mrpsm.trainer_id
    var condition = "where 1";
    if (searchText != "")
        condition += " AND ( mcc.name LIKE '%" + searchText + "%' OR  mcc.idnumber LIKE '%" + searchText + "%' ) ";
    if (isAccrediated != "")
        condition += " AND  mcc.isaccrediated LIKE '%" + isAccrediated + "%'   ";
    var join = " INNER JOIN mdl_student_entry_test_master msetm      ON mcc.id = msetm.course_id  AND msetm.student_id = '" + studentId + "' ";
    if (sortByParam != undefined && sortByParam != '') {
        if (sortByParam == 'ascCourse') {
            var orderby = " order by mcc.name asc ";
        }
        if (sortByParam == 'descCourse') {
            var orderby = " order by mcc.name desc ";
        }
        if (sortByParam == 'ascPrice') {
            var orderby = " order by mcc.cprice asc ";
        }
        if (sortByParam == 'descPrice') {
            var orderby = " order by mcc.cprice desc ";
        }
    } else {
        var orderby = " order by mcc.id desc ";
    }
    var param = {

        from: 'from mdl_course_categories as mcc',
        condition: condition,
        fields: fields,
        join: join,
        orderby: orderby
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (courseRes) { //console.log(courseRes);
        if (courseRes.length > 0) {
            var response = {
                resCode: 200,
                response: courseRes
            }
            res.status(200).send(response);

        } else {
            res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
        }
    });
});


/*
 * API to get dashboard for student
 * Created On : 13-02-2018
 * Input      :
 * Modified On
 */
router.post('/getStudentDashboard', customodule.verifyapicalltoken, function (req, res, next) {
    var dbsetting = req.db_setting;
    var studentId = req.body.studentId;
    var myCourseCnt = 0;
    var dashboardObj = {};
    if (!studentId) {
        res.status(200).send(RESPONSEMESSAGE.STUDENT.STUDENTIDREQUIRED);
    } else {
        console.log(studentId);
        //get my course count , all course with entry test completion will be counted
        var dbsetting = req.db_setting;
        var fields = " count(mcc.id) as totmycourse ";
        var condition = "where 1";
        var join = " INNER JOIN mdl_student_entry_test_master msetm  ON mcc.id = msetm.course_id  AND msetm.student_id = '" + studentId + "' ";
        var param = {

            from: 'from mdl_course_categories as mcc',
            condition: condition,
            fields: fields,
            join: join
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (myCourseRes) {
            myCourseCnt = myCourseRes[0]['totmycourse'];
            dashboardObj.myCourseCnt = myCourseCnt;
            //GET THE TOTAL MY RPL COUNT -- GET THE ACCREDIATED COURSE COUNT
            var fields = " count(mcc.id) as totrplcourse ";
            var condition = "where 1";
            condition += " AND mcc.isaccrediated = 'Y' ";
            var join = " INNER JOIN mdl_student_entry_test_master msetm  ON mcc.id = msetm.course_id  AND msetm.student_id = '" + studentId + "' ";
            var param = {

                from: 'from mdl_course_categories as mcc',
                condition: condition,
                fields: fields,
                join: join
            };
            Mastermodel.commonSelect(param, function (err) {
                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            }, function (myRplRes) {
                myRplCnt = myRplRes[0]['totrplcourse'];
                dashboardObj.myRplCnt = myRplCnt;
                //GET THE SELECTED TRAINER COUNT
                var fields = " count(mrpsm.id) as tottrainerrpl ";
                var condition = "where 1";
                condition += " AND mrpsm.is_approved = 'Y' AND mrpsm.student_id = '" + studentId + "' ";
                var join = " ";
                var param = {

                    from: 'from mdl_rpl_process_submission_master as mrpsm',
                    condition: condition,
                    fields: fields,
                    join: join
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (myTrainerRes) {
                    myTrainerCnt = myTrainerRes[0]['tottrainerrpl'];
                    dashboardObj.myTrainerCnt = myTrainerCnt;
                    //GET THE COURSE ENROLLMENT IN RPL
                    var dbsetting = req.db_setting;
                    var fields = " mcc.id,mcc.name,(case when mcc.course_image=null then mcc.course_image else concat('" + SETTING.course_image_url + "',mcc.course_image) end) as courseimage  ";
                    var condition = "where 1 AND msetm.is_qualified = 'Y' ";
                    condition += "  AND msetm.student_id = '" + studentId + "' ";
                    var join = " INNER JOIN mdl_course_categories mcc  ON msetm.course_id = mcc.id  ";
                    var orderby = " order by msetm.datetime desc ";
                    var param = {

                        from: 'from mdl_student_entry_test_master msetm ',
                        condition: condition,
                        fields: fields,
                        join: join,
                        orderby: orderby
                    };
                    Mastermodel.commonSelect(param, function (err) {
                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                    }, function (myCourseEnrollmentRes) {
                        dashboardObj.myCourseEnrollmentRes = myCourseEnrollmentRes;

                        //GET THE COURSE COMPLETION IN RPL
                        var dbsetting = req.db_setting;
                        var fields = "  mcc.id,mcc.name,(case when mcc.course_image=null then mcc.course_image else concat('" + SETTING.course_image_url + "',mcc.course_image) end) as courseimage  ";
                        var condition = "where 1";
                        condition += "  AND mrpsm.student_id = '" + studentId + "' AND mrpsm.is_approved = 'Y' ";
                        var join = " INNER JOIN mdl_course_categories mcc  ON mrpsm.course_id = mcc.id  ";
                        var orderby = " order by mrpsm.action_datetime desc ";
                        var param = {

                            from: 'from mdl_rpl_process_submission_master as mrpsm',
                            condition: condition,
                            fields: fields,
                            join: join,
                            orderby: orderby
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (myCourseCompletionRes) {
                            dashboardObj.myCourseCompletionRes = myCourseCompletionRes;
                            //GET THE DTRAFT MODE RPL SAVE LIST
                            var dbsetting = req.db_setting;
                            var fields = " mcc.id as courseid,mcc.name,mc.id as unitid ,mc.fullname as unitname,submission_datetime,(case when mcc.course_image=null then mcc.course_image else concat('" + SETTING.course_image_url + "',mcc.course_image) end) as courseimage  ";
                            var condition = "where 1";
                            condition += "  AND mrpsm.student_id = '" + studentId + "' AND mrpsm.is_submitted = 'N'  ";
                            var join = " INNER JOIN mdl_course_categories mcc  ON mrpsm.course_id = mcc.id  ";
                            join += " INNER JOIN mdl_course mc  ON mcc.id = mc.category  ";
                            var orderby = " order by mrpsm.submission_datetime desc ";
                            var param = {

                                from: 'from mdl_rpl_process_submission_master as mrpsm',
                                condition: condition,
                                fields: fields,
                                join: join,
                                orderby: orderby
                            };
                            Mastermodel.commonSelect(param, function (err) {
                                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                            }, function (myRplDraftRes) {
                                dashboardObj.myRplDraftRes = myRplDraftRes;
                                //GET THE DTRAFT MODE RPL PENDING PAYMENT
                                var dbsetting = req.db_setting;
                                var fields = " mcc.id as courseid,mcc.name,mc.id as unitid ,mc.fullname as unitname,submission_datetime,(case when mcc.course_image=null then mcc.course_image else concat('" + SETTING.course_image_url + "',mcc.course_image) end) as courseimage  ";
                                var condition = "where 1";
                                condition += "  AND mrpsm.student_id = '" + studentId + "' AND mrpsm.is_submitted = 'Y' AND mrpsm.is_paid = 'No' ";
                                var join = " INNER JOIN mdl_course_categories mcc  ON mrpsm.course_id = mcc.id  ";
                                join += " INNER JOIN mdl_course mc  ON mcc.id = mc.category  ";
                                var orderby = " order by mrpsm.submission_datetime desc ";
                                var param = {

                                    from: 'from mdl_rpl_process_submission_master as mrpsm',
                                    condition: condition,
                                    fields: fields,
                                    join: join,
                                    orderby: orderby
                                };
                                Mastermodel.commonSelect(param, function (err) {
                                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                                }, function (myRplPaymentPendingRes) {
                                    dashboardObj.myRplPaymentPendingRes = myRplPaymentPendingRes;
                                    //GET THE NOTIFICATION DETAIL
                                    var dbsetting = req.db_setting;
                                    var fields = " msn.notification_title as action_type,msn.notification_message as action_taken  ";
                                    var condition = "where 1";
                                    condition += "  AND msn.user_id = '" + studentId + "'   ";
                                    var join = " ";
                                    var param = {

                                        from: 'from mdl_user_notification as msn',
                                        condition: condition,
                                        fields: fields,
                                        join: join
                                    };
                                    Mastermodel.commonSelect(param, function (err) {
                                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                                    }, function (myNotificationRes) {
                                        dashboardObj.myNotificationRes = myNotificationRes;
                                        var response = {
                                            resCode: 200,
                                            response: dashboardObj
                                        }
                                        res.status(200).send(response);
                                    });
                                });
                            });
                        });
                    });
                })
            })
        });
    }
})
/*
 * Used to get student RPL course list
 * Created On : 05-09-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/getStudentRPLCourseList', customodule.verifyapicalltoken, function (req, res, next) {

    var dbsetting = req.db_setting;
    var searchText = req.body.searchText;
    var studentId = req.body.studentId;
    var sortByParam = req.body.sortByParam;
    var questionArr = [];
    if (node_module.validator.matches(searchText, SETTING.specialcharpattern)) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.INVALIDREQUESTSPECIALCHAR);
    } else {
        /**** Course Search By search Text and isAccrediated parameter  ****/
        var fields = " mcc.id,  mcc.name,  mcc.idnumber, mcc.description, mcc.isaccrediated , mcc.isentryteststudent , mcc.course_image as cimage, (case when mcc.course_image=null then mcc.course_image else concat('" + base_url_course_image + "',mcc.course_image) end) as courseimage, msetm.is_qualified ";
        // /*, mrpsm.is_submitted,  mrpsm.is_actiontaken, mrpsm.is_approved , mrpsm.final_submitted , mrpsm.id as rpl_submission_id,  mrpsm.trainer_status , mrpsm.trainer_id
        var condition = "where 1 AND isaccrediated = 'Y' AND  msetm.is_qualified = 'Y'";
        if (searchText != "")
            condition += " AND ( mcc.name LIKE '%" + searchText + "%' OR  mcc.idnumber LIKE '%" + searchText + "%' ) ";
        var join = " LEFT JOIN mdl_student_entry_test_master msetm      ON mcc.id = msetm.course_id  AND msetm.student_id = '" + studentId + "' ";
        //join += " LEFT JOIN mdl_student_wishlist msw                 ON mcc.id = msw.courseid     AND msw.studentid    = '" + studentId + "' ";
        //join += " LEFT JOIN mdl_student_learning_mode mslm           ON mcc.id = mslm.course_id   AND mslm.student_id  = '" + studentId + "'  AND ( mslm.unit_id = '' || mslm.unit_id IS NULL) ";
        if (sortByParam != undefined && sortByParam != '') {
            if (sortByParam == 'ascCourse') {
                var orderby = " order by mcc.name asc ";
            }
            if (sortByParam == 'descCourse') {
                var orderby = " order by mcc.name desc ";
            }
            if (sortByParam == 'ascPrice') {
                var orderby = " order by mcc.cprice asc ";
            }
            if (sortByParam == 'descPrice') {
                var orderby = " order by mcc.cprice desc ";
            }
        } else {
            var orderby = " order by mcc.id desc ";
        }
        var param = {

            from: 'from mdl_course_categories as mcc',
            condition: condition,
            fields: fields,
            join: join,
            orderby: orderby
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (courseRes) { //console.log(courseRes);

            var courseResLen = courseRes.length;
            if (courseRes.length > 0) {


                /**** Used to get student entry test answer list ****/
                asyncForEach(courseRes, function (positionitem, positionindex, positionarr) {
                    var cnt = positionindex;
                    console.log(positionitem.id);
                    var course_id = positionitem.id;
                    var fields = "mc.id as unit_id, ms.total_qcnt,ms.total_acnt , ( select count(id) as total_question from mdl_rpl_question where unit_id = mc.id AND course_id = '" + course_id + "' ) as total_question_cnt";
                    var condition = "where mc.category= '" + positionitem.id + "'  ";
                    var join = " LEFT JOIN mdl_rpl_process_submission_master ms  ON ms.unit_id = mc.id  AND ms.course_id = '" + course_id + "' AND  ms.student_id = '" + studentId + "' AND is_active = 'Y' AND is_valid != 'N' ";
                    //var join     += " LEFT JOIN mdl_rpl_question mr  ON mr.unit_id = mr.unit_id";
                    //var orderby   = " order by ms.id DESC limit 0,1 ";
                    var param = {

                        from: 'from mdl_course as mc',
                        condition: condition,
                        fields: fields,
                        join: join
                    };
                    // entrytestRes[positionindex].hii="12345" ;
                    Mastermodel.commonSelect(param, function (err) {
                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                    }, function (ansRes) {


                        questionArr.push(positionindex);
                        var questionLen = questionArr.length;
                        // console.log(entrytestRes[positionindex]);
                        //  console.log(entrytestRes);

                        if (ansRes.length > 0) {

                            courseRes[cnt].units = ansRes;
                            console.log(courseResLen, questionLen);
                            cnt++;
                            if (courseResLen == questionLen) {
                                var response = {
                                    resCode: 200,
                                    response: courseRes
                                }
                                res.status(200).send(response);
                            }
                            /************* *********************************************************/
                        } else {
                            if (courseResLen == questionLen) {
                                var response = {
                                    resCode: 200,
                                    response: courseRes
                                }
                                res.status(200).send(response);
                            }
                        }

                    }); ////end of middle select

                }); //end of first loop



            } else {
                res.status(201).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
            }
        });
    }
});



/*
 * Used to get student RPL course list New
 * Created On : 05-09-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */


router.post('/getStudentRPLCourseListNew', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    var questionArr = [];
    var dbsetting = req.db_setting;
    var searchText = req.body.searchText;
    var studentId = req.body.studentId;
    var sortByParam = req.body.sortByParam;
    //var course_id     = 58;



    if (!studentId) {
        res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NOCOURSEFOUND);
    } else if (isNaN(studentId)) {
        res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.INVALIDCOURSE);
    } else {
        /**** Used to get student entry test question list ****/
        var fields = " mcc.id,  mcc.name,  mcc.idnumber, mcc.description, mcc.isaccrediated , mcc.isentryteststudent , mcc.course_image as cimage, (case when mcc.course_image=null then mcc.course_image else concat('" + base_url_course_image + "',mcc.course_image) end) as courseimage, msetm.is_qualified ";
        var condition = "where 1 AND isaccrediated = 'Y' AND  msetm.is_qualified = 'Y' AND mcc.id = 58 ";
        //if (searchText != "")
        //condition += " AND ( mcc.name LIKE '%" + searchText + "%' OR  mcc.idnumber LIKE '%" + searchText + "%' ) ";
        var join = " LEFT JOIN mdl_student_entry_test_master msetm      ON mcc.id = msetm.course_id  AND msetm.student_id = '" + studentId + "' ";
        if (sortByParam != undefined && sortByParam != '') {
            if (sortByParam == 'ascCourse') {
                var orderby = " order by mcc.name asc ";
            }
            if (sortByParam == 'descCourse') {
                var orderby = " order by mcc.name desc ";
            }
            if (sortByParam == 'ascPrice') {
                var orderby = " order by mcc.cprice asc ";
            }
            if (sortByParam == 'descPrice') {
                var orderby = " order by mcc.cprice desc ";
            }
        } else {
            var orderby = " order by mcc.id desc ";
        }
        var param = {

            from: 'from mdl_course_categories as mcc',
            condition: condition,
            fields: fields,
            join: join,
            orderby: orderby
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (courseRes) {
            console.log(courseRes);
            var courseResLen = courseRes.length;
            var courseLen = courseRes.length;
            var fqueaArr = new Array();

            if (courseRes.length > 0) {
                /**** Used to get student entry test answer list ****/
                asyncForEach(courseRes, function (positionitem, positionindex, positionarr) {
                    var coursecnt = positionindex;
                    var courseId = positionitem.id;
                    fqueaArr.push(positionindex);

                    var fqueaArrLength = fqueaArr.length;



                    var fields = " mc.id, mc.fullname ";
                    var condition = " where mc.category = '" + positionitem.id + "' ";
                    var param = {

                        from: 'from mdl_course as mc',
                        condition: condition,
                        fields: fields
                    };
                    // entrytestRes[positionindex].hii="12345" ;
                    Mastermodel.commonSelect(param, function (err) {
                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                    }, function (unitRes) {

                        var unitLen = unitRes.length;
                        var squeaArr = new Array();

                        if (unitLen > 0) { // course have unit
                            asyncForEach(unitRes, function (positionitem1, positionindex1, positionarr1) {
                                var unitcnt = positionindex1;
                                var unitId = positionitem1.id;
                                squeaArr.push(positionindex1);

                                var squeaArrLength = squeaArr.length;

                                var fields = " mrpsm.* , mp.id as gap_traning_is_taken";
                                var condition = " where mrpsm.student_id = '" + studentId + "' AND  mrpsm.course_id = '" + courseId + "'      AND  mrpsm.unit_id = '" + unitId + "'   ";
                                var join = " LEFT JOIN mdl_gap_traning_payment mp         ON mrpsm.id = mp.rpl_process_submission_id	 AND mp.student_id = '" + studentId + "' ";
                                var orderby = " order by mrpsm.id DESC limit 0,1 ";
                                var param = {

                                    from: 'from mdl_rpl_process_submission_master as mrpsm',
                                    condition: condition,
                                    fields: fields,
                                    join: join,
                                    orderby: orderby
                                };

                                Mastermodel.commonSelect(param, function (err) {
                                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                                }, function (ansRes) {
                                    //questionArr.push(positionindex);
                                    //var questionLen = questionArr.length;
                                    if (ansRes.length > 0) {

                                        var rplId = ansRes[0].id;

                                        /************* My added code for percentage calculation ****************/
                                        /**** Used to get student entry test question list ****/
                                        var fields = "met.id, met.question, met.question_type, me.student_answer, me.masterid";
                                        var condition = "where met.course_id = '" + courseId + "'       AND  met.unit_id = '" + unitId + "'";
                                        var join = "LEFT JOIN mdl_rpl_process_question_evidence me  ON me.question_id = met.id   AND  me.masterid = '" + rplId + "'  ";
                                        var orderby = "order by met.id asc";
                                        var param = {

                                            from: 'from mdl_rpl_question as met',
                                            condition: condition,
                                            fields: fields,
                                            join: join,
                                            orderby: orderby
                                        };
                                        Mastermodel.commonSelect(param, function (err) {
                                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                                        }, function (percentageRes) {
                                            console.log(percentageRes);

                                            unitRes[unitcnt].percentage = percentageRes;
                                            unitcnt++;
                                            console.log(unitRes);
                                            if (unitLen == squeaArrLength) {

                                                courseRes[coursecnt].units = unitRes; // put unit details into course

                                            }

                                        }); //end of inner select
                                        /************* *********************************************************/
                                    } else {
                                        var percentageRes = new Array();
                                        unitRes[unitcnt].percentage = percentageRes;
                                        unitcnt++;
                                        console.log(unitRes);
                                        if (unitLen == squeaArrLength) {
                                            courseRes[coursecnt].units = unitRes; // put unit details into course
                                        }
                                    }

                                }); ////end of middle select

                            });

                        } else {

                            // no unit

                        }

                        if (courseLen == fqueaArrLength) {

                            var response = {
                                resCode: 200,
                                response: courseRes
                            }
                            res.status(200).send(response);

                        }



                    }); //end of select

                }); // end of outer async




            } else {
                res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NORECORDSFOUND);
            }
        });
    }
});

router.post('/getStudentRPLCourseListNew2', customodule.verifyapicalltoken, function (req, res, next) {
    var dbsetting = req.db_setting;
    var searchText = req.body.searchText;
    var studentId = req.body.studentId;
    var sortByParam = req.body.sortByParam;
    /**** Course Search By search Text and isAccrediated parameter  ****/
    var fields = " mcc.id,  mcc.name,  mcc.idnumber, mcc.description, mcc.isaccrediated , mcc.isentryteststudent , mcc.course_image as cimage, (case when mcc.course_image=null then mcc.course_image else concat('" + base_url_course_image + "',mcc.course_image) end) as courseimage, msetm.is_qualified ";
    var condition = "where 1 AND isaccrediated = 'Y' AND  msetm.is_qualified = 'Y'";
    if (searchText != "")
        condition += " AND ( mcc.name LIKE '%" + searchText + "%' OR  mcc.idnumber LIKE '%" + searchText + "%' ) ";
    var join = " LEFT JOIN mdl_student_entry_test_master msetm    ON mcc.id = msetm.course_id  AND msetm.student_id = '" + studentId + "' ";
    if (sortByParam != undefined && sortByParam != '') {
        if (sortByParam == 'ascCourse') {
            var orderby = " order by mcc.name asc ";
        }
        if (sortByParam == 'descCourse') {
            var orderby = " order by mcc.name desc ";
        }
        if (sortByParam == 'ascPrice') {
            var orderby = " order by mcc.cprice asc ";
        }
        if (sortByParam == 'descPrice') {
            var orderby = " order by mcc.cprice desc ";
        }
    } else {
        var orderby = " order by mcc.id desc ";
    }
    var param = {

        from: 'from mdl_course_categories as mcc',
        condition: condition,
        fields: fields,
        join: join,
        orderby: orderby
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (courseRes) { //console.log(courseRes);
        if (courseRes.length > 0) {

            asyncForEach(courseRes, function (positionitem, positionindex, positionarr) {
                var cnt = positionindex;
                console.log(positionitem.id);

                var fields = "mc.*";
                var condition = "where mc.unit_id = '" + positionitem.id + "'";
                //var join      = " LEFT JOIN mdl_gap_traning_payment mp         ON mrpsm.id = mp.rpl_process_submission_id	 AND mp.student_id = '" + studentId + "' ";
                //var orderby   = " order by mrpsm.id DESC limit 0,1 ";
                var param = {

                    from: 'from mdl_course as mc',
                    condition: condition,
                    fields: fields
                    //,join: join,
                    //orderby: orderby
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (unitRes) {

                    questionArr.push(positionindex);

                    if (courseResLen == questionLen) {
                        var response = {
                            resCode: 200,
                            response: courseRes
                        }
                        res.status(200).send(response);
                    }


                });


            });


        } else {
            res.status(201).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
        }
    });
});

/*
 * Used to get RPL unit percentage calculation
 * Created On : 14-02-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/getRPLUnitPercentageForUnits', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    var questionArr = [];
    var dbsetting = req.db_setting;
    var course_id = req.body.courseId;
    var unit_id = req.body.unitId;
    var student_id = req.body.studentId;
    if (!course_id) {
        res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.NOCOURSEFOUND);
    } else if (isNaN(course_id)) {
        res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.INVALIDCOURSE);
    } else {
        var fields = " mrsm.id  as rplId";
        var condition = " where mrsm.course_id = '" + course_id + "' AND  mrsm.unit_id = '" + unit_id + "' AND  mrsm.student_id = '" + student_id + "'    ";
        var orderby = " order by attempt desc";
        var limit = " 0,1 ";
        var param = {

            from: 'from mdl_rpl_process_submission_master as mrsm',
            condition: condition,
            fields: fields,
            orderby: orderby,
            limit: limit
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (stuRes) {
            if (stuRes.length > 0) {
                var rplId = stuRes[0].rplId;
                console.log("rplId", rplId);
                if (rplId > 0) {
                    /**** Used to get student entry test question list ****/
                    var fields = "met.id, met.question, met.question_type, me.student_answer, me.masterid";
                    var condition = "where met.course_id = '" + course_id + "'       AND  met.unit_id = '" + unit_id + "'";
                    var join = "LEFT JOIN mdl_rpl_process_question_evidence me  ON me.question_id = met.id   AND  me.masterid = '" + rplId + "'  ";
                    var orderby = "order by met.id asc";
                    var param = {

                        from: 'from mdl_rpl_question as met',
                        condition: condition,
                        fields: fields,
                        join: join,
                        orderby: orderby
                    };
                    Mastermodel.commonSelect(param, function (err) {
                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                    }, function (entrytestRes) {
                        console.log(entrytestRes);
                        var response = {
                            resCode: 200,
                            response: entrytestRes
                        }
                        res.status(200).send(response);
                    });
                }
            } else { // no saved data
                var response = {
                    resCode: 201,
                    response: "No Records Found."
                }
                res.status(200).send(response);
            }
        });
    }
});

/*
 * Used to get course unit details
 * Created On : 04-01-2018
 * Created By : Monalisa
 * Input      : courseId, studentId
 * Modified On
 */
router.post('/mytest', customodule.verifyapicalltoken, function (req, res, next) {
    var questionArr = [];
    var dbsetting = req.db_setting;
    var studentId = req.body.studentId;
    /**** Course Search By search Text and isAccrediated parameter  ****/
    var fields = " mcc.id,  mcc.name,  mcc.idnumber, mcc.description, mcc.isaccrediated , mcc.isentryteststudent , mcc.course_image as cimage, (case when mcc.course_image=null then mcc.course_image else concat('" + base_url_course_image + "',mcc.course_image) end) as courseimage, msetm.is_qualified ";
    var condition = " where 1 AND isaccrediated = 'Y' AND  msetm.is_qualified = 'Y'";
    var join = " LEFT JOIN mdl_student_entry_test_master msetm    ON mcc.id = msetm.course_id  AND msetm.student_id = '" + studentId + "' ";
    var orderby = " order by mcc.id desc ";
    var param = {

        from: 'from mdl_course_categories as mcc',
        condition: condition,
        fields: fields,
        join: join,
        orderby: orderby
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (courseRes) {
        var courseResLen = courseRes.length;
        if (courseRes.length > 0) {
            /**** Used to get student entry test answer list ****/
            asyncForEach(courseRes, function (positionitem, positionindex, positionarr) {
                var cnt = positionindex;
                console.log(positionitem.id);
                var course_id = positionitem.id;
                var fields = "mc.id as unit_id, ms.total_qcnt,ms.total_acnt , ( select count(id) as total_question from mdl_rpl_question where unit_id = mc.id AND course_id = '" + course_id + "' ) as total_question_cnt";
                var condition = "where mc.category= '" + positionitem.id + "'  ";
                var join = " LEFT JOIN mdl_rpl_process_submission_master ms  ON ms.unit_id = mc.id	 AND ms.course_id = '" + course_id + "' AND  ms.student_id = '" + studentId + "' AND is_active = 'Y'";
                //var join     += " LEFT JOIN mdl_rpl_question mr  ON mr.unit_id = mr.unit_id";
                //var orderby   = " order by ms.id DESC limit 0,1 ";
                var param = {

                    from: 'from mdl_course as mc',
                    condition: condition,
                    fields: fields,
                    join: join
                };
                // entrytestRes[positionindex].hii="12345" ;
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (ansRes) {
                    questionArr.push(positionindex);
                    var questionLen = questionArr.length;
                    if (ansRes.length > 0) {
                        courseRes[cnt].units = ansRes;
                        console.log(courseResLen, questionLen);
                        cnt++;
                        if (courseResLen == questionLen) {
                            var response = {
                                resCode: 200,
                                response: courseRes
                            }
                            res.status(200).send(response);
                        }
                        /************* *********************************************************/
                    } else {
                        if (courseResLen == questionLen) {
                            var response = {
                                resCode: 200,
                                response: courseRes
                            }
                            res.status(200).send(response);
                        }
                    }

                }); ////end of middle select

            }); //end of first loop

        } else {
            res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
        }
    });
});
/*
 * Used to delete RPL Additional File
 * Created On : 23-02-2017
 * Modified On
 */
router.post('/deleteRPLAdditionalFile', customodule.verifyapicalltoken, function (req, res, next) {
    var dbsetting = req.db_setting;
    var additionalFileId = req.body.additionalFileId;
    if (!additionalFileId) {
        res.status(201).send(RESPONSEMESSAGE.STUDENT.IDREQUIRED);
    } else if (isNaN(additionalFileId)) {
        res.status(201).send(RESPONSEMESSAGE.STUDENT.IDREQUIRED);
    } else {
        var param = {

            tablename: 'mdl_rpl_question_additional_file',
            condition: {
                'id': additionalFileId
            },
        };
        Mastermodel.commonDelete(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (success) {
            var response = {
                resCode: 200,
                resMessage: "File deleted successfully",
                response: "File deleted successfully"
            };
            res.status(200).send(response);
        });
    }
});

// POST /charge
router.post('/modeCharge', customodule.verifyapicalltoken, function (req, res, next) {

    var schdule_id = req.body.schdule_id;
    if (!schdule_id) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.NOSCHDULEID);
    } else if (isNaN(schdule_id)) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.NOSCHDULEID);
    } else {


        var fields = " price ";
        var condition = " where id = '" + schdule_id + "'  ";
        var param = {

            from: 'from mdl_trainer_schdule',
            condition: condition,
            fields: fields
        };

        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (schduleRes) {

            var amount = schduleRes[0].price;
            if (amount > 0) {

                charge(req, amount).then(data => {
                    var dbsetting = req.db_setting;
                    var transaction_id = data.id;
                    var transaction_amount = data.amount;
                    var transaction_date = data.created;
                    var transaction_currency = data.currency;
                    var description = data.description;
                    var payer_id = data.source.id;
                    var payer_name = data.source.name;

                    var course_id = req.body.course_id;
                    var unit_id = req.body.unit_id;
                    var schdule_id = req.body.schdule_id;
                    var trainer_id = req.body.trainer_id;
                    var studentId = req.body.studentId;
                    var learning_id = req.body.learning_id;

                    if (!schdule_id) {
                        res.status(201).send(RESPONSEMESSAGE.COMMON.REQUIREDFIELD);
                    } else {
                        var fields = " mt.id , mu.email";
                        var condition = " where mt.id ='" + schdule_id + "'";
                        var join = " LEFT JOIN mdl_user mu ON mt.trainerid = mu.id";
                        var param = {
                            from: 'from mdl_trainer_schdule mt',
                            condition: condition,
                            fields: fields,
                            join: join
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (ansRes) {
                            if (ansRes.length == 0) {
                                res.status(201).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
                            } else {
                                //console.log("payment_id", ansRes[0].payment_id);
                                var d = new Date();
                                var month = d.getMonth() + 1;
                                var datetime = d.getFullYear() + "-" + month + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();

                                payer_name = ansRes[0].email;

                                //console.log(payer_name);
                                //return false;
                                /*if (payer_name == "" || payer_name == null)
                                    payer_name = trainer_email;*/

                                var saveData = {};
                                saveData.transaction_id = transaction_id;
                                saveData.amount = transaction_amount;
                                saveData.date = datetime;
                                saveData.currency = transaction_currency;
                                saveData.description = description;
                                saveData.payer_id = payer_id;
                                saveData.payer_name = payer_name;
                                saveData.student_id = studentId;
                                saveData.trainer_id = trainer_id;
                                saveData.learning_mode_type_id = learning_id;
                                saveData.type = 'vc';
                                saveData.course_id = course_id;
                                saveData.unit_id = unit_id;
                                //saveData.stripe_details         = data;
                                var param = {

                                    tablename: 'mdl_payment_details',
                                    data: [saveData]
                                }
                                Mastermodel.commonInsert(param, function (err) {
                                    res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                                }, function (data) {
                                    var paymentId = data.insertId;
                                    var d = new Date();
                                    var month = d.getMonth() + 1;
                                    var datetime = d.getFullYear() + "-" + month + "-" + d.getDate();
                                    var saveData = {};
                                    saveData.student_id = studentId;
                                    saveData.course_id = course_id;
                                    saveData.unit_id = unit_id;
                                    saveData.trainer_id = trainer_id;
                                    saveData.schdule_id = schdule_id;
                                    saveData.payment_amount = transaction_amount;
                                    saveData.learning_mode_type_id = learning_id;
                                    saveData.payment_id = paymentId;
                                    saveData.payment_date = datetime;
                                    var param = {

                                        tablename: 'mdl_student_booked_vc',
                                        data: [saveData]
                                    }
                                    Mastermodel.commonInsert(param, function (err) {
                                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                                        res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                                    }, function (data) {
                                        var saveData = {};
                                        saveData.course_id = course_id;
                                        saveData.unit_id = unit_id;
                                        saveData.trainer_id = trainer_id;
                                        saveData.student_id = studentId;
                                        saveData.learning_mode_type_id = learning_id;
                                        saveData.date = datetime;
                                        var param = {

                                            tablename: 'mdl_student_trainer_list',
                                            data: [saveData]
                                        }
                                        Mastermodel.commonInsert(param, function (err) {
                                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                                            res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                                        }, function (data) {
                                            var response = {
                                                resCode: 200,
                                                resMessage: "Schdule added successfully",
                                                response: "Schdule added successfully"
                                            };
                                            res.status(200).send(response);
                                        });
                                    });
                                });
                            } //end of if
                        });
                    } //end of else
                    //res.render('thanks');
                }).catch(error => {
                    var stripeErrObj = {};
                    stripeErrObj.resCode = 201;
                    stripeErrObj.response = error.message;
                    console.log(error);
                    res.status(201).send(stripeErrObj);
                    //res.render('error', error);
                });

            } else {
                res.status(201).send(RESPONSEMESSAGE.COMMON.NOPRICE);
            }

        });


    }


});
/*
 * Used  for get payment details
 * Created On : 13-03-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/getPaymentDetails', customodule.verifyapicalltoken, function (req, res, next) {
    var dbsetting = req.db_setting;
    var rplId = req.body.rplId;
    var schduleId = req.body.schduleId;
    var gapTraningId = req.body.gapTraningId;
    var quid = req.body.quid;
    if (rplId > 0) { // RPL payment
        /** condition to get the payment detail as per the learning type id*/
        var fields = "course_id, unit_id, trainer_id";
        var condition = " where id ='" + rplId + "'   ";
        var param = {

            from: 'from mdl_rpl_process_submission_master',
            condition: condition,
            fields: fields
        };
    } else if (schduleId > 0) { // Trainer Schdule
        var fields = "course_id, unit_id, trainerid as trainer_id , price, learning_mode_type_id";
        var condition = " where id ='" + schduleId + "'   ";
        var param = {

            from: 'from mdl_trainer_schdule',
            condition: condition,
            fields: fields
        };
    } else if (gapTraningId > 0) { // gap Traning
        var fields = "course_id, unit_id, trainer_id";
        var condition = " where id ='" + gapTraningId + "'   ";
        var param = {

            from: 'from mdl_rpl_process_submission_master',
            condition: condition,
            fields: fields
        };
    }
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (ansRes) {
        if (ansRes.length > 0) {
            var courseId = ansRes[0].course_id;
            var unitId = ansRes[0].unit_id;
            var trainerId = ansRes[0].trainer_id;
            if (schduleId > 0) {
                var price = ansRes[0].price;
                var learning_mode_type_id = ansRes[0].learning_mode_type_id;
            }
            async.parallel({
                    course: function (callback) {
                        var fields = "name, idnumber,course_image";
                        var condition = " where id ='" + courseId + "'   ";
                        var param = {

                            from: 'from mdl_course_categories',
                            condition: condition,
                            fields: fields
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (courseRes) {
                            callback('', courseRes);
                        });
                    },
                    unit: function (callback) {
                        var fields = "fullname, idnumber,uprice";
                        var condition = " where id ='" + unitId + "'   ";
                        var param = {

                            from: 'from mdl_course',
                            condition: condition,
                            fields: fields
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (unitRes) {
                            callback('', unitRes);
                        });
                    },
                    trainer: function (callback) {
                        var fields = "firstname, lastname , profile_image, socialimageurl";
                        var condition = " where id ='" + trainerId + "'   ";
                        var param = {

                            from: 'from mdl_user',
                            condition: condition,
                            fields: fields
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (trainerRes) {
                            callback('', trainerRes);
                        });
                    },
                    trainerRating: function (callback) {
                        var fields = " AVG(rating) as avg_rating";
                        var condition = "where id != 0 and trainer_id = " + trainerId + "   ";
                        var param = {

                            from: 'from mdl_trainer_rating ',
                            condition: condition,
                            fields: fields
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (trainerRating) {
                            callback('', trainerRating);
                        });
                    },
                    gapTraningQuestion: function (callback) {
                        if (gapTraningId > 0) {
                            var fields = "m2.* ";
                            var condition = "where m1.masterid = '" + gapTraningId + "' AND m2.id in (" + quid + ") AND m1.is_accepted = 'N' AND m1.is_action_taken = 'Y' ";
                            var join = "LEFT JOIN mdl_rpl_question as m2   ON m1.question_id = m2.id  ";
                            var param = {

                                from: 'from mdl_rpl_process_question_evidence as m1 ',
                                condition: condition,
                                fields: fields,
                                join: join
                            };
                            Mastermodel.commonSelect(param, function (err) {
                                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                            }, function (gapTraning) {
                                callback('', gapTraning);
                            });
                        } else {
                            callback('', '');
                        }
                    },
                    schdulePrice: function (callback) {
                        if (schduleId > 0) {
                            var fields = "price, learning_mode_type_id ";
                            var condition = " Where id = '" + schduleId + "'  ";
                            var param = {

                                from: 'from mdl_trainer_schdule  ',
                                condition: condition,
                                fields: fields
                            };
                            Mastermodel.commonSelect(param, function (err) {
                                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                            }, function (schdulePrice) {
                                callback('', schdulePrice);
                            });
                        } else {
                            callback('', '');
                        }
                    },
                },
                function (err, results) {
                    var response = {
                        resCode: 200,
                        resMessage: "Payment Details",
                        response: results
                    }
                    res.status(200).send(response);
                });
        } else {
            res.status(201).send(RESPONSEMESSAGE.COMMON.NODATAEXISTS);
        }
    }); //end of select
});


/*
 * Used  for get resume builder details for student
 * Created On : 26-03-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/getResumeDetails', customodule.verifyapicalltoken, function (req, res, next) {
    var dbsetting = req.db_setting;
    var studentId = req.body.studentId;

    /** student exists or not*/
    var fields = "id";
    var condition = " where id ='" + studentId + "'   ";
    var param = {

        from: 'from mdl_user',
        condition: condition,
        fields: fields
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (usrRes) {
        if (usrRes.length > 0) {

            async.parallel({
                    contact: function (callback) {
                        var fields = "*";
                        var condition = " where studentid ='" + studentId + "'   ";
                        var param = {

                            from: 'from mdl_resume_contact_information',
                            condition: condition,
                            fields: fields
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (contactRes) {
                            callback('', contactRes[0]);
                        });
                    },
                    key: function (callback) {
                        var fields = "*";
                        var condition = " where studentid ='" + studentId + "'   ";
                        var param = {

                            from: 'from mdl_resume_key_qualifications',
                            condition: condition,
                            fields: fields
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (keyRes) {
                            callback('', keyRes[0]);
                        });
                    },
                    objective: function (callback) {
                        var fields = "*";
                        var condition = " where studentid ='" + studentId + "'   ";
                        var param = {

                            from: 'from mdl_resume_objective',
                            condition: condition,
                            fields: fields
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (objectiveRes) {
                            callback('', objectiveRes[0]);
                        });
                    },
                    coverletter: function (callback) {
                        var fields = " *";
                        var condition = "where  studentid = " + studentId + "   ";
                        var param = {

                            from: 'from mdl_resume_cover_letter ',
                            condition: condition,
                            fields: fields
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (coverletterRes) {
                            callback('', coverletterRes[0]);
                        });
                    },
                    workExperience: function (callback) {

                        var fields = "* ";
                        var condition = "where  studentid = " + studentId + " ";
                        var param = {

                            from: 'from mdl_work_experiences ',
                            condition: condition,
                            fields: fields
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (workExperienceRes) {
                            callback('', workExperienceRes);
                        });

                    },
                    education: function (callback) {

                        var fields = "*";
                        var condition = " Where studentid = " + studentId + "  ";
                        var param = {

                            from: 'from mdl_education  ',
                            condition: condition,
                            fields: fields
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (educationRes) {
                            callback('', educationRes);
                        });

                    },

                    otherExperience: function (callback) {

                        var fields = "*";
                        var condition = " Where studentid = " + studentId + "  ";
                        var param = {

                            from: 'from mdl_resume_other_experiences  ',
                            condition: condition,
                            fields: fields
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (otherRes) {
                            callback('', otherRes[0]);
                        });

                    },
                    templates: function (callback) {
                        var fields = "t.*,tm.name,tm.dynamic_body";
                        var condition = " Where studentid = " + studentId + "  ";
                        var join = " left join mdl_resume_template_master as tm  on tm.id=t.templateid  ";
                        var param = {

                            from: 'from mdl_resume_template t ',
                            condition: condition,
                            fields: fields,
                            join: join
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (templateRes) {
                            callback('', templateRes[0]);
                        });

                    },
                },
                function (err, results) {
                    var response = {
                        resCode: 200,
                        resMessage: "Student Resume Details",
                        response: results
                    }
                    res.status(200).send(response);
                });
        } else {
            res.status(201).send(RESPONSEMESSAGE.COMMON.NODATAEXISTS);
        }
    }); //end of select
});

/*
 * Used to delete user access token by token and userid
 * Created On : 06-04-2018
 * Modified On
 */
router.post('/deleteUserAccessToken', customodule.verifyapicalltoken, function (req, res) {
    var accessToken = req.body.accessToken;
    var userId = req.body.userId;
    if (!accessToken) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.REQUIREDACCESSTOKEN);
    } else if (!userId) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.REQUIREDUSERID);
    } else if (isNaN(userId)) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.INVALIDUSERID);
    } else {

        var condition = "where user_id = " + userId + "  AND token = '" + accessToken + "' ";
        var param = {
            from: 'from mdl_token_secret',
            condition: condition,
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (tokenRes) {
            if (tokenRes.length > 0) {

                var param = {
                    tablename: 'mdl_token_secret',
                    condition: {
                        'user_id': userId,
                        'token': accessToken
                    },
                };

                Mastermodel.commonDelete(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (success) {
                    var response = {
                        resCode: 200,
                        response: "Access Token deleted successfully"
                    };
                    res.status(200).send(response);
                });
            } else {

                res.status(201).send(RESPONSEMESSAGE.RESUME.NODATAFOUND);
            }
        });

    }
});

router.post('/getStudentPreference', customodule.verifyapicalltoken, function (req, res, next) {
    var dbsetting = req.db_setting;
    var studentId = req.body.studentId;

    /** student exists or not*/
    var fields = "id";
    var condition = " where id ='" + studentId + "' and  usertype = 'Student'  ";
    var param = {
        from: 'from mdl_user',
        condition: condition,
        fields: fields
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (usrRes) {
        if (usrRes.length > 0) {

            async.parallel({


                    learningattibute: function (callback) {

                        var fields = "* ";
                        var condition = "where  1 ";
                        var param = {
                            from: 'from mdl_learning_attribute ',
                            condition: condition,
                            fields: fields
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (learningattibuteRes) {
                            callback('', learningattibuteRes);
                        });

                    },
                    groupbehaviour: function (callback) {

                        var fields = "*";
                        var condition = " Where 1  ";
                        var param = {

                            from: 'from mdl_group_behaviour_type  ',
                            condition: condition,
                            fields: fields
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (groupbehaviourRes) {
                            callback('', groupbehaviourRes);
                        });

                    },

                    languagetype: function (callback) {

                        var fields = "*";
                        var condition = " Where 1  ";
                        var param = {
                            from: 'from mdl_language_type  ',
                            condition: condition,
                            fields: fields
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (languagetypeRes) {
                            callback('', languagetypeRes);
                        });

                    },
                    timeslot: function (callback) {
                        var fields = "*";
                        var condition = " Where 1 ";
                        var param = {
                            from: 'from mdl_time_slot ',
                            condition: condition,
                            fields: fields
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (timeslotRes) {
                            callback('', timeslotRes);
                        });

                    },
                },
                function (err, results) {
                    var response = {
                        resCode: 200,
                        resMessage: "Student Preference Details",
                        response: results
                    }
                    res.status(200).send(response);
                });
        } else {
            res.status(201).send(RESPONSEMESSAGE.COMMON.NODATAEXISTS);
        }
    }); //end of select
});


router.post('/getTrainerPreference', customodule.verifyapicalltoken, function (req, res, next) {
    var dbsetting = req.db_setting;
    var trainerId = req.body.trainerId;

    /** student exists or not*/
    var fields = "id";
    var condition = " where id ='" + trainerId + "'  and  usertype = 'Trainer' ";
    var param = {
        from: 'from mdl_user',
        condition: condition,
        fields: fields
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (usrRes) {
        if (usrRes.length > 0) {

            async.parallel({


                    trainingattibute: function (callback) {

                        var fields = "* ";
                        var condition = "where  1 ";
                        var param = {
                            from: 'from mdl_training_attribute ',
                            condition: condition,
                            fields: fields
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (trainingattibuteRes) {
                            callback('', trainingattibuteRes);
                        });

                    },
                    groupbehaviour: function (callback) {

                        var fields = "*";
                        var condition = " Where 1  ";
                        var param = {

                            from: 'from mdl_group_behaviour_type  ',
                            condition: condition,
                            fields: fields
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (groupbehaviourRes) {
                            callback('', groupbehaviourRes);
                        });

                    },

                    languagetype: function (callback) {

                        var fields = "*";
                        var condition = " Where 1  ";
                        var param = {
                            from: 'from mdl_language_type  ',
                            condition: condition,
                            fields: fields
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (languagetypeRes) {
                            callback('', languagetypeRes);
                        });

                    },
                    timeslot: function (callback) {
                        var fields = "*";
                        var condition = " Where 1 ";
                        var param = {
                            from: 'from mdl_time_slot ',
                            condition: condition,
                            fields: fields
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (timeslotRes) {
                            callback('', timeslotRes);
                        });

                    },
                },
                function (err, results) {
                    var response = {
                        resCode: 200,
                        resMessage: "Trainer Preference Details",
                        response: results
                    }
                    res.status(200).send(response);
                });
        } else {
            res.status(201).send(RESPONSEMESSAGE.COMMON.NODATAEXISTS);
        }
    }); //end of select
});
/*
 * Used to add career quiz
 * Created On : 06-04-2018
 * Modified On
 */

router.post('/addEditCareerQuiz', customodule.verifyapicalltoken, function (req, res) {


    var question = req.body.question;
    var category = req.body.category;
    var multiAns1 = req.body.multiAns1;
    var multiAns2 = req.body.multiAns2;
    var multiAns3 = req.body.multiAns3;
    var multiAns4 = req.body.multiAns4;
    var multipleans = req.body.multipleans;
    var ansArr = req.body.ansArr;

    if (!question) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.PROVIDEQUESTION);
    } else if (!category) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.PROVIDECATEGORY);
    } else if (isNaN(category)) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.PROVIDECATEGORY);
    } else {


        var saveData = {};
        saveData.question = question;
        saveData.course_type_id = req.body.category;
        saveData.question_type = "Multiple Choice";
        var d = new Date();
        var m1 = d.getMonth();
        var month = m1 + 1;
        var datetime = d.getFullYear() + "-" + month + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
        saveData.created_on = datetime;
        var param = {
            tablename: 'mdl_carrer_quiz_question',
            data: [saveData]
        };
        Mastermodel.commonInsert(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
        }, function (data) {
            var total = ansArr.length;
            var qcountArr = [];

            asyncForEach(ansArr, function (positionitem, positionindex, positionarr) {


                var j = positionindex;

                console.log(j, multipleans);



                if (multipleans == j + 1)
                    right_answer = 1;
                else
                    right_answer = 0;

                var saveData = {};
                saveData.question_id = data.insertId;
                saveData.option_id = j + 1;
                saveData.answer_choices = positionitem;
                saveData.right_answer = right_answer;
                saveData.created_on = datetime;


                var param = {
                    tablename: 'mdl_career_quiz_answer',
                    data: [saveData]
                };
                Mastermodel.commonInsert(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                }, function (data) {
                    qcountArr.push(positionindex);
                    var questionLen = qcountArr.length;

                    if (total == questionLen) {

                        var response = {
                            resCode: 200,
                            resMessage: "Career Quiz Question Added  successfully",
                            response: "Career Quiz Question Added successfully"
                        };
                        res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.STUDENTQUALIFIED);

                    }

                });

            });
        });




    } //end of else
}); //end of post

router.post('/deleteCareerQuiz', customodule.verifyapicalltoken, function (req, res) {

    console.log(req.body);
    var questionId = req.body.questionId;


    if (!questionId) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.INVALIDDATAPROVIDED);
    } else if (isNaN(questionId)) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.INVALIDDATAPROVIDEDl);
    } else {

        var param = {
            tablename: 'mdl_carrer_quiz_question',
            condition: {
                'id': questionId
            },
        };

        Mastermodel.commonDelete(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (success) {

            var param = {
                tablename: 'mdl_career_quiz_answer',
                condition: {
                    'question_id': questionId
                },
            };
            Mastermodel.commonDelete(param, function (err) {
                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            }, function (success) {

                var response = {
                    resCode: 200,
                    resMessage: "Question deleted successfully",
                    response: "Question deleted successfully"
                };
                res.status(200).send(response);

            });

        });

    } //end of else
}); //end of post

/*
 * Used to get Random Student Course
 * Created On : 21-05-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/getRandomCourse', customodule.verifyapicalltoken, function (req, res, next) {
    var student_id = req.body.studentId;
    if (!student_id) {
        res.status(201).send(RESPONSEMESSAGE.RPL.STUDENTREQUIRED);
    } else if (isNaN(student_id)) {
        res.status(201).send(RESPONSEMESSAGE.RPL.STUDENTIDREQUIRED);
    } else {
        var fields = " mcc.* ";
        var condition = " where 1   ";
        var orderby = " order by rand() ";
        var limit = " 0,5 ";
        var param = {
            from: 'from mdl_course_categories as mcc',
            condition: condition,
            fields: fields,
            orderby: orderby,
            limit: limit
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (courseRes) {
            if (courseRes.length > 0) {
                var response = {
                    resCode: 200,
                    response: courseRes
                }
                res.status(200).send(response);
            } else { // no saved data
                var response = {
                    resCode: 201,
                    response: "No Records Found."
                }
                res.status(201).send(response);
            }
        });
    }
});


/*
 * Used to Save Student Feedback
 * Created On : 21-05-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/saveStudentFeedback', customodule.verifyapicalltoken, function (req, res, next) {

    var user_id = req.body.userId;
    var subject = req.body.subject;
    var message = req.body.message;
    if (!user_id) {
        res.status(201).send(RESPONSEMESSAGE.RPL.USERIDREQUIRED);
    } else if (isNaN(user_id)) {
        res.status(201).send(RESPONSEMESSAGE.RPL.USERIDREQUIRED);
    } else if (!subject) {
        res.status(201).send(RESPONSEMESSAGE.RPL.SUBJECTREQUIRED);
    } else if (!message) {
        res.status(201).send(RESPONSEMESSAGE.RPL.MESSAGEREQUIRED);
    } else {

        var d = new Date();
        var m1 = d.getMonth();
        var month = m1 + 1;
        var datetime = d.getFullYear() + "-" + month + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
        var saveData = {};
        saveData.user_id = user_id;
        saveData.subject = subject;
        saveData.message = message;
        saveData.created_on = datetime;
        var param = {
            tablename: 'mdl_user_feedback',
            data: [saveData]
        };
        Mastermodel.commonInsert(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
        }, function (data) {

            var fields = " mu.firstname, mu.email ";
            var condition = " where id ='" + user_id + "' ";
            var param = {
                from: 'from mdl_user as mu',
                condition: condition,
                fields: fields
            };

            Mastermodel.commonSelect(param, function (err) {
                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            }, function (userRes) {
                if (userRes.length > 0) {

                    /******** Mail To Admin *******/
                    var emailObj = {
                        email: SETTING.ADMIN_EMAIL,
                        subject: subject,
                        message: message
                    }
                    customodule.mail('adminFeedbackEmail', emailObj);

                    /******** Mail To User *******/
                    var emailObj1 = {
                        email: userRes[0].email,
                        name: userRes[0].firstname
                    }
                    customodule.mail('userFeedbackEmail', emailObj1);

                    var response = {
                        resCode: 200,
                        response: "Feedback has been sent successfully."
                    }
                    res.status(200).send(response);
                } else { // no saved data
                    var response = {
                        resCode: 201,
                        response: "No Records Found."
                    }
                    res.status(201).send(response);
                }
            });
        });
    }
});


// test charge
router.post('/testCharge3D', customodule.verifyapicalltoken, function (req, res, next) {

    var amount = 250;
    charge(req, amount).then(data => {

          var response = {
              resCode: 200,
              response: data
          }
        res.status(200).send(response);
    }).catch(error => {
        var stripeErrObj = {};
        stripeErrObj.resCode = 201;
        stripeErrObj.response = error.message;
        console.log(error);
        res.status(201).send(stripeErrObj);
        //res.render('error', error);
    });

});

module.exports = router;