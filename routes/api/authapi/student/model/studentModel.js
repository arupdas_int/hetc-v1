var express = require('express');
var router = express.Router();
var app = express();
var customodule = require('../../../../customexport');
var node_module = require('../../../../export');
var commonFunction = require('../../../../../config/commonFunction');
var sendGroupNotification = require('../../../../../lib/sendnotification');

var RESPONSEMESSAGE = require('../../../../../config/statuscode');
var NOTIFICATIONMESSAGE = require('../../../../../config/notificationcode');
var Mastermodel = require('../../../mastermodel.js');
var Masternotificationmodel = require('../../../commonPushNotificationModel.js');
var asyncForEach = require('async-foreach').forEach;
var database = require('../../../../../config/database');
var SETTING = require('../../../../../config/setting');
var mysql = require('mysql');
var connection = mysql.createConnection(database);

var model = {
    //******************************* GET BRANCH LIST MODEL*****************
    getBranchListModel: function(requestdata, errorcallback, successcallback) {
        var mysql = require('mysql');
        var pool = mysql.createPool({
            host: SETTING.dbhost,
            user: SETTING.user,
            password: SETTING.password,
            database: requestdata.schoolCode
        });
        pool.getConnection(function(err, connection) {
            if (err) {
                errorcallback(ERROR.COMMON.ERRCONNECTDB);
            } else {
                var getTotBranchSql = "select count(*) AS TOTCNT from master_branches";
                connection.query(getTotBranchSql, function(err, res) {
                    if (err) {
                        errorcallback(ERROR.COMMON.SOMETHINGWRONG);
                    } else {
                        console.log(res[0]['TOTCNT']);
                        var pageNo = requestdata.pageNo;
                        var pageLimit = requestdata.pageLimit;
                        if (pageNo == 1) {
                            var limit = pageNo - 1;
                            var offset = pageLimit;
                        } else {
                            var limit = ((pageNo - 1) * pageLimit);
                            var offset = pageLimit;
                        }
                        var getBranchSql = "select * from master_branches WHERE status = 1 ORDER BY id DESC LIMIT " + limit + "," + offset + " ";
                        console.log(getBranchSql);
                        connection.query(getBranchSql, function(err, rows) {
                            connection.release();
                            if (err) {
                                errorcallback(ERROR.COMMON.SOMETHINGWRONG);
                            } else {
                                successcallback(rows, res[0]['TOTCNT']);
                            }
                        });
                    }
                });
            }

        });
    },
    saveRplEvidenceModel: function(questionArr, requestParam, errorcallback, successcallback) {
        var rplEvidenceArr = [];
        var masterId = requestParam.rpl_process_submission_id;
        var dbsetting = requestParam.dbsetting;
        var totalQuestioncnt = questionArr.length;
        // delete previous submission evidence and insert all the evidence again
        var param = {
            dbsetting: dbsetting,
            tablename: 'mdl_rpl_question_additional_file',
            condition: {
                'masterid': masterId
            },
        };
        Mastermodel.commonDelete(param, function(err) {
            console.log(err);
            res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function(success) {
            var param = {
                dbsetting: dbsetting,
                tablename: 'mdl_rpl_process_question_evidence',
                condition: {
                    'masterid': masterId
                },
            };
            Mastermodel.commonDelete(param, function(err) {
                console.log(err);
                res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            }, function(success) {
                node_module.asyncForEach(questionArr, function(questionItem, questionIndex, questionArr) {
                    var evidenceData = {};
                    console.log('rpl evidenc count ', rplEvidenceArr.length)
                    evidenceData.masterid = masterId;
                    evidenceData.question_id = questionArr[questionIndex].questionId;
                    evidenceData.question_type = questionArr[questionIndex].questionType;
                    evidenceData.student_answer = questionArr[questionIndex].studentans;
                    evidenceData.correct_answer = questionArr[questionIndex].correctans;
                    evidenceData.additional_comments = questionArr[questionIndex].addicomments;
                    if (questionArr[questionIndex].questionType == 'Upload') {
                        evidenceData.file = questionArr[questionIndex].studentans;
                    } else {
                        evidenceData.file = '';
                    }
                    console.log(evidenceData);
                    var evidenceParam = {
                        dbsetting: dbsetting,
                        tablename: 'mdl_rpl_process_question_evidence',
                        data: [evidenceData]
                    }
                    Mastermodel.commonInsert(evidenceParam, function(err) {
                        res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                    }, function(evidenceSuccEntrydata) {
                        var evidence_id = evidenceSuccEntrydata.insertId;
                        /** now check the is there any additional file submitted or not */
                        var isAddFilesSubmitted = 'N';
                        if (questionArr[questionIndex].addfiles == undefined) {
                            isAddFilesSubmitted = 'N';
                        } else {
                            if (questionArr[questionIndex].addfiles.length > 0) {
                                isAddFilesSubmitted = 'Y';
                            }
                        }
                        console.log('additional file length', questionArr[questionIndex].addfiles);
                        console.log('additional file status' + isAddFilesSubmitted);
                        if (isAddFilesSubmitted == 'Y') {
                            //save the additinal file
                            var additionalFileArr = [];
                            var additionalFileCnt = questionArr[questionIndex].addfiles.length;
                            node_module.asyncForEach(questionArr[questionIndex].addfiles, function(additionalItem, additionalIndex, additionalArr) {
                                var additionalFileData = {};
                                additionalFileData.evedence_id = evidence_id;
                                additionalFileData.masterid = masterId;
                                additionalFileData.question_id = questionArr[questionIndex].questionId;
                                additionalFileData.filename = additionalArr[additionalIndex].filename;
                                additionalFileData.originalfile = additionalArr[additionalIndex].originalfile;
                                additionalFileData.comment = additionalArr[additionalIndex].comment;
                                var additionalFileParam = {
                                    dbsetting: dbsetting,
                                    tablename: 'mdl_rpl_question_additional_file',
                                    data: [additionalFileData]
                                }
                                Mastermodel.commonInsert(additionalFileParam, function(err) {
                                    res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                                }, function(data) {
                                    additionalFileArr.push({
                                        "addFileId": additionalArr[additionalIndex].filename
                                    });
                                    if (additionalFileArr.length == additionalFileCnt) {
                                        //add the question id in evidence array to match with the question count
                                        rplEvidenceArr.push({
                                            "questionId": questionArr[questionIndex].questionId
                                        });
                                    }
                                    console.log('*** final submission', rplEvidenceArr.length, totalQuestioncnt, additionalFileArr.length, additionalFileCnt);
                                    if (rplEvidenceArr.length == totalQuestioncnt && additionalFileArr.length == additionalFileCnt) {
                                        var response = {
                                            resCode: 200,
                                            resData: {
                                                rpl_process_submission_id: masterId,
                                                attempt: requestParam.attempt
                                            },
                                            response: {
                                                rpl_process_submission_id: masterId,
                                                resMessage: "RPL process saved successfully"
                                            },
                                            resMessage: "RPL process saved successfully"
                                        }
                                        successcallback(response);
                                    }
                                });
                            });
                        } else {
                            rplEvidenceArr.push({
                                "questionId": questionArr[questionIndex].questionId
                            });
                            if (rplEvidenceArr.length == totalQuestioncnt) {
                                var response = {
                                    resCode: 200,
                                    resData: {
                                        rpl_process_submission_id: masterId,
                                        attempt: requestParam.attempt
                                    },
                                    response: {
                                        rpl_process_submission_id: masterId,
                                        resMessage: "RPL process saved successfully"
                                    },
                                    resMessage: "RPL process saved successfully"
                                }
                                successcallback(response);
                            }
                        }
                    });
                });
            });
        });
    },

}
module.exports = model;