var express = require('express');
var router = express.Router();
var app = express();
var customodule = require('../../../../customexport');
var node_module = require('../../../../export');
var RESPONSEMESSAGE = require('../../../../../config/statuscode');
var PERMISSION = require('../../../../../config/permission');
var Adminmodel = require('../model/adminModel');
var Mastermodel = require('../../../mastermodel.js');
var striptags = require('striptags');
/* Role Management Api Start */

/* This api is to create school and only admin can access this api
   Created  : 08-05-2017
*/
var dbName;
var counter = 0;


/*
* Used to get user profile details by field name (email, firstname, lastname)
* Created On : 07-07-2017
* Modified On
*/
router.post('/getUserByField', customodule.authenticate, function(req, res, next) {
    	if(req.body.uservalue == '' ){
    		var reqkey = "";
    	}else if (req.body.userkey != '' && (req.body.userkey == 'email' ||
    			  req.body.userkey == 'firstname' || req.body.userkey == 'lastname')) {
        	var reqkey = req.body.userkey;
            var reqvalue = "%" + req.body.uservalue + "%";
        }
    req.headers.init.then(function(client) {
        client.call({
            wsfunction: "core_user_get_users",
            method: "POST",
            args: {
                criteria: [{
                    "key": reqkey,
                    "value": reqvalue
                }]
            }
        }).then(function(info) {
			if(JSON.stringify(info.exception) != undefined){
        		var response = {
                    resCode: 201,
                    resMessage: JSON.parse(JSON.stringify(info.message))
                }
        	}else if(info.users.length==0){
        		var response = {
                    resCode: 201,
                    resMessage: RESPONSEMESSAGE.COMMON.NORECORDFOUND
                }
        	}else{
        		var response = info;
        	}
           	res.status(200).send(response);
        });
    })
});

/*
* Used to get user profile details by id
* Created On : 07-07-2017
* Modified On
*/
router.post('/getUserById', customodule.authenticate, function(req, res, next) {
    	if(req.body.userkey == 'id' && req.body.uservalue != '' ){
    		var reqkey = req.body.userkey;
            var reqvalue = req.body.uservalue;
    	}else {
        	var reqkey = "";
        }
    req.headers.init.then(function(client) {
        client.call({
            wsfunction: "core_user_get_users",
            method: "POST",
            args: {
                criteria: [{
                    "key": reqkey,
                    "value": reqvalue
                }]
            }
        }).then(function(info) {
			if(JSON.stringify(info.exception) != undefined){
        		var response = {
                    resCode: 201,
                    resMessage: JSON.parse(JSON.stringify(info.message))
                }
        	}else if(info.users.length==0){
        		var response = {
                    resCode: 201,
                    resMessage: RESPONSEMESSAGE.COMMON.NORECORDFOUND
                }
        	}else{
        		var response = info;
        	}
           	res.status(200).send(response);
        });
    })
});

/*
* Used to get list of all user profile
* Created On : 07-07-2017
* Modified On
*/
router.post('/GetUserLIst', customodule.authenticate, function(req, res, next) {
    req.headers.init.then(function(client) {
        client.call({
            wsfunction: "core_user_get_users",
            method: "POST",
            args: {
                criteria: [{
                    "key": 'email',
                    "value": "%%"
                }]
            }
        }).then(function(info) {
			if(JSON.stringify(info.exception) != undefined){
        		var response = {
                    resCode: 201,
                    resMessage: JSON.parse(JSON.stringify(info.message))
                }
        	}else if(info.users.length==0){
        		var response = {
                    resCode: 201,
                    resMessage: RESPONSEMESSAGE.COMMON.NORECORDFOUND
                }
        	}else{
        		var response = info;
        	}
           	res.status(200).send(response);
        });
    })
});

/*
* Used to Create new user
* Created On : 06-07-2017
* Modified On
*/
router.post('/createUser', customodule.authenticate, function(req, res, next) {
    req.headers.init.then(function(client) {
    	console.log(req.body);
        client.call({
            wsfunction: "core_user_create_users",
            method: "POST",
            args: {
            	users:[req.body]
            	}
        }).then(function(info) {
           if(JSON.stringify(info.exception) == undefined){
                var response = {
                    resCode: 201,
                    resMessage: RESPONSEMESSAGE.USER.REGISTERSUCCESS
                }
           }else{
                var response = {
                    resCode: 200,
                    resMessage: RESPONSEMESSAGE.USER.USERALREADYEXIST
                }
           }
           res.status(200).send(response);
        })
    })
});

/*
* Used to update user profile
* Created On : 07-07-2017
* Modified On
*/
router.post('/updateUser', customodule.authenticate, function(req, res, next) {
    req.headers.init.then(function(client) {
        client.call({
            wsfunction: "core_user_update_users",
            method: "POST",
            args: {
            	users: [req.body]
            	}
        }).then(function(info) {console.log(info);
           if(info == null){
                var response = {
                    resCode: 201,
                    resMessage: RESPONSEMESSAGE.USER.UPDATESUCCESS
                }
           }else{
                var response = {
                    resCode: 200,
                    resMessage: JSON.parse(JSON.stringify(info.message))
                }
           }
           res.status(200).send(response);
        })
    })
});

/*
* Used to get courses list and particular course by single or multiple id
* Created On : 07-07-2017
* Modified On
*/
router.post('/ListCourse', customodule.authenticate, function(req, res, next) {

    req.headers.init.then(function(client) {
    	var courseid = [];
		courseid['options'] = {"ids": req.body};
        client.call({
            wsfunction: "core_course_get_courses",
            method: "POST",
            args: courseid
        }).then(function(info) {
        	if(JSON.stringify(info.exception) != undefined){
        		var response = {
                    resCode: 201,
                    resMessage: JSON.parse(JSON.stringify(info.message))
                }
        	}else if(info.length == ''){
        		var response = {
                    resCode: 201,
                    resMessage: RESPONSEMESSAGE.COMMON.NORECORDFOUND
                }
        	}else{
        		var response = info;
        	}
           res.status(200).send(response);
        })
    })
});

/*
* Used to create new course
* Created On : 06-07-2017
* Modified On
*/
router.post('/createCourse', customodule.authenticate, function(req, res, next) {
    req.headers.init.then(function(client) {
        client.call({
            wsfunction: "core_course_create_courses",
            method: "POST",
            args: {
            	courses:[req.body]
            	}
        }).then(function(info) {
           if(JSON.stringify(info.exception) == undefined){
                var response = {
                    resCode: 200,
                    resMessage: RESPONSEMESSAGE.COURSE.COURSECREATESUCCESS
                }
           }else{
                var response = {
                    resCode: 201,
                    resMessage: JSON.parse(JSON.stringify(info.message))
                }
           }
           res.status(200).send(response);
        })
    })
});

/*
* Used to update course details
* Created On : 07-07-2017
* Modified On
*/
router.post('/editCourse', customodule.authenticate, function(req, res, next) {
    req.headers.init.then(function(client) {
        client.call({
            wsfunction: "core_course_update_courses",
            method: "POST",
            args: {
            	courses:[req.body]
            	}
        }).then(function(info) {
           if(info.warnings.length==0){
                var response = {
                    resCode: 200,
                    resMessage: RESPONSEMESSAGE.COURSE.COURSEUPDATESUCCESS
                }
           }else{
        	var errormessage = JSON.parse(JSON.stringify(info['warnings']));
                var response = {
                    resCode: 201,
                    resMessage: errormessage[0]['message']
                }
           }
           res.status(200).send(response);
        })
    })
});



/*
* Used to delete specified courses
* Created On : 06-07-2017
* Modified On
*/
router.post('/deleteCourseById', customodule.authenticate, function(req, res, next) {
    var courseId = req.body.courseId;
    if(!courseId){
         res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
    }else{
        req.headers.init.then(function(client) {
        client.call({
            wsfunction: "core_course_delete_courses",
            method: "POST",

            args: {"courseids":[req.body.courseId]}
        }).then(function(info) {
           //res.end(JSON.stringify(info));

            if(info.warnings.length==0){

                res.status(200).send(RESPONSEMESSAGE.COURSE.COURSEDELETESUCCESS);

            }else{
                res.status(200).send(RESPONSEMESSAGE.COURSE.COURSEDELETEERROR);
            }


            });
        })
    }
});




/*
* Used to get course details
* Created On : 07-07-2017
* Modified On
*/
router.post('/getCourseDetails', customodule.authenticate, function(req, res, next) {
   var courseId=[];
   courseId['options'] = {"ids": {"0" : req.body.courseId}};


    if(!req.body.courseId){
         res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
    }else{
        req.headers.init.then(function(client) {
        client.call({
            wsfunction: "core_course_get_courses",
            method: "POST",
            args: courseId
        }).then(function(info) {
            if(info.length==0){
            res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
            }else{
            res.end(JSON.stringify(info));
            }
            });
        })
    }
});


/*
* Used to get enrolled users courses
* Created On : 07-07-2017
* Modified On
*/
router.post('/getEnrolledUsersByCourse', customodule.authenticate, function(req, res, next) {
    var courseId = req.body.courseId;
    if(!courseId){
         res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
    }else{
        req.headers.init.then(function(client) {
        client.call({
            wsfunction: "core_enrol_get_enrolled_users",
            method: "POST",

            args: {"courseid":courseId}
        }).then(function(info) {
            if(info.length==0){
            res.status(200).send(RESPONSEMESSAGE.COURSE.NOUSERFOUND);
            }
            else if(info.errorcode=='invalidrecord'){
            res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
            }else{

            res.end(JSON.stringify(info));
            }
            });
        })
    }
});



/*
* Used to get course categories
* Created On : 07-07-2017
* Modified On
*/
router.get('/getAllCourseCategories',customodule.authenticate, function(req, res, next) {
    var courseId = req.body.courseId;



        req.headers.init.then(function(client) {
        client.call({
            wsfunction: "core_course_get_categories",
            method: "POST",

           // args: coursecap
        }).then(function(info) {
            if(info.length==0){
            res.status(200).send(RESPONSEMESSAGE.COURSE.NOTRAINERFOUND);
            }
            else if(info.errorcode=='invalidrecord'){
            res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
            }else{

            res.end(JSON.stringify(info));
            }
            });
        })

});


/*
* Used to create course categories
* Created On : 10-07-2017
* Modified On
*/
//if parent has no value,then 0 has to be sent
router.post('/createCourseCategory',customodule.authenticate, function(req, res, next) {
       if(req.body.name!=''){

        var categories=[];
        categories=[req.body];
        req.headers.init.then(function(client) {
        client.call({
            wsfunction: "core_course_create_categories",
            method: "POST",
             args: {
                categories:categories
                }

        }).then(function(info) {  console.log(JSON.stringify(info));
            if(info.errorcode=='categoryidnumbertaken'){
            res.status(200).send(RESPONSEMESSAGE.COURSECATEGORY.IDNUMBERERROR);
            }else if(info.errorcode=='invalidparameter'){
            res.status(200).send(RESPONSEMESSAGE.COURSECATEGORY.CATEGORYCREATEERROR);
            }else{
             res.status(200).send(RESPONSEMESSAGE.COURSECATEGORY.CATEGORYCREATESUCCESS);
            }
            });
        });

       }else{

             res.status(200).send(RESPONSEMESSAGE.COURSECATEGORY.NOCATEGORYNAMEFOUND);
        }
});


/*
* Used to create course categories
* Created On : 10-07-2017
* Modified On
*/
//if parent has no value,then 0 has to be sent
router.post('/updateCourseCategory',customodule.authenticate, function(req, res, next) {
       if(req.body.name!=''){

        var categories=[];
        categories=[req.body];
        req.headers.init.then(function(client) {
        client.call({
            wsfunction: "core_course_update_categories",
            method: "POST",
             args: {
                categories:categories
                }

        }).then(function(info) {  console.log(JSON.stringify(info));
            if(info==null){
            res.status(200).send(RESPONSEMESSAGE.COURSECATEGORY.CATEGORYUPDATESUCCESS);
            }else{
            res.status(200).send(RESPONSEMESSAGE.COURSECATEGORY.CATEGORYUPDATEERROR);
            }

            });
        });

       }else{

             res.status(200).send(RESPONSEMESSAGE.COURSECATEGORY.NOCATEGORYNAMEFOUND);
        }
});



/*
* Used to get Trainer courses
* Created On : 10-07-2017
* Modified On
*/
router.post('/getTrainersByCourse',customodule.authenticate, function(req, res, next) {
    var courseId=req.body.courseId;
    if(!courseId){
         res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
    }else{

    var coursecapabilities = [];
    coursecapabilities[0]={'courseid' : courseId};
    coursecapabilities[0].capabilities = [];
    coursecapabilities[0].capabilities.push('mod/lesson:addinstance');

        req.headers.init.then(function(client) {
        client.call({
            wsfunction: "core_enrol_get_enrolled_users_with_capability",
            method: "POST",
            args: {
                coursecapabilities:coursecapabilities
                }
        }).then(function(info) { console.log(info);
            if(info.errorcode=='invalidrecord'){
            res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
            }else if(info[0].users.length==0){
            res.status(200).send(RESPONSEMESSAGE.COURSE.NOTRAINERFOUND);
            }else{

            res.end(JSON.stringify(info));
            }
            });
        })
    }
});
/*
* Used to view quiz question and answer
* Created On : 11-07-2017
* Modified On
*/
router.post('/getQuiz',customodule.verifyapicalltoken, function(req, res, next) {

 	var condition = " where quiz.id = '" + req.body.quizid + "'" ;
    var join      =" LEFT JOIN  mdl_quiz_slots as qslots ON quiz.id = qslots.quizid ";
		join	 +=" LEFT JOIN  mdl_question as ques ON qslots.questionid = ques.id ";
    	join	 +=" LEFT JOIN  mdl_question_answers as ans ON qslots.questionid = ans.question ";

    //var fields    =
    var dbsetting = req.db_setting;

    var param = {
        dbsetting:dbsetting,
        from  :'from mdl_quiz as quiz',
        condition : condition,
        join : join,
    }

    Mastermodel.commonSelect(param,function(err){
    	console.log(err);
	},function(data){
		console.log(data);
        if (data.length > 0) {
               var response = {
                    resCode: 200,
                    response: data
                }
                res.status(200).send(response);
        } else {
            res.status(200).send(ERROR.COMMON.NORECORDFOUND);
        }
    })

});


/*
* Used to get quiz name
* Created On : 11-07-2017
* Modified On
*/
router.post('/get_quiz_name',customodule.verifyapicalltoken, function(req, res, next) {

    var dbsetting  = req.db_setting;
    var condition1  = " where quiz.id = '" + req.body.quizid + "'" ;
    var fields1     = " quiz.id as quizId, quiz.name as quizName, quiz.intro as quizDescription, ";
        fields1    += " quiz.overduehandling as quizDue, quiz.graceperiod as quizGraceperiod ";
    var param1 = {
        dbsetting:dbsetting,
        from  :'from mdl_quiz as quiz',
        condition : condition1,
        fields : fields1
    }


    Mastermodel.commonSelect(param1,function(err){
    	console.log(err);
	},function(data){
        if (data.length > 0) {
               var response = {
                    resCode: 200,
                    response: data
                }
                res.status(200).send(response);
        } else {
            res.status(200).send(ERROR.COMMON.NORECORDFOUND);
        }
    })

});


/*
* Used to get quiz ques and answer
* Created On : 14-07-2017
* Modified On
*/
router.post('/get_quiz_ques_ans', customodule.verifyapicalltoken, function(req, res, next) {
    var quizArr = [];
    var quizResponseObj = {};
    var dbsetting = req.db_setting;
    var condition = " where qslot.quizid = '" + req.body.quizid + "' ";
    var fields = " qslot.quizid as qId, qslot.page as qPage, qslot.questionid as qQuestionid, qslot.maxmark as qMaxmark, ";
    fields += " ques.name as quesName, ques.questiontext as quesText, ques.defaultmark as quesDefaultmark, ques.qtype as quesQtype ";
    var join = " LEFT JOIN  mdl_question as ques ON qslot.questionid = ques.id ";
    var param = {
        dbsetting: dbsetting,
        from: 'from mdl_quiz_slots as qslot',
        condition: condition,
        fields: fields,
        join: join
    }

    //1.to get pre assessment test questions

    Mastermodel.commonSelect(param, function(err) {
        console.log(err);
    }, function(data) {
        if (data.length > 0) {
            //now get the total length of data
            var totalQuizLen = data.length;
            //now make a loop for data
            node_module.asyncForEach(data, function(dataItem, dataIndex, dataArr) {
            	console.log('================',dataItem);
                quizResponseObj[dataIndex] = {};
                //quizResponseObj.dataIndex = dataArr;
                quizResponseObj[dataIndex].answer = [];
                quizResponseObj[dataIndex].ques  = [];
                //console.log('question id',dataArr[dataIndex]['qQuestionid']);
                //push th index in predefined array

                //now get the answer as per the quiz
                var condition1 = " where options.question = '" + dataArr[dataIndex]['qQuestionid'] + "'";
                var fields1 = " options.answer as optAnswer, options.answerformat as optAnswerformat, options.fraction as optCorrectanswer ";
                	join1   = " LEFT JOIN  mdl_question as ques ON ques.id =  '" + dataArr[dataIndex]['qQuestionid'] + "' ";
                var param1 = {
                    dbsetting: dbsetting,
                    from: 'from mdl_question_answers as options',
                    condition: condition1,
                    fields: fields1
                }
                Mastermodel.commonSelect(param1, function(err) {
                    console.log(err);
                }, function(data1) {
                    quizArr.push(dataArr[dataIndex]['qQuestionid']);
                    if (data1.length > 0) {
                       // console.log('***********************',data1);
                       //assign question to respective questionnaire
                        quizResponseObj[dataIndex].ques   = dataItem;
                        quizResponseObj[dataIndex].answer = data1;
                    }
                    console.log(dataIndex, quizArr.length, totalQuizLen);
                    //check the predifined array length and the  total quiz lentgh
                    if (quizArr.length == totalQuizLen) {
                        var response = {
                            resCode: 200,
                            response: quizResponseObj
                        }
                        res.status(200).send(response);
                    }
                })
            });
        } else {
            res.status(200).send(ERROR.COMMON.NORECORDFOUND);
        }
    })
});
router.post('/quizStartAttempt',customodule.verifyapicalltoken, function(req, res, next) {
    var quizArr = [];
    var quizResponseObj = {};
    var dbsetting  = req.db_setting;
    var condition  = " where quiz.id = '" + req.body.quizid + "'" ;
    var fields     = " quiz.preferredbehaviour as quizBehaviour, quiz.name as quizname, qcat.contextid as qcatcontextid";
	var join       = " LEFT JOIN mdl_question_categories as qcat ON qcat.name = CONCAT('Default for ', quiz.name)"
    var param = {
        dbsetting:dbsetting,
        from  :'from mdl_quiz as quiz',
        condition : condition,
        join : join,
        fields : fields
    }

    // select quiz name
    Mastermodel.commonSelect(param,function(err){
    	res.status(201).send(err);
	},function(data){
		console.log(data);
		if (data.length > 0) {
        	var param1 = {
		        dbsetting:dbsetting,
		        tablename  :'mdl_question_usages',
		        data : [{"contextid": data[0].qcatcontextid, "component":"mod_quiz", "preferredbehaviour":data[0].quizBehaviour}]
		    }

		    //insert context id in mdl_question_usages
		    Mastermodel.commonInsert(param1,function(err){
		    	res.status(201).send(err);
			},function(data1){
				console.log(data1);
				if (data1.insertId !== undefined) {
					console.log(data1.insertId);
					var layout = "";
					for (var i = 0; i < req.body.questioncount; i++) {
					    layout +=  i+1+",0,";
					}
					if (layout.charAt(layout.length - 1) == ',') {
					  layout = layout.substr(0, layout.length - 1);
					}
					var attempts = {};
					attempts.quiz = req.body.quizid;
					attempts.userid = req.body.userid;
					attempts.attempt = "1";
				    attempts.uniqueid = data1.insertId;
				    attempts.layout = layout;
				    attempts.currentpage = "0";
				    attempts.preview = "1";
					attempts.state = "inprogress";
				    attempts.timestart = Math.floor(Date.now() / 1000);
				    attempts.timefinish = "0";
				    attempts.timemodified = Math.floor(Date.now() / 1000);
				    attempts.timemodifiedoffline = Math.floor(Date.now() / 1000);


				    var param2 = {
				        dbsetting:dbsetting,
				        tablename  :'mdl_quiz_attempts',
				        data : [attempts]
				    }

				    Mastermodel.commonInsert(param2,function(err){
				    	res.status(201).send(err);
					},function(data2){
						console.log(data2);

						var condition3  = " where qslot.quizid = '" + req.body.quizid + "'  ORDER BY qslot.questionid ASC " ;
					    var fields3     = " qslot.quizid as qId, qslot.page as qPage, qslot.questionid as qQuestionid, qslot.maxmark as qMaxmark, ";
					        fields3   += " ques.name as quesName, ques.questiontext as quesText, ques.defaultmark as quesDefaultmark, ques.qtype as quesQtype, ans.answer as ansAnswer ";

					    var join3       = " LEFT JOIN  mdl_question as ques ON qslot.questionid = ques.id ";
					    	join3      += " INNER JOIN  mdl_question_answers as ans ON qslot.questionid = ans.question AND ans.fraction = '1.0000000' ";
					    var param3 = {
					        dbsetting  :dbsetting,
					        from       :'from mdl_quiz_slots as qslot',
					        condition : condition3,
					        fields     : fields3,
					        join       : join3
					    }

					    //1.to get pre assessment test questions

					    Mastermodel.commonSelect(param3,function(err){
					    	res.status(201).send(err);
						},function(data3){
					        if (data3.length > 0) {
					        	console.log(data3);
					        	var dataItem = {};
					        	node_module.asyncForEach(data3, function(dataItem, dataIndex, dataArr) {
					        		quizResponseObj[dataIndex] = {};
					        		console.log(dataItem);

								    var qAttempt = {};
								    	qAttempt.questionusageid = attempts.uniqueid;
										qAttempt.slot = dataIndex+1;
										qAttempt.behaviour = "deferredfeedback";
										qAttempt.questionid = dataItem.qQuestionid;
										qAttempt.variant = "1";
										qAttempt.maxmark = "1";
										qAttempt.minfraction = "0.0000000";
										qAttempt.maxfraction = "1.0000000";
										qAttempt.flagged = "0";
										qAttempt.questionsummary = dataItem.quesName;
										qAttempt.rightanswer = striptags(dataItem.ansAnswer);
										qAttempt.timemodified = Math.floor(Date.now() / 1000);
										console.log(qAttempt);

										var param4 = {
										        dbsetting:dbsetting,
										        tablename  :'mdl_question_attempts',
										        data : [qAttempt]
										    }
											    Mastermodel.commonInsert(param4,function(err){
											    	res.status(200).send(err);

												},function(data4){
														var qASteps = {};
												    	qASteps.questionattemptid = data4.insertId;
														qASteps.sequencenumber = "0";
														qASteps.state = "todo";
														qASteps.timecreated = Math.floor(Date.now() / 1000);
														qASteps.userid = req.body.userid;
													    var param5 = {
													        dbsetting:dbsetting,
													        tablename  :'mdl_question_attempt_steps',
													        data : [qASteps]
													    }

													    Mastermodel.commonInsert(param5,function(err){
														    	res.status(201).send(err);
														},function(data5){
															console.log(data5);
															var condition6  = " where options.question = '" + dataItem.qQuestionid + "' GROUP BY options.question   " ;
														    var fields6     = " GROUP_CONCAT(options.id) as optionsAnswer";
														    var param6 = {
														        dbsetting:dbsetting,
														        from  :'from mdl_question_answers as options',
														        condition : condition6,
														        fields : fields6
														    }


														    Mastermodel.commonSelect(param6,function(err){
														    	res.status(201).send(err);
															},function(data6){
														        	console.log(data6);
														        	if (data6.length > 0) {
															         	var qASdata = {};
																	    	qASdata.attemptstepid = data5.insertId;
																			qASdata.name = "_order";
																			qASdata.value = data6[0].optionsAnswer;
																	    var param7 = {
																	        dbsetting:dbsetting,
																	        tablename  :'mdl_question_attempt_step_data',
																	        data : [qASdata]
																	    }

																	    Mastermodel.commonInsert(param7,function(err){
																	    	res.status(201).send(err);
																		},function(data8){
																			var response = {
															                    resCode: 200,
															                    response: "Quiz Attempt Started Successfully"
															                }
															                res.status(200).send(response);
																			console.log(data8);
																	    })

															        } else {
															        	var response = {
															                    resCode: 201,
															                    response: "Quiz Attempt Not Started!!"
															                }
															        	res.status(201).send(err);
															        }



													      	})
														})
												})
					        	});

					        }
				      })

					})
				}
			})
		}
	})
});



router.post('/attemptNextQuestion',customodule.verifyapicalltoken, function(req, res, next) {
    //userid, quizid, questionid, current page, answer selected option
    //{"quizid":"2", "userid":"2","currentpage":"4", "answer":"3", "questionid":"8"}
    var dbsetting = req.db_setting;
 	var condition  = " where qat.quiz = '" + req.body.quizid + "'  AND qat.userid = '" + req.body.userid + "' " ;
    var fields     = " quAt.id as quAtid, quAt.questionusageid as quAtQuestionusageid ";

    var join       = " LEFT JOIN  mdl_question_attempts as quAt ON quAt.questionusageid = qat.uniqueid AND  quAt.questionid = '" + req.body.questionid + "'";
    var param = {
        dbsetting  :dbsetting,
        from       :'from mdl_quiz_attempts as qat',
        condition : condition,
        fields     : fields,
        join       : join
    }

    //1.to get pre assessment test questions

    Mastermodel.commonSelect(param,function(err){
    	console.log(err);
	},function(data1){


		console.log(data1);

		//to save question answer in mdl_question_attempt_steps table
		var saveData = {};
			saveData.questionattemptid = data1[0].quAtid;
			saveData.sequencenumber = "1";
			saveData.state = "complete";
		    saveData.timecreated = Math.floor(Date.now() / 1000);
		    saveData.userid = req.body.userid;
		var param = {
        dbsetting:dbsetting,
        tablename  :'mdl_question_attempt_steps',
        data : [saveData]
	    }

	    Mastermodel.commonInsert(param,function(err){
	    	console.log(err);
		},function(data){
			var stepsData = {};
			stepsData.attemptstepid = data.insertId;
			stepsData.name = "answer";
			stepsData.value = req.body.answer;
			var param = {
	        dbsetting:dbsetting,
	        tablename  :'mdl_question_attempt_step_data',
	        data : [stepsData]
		    }

		    Mastermodel.commonInsert(param,function(err){
		    	console.log(err);
			},function(data1){
				console.log(data1);
		    })
	    })


		var data = {
		    timemodified: Math.floor(Date.now() / 1000)
		}
		var updatecondition = {
		    questionusageid: data1[0].quAtQuestionusageid,
		    questionid:  req.body.questionid
		}
		var param = {
		    dbsetting: dbsetting,
		    tablename: 'mdl_question_attempts',
		    condition: updatecondition,
		    data: data
		}
		Mastermodel.commonUpdate(param, function(error) {
		    console.log(error);
		}, function(data) {
			console.log(data);

		})


	   var data = {
		    currentpage: req.body.currentpage,
		    timemodified: Math.floor(Date.now() / 1000)
		}
		var updatecondition = {
		    quiz: req.body.quizid,
		    userid: req.body.userid
		}
		var param = {
		    dbsetting: dbsetting,
		    tablename: 'mdl_quiz_attempts',
		    condition: updatecondition,
		    data: data
		}
		Mastermodel.commonUpdate(param, function(error) {
		    console.log(error);
		}, function(data) {
			console.log(data);

		})


	})
});
router.post('/finishAttempt',customodule.verifyapicalltoken,  function(req, res, next) {
	//userid, quizid,
	var quizArr =[];
   	var dbsetting = req.db_setting;
 	var condition  = " where qat.quiz = '" + req.body.quizid + "'  AND qat.userid = '" + req.body.userid + "' " ;
    var fields     = " quAt.id as quesAtId, quAt.questionusageid as usageid, quAt.questionid as questionid, quAt.maxfraction as maxfraction, atSD.value as selectedAnswer ";

    var join       = " LEFT JOIN  mdl_question_attempts as quAt ON quAt.questionusageid = qat.uniqueid ";
    	join      += " LEFT JOIN  mdl_question_attempt_steps as atS ON atS.questionattemptid = quAt.id AND atS.state = 'complete' ";
    	join      += " LEFT JOIN  mdl_question_attempt_step_data as atSD ON atSD.attemptstepid = atS.id AND atSD.name = 'answer' ";
    var param = {
        dbsetting  :dbsetting,
        from       :'from mdl_quiz_attempts as qat',
        condition : condition,
        fields     : fields,
        join       : join
    }

    //1.to get pre assessment test questions

    Mastermodel.commonSelect(param,function(err){
    	console.log(err);
	},function(data){
		//console.log(data);
		var totalLen = data.length;
		node_module.asyncForEach(data, function(dataItem, dataIndex, dataArr) {
			//console.log(data);
				var fields1 = " qA.fraction, qA.answer as qAAnswer "
			 	var condition1  = " where qA.question = '" + dataItem.questionid + "' " ;

			    var param1 = {
			        dbsetting  :dbsetting,
			        from       :' from mdl_question_answers as qA ',
			        condition : condition1,
			        fields     : fields1,
			    }

			    //1.to get pre assessment test questions

			    Mastermodel.commonSelect(param1,function(err){
			    	console.log(err);
				},function(data1){
					var dataupdate = {
					    responsesummary: striptags(data1[dataItem.selectedAnswer].qAAnswer),
					    timemodified: Math.floor(Date.now() / 1000)
					}
					var updatecondition = {
					    id: dataItem.quesAtId
					}
					var param3 = {
					    dbsetting: dbsetting,
					    tablename: 'mdl_question_attempts',
					    condition: updatecondition,
					    data: dataupdate
					}
					Mastermodel.commonUpdate(param3, function(error) {
					    console.log(error);
					}, function(data4) {
						//console.log(data4);

					})


					var saveStepsAns = {};
						saveStepsAns.questionattemptid = dataItem.quesAtId;
						saveStepsAns.sequencenumber = "2";
						saveStepsAns.timecreated = Math.floor(Date.now() / 1000);
						saveStepsAns.userid = req.body.userid;
					if(data1[dataItem.selectedAnswer].fraction == "1"){
						saveStepsAns.fraction = "1.0000000";
						saveStepsAns.state = "gradedright";
					}else if(data1[dataItem.selectedAnswer].fraction == "0"){
						saveStepsAns.fraction = "0.0000000";
						saveStepsAns.state = "gradedwrong";
					}
					    var param2 = {
					        dbsetting:dbsetting,
					        tablename  :'mdl_question_attempt_steps',
					        data : [saveStepsAns]
					    }

					    Mastermodel.commonInsert(param2,function(err){
					    	console.log(err);
					    	res.status(201).send(err);
						},function(data2){
					        //console.log(data2.insertId);
					        var saveStepsDataAns = {};
								saveStepsDataAns.attemptstepid = data2.insertId;
								saveStepsDataAns.name = "-finish";
								saveStepsDataAns.value = "1";
					        var param3 = {
						        dbsetting:dbsetting,
						        tablename  :'mdl_question_attempt_step_data',
						        data : [saveStepsDataAns]
						    }

						    Mastermodel.commonInsert(param3,function(err){
						    	console.log(err);
						    	res.status(201).send(err);
							},function(data3){
								if(saveStepsAns.fraction == "1.0000000"){
								console.log(saveStepsAns.fraction+'teststst');
											var dbsetting = req.db_setting;
								 	var condition8  = " where quiz = '" + req.body.quizid + "'  AND userid = '" + req.body.userid + "'" ;
								        fields8 = " sumgrades "
								    var param8 = {
								        dbsetting  :dbsetting,
								        from       :'from mdl_quiz_attempts ',
								        condition : condition8,
								        fields       : fields8
								    }

								    Mastermodel.commonSelect(param8,function(err){
								    	console.log(err);
									},function(data8){
										var dataupdate7 = {
											state : "finished",
										    timefinish: Math.floor(Date.now() / 1000),
										    timemodified: Math.floor(Date.now() / 1000),
										    timemodifiedoffline: Math.floor(Date.now() / 1000),

										}
										if(data8[0].sumgrades == null){
										    dataupdate7.sumgrades =	"1.00000";
										}else{
											dataupdate7.sumgrades =	data8[0].sumgrades+1;
										}
										var updatecondition7 = {
										    quiz: req.body.quizid,
										    userid: req.body.userid
										}
										var param7 = {
										    dbsetting: dbsetting,
										    tablename: 'mdl_quiz_attempts',
										    condition: updatecondition7,
										    data: dataupdate7
										}
										Mastermodel.commonUpdate(param7, function(error) {
										    console.log(error);
										}, function(data7) {
											console.log(data7);

										})
									})
								}
							})
					    })

				})
		});
	})

});


/*
* Used to save selected trainer
* Created On : 11-07-2017
* Modified On
*/
router.post('/selectTrainer',customodule.verifyapicalltoken, function(req, res, next) {
    var dbsetting = req.db_setting;

    var param = {
        dbsetting:dbsetting,
        tablename  :'mdl_fav_teacher',
        data : req.body
    }

    Mastermodel.commonInsert(param,function(err){
    	console.log(err);
    	res.status(201).send(err);
	},function(data){
        res.status(200).send(ERROR.COMMON.NORECORDFOUND);
    })

});





/*
* Used to add student entry test question for course wise
* Created On : 06-09-2017
* Created By : Monalisa
* Input      :
* Modified On
*/
router.post('/addStudentEntryTestForCourse',customodule.verifyapicalltoken, function(req, res, next) {
    var dbsetting         = req.db_setting;
    if(req.body.courseId  == '' || req.body.courseId  == undefined){
      res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NOCOURSEFOUND);
    }else{
            /**** Used for save student entry test question ***/
            var saveData              = {};
            saveData.course_id        = req.body.courseId;
            saveData.unit_id          = req.body.unitId;
            saveData.question         = req.body.question;
            saveData.question_type    = req.body.questionType;
            saveData.user_id          = 2 ;
            var d                     = new Date();
            var datetime              = d.getFullYear()+"-"+d.getMonth()+"-"+d.getDate()+" "+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
            saveData.created_on       = datetime;
            var param = {
                  dbsetting  : dbsetting,
                  tablename  :'mdl_entry_test_question',
                  data       : [saveData]
              }
           Mastermodel.commonInsert(param,function(err){
               res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
            },function(data){
                   console.log(data.insertId);
                   var right_ans       = req.body.rightAns;
                   var ans_arr         = req.body.answerArr;
                   var d               = new Date();
                   var datetime        = d.getFullYear()+"-"+d.getMonth()+"-"+d.getDate()+" "+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
                   for(var i = 0 ; i<ans_arr.length; i++)
                   {
                           /**** Used for save student entry test answer ***/
                         var saveData             = {};
                         saveData.question_id     = data.insertId;
                         saveData.option_id       = i+1;
                         saveData.answer_choices  = ans_arr[i].val;
                         if(right_ans == i+1 )
                           saveData.right_answer  = 1;
                         else
                           saveData.right_answer  = 0;
                          saveData.created_on     = datetime;
                          saveData.user_id        = 2 ;
                           var param = {
                              dbsetting  : dbsetting,
                              tablename  :'mdl_entry_test_answer',
                              data       : [saveData]
                            }
                            Mastermodel.commonInsert(param,function(err){
                             res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                            },function(data){
                                console.log(data.insertId);
                            })
                   }
                   res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.ENTRYTESTQUESTIONADDEDSUCCESSFULLY);
          })
     }
});

/*
* Used to add trainer entry test question for unit wise
* Created On : 06-09-2017
* Created By : Monalisa
* Input      :
* Modified On
*/
router.post('/addTrainerEntryTestForUnit',customodule.verifyapicalltoken, function(req, res, next) {
    var dbsetting         = req.db_setting;
    if(req.body.courseId  == '' || req.body.courseId  == undefined){
      res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NOCOURSEFOUND);
    }else{
            /**** Used for save trainer entry test question ***/
          var saveData             = {};
          saveData.course_id       = req.body.courseId;
          saveData.unit_id         = req.body.unitId;
          saveData.question        = req.body.question;
          saveData.question_type   = req.body.questionType;
          saveData.user_id         = 2 ;
          var d                    = new Date();
          var datetime             = d.getFullYear()+"-"+d.getMonth()+"-"+d.getDate()+" "+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
          saveData.created_on      = datetime;
          var param = {
                dbsetting  : dbsetting,
                tablename  :'mdl_entry_test_question_trainer',
                data       : [saveData]
            }
          Mastermodel.commonInsert(param,function(err){
            res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
          },function(data){
               console.log(data.insertId);
               var right_ans       = req.body.rightAns;
               var ans_arr         = req.body.answerArr;
               var d               = new Date();
               var datetime        = d.getFullYear()+"-"+d.getMonth()+"-"+d.getDate()+" "+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
               for(var i = 0 ; i<ans_arr.length; i++)
               {
                      /**** Used for save trainer entry test answer ***/
                     var saveData             = {};
                     saveData.question_id     = data.insertId;
                     saveData.option_id       = i+1;
                     saveData.answer_choices  = ans_arr[i].val;
                     if(right_ans == i+1 )
                       saveData.right_answer  = 1;
                     else
                       saveData.right_answer   = 0;
                       saveData.created_on     = datetime;
                       saveData.user_id        = 2 ;
                       var param = {
                          dbsetting  : dbsetting,
                          tablename  :'mdl_entry_test_answer_trainer',
                          data       : [saveData]
                        }
                        Mastermodel.commonInsert(param,function(err){
                         res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                        },function(data){
                            console.log(data.insertId);
                        })
               }
               res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.ENTRYTESTQUESTIONADDEDSUCCESSFULLY);
         })
      }
});


/*
* Used to add RPL Question
* Created On : 13-09-2017
* Created By : Monalisa
* Input      :
* Modified On
*/
router.post('/addRPLQuestion',customodule.verifyapicalltoken, function(req, res, next) {
    var dbsetting         = req.db_setting;
    if(req.body.question  == '' || req.body.question  == undefined){
      res.status(200).send(RESPONSEMESSAGE.RPL.QUESTIONBLANK);
    }else{
            /**** Used for save RPL question ***/
          var saveData             = {};
          saveData.question        = req.body.question;
          var d                    = new Date();
          var datetime             = d.getFullYear()+"-"+d.getMonth()+"-"+d.getDate()+" "+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
          saveData.addeddate       = datetime;
          var param = {
                dbsetting  : dbsetting,
                tablename  :'mdl_rpl_question',
                data       : [saveData]
            }
          Mastermodel.commonInsert(param,function(err){
            res.status(200).send(RESPONSEMESSAGE.RPL.SOMETHINGWRONG);
          },function(data){
               res.status(200).send(RESPONSEMESSAGE.RPL.RPLQUESTIONADDEDSUCCESSFULLY);
         })
      }
});

/*
* Used to add Course 
* Created On : 20-11-2017
* Created By : Monalisa
* Input      :
* Modified On
*/


  
var storage = node_module.multer.diskStorage({
    destination: function(req, file, cb) {
        //var code = JSON.parse(req.body.model).empCode;
        var dest = 'public/courseimage';
        node_module.mkdirp(dest, function(err) {
            if (err) cb(err, dest);
            else cb(null, dest);
        });
    },
    filename: function(req, file, cb) {
        cb(null, Date.now() + node_module.path.extname(file.originalname));
    }
});
var upload = node_module.multer({
    storage: storage
});
router.post('/manageCourse', customodule.verifyapicalltoken, upload.any(),  function(req, res, next) {
    console.log(req.files);
    console.log(req.body);


    //var questionArr = [];
    var dbsetting     = req.db_setting;
    var id            = req.body.id;
    var fname         = req.body.fname;
    var lname         = req.body.lname;   
    var phone         = req.body.phone; 
    var city          = req.body.city;
    var country       = req.body.country;
    var state         = req.body.state;
    var address1      = req.body.address1;
    var address2      = req.body.address2;
    var zip           = req.body.zip;
    var date_of_birth = req.body.date_of_birth;
    var gender        = req.body.gender;

    if(req.files.length  > 0)
    {
         if(req.files[0].filename)
         var profile_image = req.files[0].filename;
        else
         var profile_image = "";
    }else{
         var profile_image = "";
    }




   
  
     if(!id){
             //console.log(error);
             res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONGUSER);
    }else if (isNaN(id)){
             //console.log(error);
             res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONGUSER);
    }else if (!fname || !lname || !phone || !city  || !country  || !state  || !address1  || !zip  ){
             res.status(201).send(RESPONSEMESSAGE.USER.REQUIREDFIELDERROR);
    } else {
                
                if(profile_image != "")
                {
                         var dataupdate = {
                             firstname      :fname,
                             lastname       :lname,
                             phone1         :phone,
                             city           :city,
                             country        :country,
                             state          :state,
                             address1       :address1,
                             address2       :address2,
                             zip            :zip,
                             date_of_birth  :date_of_birth,
                             gender         :gender,
                             profile_image  :profile_image
                         }

                }else{

                         var dataupdate = {
                             firstname      :fname,
                             lastname       :lname,
                             phone1         :phone,
                             city           :city,
                             country        :country,
                             state          :state,
                             address1       :address1,
                             address2       :address2,
                             zip            :zip,
                             date_of_birth  :date_of_birth,
                             gender         :gender
                         }

                }    

                   



                    var updatecondition = {
                        id: id 
                    }
                    var param3 = {
                        dbsetting: dbsetting,
                        tablename: 'mdl_user',
                        condition: updatecondition,
                        data     : dataupdate
                    }


                     Mastermodel.commonUpdate(param3, function(error) {
                     console.log(error);
                     res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONGUSER);
                     }, function(data4) {   

                         res.status(200).send(RESPONSEMESSAGE.USER.PROFILEUPDATESUCCESSFULLY);  

                     });  




    }



});


/*
* Used to add Unit 
* Created On : 20-11-2017
* Created By : Monalisa
* Input      :
* Modified On
*/



module.exports = router;
