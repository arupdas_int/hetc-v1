var express = require('express');
var router = express.Router();
var app = express();
var customodule = require('../../../../customexport');
var node_module = require('../../../../export');
var RESPONSEMESSAGE = require('../../../../../config/statuscode');
var PERMISSION = require('../../../../../config/permission');
var Adminmodel = require('../model/adminModel');
var Mastermodel = require('../../../mastermodel.js');
var striptags = require('striptags');

var sendGroupNotification = require('../../../../../lib/sendnotification');
var NOTIFICATIONMESSAGE = require('../../../../../config/notificationcode');
var Masternotificationmodel = require('../../../commonPushNotificationModel.js');
/* Role Management Api Start */
/*
 * Used to add student entry test question for course wise
 * Created On : 06-09-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/addStudentEntryTestForCourse', customodule.verifyapicalltoken, function (req, res, next) {
    var dbsetting = req.db_setting;
    if (req.body.courseId == '' || req.body.courseId == undefined) {
        res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NOCOURSEFOUND);
    } else {
        /**** Used for save student entry test question ***/
        var saveData = {};
        saveData.course_id = req.body.courseId;
        saveData.unit_id = req.body.unitId;
        saveData.question = req.body.question;
        saveData.question_type = req.body.questionType;
        saveData.user_id = 2;
        var d = new Date();
        var datetime = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
        saveData.created_on = datetime;
        var param = {
            dbsetting: dbsetting,
            tablename: 'mdl_entry_test_question',
            data: [saveData]
        }
        Mastermodel.commonInsert(param, function (err) {
            res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
        }, function (data) {
            console.log(data.insertId);
            var right_ans = req.body.rightAns;
            var ans_arr = req.body.answerArr;
            var d = new Date();
            var datetime = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
            for (var i = 0; i < ans_arr.length; i++) {
                /**** Used for save student entry test answer ***/
                var saveData = {};
                saveData.question_id = data.insertId;
                saveData.option_id = i + 1;
                saveData.answer_choices = ans_arr[i].val;
                if (right_ans == i + 1)
                    saveData.right_answer = 1;
                else
                    saveData.right_answer = 0;
                saveData.created_on = datetime;
                saveData.user_id = 2;
                var param = {
                    dbsetting: dbsetting,
                    tablename: 'mdl_entry_test_answer',
                    data: [saveData]
                }
                Mastermodel.commonInsert(param, function (err) {
                    res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                }, function (data) {
                    console.log(data.insertId);
                })
            }
            res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.ENTRYTESTQUESTIONADDEDSUCCESSFULLY);
        })
    }
});

/*
 * Used to add trainer entry test question for unit wise
 * Created On : 06-09-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/addTrainerEntryTestForUnit', customodule.verifyapicalltoken, function (req, res, next) {
    var dbsetting = req.db_setting;
    if (req.body.courseId == '' || req.body.courseId == undefined) {
        res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NOCOURSEFOUND);
    } else {
        /**** Used for save trainer entry test question ***/
        var saveData = {};
        saveData.course_id = req.body.courseId;
        saveData.unit_id = req.body.unitId;
        saveData.question = req.body.question;
        saveData.question_type = req.body.questionType;
        saveData.user_id = 2;
        var d = new Date();
        var datetime = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
        saveData.created_on = datetime;
        var param = {
            dbsetting: dbsetting,
            tablename: 'mdl_entry_test_question_trainer',
            data: [saveData]
        }
        Mastermodel.commonInsert(param, function (err) {
            res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
        }, function (data) {
            console.log(data.insertId);
            var right_ans = req.body.rightAns;
            var ans_arr = req.body.answerArr;
            var d = new Date();
            var datetime = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
            for (var i = 0; i < ans_arr.length; i++) {
                /**** Used for save trainer entry test answer ***/
                var saveData = {};
                saveData.question_id = data.insertId;
                saveData.option_id = i + 1;
                saveData.answer_choices = ans_arr[i].val;
                if (right_ans == i + 1)
                    saveData.right_answer = 1;
                else
                    saveData.right_answer = 0;
                saveData.created_on = datetime;
                saveData.user_id = 2;
                var param = {
                    dbsetting: dbsetting,
                    tablename: 'mdl_entry_test_answer_trainer',
                    data: [saveData]
                }
                Mastermodel.commonInsert(param, function (err) {
                    res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                }, function (data) {
                    console.log(data.insertId);
                })
            }
            res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.ENTRYTESTQUESTIONADDEDSUCCESSFULLY);
        })
    }
});


/*
 * Used to add RPL Question
 * Created On : 13-09-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/addRPLQuestion', customodule.verifyapicalltoken, function (req, res, next) {
    var dbsetting = req.db_setting;
    if (req.body.question == '' || req.body.question == undefined) {
        res.status(200).send(RESPONSEMESSAGE.RPL.QUESTIONBLANK);
    } else {
        /**** Used for save RPL question ***/
        var saveData = {};
        saveData.question = req.body.question;
        var d = new Date();
        var datetime = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
        saveData.addeddate = datetime;
        var param = {
            dbsetting: dbsetting,
            tablename: 'mdl_rpl_question',
            data: [saveData]
        }
        Mastermodel.commonInsert(param, function (err) {
            res.status(200).send(RESPONSEMESSAGE.RPL.SOMETHINGWRONG);
        }, function (data) {
            res.status(200).send(RESPONSEMESSAGE.RPL.RPLQUESTIONADDEDSUCCESSFULLY);
        })
    }
});





/*
 * Used to add Course
 * Created On : 20-11-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
var storage = node_module.multer.diskStorage({
    destination: function (req, file, cb) {
        //var code = JSON.parse(req.body.model).empCode;
        var dest = 'public/courseimage';
        node_module.mkdirp(dest, function (err) {
            if (err) cb(err, dest);
            else cb(null, dest);
        });
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + node_module.path.extname(file.originalname));
    }
});
var upload = node_module.multer({
    storage: storage
});
router.post('/manageCourse', customodule.verifyapicalltoken, upload.any(), function (req, res, next) {
    console.log(req.files);
    console.log(req.body);

    //var questionArr = [];
    var dbsetting = req.db_setting;
    var id = req.body.id;
    var cname = req.body.cname;
    var idnumber = req.body.idnumber;
    var description = req.body.description;
    var cprice = req.body.cprice;
    var isaccrediated = req.body.isaccrediated;
    var isentryteststudent = req.body.isentryteststudent;
    var course_type_id = req.body.course_type_id;

    if (req.files.length > 0) {
        if (req.files[0].filename)
            var course_image = req.files[0].filename;
        else
            var course_image = "";
    } else {
        var course_image = "";
    }


    if (!cname || !idnumber || !description) {
        res.status(201).send(RESPONSEMESSAGE.USER.REQUIREDFIELDERROR);
    } else {

        if (id > 0) // update
        {
            if (course_image != "") {
                var dataupdate = {
                    name: cname,
                    idnumber: idnumber,
                    description: description,
                    cprice: cprice,
                    isaccrediated: isaccrediated,
                    course_image: course_image,
                    isentryteststudent: isentryteststudent,
                    course_type_id: course_type_id
                }
            } else {

                var dataupdate = {
                    name: cname,
                    idnumber: idnumber,
                    description: description,
                    cprice: cprice,
                    isaccrediated: isaccrediated,
                    isentryteststudent: isentryteststudent,
                    course_type_id: course_type_id
                }
            }
            var updatecondition = {
                id: id
            }
            var param3 = {
                dbsetting: dbsetting,
                tablename: 'mdl_course_categories',
                condition: updatecondition,
                data: dataupdate
            }
            Mastermodel.commonUpdate(param3, function (error) {
                console.log(error);
                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            }, function (data4) {
                res.status(200).send(RESPONSEMESSAGE.COURSE.COURSECREATESUCCESS);
            });

        } else { //insert


            var saveData = {};
            saveData.name = cname;
            saveData.idnumber = idnumber;
            saveData.description = description;
            saveData.cprice = req.body.cprice;
            saveData.isaccrediated = req.body.isaccrediated;
            saveData.course_image = course_image;
            saveData.isentryteststudent = isentryteststudent;
            saveData.course_type_id = course_type_id;
            var param = {
                dbsetting: dbsetting,
                tablename: 'mdl_course_categories',
                data: [saveData]
            }

            Mastermodel.commonInsert(param, function (err) {
                res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            }, function (data) {
                res.status(200).send(RESPONSEMESSAGE.COURSE.COURSEUPDATESUCCESS);
            })

        }


    }



});


/*
 * Used to add Unit
 * Created On : 20-11-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */


var storage = node_module.multer.diskStorage({
    destination: function (req, file, cb) {
        //var code = JSON.parse(req.body.model).empCode;
        var dest = 'public/unitimage';
        node_module.mkdirp(dest, function (err) {
            if (err) cb(err, dest);
            else cb(null, dest);
        });
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + node_module.path.extname(file.originalname));
    }
});
var upload = node_module.multer({
    storage: storage
});
router.post('/manageUnit', customodule.verifyapicalltoken, upload.any(), function (req, res, next) {
    console.log(req.files);
    console.log(req.body);

    //var questionArr = [];
    var dbsetting = req.db_setting;
    var id = req.body.id;
    var fullname = req.body.fullname;
    var shortname = req.body.shortname;
    var idnumber = req.body.idnumber;
    var category = req.body.category;
    var summary = req.body.summary;
    var uprice = req.body.uprice;
    var isentryteststudent = req.body.isentryteststudent;
    var trainer_qualified_percentage = 0; //req.body.trainer_qualified_percentage;

    if (req.files.length > 0) {
        if (req.files[0].filename)
            var unit_image = req.files[0].filename;
        else
            var unit_image = "";
    } else {
        var unit_image = "";
    }


    if (!fullname || !idnumber || !category || !uprice) {
        res.status(201).send(RESPONSEMESSAGE.USER.REQUIREDFIELDERROR);
    } else {

        if (id > 0) // update
        {
            if (unit_image != "") {
                var dataupdate = {
                    fullname: fullname,
                    shortname: shortname,
                    idnumber: idnumber,
                    category: category,
                    summary: summary,
                    uprice: uprice,
                    unit_image: unit_image,
                    isentryteststudent: isentryteststudent
                }

            } else {

                var dataupdate = {
                    fullname: fullname,
                    shortname: shortname,
                    idnumber: idnumber,
                    category: category,
                    summary: summary,
                    uprice: uprice,
                    isentryteststudent: isentryteststudent

                }
            }

            var updatecondition = {
                id: id
            }

            var param3 = {
                dbsetting: dbsetting,
                tablename: 'mdl_course',
                condition: updatecondition,
                data: dataupdate
            }


            Mastermodel.commonUpdate(param3, function (error) {
                console.log(error);
                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            }, function (data4) {
                res.status(200).send(RESPONSEMESSAGE.COURSE.UNITCREATESUCCESS);

            });





        } else { //insert




            var saveData = {};
            saveData.fullname = fullname;
            saveData.shortname = shortname;
            saveData.idnumber = idnumber;
            saveData.category = category;
            saveData.summary = summary;
            saveData.uprice = uprice;
            saveData.trainer_qualified_percentage = trainer_qualified_percentage;
            saveData.unit_image = unit_image;
            saveData.isentryteststudent = isentryteststudent;

            var param = {
                dbsetting: dbsetting,
                tablename: 'mdl_course',
                data: [saveData]
            }

            Mastermodel.commonInsert(param, function (err) {
                res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            }, function (data) {
                res.status(200).send(RESPONSEMESSAGE.COURSE.UNITUPDATESUCCESS);
            })

        }


    }



});



/*
 * Used to add gap traning material
 * Created On : 30-01-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */

router.post('/managegaptraningmaterial', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.files);
    console.log("hello body", req.body);

    //var questionArr = [];
    var dbsetting = req.db_setting;
    var id = req.body.id;
    var comment = req.body.comment;
    var qid = req.body.qid;
    var file = req.body.file;

    if (!file || !comment || !qid) {
        res.status(201).send(RESPONSEMESSAGE.USER.REQUIREDFIELDERROR);
    } else {

        if (id > 0) // update
        {

            var dataupdate = {
                comment: comment,
                file: file
            }
            var updatecondition = {
                id: id
            }
            var param3 = {
                dbsetting: dbsetting,
                tablename: 'mdl_gap_traning_documents',
                condition: updatecondition,
                data: dataupdate
            }
            Mastermodel.commonUpdate(param3, function (error) {
                console.log(error);
                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            }, function (data4) {
                res.status(200).send(RESPONSEMESSAGE.COURSE.MATERIALCREATESUCCESS);

            });

        } else { //insert

            var d = new Date();
            var month = d.getMonth() + 1;
            var datetime = d.getFullYear() + "-" + month + "-" + d.getDate();

            var saveData = {};
            saveData.qid = qid;
            saveData.comment = comment;
            saveData.file = file;
            saveData.addeddate = datetime;

            var param = {
                dbsetting: dbsetting,
                tablename: 'mdl_gap_traning_documents',
                data: [saveData]
            }
            Mastermodel.commonInsert(param, function (err) {
                res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            }, function (data) {
                res.status(200).send(RESPONSEMESSAGE.COURSE.MATERIALUPDATESUCCESS);
            })

        }


    }
});

/*
 * Used to add student entry test question for course wise
 * Created On : 22-02-2018
 * * Input      :
 * Modified On
 */
router.post('/saveRplVerificationDetails', customodule.verifyapicalltoken, function (req, res, next) {
    var dbsetting = req.db_setting;
    var course_id = req.body.cid;
    var unit_id = req.body.uid;
    var submission_master_id = req.body.subMasterid;
    var student_id = req.body.sid;
    if (!course_id || !unit_id || !submission_master_id || !student_id) {
        res.status(200).send(RESPONSEMESSAGE.COMMON.REQUIREDFIELD);
    } else {
        //save the verification detail which is verified by HETC admin
        var saveData = {};
        var d = new Date();
        var datetime = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
        saveData.course_id = req.body.cid;
        saveData.unit_id = req.body.uid;
        saveData.submission_master_id = req.body.subMasterid;
        saveData.student_id = req.body.sid;
        saveData.verification_date = datetime;
        saveData.isProcedureFollowed = req.body.isProcedureFollowed;
        saveData.isDocumentUploaded = req.body.isDocumentUploaded;
        saveData.isTrainerValidated = req.body.isTrainerValidated;
        saveData.isHetcValidated = req.body.isHetcValidated;
        saveData.verified_by = req.body.userId;
        saveData.comment = req.body.comment;
        console.log(saveData);
        var param = {
            dbsetting: dbsetting,
            tablename: 'mdl_rpl_verification_detail',
            data: [saveData]
        }
        Mastermodel.commonInsert(param, function (err) {
            res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
        }, function (data) {
            var isHetcValidated = req.body.isHetcValidated;
            if (data.insertId > 0) {
                //update the verification detail in master table
                var dataupdate = {
                    is_action_taken_hetc: 'Y',
                    is_confirmed_hetc: isHetcValidated,
                    is_valid: isHetcValidated
                }
                var updatecondition = {
                    id: req.body.subMasterid
                }
                var param3 = {
                    dbsetting: dbsetting,
                    tablename: 'mdl_rpl_process_submission_master',
                    condition: updatecondition,
                    data: dataupdate
                }
                Mastermodel.commonUpdate(param3, function (error) {
                    console.log(error);
                    if (error != null) {
                        console.log('********************************');
                        //res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                    }
                }, function (data4) {
                    //update the all rpl process invalid of particular course/unit
                    var dataupdate = {
                        is_active: isHetcValidated,
                        is_valid: isHetcValidated
                    }
                    var updatecondition = {
                        student_id: student_id,
                        course_id: course_id,
                        unit_id: unit_id
                    }
                    var updateRplParam = {
                        dbsetting: dbsetting,
                        tablename: 'mdl_rpl_process_submission_master',
                        condition: updatecondition,
                        data: dataupdate
                    }
                    Mastermodel.commonUpdate(updateRplParam, function (error) {
                        console.log(error);
                        if (error != null) {
                            console.log('********************************');
                            //res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }
                    }, function (rplMasterRes) {
                        var notificationparam = {};
                        notificationparam.userId = req.body.sid;
                        notificationparam.courseId = course_id;
                        notificationparam.unitId = unit_id;
                        notificationparam.userType = 'Student';
                        notificationparam.dbsetting = dbsetting;
                        notificationparam.pushNotificationCode = NOTIFICATIONMESSAGE.ADMIN.RPLADMINVERIFICATION;
                        Masternotificationmodel.sendPushNotification(notificationparam, function (err) {
                            var response = {
                                resCode: 201,
                                response: error
                            }
                            res.status(200).send(response);
                        }, function (succNotificationRes) {
                            var response = {
                                resCode: 200,
                                resMessage: "RPL verification has been completed by HETC successfully",
                                response: "RPL verification has been completed by HETC successfully"
                            }
                            res.status(200).send(response);
                        });
                    });
                });
            } else {
                res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            }
        })
    }
})
/*
 * Issue certificate to student
 * Created On : 22-02-2018
 * * Input      :
 * Modified On
 */
router.post('/issueCertificate', customodule.verifyapicalltoken, function (req, res, next) {
    var dbsetting = req.db_setting;
    console.log(req.body);
    //save the verification detail which is verified by HETC admin
    var saveData = {};
    var d = new Date();
    var datetime = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
    saveData.submission_master_id = req.body.submissionId;
    saveData.issued_date = datetime;
    saveData.issued_by = req.body.userId;
    console.log(saveData);
    var param = {
        dbsetting: dbsetting,
        tablename: 'mdl_issue_certificate_detail',
        data: [saveData]
    }
    Mastermodel.commonInsert(param, function (err) {
        res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
    }, function (data) {
        var submissionId = req.body.submissionId;
        var student_id = req.body.studentId;
        if (data.insertId > 0) {
            //update the verification detail in master table
            var dataupdate = {
                is_certificate_issued: 'Y'
            }
            var updatecondition = {
                id: submissionId
            }
            var param3 = {
                dbsetting: dbsetting,
                tablename: 'mdl_rpl_process_submission_master',
                condition: updatecondition,
                data: dataupdate
            }
            Mastermodel.commonUpdate(param3, function (error) {
                console.log(error);
                if (error != null) {
                    console.log('********************************');
                    //res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }
            }, function (data4) {
                var fields = " mrsm.course_id,mrsm.unit_id ";
                var condition = " where mrsm.id = '" + submissionId + "'   ";
                var limit = " 0,1 ";
                var param = {
                    dbsetting: dbsetting,
                    from: 'from mdl_rpl_process_submission_master as mrsm',
                    condition: condition,
                    fields: fields,
                    limit: limit
                };
                Mastermodel.commonSelect(param, function (err) {
                    console.log(err)
                }, function (rplSubmissionRes) {
                    var notificationparam = {};
                    notificationparam.userId = student_id;
                    notificationparam.courseId = rplSubmissionRes[0]['course_id'];
                    notificationparam.unitId = rplSubmissionRes[0]['unit_id'];
                    notificationparam.userType = 'Student';
                    notificationparam.dbsetting = dbsetting;
                    notificationparam.pushNotificationCode = NOTIFICATIONMESSAGE.ADMIN.RPLADMINCERTIFICATEISSUE;
                    Masternotificationmodel.sendPushNotification(notificationparam, function (err) {
                        var response = {
                            resCode: 201,
                            response: error
                        }
                        res.status(200).send(response);
                    }, function (succNotificationRes) {
                        var response = {
                            resCode: 200,
                            resMessage: "Certificate has been issued by HETC successfully",
                            response: "Certificate has been issued by HETC successfully"
                        }
                        res.status(200).send(RESPONSEMESSAGE.RPL.VERIFIEDSUCCHETC);
                    });
                });
            });
        } else {
            res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }
    })
})

/*
 * dashboard admin section
 * Created On : 22-02-2018
 * * Input      :
 * Modified On
 */
router.post('/getAdminDashboard', customodule.verifyapicalltoken, function (req, res, next) {
    var dbsetting = req.db_setting;
    var userId = req.body.userId;
    var myCourseCnt = 0;
    var dashboardObj = {};
    if (!userId) {
        res.status(200).send(RESPONSEMESSAGE.STUDENT.STUDENTIDREQUIRED);
    } else {
        console.log(userId);
        //get total no of course
        var dbsetting = req.db_setting;
        var fields = " count(mcc.id) as totCourseCnt ";
        var condition = "where 1";
        condition += " ";
        var join = " ";
        var param = {
            dbsetting: dbsetting,
            from: 'from mdl_course_categories mcc ',
            condition: condition,
            fields: fields,
            join: join
        };
        Mastermodel.commonSelect(param, function (err) {
            console.log(err)
        }, function (totCourseRes) {
            totCourseCnt = totCourseRes[0]['totCourseCnt'];
            dashboardObj.totCourseCnt = totCourseCnt;
            //GET THE TOTAL STUDENT COUNT
            var fields = " count(mu.id) as totstudentcnt ";
            var condition = "where 1";
            condition += " AND mu.usertype = 'Student' ";
            var join = " ";
            var param = {
                dbsetting: dbsetting,
                from: 'from mdl_user as mu',
                condition: condition,
                fields: fields,
                join: join
            };
            Mastermodel.commonSelect(param, function (err) {
                console.log(err)
            }, function (totStudentRes) {
                totStudentCnt = totStudentRes[0]['totstudentcnt'];
                dashboardObj.totStudentCnt = totStudentCnt;
                //GET THE TRAINER COUNT
                var dbsetting = req.db_setting;
                var fields = " count(mu.id) as tottrainercnt ";
                var condition = "where 1";
                condition += " AND mu.usertype = 'Trainer' ";
                var join = " ";
                var param = {
                    dbsetting: dbsetting,
                    from: 'from mdl_user as mu',
                    condition: condition,
                    fields: fields,
                    join: join
                };
                Mastermodel.commonSelect(param, function (err) {
                    console.log(err)
                }, function (totTrainerRes) {
                    totTrainerCnt = totTrainerRes[0]['tottrainercnt'];
                    dashboardObj.totTrainerCnt = totTrainerCnt;
                    //GET THE TOTAL PAYMENT COUNT
                    //GET THE PAYMENT COUNT
                    var dbsetting = req.db_setting;
                    var fields = " sum(paid_amount) as totreceivedamount ";
                    var condition = "where 1";
                    condition += " AND is_paid = 'Yes' ";
                    var join = " ";
                    var param = {
                        dbsetting: dbsetting,
                        from: 'from mdl_rpl_process_submission_master mrpsm ',
                        condition: condition,
                        fields: fields,
                        join: join
                    };
                    Mastermodel.commonSelect(param, function (err) {
                        console.log(err)
                    }, function (myPaymentRes) {
                        totPaymentAmt = totPaymentRes[0]['totreceivedamount'];
                        dashboardObj.totPaymentAmt = totPaymentAmt;

                        //GET LIST OF STUDENT PENDING FOR RPL VERIFICATION
                        var fields = "select mp.*, mc.fullname as uname, mc.idnumber as ucode,  mcc.name as cname, mcc.idnumber as ccode, mu.firstname as tfname, mu.lastname as tlname , mu1.firstname as ufname, mu1.lastname as ulname ";
                        var condition = " where mp.is_approved = 'Y'  and mp.is_valid = 'Y' and mp.is_confirmed_hetc = 'Y' ";
                        var join = "LEFT JOIN mdl_course as mc              ON mp.unit_id    = mc.id ";
                        join += "LEFT JOIN mdl_course_categories as mcc  ON mp.course_id  = mcc.id ";
                        join += "LEFT JOIN mdl_user as mu                ON mp.trainer_id = mu.id ";
                        join += "LEFT JOIN mdl_user as mu1               ON mp.student_id = mu1.id ";
                        var limit = " 0,8 ";
                        var param = {
                            dbsetting: dbsetting,
                            from: 'from mdl_rpl_process_submission_master MP ',
                            condition: condition,
                            fields: fields,
                            join: join,
                            limit: limit
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            console.log(err)
                        }, function (rplVerificationPendingRes) {
                            dashboardObj.rplVerificationPendingRes = rplVerificationPendingRes;
                            //GET THE CERTIFICATE ISSUE PENDING
                            var dbsetting = req.db_setting;
                            var fields = "select mp.*, mc.fullname as uname, mc.idnumber as ucode,  mcc.name as cname, mcc.idnumber as ccode, mu.firstname as tfname, mu.lastname as tlname , mu1.firstname as ufname, mu1.lastname as ulname ";
                            var condition = " where mp.is_approved = 'Y'  and mp.is_valid = 'Y' and mp.is_confirmed_hetc = 'Y' and mp.is_confirmed_hetc = 'Y' and is_certificate_issued = 'N' ";
                            var join = "LEFT JOIN mdl_course as mc              ON mp.unit_id    = mc.id ";
                            join += "LEFT JOIN mdl_course_categories as mcc  ON mp.course_id  = mcc.id ";
                            join += "LEFT JOIN mdl_user as mu                ON mp.trainer_id = mu.id ";
                            join += "LEFT JOIN mdl_user as mu1               ON mp.student_id = mu1.id ";
                            var limit = " 0,8 ";
                            var param = {
                                dbsetting: dbsetting,
                                from: 'from mdl_rpl_process_submission_master MP ',
                                condition: condition,
                                fields: fields,
                                join: join,
                                limit: limit
                            };
                            Mastermodel.commonSelect(param, function (err) {
                                console.log(err)
                            }, function (rplCertificateIssuePendingRes) {
                                dashboardObj.rplCertificateIssuePendingRes = rplCertificateIssuePendingRes;
                                //GET THE UNIT RPL VERIFICATION COMPLETION LIST
                                var dbsetting = req.db_setting;
                                var fields = " mcc.id as courseid,mcc.name,mc.id as unitid ,mc.fullname as unitname,submission_datetime,(case when mcc.course_image=null then mcc.course_image else concat('" + SETTING.course_image_url + "',mcc.course_image) end) as courseimage  ";
                                var condition = "where 1";
                                condition += "  AND mrpsm.trainer_id = '" + trainerId + "' AND mrpsm.is_valid = 'Y' AND mrpsm.is_actiontaken = 'Y' AND mrpsm.is_approved = 'Y' ";
                                var join = " INNER JOIN mdl_course_categories mcc  ON mrpsm.course_id = mcc.id  ";
                                join += " INNER JOIN mdl_course mc  ON mcc.id = mc.category  ";
                                var orderby = " order by mrpsm.action_datetime desc ";
                                var param = {
                                    dbsetting: dbsetting,
                                    from: 'from mdl_rpl_process_submission_master as mrpsm',
                                    condition: condition,
                                    fields: fields,
                                    join: join,
                                    orderby: orderby
                                };
                                var param = {
                                    dbsetting: dbsetting,
                                    from: 'from mdl_rpl_process_submission_master as mrpsm',
                                    condition: condition,
                                    fields: fields,
                                    join: join
                                };
                                Mastermodel.commonSelect(param, function (err) {
                                    console.log(err)
                                }, function (myRplCompletedVerificationRes) {
                                    dashboardObj.myRplCompletedVerificationRes = myRplCompletedVerificationRes;
                                    //GET THE DTRAFT MODE RPL PENDING PAYMENT
                                    var dbsetting = req.db_setting;
                                    var fields = " msn.action_type,msn.action_taken  ";
                                    var condition = "where 1";
                                    condition += "  AND msn.trainer_id = '" + trainerId + "'   ";
                                    var join = " ";
                                    var param = {
                                        dbsetting: dbsetting,
                                        from: 'from mdl_trainer_notification as msn',
                                        condition: condition,
                                        fields: fields,
                                        join: join
                                    };
                                    Mastermodel.commonSelect(param, function (err) {
                                        console.log(err)
                                    }, function (myNotificationRes) {
                                        dashboardObj.myNotificationRes = myNotificationRes;
                                        var response = {
                                            resCode: 200,
                                            response: dashboardObj
                                        }
                                        res.status(200).send(response);
                                    });
                                });
                            });
                        });
                    });
                })
            })
        });
    }
})

module.exports = router;