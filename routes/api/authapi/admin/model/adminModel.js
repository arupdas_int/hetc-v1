var database          = require('../../../../../config/database');
var qb = require('node-querybuilder').QueryBuilder(database, 'mysql', 'single');
var model = {
    createSchema :function(param,errorcallback,successcallback){
        return qb.query(param.sql,function(err,response){
            if(err){
                console.log(err);
                var sql = "DROP DATABASE "+param.dbName;
                if(param.number>0){
                    qb.query(sql,function(err,response){});
                }
                errorcallback("Database already exist Or you have syntax error in your sql query");
            }else{
                console.log("Success ",response);
                successcallback(response);
            }

        })
    }
    }
module.exports=model;
