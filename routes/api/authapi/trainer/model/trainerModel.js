var database = require('../../../../../../config/database');
var qb = require('node-querybuilder').QueryBuilder(database, 'mysql', 'single');
var ERROR = require('../../../../../../config/statuscode');
var SETTING = require('../../../../../../config/setting');
var customodule = require('../../../../../customexport');

var model = {
    //******************************* GET BRANCH LIST MODEL*****************
    getBranchListModel: function(requestdata, errorcallback, successcallback) {
        var mysql = require('mysql');
        var pool = mysql.createPool({
            host: SETTING.dbhost,
            user: SETTING.user,
            password: SETTING.password,
            database: requestdata.schoolCode
        });
        pool.getConnection(function(err, connection) {
            if (err) {
                errorcallback(ERROR.COMMON.ERRCONNECTDB);
            } else {
                var getTotBranchSql = "select count(*) AS TOTCNT from master_branches";
                connection.query(getTotBranchSql, function(err, res) {
                    if (err) {
                        errorcallback(ERROR.COMMON.SOMETHINGWRONG);
                    } else {
                        console.log(res[0]['TOTCNT']);
                        var pageNo = requestdata.pageNo;
                        var pageLimit = requestdata.pageLimit;
                        if (pageNo == 1) {
                            var limit = pageNo-1;
                            var offset = pageLimit;
                        } else {
                            var limit = ((pageNo - 1) * pageLimit);
                            var offset = pageLimit;
                        }
                        var getBranchSql = "select * from master_branches WHERE status = 1 ORDER BY id DESC LIMIT " + limit + "," + offset + " ";
                        console.log(getBranchSql);
                        connection.query(getBranchSql, function(err, rows) {
                            connection.release();
                            if (err) {
                                errorcallback(ERROR.COMMON.SOMETHINGWRONG);
                            } else {
                                successcallback(rows, res[0]['TOTCNT']);
                            }
                        });
                    }
                });
            }

        });
    },
}
module.exports = model;
