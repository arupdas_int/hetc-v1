var express = require('express');
var router = express.Router();
var app = express();
var customodule = require('../../../../customexport');
var node_module = require('../../../../export');
//var ERROR               = require('../../../../../config/statuscode');
var RESPONSEMESSAGE = require('../../../../../config/statuscode');
var Mastermodel = require('../../../mastermodel.js');
var asyncForEach = require('async-foreach').forEach;
var SETTING = require('../../../../../config/setting');
//******************************* GET COURSE LIST CONTROLLER************
router.post('/getBranch', customodule.verifyapicalltoken, function (req, res, next) {

    var pageNo = req.body.pageNo;
    var pageSize = req.body.pageLimit;
    var dbsetting = req.db_setting;
    var offset = customodule.helper.offset(pageNo, pageSize);

    var limit = offset + ',' + pageSize;
    var param = {

        from: 'from master_branches',
        limit: limit
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (branches) {
        if (branches.length > 0) {
            delete param.limit;
            Mastermodel.commonSelect(param, function (err) {
                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            }, function (count) {
                var response = {
                    resCode: 200,
                    response: branches,
                    totCnt: count.length
                };
                res.status(200).send(response);
            });
        } else {
            res.status(200).send(ERROR.COMMON.NORECORDFOUND);
        }
    });
    // if (pageNo >= 0) {
    //     var dbName = req.body.schoolCode;
    //     masterModel.getBranchListModel(req.body, function(err) {
    //         res.status(200).send(err);
    //     }, function(branches,totcnt) {
    //         if (branches.length > 0) {
    //             var response = {
    //                 resCode: 200,
    //                 response: branches,
    //                 totCnt : totcnt
    //             }
    //             res.status(200).send(response);
    //         } else {
    //             res.status(200).send(ERROR.COMMON.NORECORDFOUND);
    //         }
    //     })
    // } else {
    //     res.status(200).send(ERROR.COMMON.PAGENOVALID);
    // }
});
/*
* Course Search Page from Trainer Panel
* Created On : 30-08-2017
* Author : Monalisa

* 1st Modified On     :
* 1st Modified Reason :
*/
router.post('/getCourseList', customodule.verifyapicalltoken, function (req, res, next) {
    var db_setting = req.db_setting;
    var searchText = req.body.searchText;
    if (!searchText) {
        res.status(200).send(RESPONSEMESSAGE.TRAINER.SEARCHTEXTEMPTY);
    } else {

        var fields = "mcc.id,  mcc.name,  mcc.idnumber, mcc.description ";
        var condition = ""; //where  mcc.name LIKE '" + searchText +"' ";
        var param = {

            from: 'from  mdl_course_categories as  mcc ',
            condition: condition,
            fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (courseRes) {
            if (courseRes.length > 0) {

                /* for(var course in  courseRes)
                 {
                    console.log(courseRes[course].id);
                     var fields    = "mcc.id,  mcc.fullname, mcc.shortname, mcc.idnumber, mcc.summary ";
                     var condition = "where mc.category = '" + courseRes[course].id +"'";
                     var param = {
                         dbsetting : dbsetting,
                         from      : 'from mdl_course mc',
                         condition : condition,
                     };
                     Mastermodel.commonSelect(param,function(err){res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);},function(courseCategoryRes){

                        courseRes[course].unitDetails = courseCategoryRes;

                     });
                 }*/

                var response = {
                    resCode: 200,
                    response: courseRes
                }
                res.status(200).send(response);
            } else {
                res.status(200).send(RESPONSEMESSAGE.TRAINER.COURSEEMPTY);
            }
        });
    }
});
/*
 * Used to get course unit details
 * Created On : 05-09-2017
 * Created By : Monalisa
 * Input      : courseid
 * Modified On
 */
router.post('/getCourseUnitDetails', customodule.verifyapicalltoken, function (req, res, next) {

    var dbsetting = req.db_setting;
    var courseid = req.body.courseId;
    var trainerid = req.body.trainerId;

    /**** Course Search By search Text and isAccrediated parameter  ****/

    var fields = " mc.id,  mc.fullname,  mc.shortname,  mc.idnumber, mc.summary, mt.is_qualified, mt.trainer_id";
    var condition = "where 1";
    if (courseid != "")
        condition += " AND  mc.category = " + courseid + "  ";
    if (trainerid != "")
        condition += " AND ( mt.trainer_id = " + trainerid + " || mt.trainer_id = '' || mt.trainer_id IS NULL ) ";
    var join = " LEFT JOIN mdl_entry_test_results_trainer mt ON mc.id = mt.unit_id";
    var param = {

        from: 'from mdl_course as mc',
        condition: condition,
        fields: fields,
        join: join
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (courseRes) { //console.log(courseRes);
        if (courseRes.length > 0) {
            var response = {
                resCode: 200,
                response: courseRes
            }
            res.status(200).send(courseRes);

        } else {
            res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
        }
    });
});
/*
 * Used to get Teacher My course details
 * Created On : 19-10-2017
 * Created By : Monalisa
 * Input      : courseid
 * Modified On
 */
router.post('/getTrainerCourseDetails', customodule.verifyapicalltoken, function (req, res, next) {
    var dbsetting = req.db_setting;
    var trainerid = req.body.trainerId;
    var isAccrediated = req.body.isAccrediated;

    /**** Course Search By search Text and isAccrediated parameter  ****/


    if (isAccrediated == 'Y') //join with unit id
    {
        var fields = " msetm.course_id,  mcc.id,  mcc.fullname,  mcc.shortname,  mcc.idnumber, mcc.summary, mcc.unit_image";
        var condition = " where 1 AND trainer_id = '" + trainerid + "' ";
        var join = " LEFT JOIN mdl_course as mcc ON msetm.unit_id = mcc.id";
        var param = {

            from: 'from mdl_trainer_approved_units as msetm',
            condition: condition,
            fields: fields,
            join: join
        };
    } else {

        var fields = " msetm.course_id, mcc.id, mcc.name, mcc.idnumber, mcc.description, mcc.isaccrediated, mcc.course_image";
        var condition = " where 1 AND trainer_id = '" + trainerid + "' ";
        var join = " LEFT JOIN mdl_course_categories as mcc ON msetm.course_id = mcc.id";
        var param = {

            from: 'from mdl_trainer_approved_units as msetm',
            condition: condition,
            fields: fields,
            join: join
        };

    }

    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (courseRes) { //console.log(courseRes);
        if (courseRes.length > 0) {
            var response = {
                resCode: 200,
                response: courseRes
            }
            res.status(200).send(courseRes);

        } else {
            res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
        }
    });
});
/*
 * Used for Teacher Login
 * Created On : 19-10-2017
 * Created By : Monalisa
 * Input      : username
 * Modified On
 */
router.post('/trainerLogin', customodule.verifyapicalltoken, function (req, res, next) {
    var dbsetting = req.db_setting;
    var username = req.body.username;

    /**** Course Search By search Text and isAccrediated parameter  ****/
    var fields = " mu.id, mu.username,  mu.firstname, mu.lastname,  mu.email";
    var condition = " where 1 AND username = '" + username + "' ";
    var param = {

        from: 'from mdl_user as mu',
        condition: condition,
        fields: fields
    };

    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (courseRes) { //console.log(courseRes);
        if (courseRes.length > 0) {
            var response = {
                resCode: 200,
                response: courseRes
            }
            res.status(200).send(courseRes);

        } else {
            res.status(200).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
        }
    });
});
/*
 * Used to add Trainer Wishlist
 * Created On : 06-09-2017
 * Created By : Monalisa
 * Input      : trainerid(int), unitid(int)
 * Modified On
 */
router.post('/addTrainerWishlist', customodule.verifyapicalltoken, function (req, res, next) {
    var dbsetting = req.db_setting;
    if (req.body.trainerid == '' || req.body.trainerid == undefined) {
        res.status(201).send(RESPONSEMESSAGE.WISHLIST.NOTRAINERIDFOUND);
    } else {
        /*** This sql used for checking whether thisunit already present the trainer wishlist or not. ***/
        var trainerid = req.body.trainerid;
        var unitid = req.body.unitid;
        var courseid = req.body.courseid;
        var isAccrediated = req.body.isAccrediated;


        if (unitid > 0) {

            var fields = " mtw.id ";
            var condition = " where mtw.trainer_id = '" + trainerid + "' AND mtw.unit_id = '" + unitid + "' ";
            var param = {

                from: 'from mdl_trainer_wishlist as mtw',
                condition: condition,
                fields: fields
            };

        } else {

            var fields = " mtw.id ";
            var condition = " where mtw.trainer_id = '" + trainerid + "' AND mtw.course_id = '" + courseid + "' ";
            var param = {

                from: 'from mdl_trainer_wishlist as mtw',
                condition: condition,
                fields: fields
            };

        }

        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (wishlistRes) {
            console.log(wishlistRes);
            if (wishlistRes.length > 0) {
                res.status(201).send(RESPONSEMESSAGE.WISHLIST.UNITALREADYEXISTSINTRAINERWISHLIST);
            } else {
                /*** This is used for add data to trainer wishlist ***/
                var saveData = {};
                saveData.trainer_id = trainerid;
                saveData.unit_id = unitid;
                saveData.course_id = courseid;
                saveData.isactiontaken = 'N';
                var d = new Date();
                var datetime = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
                saveData.added_datetime = datetime;
                var param = {

                    tablename: 'mdl_trainer_wishlist',
                    data: [saveData]
                }
                Mastermodel.commonInsert(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
                }, function (data) {
                    res.status(200).send(RESPONSEMESSAGE.WISHLIST.TRAINERWISHLISTADDEDSUCCESSFULLY);
                })
            }
        });
    }
});
/*
 * Used to get entry test question for trainer as unit wise
 * Created On : 06-09-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/getTrainerEntryTestDetails', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    console.log(req.db_setting);
    var dbsetting = req.db_setting;
    var unit_id = req.body.unitId;
    var course_id = req.body.courseId;
    var trainer_id = req.body.trainerId;
    //var isAccrediated  = req.body.isAccrediated;
    if (!course_id) {
        res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NOCOURSEFOUND);
    } else if (isNaN(course_id)) {
        res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.INVALIDCOURSE);
    } else {


        if (unit_id > 0) // for non accrediated course question will from course
        {

            var fields = "met.course_id, met.unit_id, met.id, met.question, met.question_type ";
            var condition = " where met.unit_id = '" + unit_id + "' ";
            var orderby = "order by rand() ";
            var param = {

                from: 'from mdl_entry_test_question_trainer as met',
                condition: condition,
                fields: fields,
                orderby: orderby
            };



        } else { // for accrediated course question will from unit


            var fields = "met.course_id, met.unit_id, met.id, met.question, met.question_type ";
            var condition = " where met.course_id  = '" + course_id + "' ";
            var orderby = "order by rand() ";
            var param = {

                from: 'from mdl_entry_test_question_trainer as met',
                condition: condition,
                fields: fields,
                orderby: orderby
            };


        }

        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (entrytestRes) {
            console.log(entrytestRes);
            var quesLen = entrytestRes.length;
            var queaArr = new Array();
            if (entrytestRes.length > 0) {
                asyncForEach(entrytestRes, function (positionitem, positionindex, positionarr) {
                    var cnt = positionindex;
                    console.log(positionitem.id);
                    var fields = " met.* ";
                    var condition = " where met.question_id = '" + positionitem.id + "' ";
                    var param = {

                        from: 'from mdl_entry_test_answer_trainer as met',
                        condition: condition,
                        fields: fields
                    };
                    Mastermodel.commonSelect(param, function (err) {
                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                    }, function (ansRes) {
                        queaArr.push({
                            "questionId": positionitem.id
                        })
                        entrytestRes[cnt].answer = ansRes;
                        cnt++;
                        console.log(quesLen, queaArr.length);
                        if (quesLen == queaArr.length) {
                            res.status(200).send(entrytestRes);
                        }
                    })
                })
            } else {
                res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NORECORDSFOUND);
            }
        });
    }
});
/*
 * Used to submit trainer entry test result
 * Created On : 12-09-2017
 * Created By : Monalisa
 * Input      : courseId(int)
 * Modified On
 */
/*
router.post('/postTrainerEntryTestResult',customodule.verifyapicalltoken,function(req, res, next){
    //console.log(req.body);
    console.log(req.db_setting);
    var dbsetting       = req.db_setting;
    var unit_id         = req.body.unitId;
    var count           = req.body.count;
    var questionarr     = req.body.questionArr;
    var questionidarr   = req.body.questionidArr;
    var correctansarr   = req.body.correctansArr;
    var traineransarr   = req.body.traineransArr;
    var trainer_id      = req.body.trainerId;
    if(!unit_id){
         res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NOUNITFOUND);
    }else if(isNaN(unit_id)){
         res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.INVALIDUNIT);
    }else{

          var fields    = "id, ispassed";
          var condition = " where trainerid ='"+trainer_id+"' AND unitid = '"+unit_id+"' ";
          var param     = {
                              dbsetting : dbsetting,
                              from      :'from mdl_entry_test_results_trainer ',
                              condition : condition,
                              fields    : fields
                          };


          Mastermodel.commonSelect(param, function(err){res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);}, function(ansRes){
            if(ansRes.length > 0){
                   res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.TRAINERALREADYQUALIFIED);
            }else{

                    var fields    = "id";
                    var condition = "where id='"+unit_id+"'";
                    var param     = {
                            dbsetting : dbsetting,
                            from      :'from mdl_course',
                            condition : condition,
                            fields    : fields
                    };

                    Mastermodel.commonSelect(param, function(err){res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);}, function(ansRes){

                       if(ansRes.length > 0){

                             var isPassed = 0;
                             var flag     = 0;
                             var counter  = 0;
                             for(var i=0; i<count; i++)
                             {
                                 console.log(correctansarr[i].val);
                                 console.log(traineransarr[i].val);
                                 if(correctansarr[i].val == traineransarr[i].val)
                                 {
                                      flag     = 1;
                                      isPassed = 1;
                                      counter++;
                                 }
                                 else
                                 {
                                      flag     = 2;
                                      isPassed = 0;
                                      counter++;
                                 }

                                 console.log('hetc flag'+flag);
                                 console.log('hetc isPassed'+isPassed);
                                 console.log('hetc iiii'+i);
                                 console.log('hetc count'+count);

                                 if(flag == 2)
                                 {
                                    res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.TRAINERNOTQUALIFIED);
                                 }
                                 if( isPassed  == 1 && (count == i+1) ) // You have qualified for this test
                                 {

                                            console.log("hetc inside qualifies");
                                            var saveData             = {};
                                            saveData.unitid          = req.body.unitId;
                                            saveData.trainerid       = req.body.trainerId;
                                            saveData.ispassed        = 'Y';
                                            var d                    = new Date();
                                            var datetime             = d.getFullYear()+"-"+d.getMonth()+"-"+d.getDate()+" "+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
                                            saveData.datetime        = datetime;
                                              var param = {
                                                    dbsetting  : dbsetting,
                                                    tablename  :'mdl_entry_test_results_trainer',
                                                    data       : [saveData]
                                                }
                                                Mastermodel.commonInsert(param,function(err){
                                                 res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                                                },function(data){
                                                     console.log(data.insertId);
                                                     for(var j=0; j<count; j++)
                                                     {
                                                           var saveData             = {};
                                                           saveData.masterid        = data.insertId;
                                                           saveData.questionid      = questionidarr[j].val;
                                                           saveData.question        = questionarr[j].val;
                                                           saveData.trainerans      = traineransarr[j].val;
                                                           saveData.correctans      = correctansarr[j].val;
                                                           saveData.datetime        = datetime;
                                                           var param = {
                                                              dbsetting  : dbsetting,
                                                              tablename  :'mdl_entry_test_results_details_trainer',
                                                              data       : [saveData]
                                                            }
                                                            Mastermodel.commonInsert(param,function(err){
                                                                res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                                                            },function(data){
                                                                console.log(data.insertId);
                                                            })
                                                     }
                                                })
                                              res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.TRAINERQUALIFIED);
                                 }

                             }//end of for


                       }else{

                          res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NOUNITFOUND);

                       }//end of else


                    }); // end of course id select

                }//end of else

          }); //end of select

    }//end of else
});//end of post
*/

/*
 * Used to post entry test question for trainer as unit wise
 * Created On : 06-09-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/postTrainerEntryTestResult', customodule.verifyapicalltoken, function (req, res, next) {
    var unit_id = req.body.unitId;
    var course_id = req.body.courseId;
    var total = req.body.total;

    var questionarr = req.body.questionArr;
    var questionidarr = req.body.questionidArr;
    var correctansarr = req.body.correctansArr;
    var traineransarr = req.body.traineransArr;
    var trainer_id = req.body.trainerId;
    var correct = req.body.correct;

    if (!course_id) {
        res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.NOCOURSEFOUND);
    } else if (isNaN(course_id)) {
        res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.INVALIDCOURSE);
    } else if (!trainer_id) {
        res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.NOTRAINERFOUND);
    } else if (isNaN(trainer_id)) {
        res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.INVALIDTRAINER);
    } else {

        if (unit_id != 0) {
            var fields = "id,is_qualified";
            var condition = " where trainer_id ='" + trainer_id + "' AND unit_id = '" + unit_id + "' AND course_id = '" + course_id + "' ";
            var param = {

                from: 'from mdl_entry_test_results_trainer ',
                condition: condition,
                fields: fields
            };
        } else {

            var fields = "id,is_qualified";
            var condition = " where trainer_id ='" + trainer_id + "' AND course_id = '" + course_id + "' ";
            var param = {

                from: 'from mdl_entry_test_results_trainer ',
                condition: condition,
                fields: fields
            };

        }

        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (ansRes) {

            var is_qualified = "";
            if (ansRes.length > 0) {

                var is_qualified = ansRes[0].is_qualified;
            }

            if (is_qualified == 'Y') {
                res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.TRAINERALREADYQUALIFIED);
            } else {

                if (unit_id != 0) {
                    var fields = "id,trainer_qualified_percentage";
                    var condition = "where id='" + unit_id + "'";
                    var param = {
                        from: 'from mdl_course',
                        condition: condition,
                        fields: fields
                    };
                } else {

                    var fields = "id,trainer_qualified_percentage";
                    var condition = "where id='" + course_id + "'";
                    var param = {
                        from: 'from mdl_course_categories',
                        condition: condition,
                        fields: fields
                    };
                }
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (ansRes) {



                    if (ansRes.length > 0) {

                        var isPassed = 0;
                        var flag = 0;
                        var counter = 0;
                        var unitpassmark = ansRes[0].trainer_qualified_percentage;
                        var percentagecalc = Math.round((correct / total) * 100);
                        if (percentagecalc >= unitpassmark) {
                            /************* Delete From wishlist this course *******************/
                            var param = {
                                tablename: 'mdl_trainer_wishlist',
                                condition: {
                                    'trainer_id': trainer_id,
                                    'course_id': course_id,
                                    'unit_id': unit_id
                                },
                            };
                            Mastermodel.commonDelete(param, function (err) {
                                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                            }, function (success) {

                                /***************** after delete insert in trainer result table *******************/
                                var saveData = {};
                                saveData.unit_id = unit_id;
                                saveData.trainer_id = trainer_id;
                                saveData.course_id = course_id;
                                saveData.is_qualified = 'Y';
                                var d = new Date();
                                var m = d.getMonth() + 1;
                                var datetime = d.getFullYear() + "-" + m + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
                                saveData.datetime = datetime;
                                var param = {

                                    tablename: 'mdl_entry_test_results_trainer',
                                    data: [saveData]
                                };
                                Mastermodel.commonInsert(param, function (err) {
                                    res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                                }, function (data) {

                                    var total = questionarr.length;
                                    var qcountArr = [];
                                    asyncForEach(questionarr, function (positionitem, positionindex, positionarr) {
                                        var j = positionindex;
                                        var saveData = {};
                                        saveData.master_id = data.insertId;
                                        saveData.question_id = questionidarr[j];
                                        saveData.question = questionarr[j];
                                        saveData.trainer_ans = traineransarr[j];
                                        saveData.correct_ans = correctansarr[j];
                                        saveData.datetime = datetime;
                                        var param = {
                                            tablename: 'mdl_entry_test_results_details_trainer',
                                            data: [saveData]
                                        };
                                        Mastermodel.commonInsert(param, function (err) {
                                            res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                                        }, function (data) {
                                            qcountArr.push(positionindex);
                                            var questionLen = qcountArr.length;
                                            if (total == questionLen) {
                                                res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.TRAINERQUALIFIED);

                                            }
                                        });
                                    });
                                });
                                /*******************************************************************/
                            });

                        } else {
                            var saveData = {};
                            saveData.unit_id = unit_id;
                            saveData.trainer_id = trainer_id;
                            saveData.course_id = course_id;
                            saveData.is_qualified = 'N';
                            var d = new Date();
                            var m = d.getMonth() + 1;
                            var datetime = d.getFullYear() + "-" + m + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
                            saveData.datetime = datetime;
                            var param = {
                                tablename: 'mdl_entry_test_results_trainer',
                                data: [saveData]
                            };
                            Mastermodel.commonInsert(param, function (err) {
                                res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                            }, function (data) {

                                var total = questionarr.length;
                                var qcountArr = [];
                                asyncForEach(questionarr, function (positionitem, positionindex, positionarr) {

                                    var j = positionindex;
                                    var saveData = {};
                                    saveData.master_id = data.insertId;
                                    saveData.question_id = questionidarr[j];
                                    saveData.question = questionarr[j];
                                    saveData.trainer_ans = traineransarr[j];
                                    saveData.correct_ans = correctansarr[j];
                                    saveData.datetime = datetime;
                                    var param = {

                                        tablename: 'mdl_entry_test_results_details_trainer',
                                        data: [saveData]
                                    };
                                    Mastermodel.commonInsert(param, function (err) {

                                        // console.log(err);
                                        //return false;
                                        res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                                    }, function (data) {
                                        qcountArr.push(positionindex);
                                        var questionLen = qcountArr.length;
                                        if (total == questionLen) {

                                            res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.TRAINERNOTQUALIFIED);

                                        }
                                    });
                                });

                            });


                        }


                    } else {
                        res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.NOCOURSEFOUND);
                    } //end of else


                }); // end of course id select

            } //end of else

        }); //end of select

    } //end of else

}); //end of post
/*
 * Used for trainer Qualification Final Submit
 * Created On : 30-11-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
//router.post('/finalSubmitTrainerQualification', function(req, res, next) {
router.post('/finalSubmitTrainerQualification', customodule.verifyapicalltoken, function (req, res, next) {

    console.log(req.db_setting);
    var dbsetting = req.db_setting;
    var unitId = req.body.unitId;
    var trainerId = req.body.trainerId;
    var courseId = req.body.courseId;


    var fields = "id";
    var condition = " where trainer_id ='" + trainerId + "' AND unit_id = '" + unitId + "' AND course_id = '" + courseId + "' and isarchived = 0 ";
    var param = {

        from: 'from mdl_trainer_qualification_master ',
        condition: condition,
        fields: fields
    };

    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
    }, function (qualificationRes) {
        if (qualificationRes.length > 0) { // already present

            var dataupdate = {
                issubmited: 'Yes'
            }
            var updatecondition = {
                trainer_id: trainerId,
                unit_id: unitId,
                course_id: courseId,
                isarchived: 0
            }
            var param3 = {

                tablename: 'mdl_trainer_qualification_master',
                condition: updatecondition,
                data: dataupdate
            }
            Mastermodel.commonUpdate(param3, function (error) {
                console.log(error);
                res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
            }, function (data4) {
                res.status(200).send(RESPONSEMESSAGE.WISHLIST.QUALIFICATIONSUBMITTEDSUCCESSFULLY);
            });
        } else {
            res.status(201).send(RESPONSEMESSAGE.WISHLIST.NODATAEXISTS);
        }
    });



});
/*
 * Used for upload educational qualification
 * Created On : 30-11-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */

var storage = node_module.multer.diskStorage({
    destination: function (req, file, cb) {
        //var code = JSON.parse(req.body.model).empCode;
        var dest = 'public/trainerqualificationfile';
        node_module.mkdirp(dest, function (err) {
            if (err) cb(err, dest);
            else cb(null, dest);
        });
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + node_module.path.extname(file.originalname));
    }
});
var upload = node_module.multer({
    storage: storage
});
//customodule.verifyapicalltoken,
router.post('/uploadEducationalQualification', customodule.verifyapicalltoken, upload.any(), function (req, res, next) {


    var dbsetting = req.db_setting;
    var qualification = req.body.qualification;
    var passing_year = req.body.passing_year;
    var subject = req.body.subject;

    var unitId = req.body.unitId;
    var trainerId = req.body.trainerId;
    var courseId = req.body.courseId;

    if (req.body.unitId == "")
        var unitId = 0;

    if (req.files != undefined) {
        if (req.files.length > 0) {
            if (req.files[0].filename)
                var qualification_file = req.files[0].filename;
            else
                var qualification_file = "";
        } else {
            var qualification_file = "";
        }

    } else {
        var qualification_file = "";
    }


    /*********************** This checks for Trainer already qualified or not *********/
    var fields = "id, ispassed, isactiontaken, issubmited";
    var condition = " where trainer_id ='" + trainerId + "' AND unit_id = '" + unitId + "' AND course_id = '" + courseId + "' and isarchived = 0 ";
    var param = {

        from: 'from mdl_trainer_qualification_master ',
        condition: condition,
        fields: fields
    };

    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (qualificationRes) {
        if (qualificationRes.length > 0) { // already present

            var ispassed = qualificationRes[0].ispassed;
            var isactiontaken = qualificationRes[0].isactiontaken;
            var issubmited = qualificationRes[0].issubmited;
            var masterid = qualificationRes[0].id;

            if (ispassed == 'No') // not passed
            {
                if (issubmited == 'Yes') // but already submitted
                {
                    res.status(201).send(RESPONSEMESSAGE.WISHLIST.TRAINERALREADYSUBMITTED);

                } else {

                    var saveData = {};
                    saveData.masterid = masterid;
                    saveData.qualification = qualification;
                    saveData.passsing_year = passing_year;
                    saveData.subject = subject;
                    saveData.file = qualification_file;

                    var param = {

                        tablename: 'mdl_trainer_educational_qualification_details',
                        data: [saveData]
                    }

                    Mastermodel.commonInsert(param, function (err) {
                        res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
                    }, function (data) {
                        res.status(200).send(RESPONSEMESSAGE.WISHLIST.QUALIFICATIONADDEDSUCCESSFULLY);

                    });
                }

            } else // already qualified
            {
                res.status(201).send(RESPONSEMESSAGE.WISHLIST.TRAINERALREADYQUALIFIED);
            }
        } else { //not present


            /*** This is used for add data to trainer Qualification master ***/
            var saveData = {};
            saveData.trainer_id = trainerId;
            saveData.course_id = courseId;
            saveData.unit_id = unitId;
            saveData.ispassed = 'No';
            var d = new Date();
            var datetime = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
            saveData.addeddatetime = datetime;
            saveData.isactiontaken = 'No';
            var d = new Date();
            var datetime = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
            saveData.actiondatetime = datetime;
            saveData.issubmited = 'No';
            var param = {

                tablename: 'mdl_trainer_qualification_master',
                data: [saveData]
            }
            Mastermodel.commonInsert(param, function (err) {
                res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
            }, function (data) {
                //res.status(200).send(RESPONSEMESSAGE.WISHLIST.STUDENTWISHLISTADDEDSUCCESSFULLY);
                console.log(data.insertId);
                var masterId = data.insertId;

                var saveData = {};
                saveData.masterid = data.insertId;
                saveData.qualification = qualification;
                saveData.passsing_year = passing_year;
                saveData.subject = subject;
                saveData.file = qualification_file;
                var param = {

                    tablename: 'mdl_trainer_educational_qualification_details',
                    data: [saveData]
                }
                Mastermodel.commonInsert(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
                }, function (data) {
                    res.status(200).send(RESPONSEMESSAGE.WISHLIST.QUALIFICATIONADDEDSUCCESSFULLY);
                });
            })

        }
    });

});
/*
 * Used for upload work experience
 * Created On : 30-11-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
var storage = node_module.multer.diskStorage({
    destination: function (req, file, cb) {
        //var code = JSON.parse(req.body.model).empCode;
        var dest = 'public/trainerqualificationfile';
        node_module.mkdirp(dest, function (err) {
            if (err) cb(err, dest);
            else cb(null, dest);
        });
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + node_module.path.extname(file.originalname));
    }
});
var upload = node_module.multer({
    storage: storage
});
//customodule.verifyapicalltoken,
router.post('/uploadWorkExperience', customodule.verifyapicalltoken, upload.any(), function (req, res, next) {


    var dbsetting = req.db_setting;
    var work_organization_name = req.body.work_organization_name;
    var designation = req.body.designation;
    var duration_from = req.body.duration_from;
    var duration_to = req.body.duration_to;

    var unitId = req.body.unitId;
    var trainerId = req.body.trainerId;
    var courseId = req.body.courseId;
    var role_responsibility = req.body.role_responsibility;



    if (req.body.unitId == "")
        var unitId = 0;


    if (req.files != undefined) {
        if (req.files.length > 0) {
            if (req.files[0].filename)
                var experience_file = req.files[0].filename;
            else
                var experience_file = "";
        } else {
            var experience_file = "";
        }

    } else {
        var experience_file = "";
    }


    /*********************** This checks for Trainer already qualified or not *********/
    var fields = "id, ispassed, isactiontaken, issubmited";
    var condition = " where trainer_id ='" + trainerId + "' AND unit_id = '" + unitId + "' AND course_id = '" + courseId + "' and isarchived = 0 ";
    var param = {

        from: 'from mdl_trainer_qualification_master ',
        condition: condition,
        fields: fields
    };

    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (qualificationRes) {
        if (qualificationRes.length > 0) { // already present

            var ispassed = qualificationRes[0].ispassed;
            var isactiontaken = qualificationRes[0].isactiontaken;
            var issubmited = qualificationRes[0].issubmited;
            var masterid = qualificationRes[0].id;

            if (ispassed == 'No') // not passed
            {
                if (issubmited == 'Yes') // but already submitted
                {
                    res.status(201).send(RESPONSEMESSAGE.WISHLIST.TRAINERALREADYSUBMITTED);
                } else {

                    var saveData = {};
                    saveData.masterid = masterid;
                    saveData.work_organization_name = work_organization_name;
                    saveData.designation = designation;
                    saveData.form_date = duration_from;
                    saveData.to_date = duration_to;
                    saveData.file = experience_file;
                    saveData.role_responsibility = role_responsibility;


                    var param = {

                        tablename: 'mdl_trainer_work_experience_details',
                        data: [saveData]
                    }
                    Mastermodel.commonInsert(param, function (err) {
                        res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
                    }, function (data) {
                        res.status(200).send(RESPONSEMESSAGE.WISHLIST.WORKADDEDSUCCESSFULLY);
                    });

                }

            } else // already qualified
            {
                res.status(201).send(RESPONSEMESSAGE.WISHLIST.TRAINERALREADYQUALIFIED);
            }
        } else { //not present
            /*** This is used for add data to trainer Qualification master ***/
            var saveData = {};
            saveData.trainer_id = trainerId;
            saveData.course_id = courseId;
            saveData.unit_id = unitId;
            saveData.ispassed = 'No';
            var d = new Date();
            var datetime = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
            saveData.addeddatetime = datetime;
            saveData.isactiontaken = 'No';
            var d = new Date();
            var datetime = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
            saveData.actiondatetime = datetime;
            saveData.issubmited = 'No';
            var param = {

                tablename: 'mdl_trainer_qualification_master',
                data: [saveData]
            }
            Mastermodel.commonInsert(param, function (err) {
                res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
            }, function (data) {
                //res.status(200).send(RESPONSEMESSAGE.WISHLIST.STUDENTWISHLISTADDEDSUCCESSFULLY);
                console.log(data.insertId);
                var masterId = data.insertId;

                var saveData = {};
                saveData.masterid = masterId;
                saveData.work_organization_name = work_organization_name;
                saveData.designation = designation;
                saveData.form_date = duration_from;
                saveData.to_date = duration_to;
                saveData.file = experience_file;
                saveData.role_responsibility = role_responsibility;


                var param = {

                    tablename: 'mdl_trainer_work_experience_details',
                    data: [saveData]
                }
                Mastermodel.commonInsert(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
                }, function (data) {
                    res.status(200).send(RESPONSEMESSAGE.WISHLIST.WORKADDEDSUCCESSFULLY);
                });

            })

        }
    });
});
/*
 * get trainer qualification details
 * Created On : 30-11-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
router.post('/getTrainerQualificationDetails', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    var questionArr = [];
    var dbsetting = req.db_setting;

    var unitId = req.body.unitId;
    var trainerId = req.body.trainerId;
    var courseId = req.body.courseId;

    var fields = "id";
    var condition = " where trainer_id ='" + trainerId + "' AND unit_id = '" + unitId + "' AND course_id = '" + courseId + "' and isarchived = 0 ";
    var param = {

        from: 'from mdl_trainer_qualification_master ',
        condition: condition,
        fields: fields
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (qualificationRes) {
        if (qualificationRes.length > 0) { // already present
            var masterid = qualificationRes[0].id;
            var fields = " mt.*";
            var condition = " where 1 AND mt.masterid = '" + masterid + "'  ";
            var param = {

                from: 'from mdl_trainer_educational_qualification_details as mt',
                condition: condition,
                fields: fields
            };
            Mastermodel.commonSelect(param, function (err) {
                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            }, function (usrRes) {
                //console.log(rplRes);
                if (usrRes.length > 0) {
                    var response = {
                        resCode: 200,
                        response: usrRes
                    }
                    res.status(200).send(response);
                } else {
                    res.status(201).send(RESPONSEMESSAGE.WISHLIST.NODATAEXISTS);
                }
            });

        } else {
            res.status(201).send(RESPONSEMESSAGE.WISHLIST.NODATAEXISTS);
        }

    });

    /************* End ************************/


});

/*
 * get trainer work experience details
 * Created On : 30-11-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
router.post('/getTrainerWorkExperienceDetails', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    var questionArr = [];
    var dbsetting = req.db_setting;

    var unitId = req.body.unitId;
    var trainerId = req.body.trainerId;
    var courseId = req.body.courseId;


    var fields = "id";
    var condition = " where trainer_id ='" + trainerId + "' AND unit_id = '" + unitId + "' AND course_id = '" + courseId + "' and isarchived = 0 ";
    var param = {

        from: 'from mdl_trainer_qualification_master ',
        condition: condition,
        fields: fields
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (qualificationRes) {
        if (qualificationRes.length > 0) { // already present
            var masterid = qualificationRes[0].id;
            var fields = " mt.*";
            var condition = " where 1 AND mt.masterid = '" + masterid + "'  ";
            var param = {

                from: 'from mdl_trainer_work_experience_details as mt',
                condition: condition,
                fields: fields
            };
            Mastermodel.commonSelect(param, function (err) {
                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            }, function (usrRes) {
                //console.log(rplRes);
                if (usrRes.length > 0) {
                    var response = {
                        resCode: 200,
                        response: usrRes
                    }
                    res.status(200).send(response);
                } else {
                    res.status(201).send(RESPONSEMESSAGE.WISHLIST.NODATAEXISTS);
                }
            });

        } else {
            res.status(201).send(RESPONSEMESSAGE.WISHLIST.NODATAEXISTS);
        }

    });



});

/*
 * Used for edit educational qualification
 * Created On : 30-11-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */

var storage = node_module.multer.diskStorage({
    destination: function (req, file, cb) {
        //var code = JSON.parse(req.body.model).empCode;
        var dest = 'public/trainerqualificationfile';
        node_module.mkdirp(dest, function (err) {
            if (err) cb(err, dest);
            else cb(null, dest);
        });
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + node_module.path.extname(file.originalname));
    }
});
var upload = node_module.multer({
    storage: storage
});
//customodule.verifyapicalltoken,
router.post('/editEducationalQualification', customodule.verifyapicalltoken, upload.any(), function (req, res, next) {


    var dbsetting = req.db_setting;
    var qualification = req.body.qualification;
    var passing_year = req.body.passing_year;
    var subject = req.body.subject;
    var unitId = req.body.unitId;
    var trainerId = req.body.trainerId;
    var courseId = req.body.courseId;
    var qualificationid = req.body.qualificationid;

    if (req.body.unitId == "")
        var unitId = 0;

    if (req.files != undefined) {
        if (req.files.length > 0) {
            if (req.files[0].filename)
                var qualification_file = req.files[0].filename;
            else
                var qualification_file = "";
        } else {
            var qualification_file = "";
        }

    } else {
        var qualification_file = "";
    }

    if (qualification_file != "") {
        var dataupdate = {
            qualification: qualification,
            passsing_year: passing_year,
            subject: subject,
            file: qualification_file
        }

    } else {
        var dataupdate = {
            qualification: qualification,
            passsing_year: passing_year,
            subject: subject
        }
    }

    var updatecondition = {
        id: qualificationid
    }
    var param3 = {

        tablename: 'mdl_trainer_educational_qualification_details',
        condition: updatecondition,
        data: dataupdate
    }
    Mastermodel.commonUpdate(param3, function (error) {
        console.log(error);
        res.status(201).send(RESPONSEMESSAGE.TRAINER.SOMETHINGWRONG);
    }, function (data4) {
        res.status(200).send(RESPONSEMESSAGE.TRAINER.QUALIFICATIONEDITEDSUCCESSFULLY);
    });

});


//******************************* Trainer Qualification List************/
/*
 * delete trainer qualification data
 * Created On : 11-12-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/deleteTrainerQualification', customodule.verifyapicalltoken, function (req, res, next) {

    var qualificationid = req.body.qualificationid;
    var dbsetting = req.db_setting;



    var param = {

        tablename: 'mdl_trainer_educational_qualification_details',
        condition: {
            'id': qualificationid
        },
    };

    Mastermodel.commonDelete(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (success) {
        var response = {
            resCode: 200,
            resMessage: "Qualification deleted successfully",
            response: "Qualification deleted successfully"
        };
        res.status(200).send(response);
    });

});

/*
 * delete trainer work experience data
 * Created On : 11-12-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/deleteTrainerWorkExperience', customodule.verifyapicalltoken, function (req, res, next) {

    var qualificationid = req.body.workexperienceid;
    var dbsetting = req.db_setting;


    var param = {

        tablename: 'mdl_trainer_work_experience_details',
        condition: {
            'id': qualificationid
        },
    };

    Mastermodel.commonDelete(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (success) {
        var response = {
            resCode: 200,
            resMessage: "Work experience deleted successfully",
            response: "Work experience deleted successfully"
        };
        res.status(200).send(response);
    });

});

/*
 * get single data for trainer qualification details
 * Created On : 11-12-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/getSingleTrainerQualificationDetails', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    var dbsetting = req.db_setting;
    var qualificationid = req.body.qualificationid;

    var fields = "*";
    var condition = " where id = '" + qualificationid + "' ";
    var param = {

        from: 'from mdl_trainer_educational_qualification_details ',
        condition: condition,
        fields: fields
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (qualificationRes) {
        if (qualificationRes.length > 0) { // already present

            var response = {
                resCode: 200,
                response: qualificationRes
            }
            res.status(200).send(response);

        } else {
            res.status(201).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
        }

    });



});

/*
 * get single data for trainer work experience details
 * Created On : 11-12-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/getSingleTrainerWorkExperienceDetails', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    var dbsetting = req.db_setting;
    var workexperienceid = req.body.workexperienceid;

    var fields = "*";
    var condition = " where id = '" + workexperienceid + "' ";
    var param = {

        from: 'from mdl_trainer_work_experience_details ',
        condition: condition,
        fields: fields
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (qualificationRes) {
        if (qualificationRes.length > 0) { // already present

            var response = {
                resCode: 200,
                response: qualificationRes
            }
            res.status(200).send(response);

        } else {
            res.status(201).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
        }

    });



});







/*
 * Used for edit work evidence qualification
 * Created On : 11-12-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */

var storage = node_module.multer.diskStorage({
    destination: function (req, file, cb) {
        //var code = JSON.parse(req.body.model).empCode;
        var dest = 'public/trainerqualificationfile';
        node_module.mkdirp(dest, function (err) {
            if (err) cb(err, dest);
            else cb(null, dest);
        });
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + node_module.path.extname(file.originalname));
    }
});
var upload = node_module.multer({
    storage: storage
});
//customodule.verifyapicalltoken,
router.post('/editWorkExperience', customodule.verifyapicalltoken, upload.any(), function (req, res, next) {


    var dbsetting = req.db_setting;
    var work_organization_name = req.body.work_organization_name;
    var designation = req.body.designation;
    var form_date = req.body.duration_from;
    var to_date = req.body.duration_to;


    var unitId = req.body.unitId;
    var trainerId = req.body.trainerId;
    var courseId = req.body.courseId;
    var workexperienceid = req.body.workexperienceid;

    var role_responsibility = req.body.role_responsibility;

    if (req.body.unitId == "")
        var unitId = 0;

    if (req.files != undefined) {
        if (req.files.length > 0) {
            if (req.files[0].filename)
                var experience_file = req.files[0].filename;
            else
                var experience_file = "";
        } else {
            var experience_file = "";
        }

    } else {
        var experience_file = "";
    }


    if (experience_file != "") {
        var dataupdate = {
            work_organization_name: work_organization_name,
            designation: designation,
            form_date: form_date,
            to_date: to_date,
            file: experience_file,
            role_responsibility: role_responsibility
        }

    } else {
        var dataupdate = {
            work_organization_name: work_organization_name,
            designation: designation,
            form_date: form_date,
            to_date: to_date,
            role_responsibility: role_responsibility
        }
    }

    var updatecondition = {
        id: workexperienceid
    }
    var param3 = {

        tablename: 'mdl_trainer_work_experience_details',
        condition: updatecondition,
        data: dataupdate
    }
    Mastermodel.commonUpdate(param3, function (error) {
        console.log(error);
        res.status(201).send(RESPONSEMESSAGE.TRAINER.SOMETHINGWRONG);
    }, function (data4) {
        res.status(200).send(RESPONSEMESSAGE.TRAINER.QUALIFICATIONEDITEDSUCCESSFULLY);
    });

});



/*
 * used for get trainer approved units
 * Created On : 12-12-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
router.post('/getTrainerApprovedUnits', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    var dbsetting = req.db_setting;
    var trainerId = req.body.trainerId;
    var searchText = req.body.searchText;
    var isAccrediated = req.body.isAccrediated;

    var fields = "mu.*, mcc.name as cname, mcc.idnumber as ccode, mc.fullname as uname, mc.idnumber as ucode";
    var condition = " where trainer_id = '" + trainerId + "' ";
    if (searchText != "")
        condition += " AND ( mcc.name LIKE '%" + searchText + "%' OR  mcc.idnumber LIKE '%" + searchText + "%' ) ";
    if (isAccrediated != "")
        condition += " AND  mcc.isaccrediated LIKE '%" + isAccrediated + "%'   ";
    var join = "LEFT JOIN mdl_course_categories as mcc ON mcc.id = mu.course_id ";
    join += "LEFT JOIN mdl_course as mc             ON mc.id  = mu.unit_id ";
    var param = {

        from: 'from mdl_trainer_approved_units mu ',
        condition: condition,
        fields: fields,
        join: join
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (qualificationRes) {
        if (qualificationRes.length > 0) { // already present

            var response = {
                resCode: 200,
                response: qualificationRes
            }
            res.status(200).send(response);

        } else {
            res.status(201).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
        }

    });



});
/*
 * Used to get trainer wishlist
 * Created On : 14-12-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
router.post('/getTrainerWishList', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    var questionArr = [];
    var dbsetting = req.db_setting;
    var trainerId = req.body.trainerId;
    var searchText = req.body.searchText;
    var isAccrediated = req.body.isAccrediated;

    /**** Used to get RPL question list ****/
    var fields = " msw.course_id, msw.unit_id,  msw.trainer_id,  mcc.name as cname,  mcc.idnumber as ccode,  mc.fullname as uname, mc.idnumber as ucode"; //,  msetm.is_qualified
    var condition = " where 1 AND trainer_id = '" + trainerId + "' ";
    if (searchText != "")
        condition += " AND ( mcc.name LIKE '%" + searchText + "%' OR  mcc.idnumber LIKE '%" + searchText + "%' ) ";
    if (isAccrediated != "")
        condition += " AND  mcc.isaccrediated LIKE '%" + isAccrediated + "%'   ";
    var join = " LEFT JOIN mdl_course_categories as mcc            ON msw.course_id = mcc.id";
    join += " LEFT JOIN mdl_course            as mc             ON msw.unit_id = mc.id";
    //join +=  " LEFT JOIN mdl_entry_test_results_trainer as metrt ON metrt.course_id = msetm.course_id AND msetm.student_id = '"+studentId+"' AND msetm.is_qualified != 'Y' ";
    var param = {

        from: 'from mdl_trainer_wishlist as msw',
        condition: condition,
        fields: fields,
        join: join
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);

    }, function (rplRes) {

        console.log(rplRes);

        if (rplRes.length > 0) {

            var response = {
                resCode: 200,
                response: rplRes
            }
            res.status(200).send(response);
        } else {
            res.status(201).send(RESPONSEMESSAGE.RPL.NORECORDSFOUND);
        }
    });

});

/*
 * get learning style option details
 * Created On : 15-12-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
router.post('/getLearningStyleOptionsDetails', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    var questionArr = [];
    var dbsetting = req.db_setting;
    var questionId = req.body.questionId;

    var fields = "m1.*, m2.name";
    var condition = " where questionid ='" + questionId + "'  ";
    var join = "LEFT JOIN mdl_learning_attribute as m2 ON m1.type = m2.id ";
    var param = {
        from: 'from mdl_learning_style_details m1 ',
        condition: condition,
        fields: fields,
        join: join
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (qualificationRes) {
        if (qualificationRes.length > 0) { // already present


            var response = {
                resCode: 200,
                response: qualificationRes
            }
            res.status(200).send(response);

        } else {
            res.status(201).send(RESPONSEMESSAGE.WISHLIST.NODATAEXISTS);
        }

    });

    /************* End ************************/


});

/*
 * Used for upload option for learning style
 * Created On : 15-12-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
var storage = node_module.multer.diskStorage({
    destination: function (req, file, cb) {
        //var code = JSON.parse(req.body.model).empCode;
        var dest = 'public/learningstylefile';
        node_module.mkdirp(dest, function (err) {
            if (err) cb(err, dest);
            else cb(null, dest);
        });
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + node_module.path.extname(file.originalname));
    }
});
var upload = node_module.multer({
    storage: storage
});
router.post('/uploadOptionForLearningStyle', customodule.verifyapicalltoken, upload.any(), function (req, res, next) {


    var dbsetting = req.db_setting;
    var type = req.body.type;
    var questionId = req.body.questionId;
    var optionid = req.body.optionid;

    //type: 'Audio', questionId: '1', optionid: '0'

    if (req.files != undefined) {
        if (req.files.length > 0) {
            if (req.files[0].filename)
                var option_file = req.files[0].filename;
            else
                var option_file = "";
        } else {
            var option_file = "";
        }

    } else {
        var option_file = "";
    }


    var saveData = {};
    saveData.questionid = questionId;
    saveData.type = type;
    saveData.option_file = option_file;
    var param = {

        tablename: 'mdl_learning_style_details',
        data: [saveData]
    }
    Mastermodel.commonInsert(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
    }, function (data) {
        res.status(200).send(RESPONSEMESSAGE.WISHLIST.OPTIONADDEDSUCCESSFULLY);
    });


});

/*
 * used for get learning style details
 * Created On : 15-12-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
router.post('/getSingleLerningStyleDetails', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    var dbsetting = req.db_setting;
    var optionid = req.body.optionid;

    var fields = "*";
    var condition = " where id = '" + optionid + "' ";
    var param = {

        from: 'from mdl_learning_style_details ',
        condition: condition,
        fields: fields
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (qualificationRes) {
        if (qualificationRes.length > 0) { // already present

            var response = {
                resCode: 200,
                response: qualificationRes
            }
            res.status(200).send(response);

        } else {
            res.status(201).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
        }

    });



});



/*
 * Used for edit learning style
 * Created On : 11-12-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */

var storage = node_module.multer.diskStorage({
    destination: function (req, file, cb) {
        //var code = JSON.parse(req.body.model).empCode;
        var dest = 'public/learningstylefile';
        node_module.mkdirp(dest, function (err) {
            if (err) cb(err, dest);
            else cb(null, dest);
        });
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + node_module.path.extname(file.originalname));
    }
});
var upload = node_module.multer({
    storage: storage
});
router.post('/editLearningStyle', customodule.verifyapicalltoken, upload.any(), function (req, res, next) {


    var dbsetting = req.db_setting;
    var type = req.body.type;
    var questionId = req.body.questionId;
    var optionid = req.body.optionid;



    if (req.files != undefined) {
        if (req.files.length > 0) {
            if (req.files[0].filename)
                var option_file = req.files[0].filename;
            else
                var option_file = "";
        } else {
            var option_file = "";
        }

    } else {
        var option_file = "";
    }


    if (option_file != "") {
        var dataupdate = {
            type: type,
            option_file: option_file
        }

    } else {
        var dataupdate = {
            type: type
        }
    }

    var updatecondition = {
        id: optionid
    }
    var param3 = {

        tablename: 'mdl_learning_style_details',
        condition: updatecondition,
        data: dataupdate
    }
    Mastermodel.commonUpdate(param3, function (error) {
        console.log(error);
        res.status(201).send(RESPONSEMESSAGE.TRAINER.SOMETHINGWRONG);
    }, function (data4) {
        res.status(200).send(RESPONSEMESSAGE.TRAINER.QUALIFICATIONEDITEDSUCCESSFULLY);
    });

});

/*
 * Delete Trainer Learning Style
 * Created On : 15-12-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
router.post('/deleteLearningStyle', customodule.verifyapicalltoken, function (req, res, next) {

    var optionid = req.body.optionid;
    var dbsetting = req.db_setting;


    var param = {

        tablename: 'mdl_learning_style_details',
        condition: {
            'id': optionid
        },
    };

    Mastermodel.commonDelete(param, function (err) {
        res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (success) {
        var response = {
            resCode: 200,
            resMessage: "Learning style deleted successfully"
        };
        res.status(200).send(response);
    });

});

/*
 * Trainer Schdule List
 * Created On : 15-12-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
router.post('/getTrainerSchduleList', function (req, res, next) {
    //console.log(req.body);
    var questionArr = [];
    var dbsetting = '';
    var trainerId = req.body.trainerId;

    var base_url1 = "http://52.63.96.113:4444/share/courseimage/";
    var base_url2 = "http://52.63.96.113:4444/share/unitimage/";

    var d = new Date();
    var month = d.getMonth() + 1;
    var datetime = d.getFullYear() + "-" + month + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
    console.log(datetime);
    var fields = " mt.* , ml.name as learning_mode, DATE_FORMAT(mt.date, '%a %M %d %Y %H %i %p') as dateformat,  DATE_FORMAT(mt.todate, '%a %M %d %Y %H %i %p') as todateformat,  mc.summary as unit_description,  mcc.description as course_description, mc.fullname as uname, mc.idnumber as ucode, mcc.name as cname, mcc.idnumber as ccode, (case when mcc.course_image = null then mcc.course_image else concat('" + base_url1 + "',mcc.course_image) end) as courseimage, (case when mc.unit_image=null then mc.unit_image else concat('" + base_url2 + "',mc.unit_image) end) as unitimage";
    var condition = " where mt.trainerid = '" + trainerId + "'  AND mt.learning_mode_type_id in (2,4)";
    condition += " AND  mt.date >= '" + datetime + "' ";
    var join = " LEFT JOIN mdl_course mc               ON mc.id   = mt.unit_id";
    join += " LEFT JOIN mdl_course_categories  mcc  ON mcc.id  = mt.course_id ";
    join += " LEFT JOIN mdl_learning_mode_type ml   ON ml.id = mt.learning_mode_type_id ";


    var orderby = " order by mt.date asc";
    var param = {

        from: 'from mdl_trainer_schdule mt ',
        condition: condition,
        fields: fields,
        join: join,
        orderby: orderby
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (qualificationRes) {

        var quesLen = qualificationRes.length;
        var queaArr = new Array();
        if (qualificationRes.length > 0) { // already present


            asyncForEach(qualificationRes, function (positionitem, positionindex, positionarr) {
                var cnt = positionindex;
                console.log(positionitem.id);
                var schduleid = positionitem.id;

                var fields = " count(id) as no_of_student ";
                var condition = " where schdule_id = '" + schduleid + "' AND learning_mode_type_id in (2,4) AND is_valid = 'Y' ";
                var param = {

                    from: 'from mdl_student_booked_vc ',
                    condition: condition,
                    fields: fields
                };

                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (courseRes) {
                    if (courseRes.length > 0) {
                        var no_of_student = courseRes[0].no_of_student;

                        qualificationRes[cnt].no_of_student = no_of_student;

                        cnt++;
                        questionArr.push(positionindex);
                        var questionLen = questionArr.length;
                        queaArr.push({
                            "questionId": positionitem.id
                        })

                        if (quesLen == queaArr.length) {

                            var response = {
                                resCode: 200,
                                response: qualificationRes
                            }
                            res.status(200).send(response);
                        }

                    }

                });

            })


        } else {
            res.status(201).send(RESPONSEMESSAGE.WISHLIST.NODATAEXISTS);
        }

    });

    /************* End ************************/


});



/*
 * Trainer Schdule List
 * Created On : 15-12-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
router.post('/getTrainerSchduleListWeb', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    var questionArr = [];
    var dbsetting = req.db_setting;
    var trainerId = req.body.trainerId;

    var fromDate = req.body.fromDate;
    var toDate = req.body.toDate;
    var searchCourseName = req.body.searchCourseName;
    var learningMode = req.body.learningMode;

    var base_url1 = "http://52.63.96.113:4444/share/courseimage/";
    var base_url2 = "http://52.63.96.113:4444/share/unitimage/";

    var d = new Date();
    var month = d.getMonth() + 1;
    var datetime = d.getFullYear() + "-" + month + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
    var fields = " mt.* , ml.name as learning_mode, DATE_FORMAT(mt.date, '%a %M %d %Y %H %i %p') as dateformat,  DATE_FORMAT(mt.todate, '%a %M %d %Y %H %i %p') as todateformat,  mc.summary as unit_description,  mcc.description as course_description, mc.fullname as uname, mc.idnumber as ucode, mcc.name as cname, mcc.idnumber as ccode, (case when mcc.course_image = null then mcc.course_image else concat('" + base_url1 + "',mcc.course_image) end) as courseimage, (case when mc.unit_image=null then mc.unit_image else concat('" + base_url2 + "',mc.unit_image) end) as unitimage";
    var condition = " where mt.trainerid = '" + trainerId + "' ";
    condition += " AND  mt.date >= '" + datetime + "' ";


    if (searchCourseName != "")
        condition += " AND ( mcc.name LIKE '%" + searchCourseName + "%' OR  mcc.idnumber LIKE '%" + searchCourseName + "%' ) ";
    if (learningMode != "")
        condition += " AND ( mt.learning_mode_type_id = '" + learningMode + "' ) ";


    /* if (fromDate != "" && toDate != "")
         condition += " AND ( mt.date >= '" + fromDate + "' &&  mt.todate <= '" + toDate + "' ) ";*/

    if (fromDate != "" && (toDate == "" || toDate == undefined))
        condition += " AND  mt.date >= '" + fromDate + "' ";
    if (toDate != "" && (fromDate == "" || fromDate == undefined))
        condition += " AND  mt.date <= '" + toDate + "' ";
    if (fromDate != "" && toDate != "")
        condition += " AND  mt.date BETWEEN '" + fromDate + "' AND '" + toDate + "'   ";



    var join = " LEFT JOIN mdl_course mc               ON mc.id   = mt.unit_id";
    join += " LEFT JOIN mdl_course_categories  mcc  ON mcc.id  = mt.course_id ";
    join += " LEFT JOIN mdl_learning_mode_type ml   ON ml.id = mt.learning_mode_type_id ";

    var orderby = " order by mt.date asc";
    var param = {

        from: 'from mdl_trainer_schdule mt ',
        condition: condition,
        fields: fields,
        join: join,
        orderby: orderby
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (qualificationRes) {

        var quesLen = qualificationRes.length;
        var queaArr = new Array();
        if (qualificationRes.length > 0) { // already present


            asyncForEach(qualificationRes, function (positionitem, positionindex, positionarr) {
                var cnt = positionindex;
                var schduleid = positionitem.id;

                var fields = " count(id) as no_of_student ";
                var condition = " where schdule_id = '" + schduleid + "' ";
                var param = {

                    from: 'from mdl_student_booked_vc ',
                    condition: condition,
                    fields: fields
                };

                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (courseRes) {
                    if (courseRes.length > 0) {
                        var no_of_student = courseRes[0].no_of_student;

                        qualificationRes[cnt].no_of_student = no_of_student;

                        cnt++;
                        questionArr.push(positionindex);
                        var questionLen = questionArr.length;
                        queaArr.push({
                            "questionId": positionitem.id
                        })

                        if (quesLen == queaArr.length) {

                            var response = {
                                resCode: 200,
                                response: qualificationRes
                            }
                            res.status(200).send(response);
                        }

                    }

                });

            })


        } else {
            res.status(201).send(RESPONSEMESSAGE.WISHLIST.NODATAEXISTS);
        }

    });

    /************* End ************************/


});



/*
 * Trainer Schdule Log List
 * Created On : 23-01-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
router.post('/getTrainerSchduleLogList', function (req, res, next) {
    //console.log(req.body);
    var questionArr = [];
    var dbsetting = '';
    var trainerId = req.body.trainerId;

    var base_url1 = "http://52.63.96.113:4444/share/courseimage/";
    var base_url2 = "http://52.63.96.113:4444/share/unitimage/";

    var d = new Date();
    var month = d.getMonth() + 1;
    var datetime = d.getFullYear() + "-" + month + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
    console.log(datetime);
    var fields = " mt.* , ml.name as learning_mode, DATE_FORMAT(mt.date, '%a %M %d %Y %H %i %p') as dateformat,  mc.summary as unit_description,  mcc.description as course_description, mc.fullname as uname, mc.idnumber as ucode, mcc.name as cname, mcc.idnumber as ccode, (case when mcc.course_image = null then mcc.course_image else concat('" + base_url1 + "',mcc.course_image) end) as courseimage, (case when mc.unit_image=null then mc.unit_image else concat('" + base_url2 + "',mc.unit_image) end) as unitimage";
    var condition = " where mt.trainerid = '" + trainerId + "' AND mt.learning_mode_type_id in (2,4)  ";
    condition += " AND  mt.date < '" + datetime + "' ";
    var join = " LEFT JOIN mdl_course mc             ON mc.id  = mt.unit_id";
    join += " LEFT JOIN mdl_course_categories mcc ON mcc.id = mt.course_id ";
    join += " LEFT JOIN mdl_learning_mode_type ml ON ml.id  = mt.learning_mode_type_id ";
    var orderby = " order by mt.date asc";
    var param = {

        from: 'from mdl_trainer_schdule mt ',
        condition: condition,
        fields: fields,
        join: join,
        orderby: orderby
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (qualificationRes) {

        var quesLen = qualificationRes.length;
        var queaArr = new Array();
        if (qualificationRes.length > 0) { // already present


            asyncForEach(qualificationRes, function (positionitem, positionindex, positionarr) {
                var cnt = positionindex;
                console.log(positionitem.id);
                var schduleid = positionitem.id;

                var fields = " count(id) as no_of_student ";
                var condition = " where schdule_id = '" + schduleid + "' AND learning_mode_type_id in (2,4) AND is_valid = 'Y' ";
                var param = {

                    from: 'from mdl_student_booked_vc ',
                    condition: condition,
                    fields: fields
                };

                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (courseRes) {
                    if (courseRes.length > 0) {
                        var no_of_student = courseRes[0].no_of_student;

                        qualificationRes[cnt].no_of_student = no_of_student;

                        cnt++;
                        questionArr.push(positionindex);
                        var questionLen = questionArr.length;
                        queaArr.push({
                            "questionId": positionitem.id
                        })

                        if (quesLen == queaArr.length) {

                            var response = {
                                resCode: 200,
                                response: qualificationRes
                            }
                            res.status(200).send(response);
                        }

                    }

                });

            })


        } else {
            res.status(201).send(RESPONSEMESSAGE.WISHLIST.NODATAEXISTS);
        }

    });

    /************* End ************************/


});




/*
 * Trainer Schdule Log List
 * Created On : 23-01-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
router.post('/getTrainerSchduleLogListWeb', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    var questionArr = [];
    var dbsetting = req.db_setting;
    var trainerId = req.body.trainerId;

    var fromDate = req.body.fromDate;
    var toDate = req.body.toDate;
    var searchCourseName = req.body.searchCourseName;
    var learningMode = req.body.learningMode;

    var base_url1 = "http://52.63.96.113:4444/share/courseimage/";
    var base_url2 = "http://52.63.96.113:4444/share/unitimage/";

    var d = new Date();
    var month = d.getMonth() + 1;
    var datetime = d.getFullYear() + "-" + month + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
    var fields = " mt.* , ml.name as learning_mode, DATE_FORMAT(mt.date, '%a %M %d %Y %H %i %p') as dateformat,  mc.summary as unit_description,  mcc.description as course_description, mc.fullname as uname, mc.idnumber as ucode, mcc.name as cname, mcc.idnumber as ccode, (case when mcc.course_image = null then mcc.course_image else concat('" + base_url1 + "',mcc.course_image) end) as courseimage, (case when mc.unit_image=null then mc.unit_image else concat('" + base_url2 + "',mc.unit_image) end) as unitimage";
    var condition = " where mt.trainerid = '" + trainerId + "'   ";
    condition += " AND  mt.date < '" + datetime + "' ";

    if (searchCourseName != "")
        condition += " AND ( mcc.name LIKE '%" + searchCourseName + "%' OR  mcc.idnumber LIKE '%" + searchCourseName + "%' ) ";
    if (learningMode != "")
        condition += " AND ( mt.learning_mode_type_id = '" + learningMode + "' ) ";
    if (fromDate != "" && (toDate == "" || toDate == undefined))
        condition += " AND  mt.date >= '" + fromDate + "' ";
    if (toDate != "" && (fromDate == "" || fromDate == undefined))
        condition += " AND  mt.date <= '" + toDate + "' ";
    if (fromDate != "" && toDate != "")
        condition += " AND  mt.date BETWEEN '" + fromDate + "' AND '" + toDate + "'   ";


    var join = " LEFT JOIN mdl_course mc             ON mc.id  = mt.unit_id";
    join += " LEFT JOIN mdl_course_categories mcc ON mcc.id = mt.course_id ";
    join += " LEFT JOIN mdl_learning_mode_type ml ON ml.id  = mt.learning_mode_type_id ";
    var orderby = " order by mt.date asc";
    var param = {

        from: 'from mdl_trainer_schdule mt ',
        condition: condition,
        fields: fields,
        join: join,
        orderby: orderby
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (qualificationRes) {

        var quesLen = qualificationRes.length;
        var queaArr = new Array();
        if (qualificationRes.length > 0) { // already present


            asyncForEach(qualificationRes, function (positionitem, positionindex, positionarr) {
                var cnt = positionindex;
                console.log(positionitem.id);
                var schduleid = positionitem.id;

                var fields = " count(id) as no_of_student ";
                var condition = " where schdule_id = '" + schduleid + "'  ";
                var param = {

                    from: 'from mdl_student_booked_vc ',
                    condition: condition,
                    fields: fields
                };

                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (courseRes) {
                    if (courseRes.length > 0) {
                        var no_of_student = courseRes[0].no_of_student;

                        qualificationRes[cnt].no_of_student = no_of_student;

                        cnt++;
                        questionArr.push(positionindex);
                        var questionLen = questionArr.length;
                        queaArr.push({
                            "questionId": positionitem.id
                        })

                        if (quesLen == queaArr.length) {

                            var response = {
                                resCode: 200,
                                response: qualificationRes
                            }
                            res.status(200).send(response);
                        }

                    }

                });

            })


        } else {
            res.status(201).send(RESPONSEMESSAGE.WISHLIST.NODATAEXISTS);
        }

    });

    /************* End ************************/


});


/*
 * Delete trainer vc Schdule
 * Created On : 23-01-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
router.post('/deleteTrainerSchdule', customodule.verifyapicalltoken, function (req, res, next) {

    var id = req.body.id;
    var dbsetting = req.db_setting;


    var param = {

        tablename: 'mdl_trainer_schdule',
        condition: {
            'id': id
        },
    };

    Mastermodel.commonDelete(param, function (err) {
        res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (success) {
        var response = {
            resCode: 200,
            resMessage: "Schdule deleted successfully",
            response: "Schdule deleted successfully"
        };
        res.status(200).send(response);
    });

});


/*
 * Used to get entry test question for trainer as unit wise
 * Created On : 06-09-2017
 * Created By : Monalisa
 * Input      : unitId(int)
 * Modified On
 */
router.post('/getTrainerEntryTestDetailsApp', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    console.log(req.db_setting);
    var dbsetting = req.db_setting;
    var unit_id = req.body.unitId;
    var course_id = req.body.courseId;
    var trainer_id = req.body.trainerId;
    //var isAccrediated  = req.body.isAccrediated;
    if (!course_id) {
        res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NOCOURSEFOUND);
    } else if (isNaN(course_id)) {
        res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.INVALIDCOURSE);
    } else {


        if (unit_id > 0) // for non accrediated course question will from course
        {

            var fields = "met.course_id, met.unit_id, met.id, met.question, met.question_type ";
            var condition = " where met.unit_id = '" + unit_id + "' ";
            var orderby = "order by rand() ";
            var param = {

                from: 'from mdl_entry_test_question_trainer as met',
                condition: condition,
                fields: fields,
                orderby: orderby
            };



        } else { // for accrediated course question will from unit


            var fields = "met.course_id, met.unit_id, met.id, met.question, met.question_type ";
            var condition = " where met.course_id  = '" + course_id + "' ";
            var orderby = "order by rand() ";
            var param = {

                from: 'from mdl_entry_test_question_trainer as met',
                condition: condition,
                fields: fields,
                orderby: orderby
            };


        }

        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (entrytestRes) {
            console.log(entrytestRes);
            var quesLen = entrytestRes.length;
            var queaArr = new Array();
            if (entrytestRes.length > 0) {
                asyncForEach(entrytestRes, function (positionitem, positionindex, positionarr) {
                    var cnt = positionindex;
                    console.log(positionitem.id);
                    var fields = " met.* ";
                    var condition = " where met.question_id = '" + positionitem.id + "' ";
                    var param = {

                        from: 'from mdl_entry_test_answer_trainer as met',
                        condition: condition,
                        fields: fields
                    };
                    Mastermodel.commonSelect(param, function (err) {
                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                    }, function (ansRes) {
                        queaArr.push({
                            "questionId": positionitem.id
                        })
                        entrytestRes[cnt].answer = ansRes;
                        cnt++;
                        console.log(quesLen, queaArr.length);
                        if (quesLen == queaArr.length) {

                            var response = {
                                resCode: 200,
                                response: entrytestRes
                            };
                            res.status(200).send(response);
                        }
                    })
                })
            } else {
                res.status(201).send(RESPONSEMESSAGE.ENTRYTEST.NORECORDSFOUND);
            }
        });
    }
});

/*
 * Used to save online schdule for trainer
 * Created On : 23-12-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/addEditOnlineSchdule', customodule.verifyapicalltoken, function (req, res, next) {



    var dbsetting = req.db_setting;
    var date = req.body.date;
    //var time      = req.body.time;
    var price = req.body.price;
    var no_of_audience = req.body.no_of_audience;
    var deadline = req.body.deadline;
    var trainerId = req.body.trainerId;
    var schduleId = req.body.schduleId;

    var courseId = req.body.courseId;
    var unitId = req.body.unitId;
    var duration = req.body.duration;

    var todate = req.body.todate;
    var learning_mode_type_id = req.body.learning_mode_type_id;

    //var datetime = req.body.datetime;

    //console.log("datetime",datetime);

    if (schduleId > 0) {
        var dataupdate = {
            date: date,
            todate: todate,
            learning_mode_type_id: learning_mode_type_id,
            //time        : time,
            price: price,
            no_of_audience: no_of_audience,
            deadline: deadline,
            course_id: courseId,
            unit_id: unitId,
            duration: duration
        }
        var updatecondition = {
            id: schduleId
        }
        var param3 = {

            tablename: 'mdl_trainer_schdule',
            condition: updatecondition,
            data: dataupdate
        }
        Mastermodel.commonUpdate(param3, function (error) {
            console.log(error);
            res.status(201).send(RESPONSEMESSAGE.TRAINER.SOMETHINGWRONG);
        }, function (data4) {
            res.status(200).send(RESPONSEMESSAGE.TRAINER.TRAINERSCHDULEUPDATEDSUCCESSFULLY);
        });


    } else {
        var saveData = {};
        saveData.date = req.body.date;
        //saveData.time           = req.body.time;
        saveData.price = req.body.price;
        saveData.no_of_audience = req.body.no_of_audience;
        saveData.deadline = req.body.deadline;
        saveData.trainerid = req.body.trainerId;
        saveData.course_id = req.body.courseId;
        saveData.unit_id = req.body.unitId;
        saveData.duration = req.body.duration;
        saveData.todate = req.body.todate;
        saveData.learning_mode_type_id = req.body.learning_mode_type_id;
        var param = {

            tablename: 'mdl_trainer_schdule',
            data: [saveData]
        }
        Mastermodel.commonInsert(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
        }, function (data) {
            res.status(200).send(RESPONSEMESSAGE.TRAINER.TRAINERSCHDULEADDEDSUCCESSFULLY);
        })
    }



});

/*
 * Used to get Single Online Schdule
 * Created On : 23-12-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
router.post('/getSingleOnlineSchdule', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    var questionArr = [];
    var dbsetting = req.db_setting;
    var schduleId = req.body.schduleId;

    /**** Used to get RPL question list ****/
    var fields = " *";
    var condition = " where 1 AND id = '" + schduleId + "' ";

    var param = {

        from: 'from mdl_trainer_schdule',
        condition: condition,
        fields: fields
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (rplRes) {

        console.log(rplRes);

        var response = {
            resCode: 200,
            response: rplRes[0]
        }

        if (rplRes.length > 0) {
            res.status(200).send(response);
        } else {
            res.status(200).send(RESPONSEMESSAGE.RPL.NORECORDSFOUND);
        }
    });

});



/*
 * Used to get learning mode
 * Created On : 02-03-2018
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
router.post('/getLearningMode', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    var questionArr = [];
    var dbsetting = req.db_setting;


    /**** Used to get RPL question list ****/
    var fields = "*";
    var condition = "where id in (2,3,4) ";

    var param = {

        from: 'from mdl_learning_mode_type',
        condition: condition,
        fields: fields
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (learningRes) {

        //console.log(rplRes);

        var response = {
            resCode: 200,
            response: learningRes
        }

        if (learningRes.length > 0) {
            res.status(200).send(response);
        } else {
            res.status(201).send(RESPONSEMESSAGE.RPL.NORECORDSFOUND);
        }
    });

});

/*
 * Used to get f2f Online Schdule
 * Created On : 23-12-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
router.post('/getf2fSingleOnlineSchdule', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    var questionArr = [];
    var dbsetting = req.db_setting;
    var schduleId = req.body.schduleId;

    /**** Used to get RPL question list ****/
    var fields = " *";
    var condition = " where 1 AND id = '" + schduleId + "' ";

    var param = {

        from: 'from mdl_trainer_f2f_schdule',
        condition: condition,
        fields: fields
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (rplRes) {

        console.log(rplRes[0]);
        var response = {
            resCode: 200,
            response: rplRes[0]
        }
        if (rplRes.length > 0) {
            res.status(200).send(response);
        } else {
            res.status(201).send(RESPONSEMESSAGE.RPL.NORECORDSFOUND);
        }
    });

});

/*
 * Used to save f2f schdule for trainer
 * Created On : 23-12-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/addEditf2fOnlineSchdule', customodule.verifyapicalltoken, function (req, res, next) {



    var dbsetting = req.db_setting;
    var date = req.body.date;
    //var time = req.body.time;
    var price = req.body.price;
    var no_of_audience = req.body.no_of_audience;
    var deadline = req.body.deadline;
    var trainerId = req.body.trainerId;
    var schduleId = req.body.schduleId;

    var courseId = req.body.courseId;
    var unitId = req.body.unitId;
    var duration = req.body.duration;

    if (schduleId > 0) {
        var dataupdate = {
            date: date,
            duration: duration,
            price: price,
            no_of_audience: no_of_audience,
            deadline: deadline,
            course_id: courseId,
            unit_id: unitId
        }

        var updatecondition = {
            id: schduleId
        }

        var param3 = {

            tablename: 'mdl_trainer_f2f_schdule',
            condition: updatecondition,
            data: dataupdate
        }
        Mastermodel.commonUpdate(param3, function (error) {
            console.log(error);
            res.status(201).send(RESPONSEMESSAGE.TRAINER.SOMETHINGWRONG);
        }, function (data4) {
            res.status(200).send(RESPONSEMESSAGE.TRAINER.TRAINERSCHDULEUPDATEDSUCCESSFULLY);
        });


    } else {
        var saveData = {};
        saveData.date = req.body.date;
        saveData.price = req.body.price;
        saveData.no_of_audience = req.body.no_of_audience;
        saveData.deadline = req.body.deadline;
        saveData.trainerid = req.body.trainerId;
        saveData.duration = req.body.duration;

        saveData.course_id = req.body.courseId;
        saveData.unit_id = req.body.unitId;

        var param = {

            tablename: 'mdl_trainer_f2f_schdule',
            data: [saveData]
        }
        Mastermodel.commonInsert(param, function (err) {
            res.status(200).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
        }, function (data) {
            res.status(200).send(RESPONSEMESSAGE.TRAINER.TRAINERSCHDULEADDEDSUCCESSFULLY);
        })
    }



});

/*
 * get f2f trainer schdule upcoming list
 * Created On : 23-12-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
router.post('/getf2fTrainerSchduleList', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    var questionArr = [];
    var dbsetting = req.db_setting;
    var trainerId = req.body.trainerId;

    var d = new Date();
    var month = d.getMonth() + 1;
    var datetime = d.getFullYear() + "-" + month + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
    console.log(datetime);

    var fields = " mt.* , mc.fullname as uname, mc.idnumber as ucode, mcc.name as cname, mcc.idnumber as ccode";
    var condition = " where mt.trainerid = '" + trainerId + "'  ";
    condition += " AND  mt.date >= '" + datetime + "' ";

    var join = " LEFT JOIN mdl_course mc             ON mc.id  = mt.unit_id";
    join += " LEFT JOIN mdl_course_categories mcc ON mcc.id = mt.course_id ";
    var orderby = " order by mt.date asc";
    var param = {

        from: 'from mdl_trainer_f2f_schdule mt ',
        condition: condition,
        fields: fields,
        join: join,
        orderby: orderby
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (qualificationRes) {
        if (qualificationRes.length > 0) { // already present

            var response = {
                resCode: 200,
                response: qualificationRes
            }
            res.status(200).send(response);


        } else {
            res.status(201).send(RESPONSEMESSAGE.WISHLIST.NODATAEXISTS);
        }

    });

    /************* End ************************/
});



/*
 * get f2f trainer schdule completed  list
 * Created On : 23-12-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
router.post('/getf2fTrainerSchduleLogList', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    var questionArr = [];
    var dbsetting = req.db_setting;
    var trainerId = req.body.trainerId;

    var d = new Date();
    var month = d.getMonth() + 1;
    var datetime = d.getFullYear() + "-" + month + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
    console.log(datetime);

    var fields = " mt.* , mc.fullname as uname, mc.idnumber as ucode, mcc.name as cname, mcc.idnumber as ccode";
    var condition = " where mt.trainerid = '" + trainerId + "'  ";
    condition += " AND  mt.date < '" + datetime + "' ";

    var join = " LEFT JOIN mdl_course mc             ON mc.id  = mt.unit_id";
    join += " LEFT JOIN mdl_course_categories mcc ON mcc.id = mt.course_id ";
    var orderby = " order by mt.date asc";
    var param = {

        from: 'from mdl_trainer_f2f_schdule mt ',
        condition: condition,
        fields: fields,
        join: join,
        orderby: orderby
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (qualificationRes) {
        if (qualificationRes.length > 0) { // already present

            var response = {
                resCode: 200,
                response: qualificationRes
            }
            res.status(200).send(response);


        } else {
            res.status(201).send(RESPONSEMESSAGE.WISHLIST.NODATAEXISTS);
        }

    });

    /************* End ************************/
});


/*
 * delete f2f trainer schdule
 * Created On : 23-12-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
router.post('/deletef2fTrainerSchdule', customodule.verifyapicalltoken, function (req, res, next) {

    var id = req.body.id;
    var dbsetting = req.db_setting;


    var param = {

        tablename: 'mdl_trainer_f2f_schdule',
        condition: {
            'id': id
        },
    };

    Mastermodel.commonDelete(param, function (err) {
        res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (success) {
        var response = {
            resCode: 200,
            resMessage: "Schdule deleted successfully",
            response: "Schdule deleted successfully"
        };
        res.status(200).send(response);
    });

});


/*
 * approve/reject rpl unit request
 * Created On : 24-12-2017
 * Created By : Monalisa
 * Input      : NA
 * Modified On
 */
router.post('/approveRejectRPLDetailsUnitRequest', customodule.verifyapicalltoken, function (req, res, next) {


    var dbsetting = req.db_setting;

    var id = req.body.id;
    var comment = req.body.comment;
    var is_accepted = req.body.is_accepted;

    if (!id) {
        res.status(201).send(RESPONSEMESSAGE.COMMONALL.IDREQUIRED);
    } else if (!comment) {
        res.status(201).send(RESPONSEMESSAGE.COMMONALL.COMMENTREQUIRED);
    } else if (!is_accepted) {
        res.status(201).send(RESPONSEMESSAGE.COMMONALL.APPROVEREJECTREQUIRED);
    } else {


        var dataupdate = {
            is_accepted: is_accepted,
            comment: comment,
            is_action_taken: 'Y'
        }
        var updatecondition = {
            id: id
        }
        var param3 = {

            tablename: 'mdl_rpl_process_question_evidence',
            condition: updatecondition,
            data: dataupdate
        }
        Mastermodel.commonUpdate(param3, function (error) {
            console.log(error);
            res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
        }, function (data4) {

            var response = {
                resCode: 200,
                resMessage: "success",
                response: "success"
            }
            res.status(200).send(response);

        })


    }

});






/*
 * Used to get Trainer course details
 * Created On : 05-09-2017
 * Created By : Monalisa
 * Input      : courseid
 * Modified On
 */
router.post('/getTrainerCourseOverview', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    //console.log(req.db_setting);
    var dbsetting = req.db_setting;
    var courseId = req.body.courseId;
    var trainerid = req.body.trainerId;
    /**** Course Search By search Text and isAccrediated parameter  ****/

    /*select mcc.*, mt.is_qualified, mt.unit_id, mt.trainer_id from mdl_course_categories mcc
    LEFT JOIN mdl_entry_test_results_trainer mt ON mcc.id = mt.course_id AND mt.unit_id = 0
    where mt.trainer_id = 72 || mt.trainer_id = '' || mt.trainer_id IS NULL */


    var fields = " mcc.id,  mcc.name,  mcc.idnumber, mcc.course_image, mcc.description, mcc.isaccrediated, mt.is_qualified, mt.unit_id, mtq.ispassed, mtq.isactiontaken, mtq.issubmited, mtq.comment , mtw.iswishlistadd ";
    var condition = " where 1 AND mcc.id = " + courseId;
    var join = " LEFT JOIN mdl_entry_test_results_trainer mt     ON mcc.id = mt.course_id    AND mt.unit_id = 0     AND mt.trainer_id = " + trainerid + " ";
    join += " LEFT JOIN mdl_trainer_qualification_master  mtq ON mcc.id = mtq.course_id   AND mtq.unit_id = 0    AND mtq.trainer_id = " + trainerid + " ";
    join += " LEFT JOIN mdl_trainer_wishlist mtw              ON mcc.id  = mtw.course_id  AND mtw.trainer_id = " + trainerid + " ";
    if (trainerid != "")
        condition += " AND ( mt.trainer_id = " + trainerid + " || mt.trainer_id = '' || mt.trainer_id IS NULL ) ";

    var param = {

        from: 'from mdl_course_categories as mcc',
        condition: condition,
        fields: fields,
        join: join
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (courseRes) { //console.log(courseRes);
        if (courseRes.length > 0) {
            var response = {
                resCode: 200,
                response: courseRes[0]
            }
            res.status(200).send(response);

        } else {
            res.status(201).send(RESPONSEMESSAGE.COURSE.NORECORDFOUND);
        }
    });



});

/*
 * Trainer approved unit list
 * Created On : 17-12-2017
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/getTrainerRPLUnitSchdule', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    var questionArr = [];
    var dbsetting = req.db_setting;
    var trainerId = req.body.trainerId;
    var courseId = req.body.courseId;

    var fields = "ms.unit_id, mc.fullname, mc.idnumber";
    var condition = " where ms.trainer_id ='" + trainerId + "' AND ms.course_id =  '" + courseId + "' ";
    var join = " LEFT JOIN mdl_course mc     ON mc.id = ms.unit_id  ";
    var param = {

        from: 'from mdl_trainer_approved_units ms ',
        condition: condition,
        fields: fields,
        join: join
    };
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (qualificationRes) {
        if (qualificationRes.length > 0) { // already present

            var response = {
                resCode: 200,
                response: qualificationRes
            }
            res.status(200).send(response);


        } else {
            res.status(201).send(RESPONSEMESSAGE.WISHLIST.NODATAEXISTS);
        }

    });

    /************* End ************************/


});



/*
 * Used to trainer approved course unit
 * Created On : 19-01-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/trainerApprovedCourseUnit', customodule.verifyapicalltoken, function (req, res, next) {
    var trainerId = req.body.trainerId;
    var dbsetting = req.db_setting;


    if (!trainerId || trainerId == 0) {
        res.status(201).send(RESPONSEMESSAGE.RPL.TRAINERIDREQUIRED);
    } else {


        var fields = " distinct(mt.course_id), mcc.name as cname, mcc.idnumber as ccode ";
        var condition = " where mt.trainer_id = " + trainerId + "  ";
        var join = " LEFT JOIN mdl_course_categories as mcc ON mcc.id = mt.course_id   ";
        var orderby = " order by cname asc";

        var param = {

            from: 'from mdl_trainer_approved_units mt ',
            condition: condition,
            fields: fields,
            join: join,
            orderby: orderby
        };

        console.log("param222", param);


        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (userRes) {
            if (userRes.length > 0) {
                var response = {
                    resCode: 200,
                    response: userRes
                }
                res.status(201).send(response);
            } else {
                res.status(201).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
            }
        }); //end of select


    }
}); //e

/*
 * API to get dashboard for trainer
 * Created On : 13-02-2018
 * Input      :
 * Modified On
 */
router.post('/getTrainerDashboard', customodule.verifyapicalltoken, function (req, res, next) {
    var dbsetting = req.db_setting;
    var trainerId = req.body.trainerId;
    var myCourseCnt = 0;
    var dashboardObj = {};
    if (!trainerId) {
        res.status(200).send(RESPONSEMESSAGE.STUDENT.STUDENTIDREQUIRED);
    } else {
        console.log(trainerId);
        //get total no of student registered in RPL
        var dbsetting = req.db_setting;
        var fields = " count( distinct(mrpsm.student_id) ) as totstudent ";
        var condition = "where 1";
        condition += " AND mrpsm.trainer_id = " + trainerId + "  ";
        var join = " ";
        var param = {

            from: 'from mdl_student_trainer_list mrpsm ',
            condition: condition,
            fields: fields,
            join: join
        };
        Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (myStudentRes) {

            var d = new Date();
            var month = d.getMonth() + 1;
            var datetime = d.getFullYear() + "-" + month + "-" + d.getDate() + " " + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();

            totStudentCnt = myStudentRes[0]['totstudent'];
            dashboardObj.totStudentCnt = totStudentCnt;
            //GET THE CLASS COUNT BY COUNTING THE VARTUAL CLASS ROOM
            var fields = " count(mts.id) as totclasscnt ";
            var condition = "where 1";
            condition += " AND mts.trainerid = '" + trainerId + "' AND  mts.todate >= '" + datetime + "' ";
            var join = " ";
            var param = {

                from: 'from mdl_trainer_schdule as mts',
                condition: condition,
                fields: fields,
                join: join
            };
            Mastermodel.commonSelect(param, function (err) {
                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            }, function (myRplRes) {
                totClassCnt = myRplRes[0]['totclasscnt'];
                dashboardObj.totClassCnt = totClassCnt;
                //GET THE PAYMENT COUNT
                var dbsetting = req.db_setting;
                var fields = " sum(amount) as totreceivedamount ";
                var condition = "where 1";
                condition += " AND mpd.trainer_id = " + trainerId + " AND mpd.is_refund = 'N' ";
                var join = " ";
                var param = {

                    from: 'from mdl_payment_details mpd ',
                    condition: condition,
                    fields: fields,
                    join: join
                };
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (myPaymentRes) {
                    var trainerReceivedAmount = 0;
                    if (myPaymentRes[0]['totreceivedamount'] != null) {
                        trainerReceivedAmount = myPaymentRes[0]['totreceivedamount'];
                    }
                    dashboardObj.totReceivedAmt = trainerReceivedAmount;
                    //GET THE PENDING UNIT FOR APPROVAL
                    var dbsetting = req.db_setting;
                    var fields = " mc.id as unitid,mcc.name as coursename,mc.fullname as unitname,(case when mc.unit_image=null then mc.unit_image else concat('" + SETTING.course_image_url + "',mc.unit_image) end) as unitimage  ";
                    var condition = "where 1";
                    condition += "  AND mtqm.trainer_id = " + trainerId + " AND isactiontaken = 'No' AND issubmited = 'Yes' ";
                    var join = "  INNER JOIN mdl_course mc ON mc.id = mtqm.unit_id ";
                    join += "  INNER JOIN mdl_course_categories mcc ON mcc.id = mc.category ";

                    var limit = " 0,5 ";
                    var param = {

                        from: 'from mdl_trainer_qualification_master as mtqm',
                        condition: condition,
                        fields: fields,
                        join: join,
                        limit: limit
                    };
                    Mastermodel.commonSelect(param, function (err) {
                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                    }, function (myUnitPendingRes) {
                        dashboardObj.myUnitPendingRes = myUnitPendingRes;

                        //GET THE UNIT APPROVED BY ADMIN
                        var fields = "mu.*, mcc.name as cname, mcc.idnumber as ccode, mc.fullname as uname, mc.idnumber as ucode,(case when mc.unit_image=null then mc.unit_image else concat('" + SETTING.course_image_url + "',mc.unit_image) end) as unitimage";
                        var condition = " where trainer_id = '" + trainerId + "' ";
                        var join = "LEFT JOIN mdl_course_categories as mcc ON mcc.id = mu.course_id ";
                        join += "LEFT JOIN mdl_course as mc             ON mc.id  = mu.unit_id ";
                        var limit = " 0,5 ";
                        var param = {

                            from: 'from mdl_trainer_approved_units mu ',
                            condition: condition,
                            fields: fields,
                            join: join,
                            limit: limit
                        };
                        Mastermodel.commonSelect(param, function (err) {
                            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                        }, function (myApprovedUnitRes) {
                            dashboardObj.myApprovedUnitRes = myApprovedUnitRes;
                            //GET THE DTRAFT MODE RPL VERIFIED LIST
                            var dbsetting = req.db_setting;
                            var fields = " mu.firstname,mu.lastname,mcc.id as courseid,mcc.name,mc.id as unitid ,mc.fullname as unitname,submission_datetime,(case when mcc.course_image=null then mcc.course_image else concat('" + SETTING.course_image_url + "',mcc.course_image) end) as courseimage  ";
                            var condition = "where 1";
                            condition += "  AND mptr.trainer_id = '" + trainerId + "' AND mrpsm.is_valid = 'Y' AND mrpsm.is_actiontaken = 'N' AND mrpsm.is_approved = 'N' ";
                            var join = " INNER JOIN mdl_course_categories mcc  ON mrpsm.course_id = mcc.id  ";
                            join += " INNER JOIN mdl_course mc  ON mcc.id = mc.category  ";
                            join += " INNER JOIN mdl_rpl_process_trainer_request mptr  ON mptr.rpl_process_submission_id = mrpsm.id  ";
                            join += " LEFT JOIN mdl_user mu  ON mu.id = mrpsm.student_id  ";
                            var orderby = " order by mptr.request_date desc ";
                            var param = {

                                from: 'from mdl_rpl_process_submission_master as mrpsm',
                                condition: condition,
                                fields: fields,
                                join: join,
                                orderby: orderby
                            };
                            Mastermodel.commonSelect(param, function (err) {
                                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                            }, function (myRplDraftRes) {
                                dashboardObj.myRplDraftRes = myRplDraftRes;
                                //GET THE UNIT RPL VERIFICATION COMPLETION LIST
                                var dbsetting = req.db_setting;
                                var fields = " mu.firstname,mu.lastname,mcc.id as courseid,mcc.name,mc.id as unitid ,mc.fullname as unitname,submission_datetime,(case when mcc.course_image=null then mcc.course_image else concat('" + SETTING.course_image_url + "',mcc.course_image) end) as courseimage  ";
                                var condition = "where 1";
                                condition += "  AND mrpsm.trainer_id = '" + trainerId + "' AND mrpsm.is_valid = 'Y' AND mrpsm.is_actiontaken = 'Y' AND mrpsm.is_approved = 'Y' ";
                                var join = " INNER JOIN mdl_course_categories mcc  ON mrpsm.course_id = mcc.id  ";
                                join += " INNER JOIN mdl_course mc  ON mcc.id = mc.category  ";
                                join += " LEFT JOIN mdl_user mu  ON mu.id = mrpsm.student_id  ";
                                var orderby = " order by mrpsm.action_datetime desc ";
                                var param = {

                                    from: 'from mdl_rpl_process_submission_master as mrpsm',
                                    condition: condition,
                                    fields: fields,
                                    join: join,
                                    orderby: orderby
                                };
                                var param = {

                                    from: 'from mdl_rpl_process_submission_master as mrpsm',
                                    condition: condition,
                                    fields: fields,
                                    join: join
                                };
                                Mastermodel.commonSelect(param, function (err) {
                                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                                }, function (myRplCompletedVerificationRes) {
                                    dashboardObj.myRplCompletedVerificationRes = myRplCompletedVerificationRes;
                                    //GET THE DTRAFT MODE RPL PENDING PAYMENT
                                    var dbsetting = req.db_setting;
                                    var fields = " msn.notification_title as action_type,msn.notification_message as action_taken  ";
                                    var condition = "where 1";
                                    condition += "  AND msn.user_id = '" + trainerId + "'   ";
                                    var join = " ";
                                    var param = {

                                        from: 'from mdl_user_notification as msn',
                                        condition: condition,
                                        fields: fields,
                                        join: join
                                    };
                                    Mastermodel.commonSelect(param, function (err) {
                                        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                                    }, function (myNotificationRes) {
                                        dashboardObj.myNotificationRes = myNotificationRes;
                                        var response = {
                                            resCode: 200,
                                            response: dashboardObj
                                        }
                                        res.status(200).send(response);
                                    });
                                });
                            });
                        });
                    });
                })
            })
        });
    }
})
//get the training style question
/*
 * Used to get dynamic learning style data;
 * Created On : 02-02-2018
 * Created By : Monalisa
 * Input      :
 * Modified On
 */
router.post('/getDynamicTrainerTrainingStyle', customodule.verifyapicalltoken, function (req, res, next) {
    //console.log(req.body);
    var quizJSON = {};
    var questionArr = [];
    var dbsetting = req.db_setting;

    var fields = "mlsq.id, mlsq.question as q ";
    var condition = "where 1 ";
    var param = {

        from: 'from mdl_training_style_question as mlsq',
        condition: condition,
        fields: fields
    };
    quizJSON.info = {
        "name": "Test Your Knowledge!!",
        "main": "<p class='mb-15'>Think you're smart enough to be on Jeopardy? Find out with this super crazy knowledge quiz!</p>",
        "results": "<h5>Learn More</h5><p>If you are an auditory learner, you learn by hearing and listening. You understand and remember things you have heard. You store information by the way it sounds, and you have an easier time understanding spoken instructions than written ones. You often learn by reading out loud because you have to hear it or speak it in order to know it.</p><p>As an auditory learner, you probably hum or talk to yourself or others if you become bored. People may think you are not paying attention, even though you may be hearing and understanding everything being said.</p>",
        "level1": "Jeopardy Ready",
        "level2": "Jeopardy Contender",
        "level3": "Jeopardy Amateur",
        "level4": "Jeopardy Newb",
        "level5": "Stay in school, kid..."
    }
    Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (entrytestRes) {
        console.log(entrytestRes);
        var entryTestquestionLen = entrytestRes.length;
        if (entrytestRes.length > 0) {

            /**** Used to get student gap traning documents ****/
            asyncForEach(entrytestRes, function (positionitem, positionindex, positionarr) {
                var cnt = positionindex;
                console.log(positionitem.id);
                var fields = " mlsd.id, mlsd.type as value, (case when mlsd.option_file=null then mlsd.option_file else concat('" + SETTING.learning_style_image_url + "',mlsd.option_file) end) as `option` ";
                var condition = " where mlsd.questionid = '" + positionitem.id + "' ";
                var param = {

                    from: 'from mdl_training_style_details as mlsd',
                    condition: condition,
                    fields: fields
                };
                // entrytestRes[positionindex].hii="12345" ;
                Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (ansRes) {
                    questionArr.push(positionindex);
                    var questionLen = questionArr.length;
                    // console.log(entrytestRes[positionindex]);
                    //  console.log(entrytestRes);
                    entrytestRes[cnt].a = ansRes;
                    cnt++;
                    console.log(entryTestquestionLen, questionLen);
                    if (entryTestquestionLen == questionLen) {
                        quizJSON.questions = entrytestRes;
                        var response = {
                            resCode: 200,
                            quizJSON: quizJSON
                        }
                        res.status(200).send(response);
                    }
                })
            });

        } else {
            res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NORECORDSFOUND);
        }
    });
});

//save the training style
//save the learning style
router.post('/saveTrainerTrainingStyle', customodule.verifyapicalltoken, function (req, res, next) {
    console.log(req.body.answerArr);
    console.log(req.body.trainerId);
    var dbsetting = req.db_setting;
    var trainerId = req.body.trainerId;
    if (req.body.answerArr != undefined) {
        if (req.body.answerArr.length > 0) {
            var reqAnswserArr = req.body.answerArr;
            var duplicateAnserArr = [];
            duplicateAnswerObj = reqAnswserArr.reduce(function (prev, cur) {
                prev[cur] = (prev[cur] || 0) + 1;
                return prev;
            }, {});
            // map is an associative array mapping the elements to their frequency:
            //var duplicateAnswerObj = JSON.stringify(duplicateAnserArr);
            var maxAnswerKey = Object.keys(duplicateAnswerObj).reduce(function (a, b) {
                return duplicateAnswerObj[a] > duplicateAnswerObj[b] ? a : b
            });
            //get the learning detail
            var fields = " msa.* ";
            var condition = " where msa.id = '" + maxAnswerKey + "' ";
            var param = {

                from: 'from mdl_training_attribute as msa',
                condition: condition,
                fields: fields
            };
            // entrytestRes[positionindex].hii="12345" ;
            Mastermodel.commonSelect(param, function (err) {
                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            }, function (learningTypeRes) {
                var dataupdate = {
                    trainingtypeid: maxAnswerKey
                }
                var updatecondition = {
                    id: trainerId
                }
                var param = {

                    tablename: 'mdl_user',
                    condition: updatecondition,
                    data: dataupdate
                }
                Mastermodel.commonUpdate(param, function (error) {

                }, function (data) {
                    var success = {
                        "resCode": 200,
                        "response": learningTypeRes
                    }
                    res.status(200).send(success);
                })
            })
        } else {
            res.status(201).send(RESPONSEMESSAGE.USER.PROFILEVIEDOREQUIRED);
        }
    } else {
        res.status(201).send(RESPONSEMESSAGE.USER.PROFILEVIEDOREQUIRED);
    }
});



module.exports = router;