var express = require('express');
var router = express.Router();
var app = express();
var customodule = require('../../../../customexport');
var node_module = require('../../../../export');
var commonFunction = require('../../../../../config/commonFunction');
var sendGroupNotification = require('../../../../../lib/sendnotification');
var RESPONSEMESSAGE = require('../../../../../config/statuscode');
var NOTIFICATIONMESSAGE = require('../../../../../config/notificationcode');
var Mastermodel = require('../../../mastermodel.js');
var Masternotificationmodel = require('../../../commonPushNotificationModel.js');
var asyncForEach = require('async-foreach').forEach;
var database = require('../../../../../config/database');
var SETTING = require('../../../../../config/setting');
var studentModel = require('../../student/model/studentModel');
var async = require('async');
var mysql = require('mysql');
var connection = mysql.createConnection(database);

//* ****************************** GET COURSE LIST CONTROLLER************
router.post('/testMultipleInsert', customodule.verifyapicalltoken, function (req, res, next) {
  var dbsetting = req.db_setting;
  var saveDataArr = [];
  saveDataArr.push({
    "userid": 5,
    "fieldid": 3,
    "data": 'kolkata',
    "dataformat": 0,
  }, {
    "userid": 5,
    "fieldid": 4,
    "data": 'kolkata',
    "dataformat": 0,
  }, {
    "userid": 5,
    "fieldid": 5,
    "data": 'kolkata',
    "dataformat": 0,
  }, {
    "userid": 5,
    "fieldid": 6,
    "data": 'kolkata',
    "dataformat": 0,
  }, {
    "userid": 5,
    "fieldid": 7,
    "data": 'kolkata',
    "dataformat": 0,
  });

  var insertParam = {

    tablename: 'mdl_user_info_data',
    data: saveDataArr
  };
  Mastermodel.commonInsert(insertParam, function (error) {
    res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
  }, function (success) {
    var response = {
      resCode: 200,
      resMessage: "Cover letter created successfully"
    };
    res.status(200).send(response);
  });
});
/* GENERATE MD5 PASSWORD */
router.post('/testPassword', customodule.verifyapicalltoken, function (req, res, next) {
  var password = 'password';
  var newPassword = node_module.crypto.createHash('md5').update(customodule.setting.passprefix + password).digest("hex");
  console.log(newPassword);
});
/* TEST FILE UPLOAD */
var storage = node_module.multer.diskStorage({
  destination: function (req, file, cb) {
    var dest = 'public/test';
    node_module.mkdirp(dest, function (err) {
      if (err) cb(err, dest);
      else cb(null, dest);
    });
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + node_module.path.extname(file.originalname));
  }
});

var upload = node_module.multer({
  storage: storage
});
router.post('/testFileupload', upload.any(), function (req, res) {
  //cb(null, file.fieldname + '-' + Date.now())
  console.log('uploading file');
  console.log(req.files);
});
/*
 * Login
 * Created On : 18-12-2017
 * Created By : Dipti
 * Input      : NA
 * Modified On
 */
router.post('/userLogin', customodule.verifyapicalltoken, function (req, res, next) {
  var dbsetting = req.db_setting;
  var username = req.body.username;
  var password = req.body.password;
  var userType = req.body.userType;
  var userLoginType = req.body.userLoginType;
  var fields = " mu.id, mu.isverified, mu.username, mu.email";
  var condition = " where 1 AND mu.username = '" + username + "' AND mu.password = '" + password + "' AND mu.usertype = '" + userType + "' ";
  var param = {

    from: 'from mdl_user as mu',
    condition: condition,
    fields: fields
  };
  //ist check the user login type wheather through common login or scoical login
  if (!userLoginType) {
    res.status(201).send(RESPONSEMESSAGE.USER.REQUIREDFIELDERROR);
  } else {
    if (userLoginType == 'commonLogin') {
      Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
      }, function (rplRes) {
        if (rplRes.length > 0) {
          var isverified = rplRes[0].isverified;
          if (isverified == 1) {
            /****** Login Email ********************/
            var username = rplRes[0].username;
            var email = rplRes[0].email;
            var emailObj = {
              name: username,
              email: email
            };
            customodule.mail('loginEmail', emailObj);
            /***************************************/
            var response = {
              resCode: 200,
              response: {
                userid: rplRes[0].id
              }
            };
            res.status(200).send(response);
          } else {
            var response = {
              resCode: 202,
              response: {
                userid: rplRes[0].id
              }
            };
            res.status(202).send(response);
          }
        } else {
          res.status(201).send(RESPONSEMESSAGE.USER.USERNOTLOGGEDIN);
        }
      });
    }
    if (userLoginType == 'socialLogin') {
      var socialUserId = req.body.socialUserId;
      var regType = req.body.regType;
      var imageUrl = req.body.imageUrl;
      var email = req.body.userEmail;
      if (!socialUserId || !regType) {
        res.status(201).send(RESPONSEMESSAGE.USER.REQUIREDFIELDERROR);
      } else {
        /**** find the social user from database ****/
        var fields = " mu.id, mu.isverified";
        if (email == '') {
          var condition = " where mu.socialuserid = '" + socialUserId + "'  AND mu.regtype = '" + regType + "' AND usertype = '" + userType + "' ";
        } else {
          var condition = " where mu.email = '" + email + "'  AND usertype = '" + userType + "' ";
        }
        console.log('condition' + condition);
        var param = {

          from: 'from mdl_user as mu',
          condition: condition,
          fields: fields
        };
        Mastermodel.commonSelect(param, function (err) {
          res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (rplRes) {
          if (rplRes.length > 0) {
            var response = {
              resCode: 200,
              response: rplRes[0]
            };
            res.status(200).send(response);
          } else {
            var isVerified = 0;
            if (email != '') {
              isVerified = 1;
            }
            //if the user is not exist then insert the user record in to the database
            var insertUserSql = "INSERT INTO `mdl_user` (`auth`, `username`, `password`, `email`, `firstname`, `lastname`, `phone1`, `city`, `country`, `socialimageurl`, `socialuserid`, `regtype`, `state`, `address1`, `address2`, `zip`, `userType`, `isverified`) VALUES ('manual', '', '', '" + email + "', '', '', '', '', '', '" + imageUrl + "', '" + socialUserId + "', '" + regType + "', '', '', '', '', '" + userType + "', " + isVerified + ")";
            console.log(insertUserSql);
            connection.query(insertUserSql, function (insertUserError, insertUserData, insertUserFields) {
              if (insertUserError != null) {
                res.status(201).send(RESPONSEMESSAGE.USER.REQUIREDFIELDERROR);
              } else {
                console.log('insertUserData', insertUserData);
                var userid = insertUserData.insertId;
                var userObj = {
                  userid: userid,
                  isverified: isVerified
                };
                var response = {
                  resCode: 200,
                  response: userObj
                };
                res.status(200).send(response);
              }
            });
          }
        });
      }
    }
  }
});

router.post('/verifyEmailOtp', customodule.verifyapicalltoken, function (req, res, next) {
  var dbsetting = req.db_setting;
  var username = req.body.username;
  var userid = req.body.userid;
  var email = req.body.email;
  /****** insert into user otp verification table ***********************/
  var otp = commonFunction.randomFixedInteger(4);
  var saveData = {};
  saveData.user_id = userid;
  saveData.type = 'registration';
  saveData.otp = otp;
  var param = {

    tablename: 'mdl_user_opt_verification',
    data: [saveData]
  };
  Mastermodel.commonInsert(param, function (err) {
    res.status(201).send(RESPONSEMESSAGE.USER.OTPWRONGUSER);
  }, function (data) {
    // mail send for otp will call here

    var emailObj = {
      username: username,
      email: email,
      otp: otp
    };
    customodule.mail('otpverification', emailObj);
    var userObj = {
      userid: userid
    };
    var response = {
      resCode: 200,
      response: userObj
    };
    res.status(200).send(response);
  });
});
router.post('/sendEmail', customodule.verifyapicalltoken, function (req, res, next) {
  var emailObj = {
    username: 'dipti',
    email: 'diptiranjan.nayak@indusnet.co.in',
    otp: '7865'
  };
  customodule.mail('otpverification', emailObj);
});

/**
 * Update profile api
 *
 */
router.post('/updateProfile', customodule.verifyapicalltoken, upload.any(), function (req, res, next) {
  var dbsetting = req.db_setting;
  var id = req.body.id;
  var fname = req.body.fname;
  var lname = req.body.lname;
  var phone = req.body.phone;
  var city = req.body.city;
  var country = req.body.country;
  var state = req.body.state;
  var address1 = req.body.address1;
  var address2 = req.body.address2;
  var zip = req.body.zip;
  var date_of_birth = req.body.date_of_birth;
  var gender = req.body.gender;
  var istrusted = req.body.istrusted;
  var profileVideo = req.body.profileVideo;
  var selfDescription = req.body.selfDescription;

  if (req.files != undefined) {
    if (req.files.length > 0) {
      if (req.files[0].filename)
        var profile_image = req.files[0].filename;
      else
        var profile_image = "";
    } else {
      var profile_image = "";
    }
  } else {
    var profile_image = "";
  }
  if (!id) {
    //console.log(error);
    res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONGUSER);
  } else if (isNaN(id)) {
    //console.log(error);
    res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONGUSER);
  } else if (!fname || !lname || !phone || !city || !country || !state || !address1 || !zip) {
    res.status(201).send(RESPONSEMESSAGE.USER.REQUIREDFIELDERROR);
  } else {
    if (profile_image != "") {
      var dataupdate = {
        firstname: fname,
        lastname: lname,
        phone1: phone,
        city: city,
        country: country,
        state: state,
        address1: address1,
        address2: address2,
        zip: zip,
        date_of_birth: date_of_birth,
        gender: gender,
        istrusted: istrusted,
        profile_image: profile_image,
        profile_video: profileVideo,
        self_description: selfDescription
      }

    } else {
      var dataupdate = {
        firstname: fname,
        lastname: lname,
        phone1: phone,
        city: city,
        country: country,
        state: state,
        address1: address1,
        address2: address2,
        zip: zip,
        date_of_birth: date_of_birth,
        istrusted: istrusted,
        gender: gender,
        profile_video: profileVideo,
        self_description: selfDescription
      }

    }
    var updatecondition = {
      id: id
    }
    var param3 = {

      tablename: 'mdl_user',
      condition: updatecondition,
      data: dataupdate
    }
    Mastermodel.commonUpdate(param3, function (error) {
      console.log(error);
      res.status(201).send(RESPONSEMESSAGE.USER.SOMETHINGWRONGUSER);
    }, function (data4) {
      res.status(200).send(RESPONSEMESSAGE.USER.PROFILEUPDATESUCCESSFULLY);
    });
  }
});
/**
 * Upload profile video
 */
var storage = node_module.multer.diskStorage({
  destination: function (req, file, cb) {
    var dest = 'public/trainerProfileVideo';
    node_module.mkdirp(dest, function (err) {
      if (err) cb(err, dest);
      else cb(null, dest);
    });
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + node_module.path.extname(file.originalname));
  }
});
var profileVideoUpload = node_module.multer({
  storage: storage
});
router.post('/uploadProfileVideo', customodule.verifyapicalltoken, profileVideoUpload.any(), function (req, res, next) {
  console.log(req.files);
  if (req.files != undefined) {
    if (req.files.length > 0) {
      var filename = req.files[0].filename;
      var fileObj = {};
      fileObj.url = customodule.setting.hetc_base_url + 'trainerProfileVideo/' + filename;
      fileObj.name = filename;
      var success = {
        "resCode": 200,
        "response": fileObj
      }
      res.status(200).send(success);
    } else {
      res.status(201).send(RESPONSEMESSAGE.USER.PROFILEVIEDOREQUIRED);
    }
  } else {
    res.status(201).send(RESPONSEMESSAGE.USER.PROFILEVIEDOREQUIRED);
  }
});
//save the learning style
router.post('/saveStudentLearningStyle', customodule.verifyapicalltoken, profileVideoUpload.any(), function (req, res, next) {
  console.log(req.body.answerArr);
  console.log(req.body.studentId);
  var dbsetting = req.db_setting;
  var studentId = req.body.studentId;
  if (req.body.answerArr != undefined) {
    if (req.body.answerArr.length > 0) {
      var reqAnswserArr = req.body.answerArr;
      var duplicateAnserArr = [];
      duplicateAnswerObj = reqAnswserArr.reduce(function (prev, cur) {
        prev[cur] = (prev[cur] || 0) + 1;
        return prev;
      }, {});
      // map is an associative array mapping the elements to their frequency:
      //var duplicateAnswerObj = JSON.stringify(duplicateAnserArr);
      var maxAnswerKey = Object.keys(duplicateAnswerObj).reduce(function (a, b) {
        return duplicateAnswerObj[a] > duplicateAnswerObj[b] ? a : b
      });
      //get the learning detail
      var fields = " msa.* ";
      var condition = " where msa.id = '" + maxAnswerKey + "' ";
      var param = {

        from: 'from mdl_learning_attribute as msa',
        condition: condition,
        fields: fields
      };
      // entrytestRes[positionindex].hii="12345" ;
      Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
      }, function (learningTypeRes) {
        var dataupdate = {
          learningtypeid: maxAnswerKey
        }
        var updatecondition = {
          id: studentId
        }
        var param = {

          tablename: 'mdl_user',
          condition: updatecondition,
          data: dataupdate
        }
        Mastermodel.commonUpdate(param, function (error) {

        }, function (data) {
          var success = {
            "resCode": 200,
            "response": learningTypeRes
          }
          res.status(200).send(success);
        })
      })
    } else {
      res.status(201).send(RESPONSEMESSAGE.USER.PROFILEVIEDOREQUIRED);
    }
  } else {
    res.status(201).send(RESPONSEMESSAGE.USER.PROFILEVIEDOREQUIRED);
  }
});

/*
 * dashboard admin section
 * Created On : 22-02-2018
 * * Input      :
 * Modified On
 */
router.post('/getAdminDashboard', customodule.verifyapicalltoken, function (req, res, next) {
  var dbsetting = req.db_setting;
  var userId = req.body.userId;
  var myCourseCnt = 0;
  var dashboardObj = {};
  if (!userId) {
    res.status(200).send(RESPONSEMESSAGE.STUDENT.STUDENTIDREQUIRED);
  } else {
    console.log(userId);
    //get total no of course
    var dbsetting = req.db_setting;
    var fields = " count(mcc.id) as totCourseCnt ";
    var condition = "where 1";
    condition += " ";
    var join = " ";
    var param = {

      from: 'from mdl_course_categories mcc ',
      condition: condition,
      fields: fields,
      join: join
    };
    Mastermodel.commonSelect(param, function (err) {
      res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (totCourseRes) {
      totCourseCnt = totCourseRes[0]['totCourseCnt'];
      dashboardObj.totCourseCnt = totCourseCnt;
      //GET THE TOTAL STUDENT COUNT
      var fields = " count(mu.id) as totstudentcnt ";
      var condition = "where 1";
      condition += " AND mu.usertype = 'Student' ";
      var join = " ";
      var param = {

        from: 'from mdl_user as mu',
        condition: condition,
        fields: fields,
        join: join
      };
      Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
      }, function (totStudentRes) {
        totStudentCnt = totStudentRes[0]['totstudentcnt'];
        dashboardObj.totStudentCnt = totStudentCnt;
        //GET THE TRAINER COUNT
        var dbsetting = req.db_setting;
        var fields = " count(mu.id) as tottrainercnt ";
        var condition = "where 1";
        condition += " AND mu.usertype = 'Trainer' ";
        var join = " ";
        var param = {

          from: 'from mdl_user as mu',
          condition: condition,
          fields: fields,
          join: join
        };
        Mastermodel.commonSelect(param, function (err) {
          res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (totTrainerRes) {
          totTrainerCnt = totTrainerRes[0]['tottrainercnt'];
          dashboardObj.totTrainerCnt = totTrainerCnt;
          //GET THE TOTAL PAYMENT COUNT
          //GET THE PAYMENT COUNT
          var dbsetting = req.db_setting;
          var fields = " sum(paid_amount) as totreceivedamount ";
          var condition = "where 1";
          condition += " AND is_paid = 'Yes' ";
          var join = " ";
          var param = {

            from: 'from mdl_rpl_process_submission_master mrpsm ',
            condition: condition,
            fields: fields,
            join: join
          };
          Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
          }, function (totPaymentRes) {
            totPaymentAmt = totPaymentRes[0]['totreceivedamount'];
            dashboardObj.totPaymentAmt = totPaymentAmt;

            //GET LIST OF STUDENT PENDING FOR RPL VERIFICATION
            var fields = " mp.*, mc.fullname as uname, mc.idnumber as ucode,  mcc.name as cname, mcc.idnumber as ccode, mu.firstname as tfname, mu.lastname as tlname , mu1.firstname as ufname, mu1.lastname as ulname ";
            var condition = " where mp.is_approved = 'Y'  and mp.is_valid = 'Y' and mp.is_confirmed_hetc = 'Y' ";
            var join = "LEFT JOIN mdl_course as mc              ON mp.unit_id    = mc.id ";
            join += "LEFT JOIN mdl_course_categories as mcc  ON mp.course_id  = mcc.id ";
            join += "LEFT JOIN mdl_user as mu                ON mp.trainer_id = mu.id ";
            join += "LEFT JOIN mdl_user as mu1               ON mp.student_id = mu1.id ";
            var limit = " 0,8 ";
            var param = {

              from: 'from mdl_rpl_process_submission_master mp ',
              condition: condition,
              fields: fields,
              join: join,
              limit: limit
            };
            Mastermodel.commonSelect(param, function (err) {
              res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            }, function (rplVerificationPendingRes) {
              dashboardObj.rplVerificationPendingRes = rplVerificationPendingRes;
              //GET THE CERTIFICATE ISSUE PENDING
              var dbsetting = req.db_setting;
              var fields = " mp.*, mc.fullname as uname, mc.idnumber as ucode,  mcc.name as cname, mcc.idnumber as ccode, mu.firstname as tfname, mu.lastname as tlname , mu1.firstname as ufname, mu1.lastname as ulname ";
              var condition = " where mp.is_approved = 'Y'  and mp.is_valid = 'Y' and mp.is_confirmed_hetc = 'Y' and mp.is_confirmed_hetc = 'Y' and is_certificate_issued = 'N' ";
              var join = "LEFT JOIN mdl_course as mc              ON mp.unit_id    = mc.id ";
              join += "LEFT JOIN mdl_course_categories as mcc  ON mp.course_id  = mcc.id ";
              join += "LEFT JOIN mdl_user as mu                ON mp.trainer_id = mu.id ";
              join += "LEFT JOIN mdl_user as mu1               ON mp.student_id = mu1.id ";
              var limit = " 0,8 ";
              var param = {

                from: 'from mdl_rpl_process_submission_master mp ',
                condition: condition,
                fields: fields,
                join: join,
                limit: limit
              };
              Mastermodel.commonSelect(param, function (err) {
                res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
              }, function (rplCertificateIssuePendingRes) {
                dashboardObj.rplCertificateIssuePendingRes = rplCertificateIssuePendingRes;
                //GET THE PENDING FOR REFUND REQUEST
                var dbsetting = req.db_setting;
                var fields = " mrrl.*,mu.firstname as ufname,mu.lastname as ulname,mcc.name as cname,mc.fullname as uname  ";
                var condition = "where 1";
                condition += "  AND mrrl.is_action = 'No' ";
                var join = " INNER JOIN mdl_user mu  ON mu.id = mrrl.student_id  ";
                join += "LEFT JOIN mdl_course as mc              ON mrrl.unit_id    = mc.id ";
                join += "LEFT JOIN mdl_course_categories as mcc  ON mrrl.course_id  = mcc.id ";
                var orderby = " order by mrrl.date desc ";
                var param = {

                  from: 'from mdl_refund_request_log as mrrl',
                  condition: condition,
                  fields: fields,
                  join: join,
                  orderby: orderby
                };
                Mastermodel.commonSelect(param, function (err) {
                  res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (pendingForApprovalRefundRes) {
                  dashboardObj.pendingForApprovalRefundRes = pendingForApprovalRefundRes;
                  //GET THE APPROVAL PENDING FOR TRAINER FOR UNIT
                  var dbsetting = req.db_setting;
                  var fields = " mtqm.*,mu.firstname as ufname,mu.lastname as ulname,mcc.name as cname,mc.fullname as uname ";
                  var condition = "where 1 ";
                  var join = " INNER JOIN mdl_user mu  ON mu.id = mtqm.trainer_id  ";
                  join += " INNER JOIN mdl_course_categories mcc  ON mcc.id = mtqm.course_id  ";
                  join += " INNER JOIN mdl_course mc  ON mc.id = mtqm.unit_id  ";
                  var param = {

                    from: 'from mdl_trainer_qualification_master as mtqm',
                    condition: condition,
                    fields: fields,
                    join: join
                  };
                  Mastermodel.commonSelect(param, function (err) {
                    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                  }, function (peningUnitApprovalRequestRes) {
                    dashboardObj.peningUnitApprovalRequestRes = peningUnitApprovalRequestRes;
                    var response = {
                      resCode: 200,
                      response: dashboardObj
                    }
                    res.status(200).send(response);
                  });
                });
              });
            });
          });
        })
      })
    });
  }
})
/*
 * created to insert the device key and token
 * Created On : 22-02-2018
 * * Input      :
 * Modified On
 */
router.post('/setDeviceToken', customodule.verifyapicalltoken, function (req, res, next) {
  var dbsetting = req.db_setting;
  var userId = req.body.userId;
  var deviceType = req.body.deviceType;
  var deviceId = req.body.deviceId;
  var deviceToken = req.body.deviceToken;
  if (!userId || !deviceType || !deviceId || !deviceToken) {
    res.status(200).send(RESPONSEMESSAGE.USER.REQUIREDFIELD);
  } else {
    //check the user id has the device token
    var fields = " id  ";
    var condition = "where 1";
    condition += "  AND mudt.device_type = '" + deviceType + "' AND mudt.device_id = '" + deviceId + "' and user_id = '" + userId + "' ";
    var orderby = " ";
    var join = "";
    var param = {

      from: 'from mdl_user_device_token as mudt',
      condition: condition,
      fields: fields,
      join: join,
      orderby: orderby
    };
    Mastermodel.commonSelect(param, function (err) {
      res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (getDeviceTokenRes) {
      console.log('device token length', getDeviceTokenRes.length);
      //check if any paricular device exist or not
      if (getDeviceTokenRes.length == 0) {
        //insert the record in device token table
        var data = {
          user_id: userId,
          device_type: deviceType,
          device_id: deviceId,
          device_token: deviceToken
        };
        var insertParam = {

          tablename: 'mdl_user_device_token',
          data: [data]
        };
        Mastermodel.commonInsert(insertParam, function (error) {
          console.log('Insert', error);
          res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (success) {
          var response = {
            resCode: 200,
            resMessage: "Device token added successfully",
            response: data
          }
          res.status(200).send(response);
        });
      } else {
        //update the device token
        var updatecondition = {
          user_id: userId,
          device_type: deviceType,
          device_id: deviceId
        };
        var data = {
          device_token: deviceToken
        };
        var updateParam = {

          tablename: 'mdl_user_device_token',
          condition: updatecondition,
          data: data
        };
        console.log(updatecondition, data);

        Mastermodel.commonUpdate(updateParam, function (error) {
          console.log(error);
          res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (success) {
          if (success) {
            var response = {
              resCode: 200,
              resMessage: "Device token has been updated successfully",
              response: data
            }
            res.status(200).send(response);
          }
        });
      }
    });
  }
});
//test push notification
router.post('/testPushNotification', customodule.verifyapicalltoken, function (req, res, next) {
  var dbsetting = req.db_setting;
  //get the device token
  var userId = 2;
  var fields = " mudt.device_id,mudt.device_token,mudt.device_type  ";
  var condition = "where 1";
  condition += "  AND mudt.user_id = '" + userId + "' ";
  var orderby = " ";
  var join = "";
  var param = {

    from: 'from mdl_user_device_token as mudt',
    condition: condition,
    fields: fields,
    join: join,
    orderby: orderby
  };
  Mastermodel.commonSelect(param, function (err) {
    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
  }, function (getDeviceTokenRes) {
    //save the push notification
    var data = {
      user_id: userId,
      user_type: 'Student',
      notification_title: NOTIFICATIONMESSAGE.STUDENT.RPLSUMIT.NOTIFICATIONTITLE,
      notification_message: NOTIFICATIONMESSAGE.STUDENT.RPLSUMIT.NOTIFICATIONMESSAGE
    };
    //get the current date
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd
    }
    if (mm < 10) {
      mm = '0' + mm
    }
    currentdate = mm + '/' + dd + '/' + yyyy;

    var insertParam = {

      tablename: 'mdl_user_notification',
      data: [data]
    };
    Mastermodel.commonInsert(insertParam, function (error) {
      console.log('Insert', error);
      res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (success) {
      //push notification code
      var pushNotificationTitle = NOTIFICATIONMESSAGE.STUDENT.RPLSUMIT.NOTIFICATIONTITLE;
      var pushNotificationDesc = NOTIFICATIONMESSAGE.STUDENT.RPLSUMIT.NOTIFICATIONMESSAGE;
      var pushNotificationTag = NOTIFICATIONMESSAGE.STUDENT.RPLSUMIT.NOTIFICATIONTAG;
      var pushNotificationCode = NOTIFICATIONMESSAGE.STUDENT.RPLSUMIT.NOTIFICATIONCODE;
      var pushNotificationDate = currentdate;
      sendGroupNotification(pushNotificationTitle, getDeviceTokenRes, pushNotificationTag, pushNotificationDesc, pushNotificationCode, pushNotificationDate);
      console.log(pushNotificationDate);
      var response = {
        resCode: 200,
        resMessage: "Device token added successfully",
        response: data
      }
      res.status(200).send(response);
    });
  });
});

/*
 * Used to get entry test question for student for course wise
 * Created On : 25-02-2017
 * Input      :
 * Modified On
 */
router.post('/gerCarrerQuizQuestion', customodule.verifyapicalltoken, function (req, res, next) {
  //console.log(req.body);
  var questionArr = [];
  var dbsetting = req.db_setting;
  /**** Used to get student entry test question list ****/
  var fields = " mcqq.course_type_id, mcqq.id, mcqq.question , mcqq.question_type ";
  var condition = " where mcqq.id != 0 and question_type = 'Multiple Choice' ";
  var limit = 10;
  var orderby = " ORDER BY RAND() ";
  var param = {

    from: 'from mdl_carrer_quiz_question as mcqq',
    condition: condition,
    fields: fields,
    limit: limit,
    orderby: orderby
  };
  Mastermodel.commonSelect(param, function (err) {
    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
  }, function (entrytestRes) {
    var entryTestquestionLen = entrytestRes.length;
    var quesLen = entrytestRes.length;
    var queaArr = [];

    if (entrytestRes.length > 0) {
      /**** Used to get student entry test answer list ****/
      asyncForEach(entrytestRes, function (positionitem, positionindex, positionarr) {
        var cnt = positionindex;
        var fields = " mcqa.* ";
        var condition = " where mcqa.question_id = '" + positionitem.id + "' ";
        var param = {
          from: 'from mdl_career_quiz_answer as mcqa',
          condition: condition,
          fields: fields
        };
        // entrytestRes[positionindex].hii="12345" ;
        Mastermodel.commonSelect(param, function (err) {
          res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (ansRes) {
          questionArr.push(positionindex);
          var questionLen = questionArr.length;

          queaArr.push({
            "questionId": positionitem.id
          });
          entrytestRes[cnt].answer = ansRes;
          cnt++;
          if (quesLen == queaArr.length) {

            var response = {
              resCode: 200,
              response: entrytestRes
            };
            res.status(200).send(response);
          }
        });
      });
    } else {
      res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NORECORDSFOUND);
    }
  });
});

/*
 * Used to post career quiz question from student 
 * Created On : 18-04-2018
 * Input      : answer array
 * Modified On
 */
router.post('/postCarrerQuizQuestionModified', customodule.verifyapicalltoken, function (req, res, next) {

  var student_id = req.body.studentId;
  var questionArr = req.body.questionArr;
  var questionidArr = req.body.questionidArr;
  var correctansArr = req.body.correctansArr;
  var studentansArr = req.body.studentansArr;

  async.eachSeries(questionidArr, function (student, callback) {
    if (student) {
      //Do something here.
      //This will print the each student name and marks in forward series.
      console.log("Name: " + student.name + " Marks :" + student.marks);
      callback(null);
    }
  });


});


/*
 * Used to post RPL entry test question from student for course wise
 * Created On : 08-09-2017
 * Input      : answer array
 * Modified On
 */
router.post('/postCarrerQuizQuestionv1', customodule.verifyapicalltoken, function (req, res, next) {
  console.log(req.body);
  var student_id = req.body.studentId;
  var questionArr = req.body.questionArr;
  var questionidArr = req.body.questionidArr;
  var correctansArr = req.body.correctansArr;
  var studentansArr = req.body.studentansArr;
  var correctAnswerId = req.body.correctAnswerId;
  var correct_answer_string = correctAnswerId.toString();

  /**** Used to get course list list ****/

  var fields = " mcc.id as courseId,mcc.name as cname, mcc.idnumber as ccode, mc.fullname as uname, mc.idnumber as ucode,(case when mcc.course_image='' then mcc.course_image else concat('" + SETTING.course_image_url + "',mcc.course_image) end) as courseimage";
  var condition = " where mcc.id != 0  AND mcc.course_type_id in ( " + correct_answer_string + ") ";
  var join = "LEFT JOIN mdl_course as mc  ON mcc.id  = mc.category ";
  var limit = " 0,10 ";
  var orderby = " ORDER BY RAND() ";
  var param = {
    from: 'from mdl_course_categories mcc ',
    condition: condition,
    fields: fields,
    join: join,
    limit: limit,
    orderby: orderby
  };
  Mastermodel.commonSelect(param, function (err) {
    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
  }, function (recomendedCourseRes) {
    var response = {
      resCode: 200,
      response: recomendedCourseRes
    };
    res.status(200).send(response);
  });

}); //end of post

router.post('/postCarrerQuizQuestion', function (req, res, next) {

  var questionidArr = req.body.questionidArr;
  var studentansArr = req.body.studentansArr;
  var courseTypeArr = [];


  var flag = 0;
  /* async.eachSeries(questionidArr, function (question, callbackCourse) {

     var student_answer = studentansArr[flag];
     flag++;
     var fields = " mcq.course_type_id , mqa.answer_choices";
     var condition = " where mcq.id  =  " + question + " ";
     var join = "LEFT JOIN mdl_career_quiz_answer as mqa  ON mcq.id  = mqa.question_id AND mqa.right_answer = 1 ";
     var param = {
       from: 'from mdl_carrer_quiz_question mcq ',
       condition: condition,
       fields: fields,
       join: join
     };
     Mastermodel.commonSelect(param, function (err) {
       res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
     }, function (recomendedCourseRes) {
       var course_type_id = recomendedCourseRes[0].course_type_id;
       var original_answer = recomendedCourseRes[0].answer_choices;
       if (student_answer == original_answer) {
         courseTypeArr.push(course_type_id);

       }

     });

     callbackCourse(courseTypeArr);

   });

  function callbackCourse(param) {
   console.log("param",param);
  }*/

  var total = questionidArr.length;
  var qcountArr = [];

  asyncForEach(questionidArr, function (positionitem, positionindex, positionarr) {
    var question = positionitem;
    var student_answer = studentansArr[positionindex];

    var fields = " mcq.course_type_id , mqa.answer_choices";
    var condition = " where mcq.id  =  " + question + " ";
    var join = "LEFT JOIN mdl_career_quiz_answer as mqa  ON mcq.id  = mqa.question_id AND mqa.right_answer = 1 ";
    var param = {
      from: 'from mdl_carrer_quiz_question mcq ',
      condition: condition,
      fields: fields,
      join: join
    };
    Mastermodel.commonSelect(param, function (err) {
      res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (recomendedCourseRes) {

      qcountArr.push(positionindex);
      var questionLen = qcountArr.length;


      var course_type_id = recomendedCourseRes[0].course_type_id;
      var original_answer = recomendedCourseRes[0].answer_choices;
      if (student_answer == original_answer) {
        courseTypeArr.push(course_type_id);
      }


      if (total == questionLen) {


        if (courseTypeArr.length > 0) {

          var correct_answer_string = courseTypeArr.toString();
          var fields = " mcc.id as courseId,mcc.name as cname, mcc.idnumber as ccode, mc.fullname as uname, mc.idnumber as ucode,(case when mcc.course_image='' then mcc.course_image else concat('" + SETTING.course_image_url + "',mcc.course_image) end) as courseimage";
          var condition = " where mcc.id != 0  AND mcc.course_type_id in ( " + correct_answer_string + ") ";
          var join = "LEFT JOIN mdl_course as mc  ON mcc.id  = mc.category ";
          var limit = " 0,10 ";
          var orderby = " ORDER BY RAND() ";
          var param = {
            from: 'from mdl_course_categories mcc ',
            condition: condition,
            fields: fields,
            join: join,
            limit: limit,
            orderby: orderby
          };
          Mastermodel.commonSelect(param, function (err) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
          }, function (recomendedCourseRes) {
            var response = {
              resCode: 200,
              response: recomendedCourseRes
            };
            res.status(200).send(response);
          });

        } else {

          var response = {
            resCode: 201,
            response: "No Records Found"
          };
          res.status(200).send(response);

        }

      }




    });

  });

}); //end of post




/*
 * Used to get the recomended course
 * Created On : 25-02-2018
 * Input      : recomended course type
 * Modified On
 */
router.post('/getRecomendedCourseList', customodule.verifyapicalltoken, function (req, res, next) {
  /**** Used to get course list list ****/
  var fields = " mcc.name as cname, mcc.idnumber as ccode, mc.fullname as uname, mc.idnumber as ucode,(case when mcc.course_image=null then mcc.course_image else concat('" + SETTING.course_image_url + "',mcc.course_image) end) as courseimage";
  var condition = " where mcc.id != 0 ";
  var join = "LEFT JOIN mdl_course as mc             ON mcc.id  = mc.category ";
  var limit = " 0,10 ";
  var orderby = " ORDER BY RAND() ";
  var param = {

    from: 'from mdl_course_categories mcc ',
    condition: condition,
    fields: fields,
    join: join,
    limit: limit,
    orderby: orderby
  };
  Mastermodel.commonSelect(param, function (err) {
    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
  }, function (recomendedCourseRes) {
    var response = {
      resCode: 200,
      response: recomendedCourseRes
    };
    res.status(200).send(response);
  });
}); //end of post
/*
 * Used to get the notification list
 * Created On :
 * Input      : user id
 * Modified On
 */
router.post('/getNotificationList', customodule.verifyapicalltoken, function (req, res, next) {
  var userId = req.body.userId;
  if (!userId) {
    res.status(200).send(RESPONSEMESSAGE.USER.REQUIREDFIELDERROR);
  } else {
    /**** Used to get course list list ****/
    var dbsetting = req.db_setting;
    var fields = " mun.*";
    var condition = " where mun.id != 0 and user_id =" + userId;
    var join = "";
    var limit;
    if (req.body.getNotificationReqNo != undefined) {
      limit = " 0, " + req.body.getNotificationReqNo + " ";
    } else {
      limit = "";
    }
    var orderby = " ORDER BY id DESC ";
    var param = {

      from: 'from mdl_user_notification mun ',
      condition: condition,
      fields: fields,
      join: join,
      limit: limit,
      orderby: orderby
    };
    Mastermodel.commonSelect(param, function (err) {
      res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (notificationRes) {
      if (notificationRes.length > 0) {
        var response = {
          resCode: 200,
          response: notificationRes,
          resNotificationCount: notificationRes.length
        }
        res.status(200).send(response);
      } else {
        var response = {
          resCode: 201,
          response: 'No notification found'
        };
        res.status(200).send(response);
      }
    });
  }
}); //end of post
router.post('/testCommonPushNotification', customodule.verifyapicalltoken, function (req, res, next) {
  var dbsetting = req.db_setting;
  var notificationparam = {};
  notificationparam.userId = req.body.userId;
  notificationparam.userType = 'Student';
  notificationparam.dbsetting = dbsetting;
  notificationparam.pushNotificationCode = NOTIFICATIONMESSAGE.STUDENT.RPLSUMIT;
  Masternotificationmodel.sendPushNotification(notificationparam, function (err) {
    var response = {
      resCode: 201,
      response: error
    };
    res.status(200).send(response);
  }, function (succNotificationRes) {
    var response = {
      resCode: 200,
      response: succNotificationRes
    };
    res.status(200).send(response);
  });
});
/*
 * Used to get the my trainer list
 * Created On :
 * Input      : student id
 * Modified On
 */

router.post('/getMyTrainerList', customodule.verifyapicalltoken, function (req, res, next) {
  /**** Used to get my trainer list ****/
  var dbsetting = req.db_setting;
  var studentId = req.body.studentId;
  var fields = " mstl.id,mstl.trainer_id,mstl.student_id,mstl.course_id,mstl.unit_id,mu.firstname, mu.lastname, mcc.id as courseid,mcc.name,mc.id as unitid ,mc.fullname as unitname,(case when mcc.course_image=null then mcc.course_image else concat('" + SETTING.course_image_url + "',mcc.course_image) end) as courseimage ";
  var condition = " where mstl.id != 0 and mstl.student_id = " + studentId + " ";
  var join = "LEFT JOIN mdl_user as mu ON mstl.trainer_id  = mu.id ";
  join += " LEFT JOIN mdl_course as mc              ON mc.id  = mstl.unit_id ";
  join += " LEFT JOIN mdl_course_categories as mcc  ON mcc.id = mstl.course_id ";
  var orderby = " ORDER BY mstl.id DESC ";
  var param = {

    from: 'from mdl_student_trainer_list mstl ',
    condition: condition,
    fields: fields,
    join: join,
    orderby: orderby
  };
  Mastermodel.commonSelect(param, function (err) {
    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
  }, function (myTrainerListRes) {
    if (myTrainerListRes.length > 0) {
      var response = {
        resCode: 200,
        response: myTrainerListRes
      };
      res.status(200).send(response);
    } else {
      res.status(200).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
    }
  });
});
/*
 * Used to get the my chat trainer list
 * Created On :
 * Input      : student id
 * Modified On
 */

router.post('/getMyChatTrainerList', customodule.verifyapicalltoken, function (req, res, next) {
  /**** Used to get my trainer list ****/
  var dbsetting = req.db_setting;
  var studentId = req.body.studentId;
  var fields = " DISTINCT mstl.trainer_id,mu.firstname, mu.lastname ";
  var condition = " where mstl.id != 0 and mstl.student_id = " + studentId + " ";
  var join = "LEFT JOIN mdl_user as mu ON mstl.trainer_id  = mu.id ";
  join += " LEFT JOIN mdl_course as mc              ON mc.id  = mstl.unit_id ";
  join += " LEFT JOIN mdl_course_categories as mcc  ON mcc.id = mstl.course_id ";
  var orderby = " ORDER BY mstl.id DESC ";
  var param = {

    from: 'from mdl_student_trainer_list mstl ',
    condition: condition,
    fields: fields,
    join: join,
    orderby: orderby
  };
  Mastermodel.commonSelect(param, function (err) {
    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
  }, function (recomendedCourseRes) {
    res.status(200).send(recomendedCourseRes);
  });
});
/*
 * Used to get the my trainer list
 * Created On :
 * Input      : student id
 * Modified On
 */

router.post('/getMyStudentList', customodule.verifyapicalltoken, function (req, res, next) {
  /**** Used to get my trainer list ****/
  var dbsetting = req.db_setting;
  var trainerId = req.body.trainerId;
  var fields = " mstl.trainer_id,mstl.student_id,mu.firstname, mu.lastname,mstl.course_id,mstl.unit_id, mcc.id as courseid,mcc.name,mc.id as unitid ,mc.fullname as unitname,(case when mcc.course_image=null then mcc.course_image else concat('" + SETTING.course_image_url + "',mcc.course_image) end) as courseimage ";
  var condition = " where mstl.id != 0 and mstl.trainer_id = " + trainerId + " ";
  var join = "LEFT JOIN mdl_user as mu ON mstl.student_id  = mu.id ";
  join += " LEFT JOIN mdl_course as mc              ON mc.id  = mstl.unit_id ";
  join += " LEFT JOIN mdl_course_categories as mcc  ON mcc.id = mstl.course_id ";
  var orderby = " ORDER BY mstl.id DESC ";
  var param = {

    from: 'from mdl_student_trainer_list mstl ',
    condition: condition,
    fields: fields,
    join: join,
    orderby: orderby
  };
  Mastermodel.commonSelect(param, function (err) {
    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
  }, function (myStudentListRes) {
    var response = {
      resCode: 200,
      response: myStudentListRes
    };
    res.status(200).send(response);
    //res.status(200).send(recomendedCourseRes);
  });
});
/*
 * Used to get the my chat trainer list
 * Created On :
 * Input      : student id
 * Modified On
 */

router.post('/getMyChatUser', customodule.verifyapicalltoken, function (req, res, next) {
  /**** Used to get my trainer list ****/
  var dbsetting = req.db_setting;
  var userId = req.body.userId;
  var userType = req.body.userType;
  if (userType == 'Trainer') {
    var fields = " distinct mstl.student_id as userId,mu.firstname, mu.lastname,(case when mu.profile_image=null then mu.profile_image else concat('" + SETTING.user_image_url + "',mu.profile_image) end) as userimage ";
    var condition = " where mstl.id != 0 and mstl.trainer_id = " + userId + " ";
    var join = "LEFT JOIN mdl_user as mu ON mstl.student_id  = mu.id ";
    join += " LEFT JOIN mdl_course as mc              ON mc.id  = mstl.unit_id ";
    join += " LEFT JOIN mdl_course_categories as mcc  ON mcc.id = mstl.course_id ";
    var orderby = " GROUP BY mstl.student_id DESC ";
  }
  if (userType == 'Student') {
    var fields = " distinct mstl.trainer_id as userId,mu.firstname, mu.lastname,(case when mu.profile_image=null then mu.profile_image else concat('" + SETTING.user_image_url + "',mu.profile_image) end) as userimage ";
    var condition = " where mstl.id != 0 and mstl.student_id = " + userId + " ";
    var join = "LEFT JOIN mdl_user as mu ON mstl.trainer_id  = mu.id ";
    join += " LEFT JOIN mdl_course as mc              ON mc.id  = mstl.unit_id ";
    join += " LEFT JOIN mdl_course_categories as mcc  ON mcc.id = mstl.course_id ";
    var orderby = " GROUP BY mstl.trainer_id DESC ";
  }
  if (userType == 'Admin') {
    var fields = " distinct mstl.trainer_id,mu.firstname, mu.lastname ";
    var condition = " where mstl.id != 0 and mstl.student_id = " + userId + " ";
    var join = "LEFT JOIN mdl_user as mu ON mstl.trainer_id  = mu.id ";
    join += " LEFT JOIN mdl_course as mc              ON mc.id  = mstl.unit_id ";
    join += " LEFT JOIN mdl_course_categories as mcc  ON mcc.id = mstl.course_id ";
    var orderby = " GROUP BY mstl.trainer_id DESC ";
  }
  var param = {
    from: 'from mdl_student_trainer_list mstl ',
    condition: condition,
    fields: fields,
    join: join,
    orderby: orderby
  };
  Mastermodel.commonSelect(param, function (err) {
    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
  }, function (myChatRes) {
    if (myChatRes.length > 0) {
      //var getUnreadMessageSql = "SELECT from_user_id,COUNT(id) as unreadCnt FROM mdl_chat_messages WHERE to_user_id = " + senderId + " AND is_read = 0 GROUP BY from_user_id";
      var fields = " from_user_id,COUNT(id) as unreadCnt ";
      var condition = " where to_user_id = " + userId + " AND is_read = 0 ";
      var join = "  ";
      var orderby = " GROUP BY from_user_id ";
      var param = {
        from: 'from mdl_chat_messages ',
        condition: condition,
        fields: fields,
        join: join,
        orderby: orderby
      };
      Mastermodel.commonSelect(param, function (err) {
        res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
      }, function (unreadChatRes) {
        node_module.async.forEachOf(myChatRes, (value, key, callback) => {
          console.log('###################', value.userId, unreadChatRes, key);
          var unreadUserArr = node_module.underscore.filter(unreadChatRes, function (unreadChatRes) {
            return unreadChatRes.from_user_id == value.userId;
          });
          if (unreadUserArr.length > 0) {
            myChatRes[key].unReadCnt = unreadUserArr[0].unreadCnt;
          } else {
            myChatRes[key].unReadCnt = 0;
          }
          callback();
        }, err => {
          if (err) console.error(err.message);
          var response = {
            resCode: 200,
            resMessage: "Chat user list",
            response: myChatRes
          };
          res.status(200).send(response);
        });
      });
    } else {
      var response = {
        resCode: 201,
        resMessage: 'No user found !!',
        response: 'No user found !!'
      };
      res.status(200).send(response);
    }
  });
});
/*
 * Rate trainer
 * Created On :
 * Input      : star rating
 * Modified On
 */

router.post('/rateTrainer', customodule.verifyapicalltoken, function (req, res, next) {
  /**** Used to rate the trainer ****/
  var dbsetting = req.db_setting;
  var trainerId = req.body.trainerId;
  var studentId = req.body.studentId;
  var courseId = req.body.courseId;
  var unitId = req.body.unitId;
  var rating = req.body.rating;
  console.log(trainerId, studentId, courseId, unitId, rating);
  //check the input paramater provide or not
  if (!trainerId || !studentId || !courseId) {
    res.status(200).send(RESPONSEMESSAGE.COMMON.REQUIREDFIELD);
  } else {
    //check the type of datatype requested from user
    console.log(node_module.checktypes.integer(trainerId));
    console.log(node_module.checktypes.integer(studentId));
    console.log(node_module.checktypes.integer(courseId));
    console.log(node_module.checktypes.integer(unitId));
    //if (!node_module.checktypes.integer(trainerId) || !node_module.checktypes.integer(studentId) || !node_module.checktypes.integer(courseId) || !node_module.checktypes.integer(unitId)) {
    //res.status(200).send(RESPONSEMESSAGE.COMMON.INVALIDDATAPROVIDED);
    //} else {
    //now check the student alligned to particular trainer or not
    var fields = " mstl.trainer_id ";
    var condition = " where mstl.id != 0 and mstl.student_id = " + studentId + " and mstl.trainer_id = " + trainerId + " ";
    var orderby = " ORDER BY mstl.id DESC ";
    var join = "";
    var param = {

      from: 'from mdl_student_trainer_list mstl ',
      condition: condition,
      fields: fields,
      join: join,
      orderby: orderby
    };
    Mastermodel.commonSelect(param, function (err) {
      res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (myTrainerRes) {
      if (myTrainerRes.length > 0) {
        //check wheather student have already given the rating
        var data = {
          trainer_id: trainerId,
          student_id: studentId,
          course_id: courseId,
          unit_id: unitId,
          rating: rating
        };
        var insertParam = {

          tablename: 'mdl_trainer_rating',
          data: [data]
        };
        Mastermodel.commonInsert(insertParam, function (error) {
          console.log('Insert', error);
          res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (success) {
          var response = {
            resCode: 200,
            response: "You have rate the trainer successfully"
          }
          res.status(200).send(response);
        });

      } else {
        res.status(200).send(RESPONSEMESSAGE.COMMON.PERMISSIONDENIED);
      }
    });
    //}
  }
});
/*
 * Rate student
 * Created On :
 * Input      : star rating
 * Modified On
 */

router.post('/rateStudent', customodule.verifyapicalltoken, function (req, res, next) {
  /**** Used to rate the trainer ****/
  var dbsetting = req.db_setting;
  var trainerId = req.body.trainerId;
  var studentId = req.body.studentId;
  var courseId = req.body.courseId;
  var unitId = req.body.unitId;
  var rating = req.body.rating;
  console.log(trainerId, studentId, courseId, unitId, rating);
  //check the input paramater provide or not
  if (!trainerId || !studentId || !courseId) {
    res.status(200).send(RESPONSEMESSAGE.COMMON.REQUIREDFIELD);
  } else {
    //now check the student alligned to particular trainer or not
    var fields = " mstl.trainer_id ";
    var condition = " where mstl.id != 0 and mstl.student_id = " + studentId + " and mstl.trainer_id = " + trainerId + " ";
    var orderby = " ORDER BY mstl.id DESC ";
    var join = "";
    var param = {

      from: 'from mdl_student_trainer_list mstl ',
      condition: condition,
      fields: fields,
      join: join,
      orderby: orderby
    };
    Mastermodel.commonSelect(param, function (err) {
      res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
    }, function (myTrainerRes) {
      if (myTrainerRes.length > 0) {
        var data = {
          trainer_id: trainerId,
          student_id: studentId,
          course_id: courseId,
          unit_id: unitId,
          rating: rating
        };
        var insertParam = {

          tablename: 'mdl_student_rating',
          data: [data]
        };
        Mastermodel.commonInsert(insertParam, function (error) {
          console.log('Insert', error);
          res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (success) {
          var response = {
            resCode: 200,
            resMessage: "You have rate student successfully",
            response: data
          };
          res.status(200).send(response);
        });

      } else {
        res.status(200).send(RESPONSEMESSAGE.COMMON.PERMISSIONDENIED);
      }
    });

  }
});
/*
 * Get rpl list
 * Created On :
 * Input      : student id
 * Modified On
 */

router.post('/getStudentRplList', customodule.verifyapicalltoken, function (req, res, next) {
  /**** Used to rate the student ****/
  var dbsetting = req.db_setting;
  var studentId = req.body.studentId;
  var fields = " mp.*, mc.fullname as uname, mc.idnumber as ucode,  mcc.name as cname, mcc.idnumber as ccode, mu.firstname as tfname, mu.lastname as tlname , mu1.firstname as ufname, mu1.lastname as ulname ";
  var condition = " where mp.is_approved = 'Y'  and mp.is_valid = 'Y' and mp.is_confirmed_hetc = 'Y' and mp.student_id = " + studentId + " ";
  var join = "LEFT JOIN mdl_course as mc              ON mp.unit_id    = mc.id ";
  join += " LEFT JOIN mdl_course_categories as mcc  ON mp.course_id  = mcc.id ";
  join += " LEFT JOIN mdl_user as mu                ON mp.trainer_id = mu.id ";
  join += " LEFT JOIN mdl_user as mu1               ON mp.student_id = mu1.id ";
  var orderby = " ORDER BY mp.id DESC ";

  var param = {

    from: 'from mdl_rpl_process_submission_master mp ',
    condition: condition,
    fields: fields,
    join: join,
    orderby: orderby
  };
  Mastermodel.commonSelect(param, function (err) {
    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
  }, function (myRplRes) {
    if (myRplRes.length > 0) {
      var response = {
        resCode: 200,
        resMessage: "RPL log list",
        response: myRplRes
      };
      res.status(200).send(response);
    } else {
      res.status(200).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
    }
  });
});
/*
 * Get tariner rating
 * Created On :
 * Input      : trainer id
 * Modified On
 */

router.post('/getTrainerRating', customodule.verifyapicalltoken, function (req, res, next) {
  /**** Used to rate the student ****/
  var dbsetting = req.db_setting;
  var trainerId = req.body.trainerId;
  var fields = " trainer_id, AVG(rating) as avg_rating  ";
  var condition = " where mtr.id != 0 and mtr.trainer_id = " + trainerId + " ";
  var join = " ";
  var orderby = " ";
  var groupby = " GROUP BY trainer_id ";

  var param = {

    from: 'from mdl_trainer_rating mtr ',
    condition: condition,
    fields: fields,
    join: join,
    orderby: orderby
  };
  Mastermodel.commonSelect(param, function (err) {
    res.status(200).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
  }, function (ratingRes) {
    if (ratingRes.length > 0) {
      var response = {
        resCode: 200,
        response: "Rating list",
        resData: ratingRes[0]
      };
      res.status(200).send(response);
    } else {
      res.status(200).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
    }
  });
});

/*
 * Get student rating
 * Created On :
 * Input      : trainer id
 * Modified On
 */

router.post('/getStudentRating', customodule.verifyapicalltoken, function (req, res, next) {
  /**** Used to rate the student ****/
  var dbsetting = req.db_setting;
  var studentId = req.body.studentId;
  var fields = " student_id, AVG(rating) as avg_rating  ";
  var condition = " where mtr.id != 0 and mtr.student_id = " + studentId + " ";
  var join = " ";
  var orderby = " ";
  var groupby = " GROUP BY student_id ";

  var param = {

    from: 'from mdl_student_rating mtr ',
    condition: condition,
    fields: fields,
    join: join,
    orderby: orderby
  };
  Mastermodel.commonSelect(param, function (err) {
    res.status(200).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
  }, function (ratingRes) {
    if (ratingRes.length > 0) {
      var response = {
        resCode: 200,
        response: "Rating list",
        resData: ratingRes[0]
      };
      res.status(200).send(response);
    } else {
      res.status(200).send(RESPONSEMESSAGE.COMMON.NORECORDFOUND);
    }
  });
});

/*
 * Save RPL section
 * Created On :
 * Input      : evidence submission document by student
 * Created On : 03/03/2018
 * Modified On : 14/03/2018
 */
router.post('/saveRplEvidence', customodule.verifyapicalltoken, function (req, res, next) {

  console.log(req.body);


  var totalQuesCnt = req.body.totalQuesCnt;
  var dbsetting = req.db_setting;
  var courseId = req.body.courseId;
  var unitId = req.body.unitId;
  var studentId = req.body.studentId;
  var masterId = req.body.masterId;
  var isResubmit = req.body.isResubmit;
  var isFinalSubmit = req.body.isFinalSubmit;
  var questionArr = req.body.questionArr;
  var totalQuestioncnt = questionArr.length;
  var totalAnscnt = questionArr.length;
  if (!totalQuesCnt || !courseId || !unitId || !studentId) {
    res.status(200).send(RESPONSEMESSAGE.COMMON.REQUIREDFIELD);
  } else {
    /************ check the question count submitted by student *****************/
    if (questionArr.length > 0) {
      /************ if the student will submit the evidence first time then master id will be 0 *****************/
      if (masterId > 0) {
        //update condition parameter as per the save rpl evidence
        var dataupdate = {};
        if (isFinalSubmit == 'N' && isResubmit == 'N') {
          //** now save the rpl submission evidence */
          dataupdate = {
            is_saved: "Y",
            total_qcnt: totalQuesCnt,
            total_acnt: totalAnscnt
          };
        }
        if (isFinalSubmit == 'N' && isResubmit == 'Y') {
          dataupdate = {
            is_saved: "Y",
            final_submitted: "Y",
            total_qcnt: totalQuesCnt,
            total_acnt: totalAnscnt
          };
        }
        if (isFinalSubmit == 'Y' && isResubmit == 'N') {
          dataupdate = {
            is_submitted: "Y",
            total_qcnt: totalQuesCnt,
            total_acnt: totalAnscnt
          };
        }
        if (isFinalSubmit == 'Y' && isResubmit == 'Y') {
          dataupdate = {
            is_saved: "Y",
            final_submitted: "Y",
            is_submitted: "Y",
            total_qcnt: totalQuesCnt,
            total_acnt: totalAnscnt
          };
        }
        var updatecondition = {
          id: masterId
        };
        var param3 = {

          tablename: 'mdl_rpl_process_submission_master',
          condition: updatecondition,
          data: dataupdate
        };

        Mastermodel.commonUpdate(param3, function (error) {
          console.log(error);
          res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
        }, function (data4) {
          var fields = " attempt,trainer_id ";
          var condition = " where id ='" + masterId + "'  ";
          var param = {

            from: 'from mdl_rpl_process_submission_master ',
            condition: condition,
            fields: fields,
            orderby: orderby,
            limit: limit
          };
          Mastermodel.commonSelect(param, function (err) {
            res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
          }, function (rplMasterSubmissionRes) {
            var requestParam = {};
            requestParam.rpl_process_submission_id = masterId;
            requestParam.attempt = rplMasterSubmissionRes[0].attempt;
            requestParam.dbsetting = dbsetting;
            studentModel.saveRplEvidenceModel(questionArr, requestParam, function (error) {
              var response = {
                resCode: 201,
                response: error
              };
              res.status(200).send(response);
            }, function (success) {
              if (isFinalSubmit == 'Y' && isResubmit == 'Y') {
                var saveData = {};
                saveData.rpl_process_submission_id = masterId;
                saveData.trainer_id = rplMasterSubmissionRes[0].trainer_id;
                saveData.is_accepted = "Y";
                saveData.trainer_comment = "";
                saveData.is_action_taken = "Y";
                saveData.is_resubmit = "Y";
                var param = {

                  tablename: 'mdl_rpl_process_trainer_request',
                  data: [saveData]
                };
                Mastermodel.commonInsert(param, function (err) {
                  res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                }, function (data) {
                  var notificationparam = {};
                  notificationparam.userId = studentId;
                  notificationparam.courseId = courseId;
                  notificationparam.unitId = unitId;
                  notificationparam.userType = 'Student';
                  notificationparam.dbsetting = dbsetting;
                  notificationparam.pushNotificationCode = NOTIFICATIONMESSAGE.STUDENT.RPLSUBMITDOCUMENT;
                  Masternotificationmodel.sendPushNotification(notificationparam, function (err) {
                    var response = {
                      resCode: 201,
                      response: error
                    };
                    res.status(200).send(response);
                  }, function (succNotificationRes) {
                    res.status(200).send(success);
                  });
                });
              } else {
                var notificationparam = {};
                notificationparam.userId = studentId;
                notificationparam.courseId = courseId;
                notificationparam.unitId = unitId;
                notificationparam.userType = 'Student';
                notificationparam.dbsetting = dbsetting;
                if (isFinalSubmit == 'N') {
                  notificationparam.pushNotificationCode = NOTIFICATIONMESSAGE.STUDENT.RPLSAVEDRAFT;
                }
                if (isFinalSubmit == 'Y') {
                  notificationparam.pushNotificationCode = NOTIFICATIONMESSAGE.STUDENT.RPLSUBMITDOCUMENT;
                }
                Masternotificationmodel.sendPushNotification(notificationparam, function (err) {
                  var response = {
                    resCode: 201,
                    response: error
                  };
                  res.status(200).send(response);
                }, function (succNotificationRes) {
                  res.status(200).send(success);
                });
              }
            });
          });
        });
      } else {
        // ** logic for master id = 0 , if a student will save the document for 1st time
        //check the previous attempt id  and incrrement the attempt id
        var fields = " id, attempt, trainer_id ";
        var condition = " where student_id ='" + studentId + "' AND course_id = '" + courseId + "' AND unit_id = '" + unitId + "' AND is_valid = 'Y' ";
        var orderby = "ORDER BY id DESC ";
        var limit = "0,1";
        var param = {

          from: 'from mdl_rpl_process_submission_master ',
          condition: condition,
          fields: fields,
          orderby: orderby,
          limit: limit
        };
        Mastermodel.commonSelect(param, function (err) {
          res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
        }, function (rplSubmissionRes) {
          var attemptCnt;
          var selectedTrainerId = 0;
          if (rplSubmissionRes.length > 0) {
            attemptCnt = rplSubmissionRes[0].attempt + 1;
            selectedTrainerId = rplSubmissionRes[0].trainer_id;
          } else {
            attemptCnt = 1;
          }
          //** update the is active case for submut or resubmit option */
          var dataUpdate = {
            is_active: 'N'
          };
          var updateCondition = {
            course_id: courseId,
            student_id: studentId,
            unit_id: unitId,
          };
          var updateSubmissionParam = {

            tablename: 'mdl_rpl_process_submission_master',
            condition: updateCondition,
            data: dataUpdate
          };
          Mastermodel.commonUpdate(updateSubmissionParam, function (error) {
            res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
          }, function (updateSubmissionlog) {
            //check the resubmit id if a student resubmit the evidence again
            var saveData = {};
            saveData.course_id = req.body.courseId;
            saveData.student_id = req.body.studentId;
            saveData.unit_id = req.body.unitId;
            saveData.total_qcnt = totalQuesCnt;
            saveData.total_acnt = totalAnscnt;
            saveData.attempt = attemptCnt;
            if (isFinalSubmit == 'N' && isResubmit == 'N') {
              //** now save the rpl submission evidence */
              saveData.is_saved = "Y";
            }
            if (isFinalSubmit == 'N' && isResubmit == 'Y') {
              saveData.is_saved = "Y";
              saveData.final_submitted = "Y";
              saveData.trainer_status = 1;
              saveData.trainer_id = selectedTrainerId;
              var isPaid = "N0";
              if (attemptCnt % 2 == 0)
                isPaid = "Yes";
              else
                isPaid = "No";
              saveData.is_paid = isPaid;
            }
            if (isFinalSubmit == 'Y' && isResubmit == 'N') {
              saveData.is_submitted = "Y";
            }
            if (isFinalSubmit == 'Y' && isResubmit == 'Y') {
              saveData.is_saved = "Y";
              saveData.final_submitted = "Y";
              saveData.is_submitted = "Y";
              saveData.trainer_status = 1;
              saveData.trainer_id = selectedTrainerId;
              var isPaid = "N0";
              if (attemptCnt % 2 == 0)
                isPaid = "Yes";
              else
                isPaid = "No";
              saveData.is_paid = isPaid;
            }

            var param = {

              tablename: 'mdl_rpl_process_submission_master',
              data: [saveData]
            }
            Mastermodel.commonInsert(param, function (err) {
              res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
            }, function (data) {
              var rpl_process_submission_id = data.insertId;
              var requestParam = {};
              requestParam.rpl_process_submission_id = rpl_process_submission_id;
              requestParam.dbsetting = dbsetting;
              requestParam.attempt = attemptCnt;
              studentModel.saveRplEvidenceModel(questionArr, requestParam, function (error) {
                var response = {
                  resCode: 201,
                  response: error
                }
                res.status(200).send(response);
              }, function (success) {
                if (isFinalSubmit == 'Y' && isResubmit == 'Y') {
                  var saveData = {};
                  saveData.rpl_process_submission_id = rpl_process_submission_id;
                  saveData.trainer_id = selectedTrainerId;
                  saveData.is_accepted = "Y";
                  saveData.trainer_comment = "";
                  saveData.is_action_taken = "Y";
                  saveData.is_resubmit = "Y";
                  var param = {

                    tablename: 'mdl_rpl_process_trainer_request',
                    data: [saveData]
                  }
                  Mastermodel.commonInsert(param, function (err) {
                    res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.SOMETHINGWRONG);
                  }, function (data) {
                    var notificationparam = {};
                    notificationparam.userId = studentId;
                    notificationparam.courseId = courseId;
                    notificationparam.unitId = unitId;
                    notificationparam.userType = 'Student';
                    notificationparam.dbsetting = dbsetting;
                    notificationparam.pushNotificationCode = NOTIFICATIONMESSAGE.STUDENT.RPLSUBMITDOCUMENT;
                    Masternotificationmodel.sendPushNotification(notificationparam, function (err) {
                      var response = {
                        resCode: 201,
                        response: error
                      }
                      res.status(200).send(response);
                    }, function (succNotificationRes) {
                      res.status(200).send(success);
                    });
                  })

                } else {
                  var notificationparam = {};
                  notificationparam.userId = studentId;
                  notificationparam.courseId = courseId;
                  notificationparam.unitId = unitId;
                  notificationparam.userType = 'Student';
                  notificationparam.dbsetting = dbsetting;
                  if (isFinalSubmit == 'N') {
                    notificationparam.pushNotificationCode = NOTIFICATIONMESSAGE.STUDENT.RPLSAVEDRAFT;
                  }
                  if (isFinalSubmit == 'Y') {
                    notificationparam.pushNotificationCode = NOTIFICATIONMESSAGE.STUDENT.RPLSUBMITDOCUMENT;
                  }
                  Masternotificationmodel.sendPushNotification(notificationparam, function (err) {
                    var response = {
                      resCode: 201,
                      response: error
                    }
                    res.status(200).send(response);
                  }, function (succNotificationRes) {
                    res.status(200).send(success);
                  });
                }
              })
            });
          });
        });
      }
    } else {
      res.status(200).send(RESPONSEMESSAGE.STUDENT.NOEVIDENCESUBMITTED);
    }
  }
});
/*
 * Get the recomended list of trainer from azure ml
 * Created On : 02/02/2018
 * Input      : paramater to get the recomended trainer list
 * Modified On
 */
router.post('/getRecomendedTrainerList', customodule.verifyapicalltoken, function (req, res, next) {
  //get the request param
  var courseId = req.body.courseId;
  var unitId = req.body.unitId;
  //var learningStyle = req.body.learningStyle;
  if (!courseId && !unitId) {
    res.status(200).send(RESPONSEMESSAGE.COMMON.REQUIREDFIELD);
  } else {
    var reqParam = {
      "Inputs": {
        "input1": {
          "ColumnNames": [
            "trainerName",
            "trainerId",
            "trainingStyle",
            "gender",
            "subject",
            "unit",
            "rating"
          ],
          "Values": [
            [
              "value",
              "0",
              "0",
              "0",
              "0",
              "0",
              "0"
            ]
          ]
        }
      },
      "GlobalParameters": {}
    }
    reqParam.Inputs.input1.Values[0][4] = courseId;
    reqParam.Inputs.input1.Values[0][5] = unitId;
    console.log(reqParam);
    //   var response = {
    //     resCode: 200,
    //     response: reqParam
    // }
    // res.status(200).send(response);

    //send the request parameter through api call
    var client = new node_module.restClient();
    var reqParam = reqParam;
    var apiurl = 'https://ussouthcentral.services.azureml.net/workspaces/40c34aaab4b24338992f6a9a1c1aae40/services/c4eced31eea549c28d7e27f9f053e272/execute?api-version=2.0&details=true';
    var args = {
      data: reqParam,
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": "Bearer 0yn3DoCHjqTb4Z+ZTkS2pUitcYUssbHd2PMeGRJZJdexEH9jnSWKtEX/EW+adaDXlG4ZmhE/YHtV/f2I3iIeOQ=="
      }
    };
    var req = client.post(apiurl, args, function (data, response) {
      var recomendedTrainerRes = data.Results.output1.value.Values[0];
      if (recomendedTrainerRes.length > 0) {
        var finalRecomendationIndex = recomendedTrainerRes.length - 1;
        var recomendationTrainerArr = [];
        asyncForEach(recomendedTrainerRes, function (recomendeditem, recomendedindex, recomendedarr) {
          if (recomendedindex > 2 && recomendedindex !== finalRecomendationIndex) {
            if (recomendeditem > 0) {
              console.log(recomendedindex, recomendeditem);
              console.log(data.Results.output1.value.ColumnNames[recomendedindex]);
              var trainerStr = data.Results.output1.value.ColumnNames[recomendedindex];
              if (/"/.test(trainerStr)) {
                var recomendedTrainerId = trainerStr.match(/"(.*?)"/)[1];
              } else {
                var recomendedTrainerId = trainerStr;
              }
              recomendedTrainerId = parseInt(recomendedTrainerId);
              console.log(recomendedTrainerId, Number.isInteger(recomendedTrainerId));
              if (Number.isInteger(recomendedTrainerId)) {
                recomendationTrainerArr.push(recomendedTrainerId);
              }
            }
          }
          console.log(recomendedTrainerRes.length, recomendedindex);
          if (recomendedTrainerRes.length - 1 == recomendedindex) {
            if (recomendationTrainerArr.length > 0) {
              var trainerDetailArr = [];
              asyncForEach(recomendationTrainerArr, function (recomendedTrainerItem, recomendedTrainerIndex, recomendedTrainerArr) {
                //get the trainer id and name
                var dbsetting = req.db_setting;
                var fields = " mu.id, mu.profile_image,mu.firstname, mu.lastname ";
                var condition = " where 1 AND mu.id = " + recomendedTrainerItem + " ";
                var join = " ";
                var param = {

                  from: 'from mdl_user as mu',
                  condition: condition,
                  fields: fields,
                  join: join
                };
                Mastermodel.commonSelect(param, function (err) {
                  res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
                }, function (userRes) {
                  trainerDetailArr.push(userRes);
                  if (recomendationTrainerArr.length == trainerDetailArr.length) {
                    var response = {
                      resCode: 200,
                      response: trainerDetailArr
                    }
                    res.status(200).send(response);
                  }
                });
              });
            } else {
              res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NORECORDSFOUND);
            }
          }
        });
      } else {
        res.status(200).send(RESPONSEMESSAGE.ENTRYTEST.NORECORDSFOUND);
      }
    });
    req.on('requestTimeout', function (req) {
      res.status(201).send(ERROR.COMMON.REQUESTERROR);
    });
    req.on('responseTimeout', function (res) {
      res.status(201).send(ERROR.COMMON.RESPONSEERROR);
    });
    req.on('error', function (err) {
      res.status(201).send(ERROR.COMMON.RESPONSEERROR);
    });
  }
});
//test to convert into csv
router.post('/generateRecommendedCsv', function (req, res, next) {
  var data = [];
  var columns = {
    trainername: 'trainerName',
    trainerid: 'trainerId',
    trainingstyle: 'trainingStyle',
    gender: 'gender',
    subject: 'subject',
    unit: 'unit',
    rating: 'rating'
  };
  var dbsetting = req.db_setting;
  var fields = " (select AVG(rating) from mdl_trainer_rating as mtrt where mtau.trainer_id = mtrt.trainer_id and mtau.course_id = mtrt.course_id  and mtau.unit_id = mtrt.unit_id ) as avgrating,mtau.trainer_id, mu.firstname, mu.lastname, mu.trainingtypeid,mu.gender,(CASE WHEN mu.gender = 'Male' THEN 1 WHEN mu.gender = 'Female' THEN 2 END) AS gender,mtau.course_id,mtau.unit_id ";
  var condition = " where 1 AND mtau.id != 0 ";
  var join = " LEFT JOIN mdl_user mu ON mu.id = mtau.trainer_id ";
  var param = {

    from: 'from mdl_trainer_approved_units as mtau',
    condition: condition,
    fields: fields,
    join: join
  };
  Mastermodel.commonSelect(param, function (err) {
    res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
  }, function (rplCourseRes) {
    if (rplCourseRes.length > 0) {
      var courseArr = [];
      asyncForEach(rplCourseRes, function (courseitem, courseindex, coursearr) {
        courseArr.push(courseindex);
        var rating = 0;
        console.log(coursearr[courseindex].avgrating);
        if (coursearr[courseindex].avgrating != null) {
          rating = coursearr[courseindex].avgrating;
        }
        data.push([coursearr[courseindex].firstname + '' + coursearr[courseindex].lastname, coursearr[courseindex].trainer_id, coursearr[courseindex].trainingtypeid, coursearr[courseindex].gender, coursearr[courseindex].course_id, coursearr[courseindex].unit_id, rating]);
        if (rplCourseRes.length == courseArr.length) {
          node_module.csvstringify(data, {
            header: true,
            columns: columns
          }, (err, output) => {
            if (err) throw err;
            node_module.fs.writeFile('public/dataset/trainer-dataset.csv', output, (err) => {
              if (err) throw err;
              var responseParam = {
                statusCode: 200,
                response: 'Data Saved successfully.'
              }
              res.status(200).send(responseParam);
            });
          });
        }
      });
    } else {
      var responseParam = {
        statusCode: 200,
        response: 'Data Saved successfully.'
      }
      res.status(200).send(responseParam);
    }
  });
});

//test to convert into csv
router.post('/generateRecommendedOtherModeCsv', function (req, res, next) {
  var data = [];
  var columns = {
    trainername: 'trainerName',
    trainerid: 'trainerId',
    trainingstyle: 'trainingStyle',
    gender: 'gender',
    subject: 'subject',
    unit: 'unit',
    rating: 'rating',
    language: 'language',
    timeslot: 'timeslot',
    trainingmode: 'trainingmode'
  };
  var dbsetting = req.db_setting;
  var fields = " (select AVG(rating) from mdl_trainer_rating as mtrt where mtau.trainerid = mtrt.trainer_id and mtau.course_id = mtrt.course_id  and mtau.unit_id = mtrt.unit_id ) as avgrating,mtau.trainerid, mu.firstname, mu.lastname, mu.trainingtypeid,mu.gender,(CASE WHEN mu.gender = 'Male' THEN 1 WHEN mu.gender = 'Female' THEN 2 END) AS gender,mtau.course_id,mtau.unit_id ";
  var condition = " where 1 AND mtau.id != 0 ";
  var join = " LEFT JOIN mdl_user mu ON mu.id = mtau.trainerid ";
  var param = {

    from: 'from mdl_trainer_schdule as mtau',
    condition: condition,
    fields: fields,
    join: join
  };
  Mastermodel.commonSelect(param, function (err) {
    res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
  }, function (rplCourseRes) {
    if (rplCourseRes.length > 0) {
      var courseArr = [];
      asyncForEach(rplCourseRes, function (courseitem, courseindex, coursearr) {
        courseArr.push(courseindex);
        var rating = 0;
        console.log(coursearr[courseindex].avgrating);
        if (coursearr[courseindex].avgrating != null) {
          rating = coursearr[courseindex].avgrating;
        }
        data.push([coursearr[courseindex].firstname + '' + coursearr[courseindex].lastname, coursearr[courseindex].trainerid, coursearr[courseindex].trainingtypeid, coursearr[courseindex].gender, coursearr[courseindex].course_id, coursearr[courseindex].unit_id, rating, 1, 1, 1]);
        if (rplCourseRes.length == courseArr.length) {
          node_module.csvstringify(data, {
            header: true,
            columns: columns
          }, (err, output) => {
            if (err) throw err;
            node_module.fs.writeFile('public/dataset/trainer-dataset-other-mode.csv', output, (err) => {
              if (err) throw err;
              var responseParam = {
                statusCode: 200,
                response: 'Data Saved successfully.'
              }
              res.status(200).send(responseParam);
            });
          });
        }
      });
    } else {
      var responseParam = {
        statusCode: 200,
        response: 'Data Saved successfully.'
      }
      res.status(200).send(responseParam);
    }
  });
});

//get student action history
router.post('/generateStudentActionHistory', function (req, res, next) {
  var data = [];
  var columns = {
    studentname: 'studentName',
    studentid: 'studentId',
    trainerId: 'trainerId',
    courseCategoryId: 'courseCategoryId',
    courseCategoryName: 'courseCategoryName',
    courseid: 'courseId',
    unitid: 'unitId'
  };
  var dbsetting = req.db_setting;
  var fields = " mstl.trainer_id as trainerId, mstl.student_id as studentId, mu.firstname,mu.lastname,mu.gender,(CASE WHEN mu.gender = 'Male' THEN 1 WHEN mu.gender = 'Female' THEN 2 END) AS gender,mstl.course_id,mstl.unit_id,mct.id as coursecategoryid,mct.name as coursecategoryname ";
  var condition = " where 1 AND mstl.id != 0 ";
  var join = " LEFT JOIN mdl_user mu ON mu.id = mstl.student_id ";
  join += " LEFT JOIN mdl_course_categories mcc ON mcc.id = mstl.course_id ";
  join += " LEFT JOIN mdl_course_type mct ON mct.id = mcc.course_type_id ";
  var param = {

    from: 'from mdl_student_trainer_list as mstl',
    condition: condition,
    fields: fields,
    join: join
  };
  Mastermodel.commonSelect(param, function (err) {
    res.status(200).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
  }, function (rplCourseRes) {
    if (rplCourseRes.length > 0) {
      var courseArr = [];
      asyncForEach(rplCourseRes, function (courseitem, courseindex, coursearr) {
        courseArr.push(courseindex);
        data.push([coursearr[courseindex].firstname + '' + coursearr[courseindex].lastname, coursearr[courseindex].studentId, coursearr[courseindex].trainerId, coursearr[courseindex].coursecategoryid, coursearr[courseindex].coursecategoryname, coursearr[courseindex].course_id, coursearr[courseindex].unit_id]);
        if (rplCourseRes.length == courseArr.length) {
          node_module.csvstringify(data, {
            header: true,
            columns: columns
          }, (err, output) => {
            if (err) throw err;
            node_module.fs.writeFile('public/dataset/student-action-history-dataset.csv', output, (err) => {
              if (err) throw err;
              var responseParam = {
                statusCode: 200,
                response: 'Data Saved successfully.'
              }
              res.status(200).send(responseParam);
            });
          });
        }
      });
    } else {
      var responseParam = {
        statusCode: 200,
        response: 'Data Saved successfully.'
      }
      res.status(200).send(responseParam);
    }
  });
});
/*
 * Share resume api
 * Created On :
 * Input      : studentid and mail id
 * Modified On
 */

router.post('/shareResume', customodule.verifyapicalltoken, function (req, res, next) {
  /**** Used to rate the student ****/
  var dbsetting = req.db_setting;
  var studentId = req.body.studentId;

  var fields = "templateid";
  var condition = "where studentid = " + studentId;
  var param = {
    from: 'from mdl_resume_template ',
    condition: condition,
    fields: fields
  };
  Mastermodel.commonSelect(param, function (err) {
    res.status(201).send(RESPONSEMESSAGE.COMMON.SOMETHINGWRONG);
  }, function (resumeRes) {
    if (resumeRes.length > 0) {

      var templateid = resumeRes[0].templateid;
      var filename = "Student" + studentId + "00" + templateid + ".pdf";

      var resumeShareEmail = req.body.resumeShareEmail;
      var emailObj = {
        email: resumeShareEmail,
        link: 'http://qwolonlinelearning.com/moodle/pdfresume/' + filename
      }
      customodule.mail('shareresume', emailObj);
      var response = {
        resCode: 200,
        response: "Resume has been shared successfully."
      }
      res.status(200).send(response);


    } else {

      res.status(201).send(RESPONSEMESSAGE.RESUME.NODATAFOUND);
    }
  });

});

module.exports = router;