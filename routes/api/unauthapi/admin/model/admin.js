var database          = require('../../../../../config/database');
var qb = require('node-querybuilder').QueryBuilder(database, 'mysql', 'single');
var model = {
  getUserTypeByCondiction :function(condiction,errorcallback,successcallback){
    return qb.select('*')
      .where(condiction)
      .get('sd_user_type', function(err,response) {
          if (err) errorcallback("Uh oh! Couldn't get results: " + err.msg);
          successcallback(response);
      });
  },
  addUser :function(data,errorcallback,successcallback){
    return qb.insert('users', data, function(err, res) {
          //  qb.release();
            if (err) errorcallback(err);
            successcallback(res.insertId);
        });
  },
  updateUserByCondiction :function(data,condiction,errorcallback,successcallback){
    return qb.update('users', data, condiction ,function(err, res) {
      if (err) errorcallback("Uh oh! Couldn't update results: " + err.msg);
      successcallback(true);
    });
  },
  removeAccessToken :function(cond,errorcallback,successcallback){
    qb.delete('token', cond, function(err, res) {
      if (err) errorcallback("Uh oh! Couldn't delete results: " + err);
      successcallback(true);
    });
  },
}
module.exports=model;
