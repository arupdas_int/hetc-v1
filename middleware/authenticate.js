var config = require('../config/setting.js');
var node_module = require('../routes/export');
var datetime = require('node-datetime');
var ERROR = {
    UNAUTH: {
        success: false,
        resCode: 401,
        resMessage: "Unauthorized Access"
    },
    TOKENEXP: {
        success: false,
        resCode: 401,
        resMessage: "Accesstoken Expire"
    },
    UNAUTHENTICATE: {
        success: false,
        resCode: 401,
        resMessage: "Failed to authenticate token."
    }
};

function isAuthenticated(req, res, next) {
    // CHECK THE USER STORED IN SESSION FOR A CUSTOM VARIABLE
    // you can do this however you want with whatever variables you set up
    //  var token  = req.headers['token'];
    var apikey = (!req.headers['apikey']) ? req.body['apikey'] : req.headers['apikey'];
    console.log('apikey', req.headers['apikey']);
    if (apikey != '') {
        var init = node_module.moodle_client.init({
            wwwroot: config.local_base_url,
            token: req.headers.apikey,
            service: "nodeweb"
        });
/*
* Used to check site information
* Modified On :06-07-2017
*/
        init.then(function(client) {
	        client.call({
	            wsfunction: "core_webservice_get_site_info",
	            method: "POST",
	        }).then(function(info) {
	        	req.headers.init = init;
        		next();
	        }).catch(function(err) {
	        	res.status(200).send(ERROR.UNAUTH);
			});
    	})
    } else {
        res.status(200).send(ERROR.UNAUTH);
    }
}
module.exports = isAuthenticated;
