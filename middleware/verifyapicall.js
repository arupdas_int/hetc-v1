module.exports=function (req, res, next) {
  var config    = require('../config/setting.js');       
  var aliasname = req.headers['schoolcode'];
  var database  = require('../config/database');
  var qb        = require('node-querybuilder').QueryBuilder(database, 'mysql', 'single');
  var ERROR     = {
      SCHOOLCODEREQUIRED:{
          "resCode":201,
          "response":"School code required to access this api"
      },
      WRONGSCHOOLCODE:{
          "resCode":201,
          "response":"Provided school code is invalid"
      },
      UNAUTH : {
          success : false,
          resCode : 401,
          resMessage : "Unauthorized Access"
      }
  }
  if(!aliasname){
      res.status(200).send(ERROR.SCHOOLCODEREQUIRED);
  }else{
      var condiction = {
        alias_name:aliasname,
      }
      qb.select('*')
        .where(condiction)
        .get('school', function(err,response) {
            if(response.length>0){
              req.school_name = response[0].school_name;
              req.alias_name = response[0].alias_name;
              var db_setting = {
                host: response[0].host,
                database: response[0].dbname,
                user: response[0].username,
                password: response[0].password
              }
              req.db_setting = db_setting;
              var apikey = (!req.headers['apikey'])?req.body['apikey']:req.headers['apikey'];
              if (apikey==config.definedapikey) {
                next();
              }else{
                res.status(401).json(ERROR.UNAUTH);
              }
            }else{
              res.status(200).send(ERROR.WRONGSCHOOLCODE);
            }
        });
  }
}
