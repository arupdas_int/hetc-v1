// module.exports = function(req, res, next) {
//     res.header("Content-Type", "application/x-www-form-urlencoded");
//     res.setHeader('Access-Control-Allow-Credentials', true);
//     res.header("Access-Control-Allow-Origin", 'http://qwolonlinelearning.com');
//     res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization,apikey,token,userLoginType");
//     next();
// }

module.exports = function(req, res, next) {
    res.header("Content-Type", "application/x-www-form-urlencoded");
    res.setHeader('Access-Control-Allow-Credentials', true);

    var allowedOrigins = ['http://qwolonlinelearning.com', 'http://localhost:8080'];
    var origin = req.headers.origin;
    console.log(origin);
    if (allowedOrigins.indexOf(origin) > -1) {
        console.log(origin);
        //res.setHeader('Access-Control-Allow-Origin', origin);
        res.header("Access-Control-Allow-Origin", origin);
    }
    //res.header("Access-Control-Allow-Origin", 'http://qwolonlinelearning.com');
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization,apikey,token,userLoginType");
    next();
}