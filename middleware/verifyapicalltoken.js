module.exports = function (req, res, next) {
    var database = require('../config/database');
    var dbsetting = database;
    var config = require('../config/setting.js');
    var Mastermodel = require('../routes/api/mastermodel');
    var datetime = require('node-datetime');
    var ERROR = {
        UNAUTH: {
            success: false,
            resCode: 401,
            resMessage: "Unauthorized Access",
            response: "Unauthorized Access"
        },
        TOKENEXP: {
            success: false,
            resCode: 401,
            resMessage: "Accesstoken Expire",
            response: "Accesstoken Expire"
        },
        UNAUTHENTICATE: {
            success: false,
            resCode: 401,
            resMessage: "Failed to authenticate token.",
            response: "Failed to authenticate token."
        }
    };
    //  var token  = req.headers['token'];
    var token = (!req.headers['token']) ? req.body['token'] : req.headers['token'];
    var reqFrom = (req.headers['reqFrom']) ? req.headers['reqFrom'] : "";
    var ret = new Date();
    var currentime = ret.getTime();
    /* Set the expire time of logout to 24 hours */
    var exp_time = ret.setTime(ret.getTime() + 60 * 60000);
    var expiredate = datetime.create(new Date(exp_time));
    var expire = expiredate.format("Y-m-d H:M:S");
    var apikey = (!req.headers['apikey']) ? req.body['apikey'] : req.headers['apikey'];
    var cond = {
        token: token
    };
    console.log('header key', apikey);
    console.log('config key', config.definedapikey);
    if (apikey == config.definedapikey) {
        console.log("reqFrom", reqFrom);
        console.log("token", token);
        if (token) {
            var fields = " mts.user_id, mts.token, mts.expire_time ";
            var condition = "where mts.token = '" + token + "'";
            var param = {
                dbsetting: dbsetting,
                from: 'from mdl_token_secret mts ',
                condition: condition,
                fields: fields
            };
            Mastermodel.commonSelect(param, function (err) {
                console.log(err);
                res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
            }, function (tokenRes) {
                if (tokenRes.length > 0) {
                    var expire_time = new Date(tokenRes[0].expire_time).getTime();

                    // console.log(reqFrom);

                    if (reqFrom == 'web') {

                        if (currentime < expire_time) {
                            var dataupdate = {
                                expire_time: expire
                            };
                            var updatecondition = {
                                token: token
                            };
                            var tokenParam = {
                                dbsetting: dbsetting,
                                tablename: 'mdl_token_secret',
                                condition: updatecondition,
                                data: dataupdate
                            };
                            Mastermodel.commonUpdate(tokenParam, function (error) {
                                console.log(error);
                                res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
                            }, function (updateTokenRes) {
                                next();
                            });
                        } else {
                            res.status(200).send(ERROR.TOKENEXP);
                        }

                    } else { // for mobile  
                        //  console.log("Mobile section");
                        var dataupdate = {
                            expire_time: expire
                        };
                        var updatecondition = {
                            token: token
                        };
                        var tokenParam = {
                            dbsetting: dbsetting,
                            tablename: 'mdl_token_secret',
                            condition: updatecondition,
                            data: dataupdate
                        };
                        Mastermodel.commonUpdate(tokenParam, function (error) {
                            console.log(error);
                            res.status(201).send(RESPONSEMESSAGE.WISHLIST.SOMETHINGWRONG);
                        }, function (updateTokenRes) {
                            next();
                        });


                    }




                } else {
                    //res.status(200).send(ERROR.UNAUTHENTICATE);
                    res.status(200).send(ERROR.UNAUTHENTICATE);
                }
            });
        } else {
            //res.status(200).json(ERROR.UNAUTH);
            res.status(200).send(ERROR.UNAUTH);
        }
    } else {
        //res.status(200).send(ERROR.UNAUTH);
        res.status(200).send(ERROR.UNAUTH);
    }
};