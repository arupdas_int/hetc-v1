module.exports=function (req, res, next) {
    var config    = require('../config/setting.js');
    var ERROR = {
        UNAUTH : {
            success : false,
            resCode : 401,
            resMessage : "Unauthorized Access"
        }
    };
    var apikey = (!req.headers['apikey'])?req.body['apikey']:req.headers['apikey'];
    if (apikey==config.definedapikey) {
      next();
    }else{
      res.status(401).json(ERROR.UNAUTH);
    }
}
