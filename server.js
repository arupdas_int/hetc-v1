﻿require("./config/constants.js");
var express = require('express');
var path = require('path');
var app = express();
var port = 4444;
var bodyParser = require('body-parser');
var header = require('./middleware/header');
var session = require('express-session');
var helmet = require('helmet');
app.use(helmet());
app.use(helmet.noCache())
app.use(helmet.xssFilter())

//var md5             = require('md5');
/** Api Controllers **/
/* App configuration Settings */
var request = require('request');
var fs = require('fs')
var cors = require('cors');
app.use(cors())
//for api exposure
app.use('/hetcrplapis', express.static(path.join(__dirname, 'views')));
/* Routing to seperate school and admin */
var UnauthAdmin = require('./routes/api/unauthapi/admin/controller/admin');
var AuthAdmin = require('./routes/api/authapi/admin/controller/adminController');
var AuthStudent = require('./routes/api/authapi/student/controller/studentController');
var AuthTrainer = require('./routes/api/authapi/trainer/controller/trainerController');
var AuthTest = require('./routes/api/authapi/trainer/controller/testController');
app.use(bodyParser.json({
    limit: '50mb'
}));
app.use(bodyParser.urlencoded({
    extended: true,
    limit: '50mb'
}));
app.use('/share', express.static(path.join(__dirname, 'public')));
app.use(session({
    secret: 'mysecretkey'
}));
//app.use(session({secret: 'mysecretkey'},cookie:{maxAge:86400000}));
app.use(header);
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Credentials", true);
    next();
});

//Morgan Start
var morgan = require('morgan');
var rfs = require('rotating-file-stream');
var logDirectory = path.join(__dirname, 'qwolApiLogs');
//Checks log directory exist
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);
//Create a rotating write stream
var accessLogApiCallStream = rfs('qwolApiCallLog.log', {
    interval: '1d', // rotate daily
    path: logDirectory
})
// Logs created before API call
app.use(morgan(function (tokens, req, res) {
    return [
        "method: " + req.method + ";",
        "url: " + req.protocol + "://" + req.get('host') + req.originalUrl + ";",
        "time: " + new Date() + ";",
        "host: " + req.headers.host + ";",
        "origin: " + req.headers.origin + ";",
        "content-type: " + req.headers['content-type'] + ";",
        "                                                       ",
        "                                                       "
    ].join(' ')
}, {
    immediate: true,
    stream: accessLogApiCallStream
}));
// Logs created after API call
app.use(morgan(function (tokens, req, res) {
    return [
        "url: " + req.protocol + "://" + req.get('host') + req.originalUrl + ";",
        "status: " + tokens.status(req, res) + ";",
        "time: " + tokens['response-time'](req, res) + "ms ;",
        "                                                       ",
        "                                                       "
    ].join(' ')
}, {
    immediate: false,
    stream: accessLogApiCallStream
}));
//End Morgan


/* Set the path for all controllers */
/* Set the path for all controllers */
app.use('/api/unAuthAdmin', UnauthAdmin);
app.use('/api/authAdmin', AuthAdmin);
app.use('/api/authStudent', AuthStudent);
app.use('/api/unAuthTrainer', AuthTrainer);
app.use('/api/authTest', AuthTest);
app.get('/public/*', function (req, res) {
    res.sendFile(path.join(__dirname, req.originalUrl));
});
app.listen(port, function () {
    console.log('Server started at port ' + port);
});
module.exports = app;