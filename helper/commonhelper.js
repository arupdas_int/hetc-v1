module.exports = {
  offset:function(pageNo,pageSize){
      if(pageNo>1){
          var offset = (pageNo-1)*pageSize;
      }else{
          var offset = 0;
      }
      return offset;
  },
  convertToSlug:function(str) {
      console.log(str);
      //replace all special characters | symbols with a space
      str = str.replace(/[`~!@#$%^&*()_\-+=\[\]{};:'"\\|\/,.<>?\s]/g, ' ').toLowerCase();
      // trim spaces at start and end of string
      str = str.replace(/^\s+|\s+$/gm, '');
      // replace space with dash/hyphen
      str = str.replace(/\s+/g, '-');
      return str;
  }
}
