var NOTIFICATIONMSSAGE = {
    STUDENT: {
        ENTRYTESTQUALIFIEDSUCCESS: {
            "NOTIFICATIONTITLE": "Successfully qualified in entry test ",
            "NOTIFICATIONMESSAGE": " You have qualified in entry test successfully .",
            "NOTIFICATIONTAG": "Successfully qualified in entry test ",
            "NOTIFICATIONCODE": "Successfully qualified in entry test ",
        },
        ENTRYTESTQUALIFIEDFAILURE: {
            "NOTIFICATIONTITLE": "Not qualified in entry test. Please try again ",
            "NOTIFICATIONMESSAGE": " Sorry, You are not qualified in entry test ",
            "NOTIFICATIONTAG": "Not qualified in entry test. Please try again ",
            "NOTIFICATIONCODE": "Not qualified in entry test. Please try again ",
        },
        RPLSAVEDRAFT: {
            "NOTIFICATIONTITLE": "RPL document submission saved in draft mode ",
            "NOTIFICATIONMESSAGE": " RPL document saved in draft mode successfully .",
            "NOTIFICATIONTAG": "RPL document submission saved in draft mode ",
            "NOTIFICATIONCODE": "RPL document submission saved in draft mode ",
        },
        RPLSUBMITDOCUMENT: {
            "NOTIFICATIONTITLE": "RPL document submitted successfully ",
            "NOTIFICATIONMESSAGE": " RPL document submitted successfully .",
            "NOTIFICATIONTAG": "RPL document submitted successfully ",
            "NOTIFICATIONCODE": "RPL document submitted successfully ",
        },
        RPLSUMIT: {
            "NOTIFICATIONTITLE": "Request for approval of RPL request ",
            "NOTIFICATIONMESSAGE": "A request has been received for approval of RPL request .",
            "NOTIFICATIONTAG": "Request for approval of RPL request ",
            "NOTIFICATIONCODE": "Request for approval of RPL request "
        },
        APPREQUEST: {
            "NOTIFICATIONTITLE": "Request has been apprved by Trainer",
            "NOTIFICATIONMESSAGE": "Request has been apprved by Trainer .",
            "NOTIFICATIONTAG": "Request has been apprved by Trainer",
            "NOTIFICATIONCODE": "Request has been apprved by Trainer"
        },
        REJREQUEST: {
            "NOTIFICATIONTITLE": "Request has been rejected by Trainer ",
            "NOTIFICATIONMESSAGE": "Request has been rejected by Trainer .",
            "NOTIFICATIONTAG": "Request has been rejected by Trainer ",
            "NOTIFICATIONCODE": "Request has been rejected by Trainer "
        }
    },
    TRAINER: {
        PENDINGRPLREQUEST: {
            "NOTIFICATIONTITLE": "Request for approval of RPL request ",
            "NOTIFICATIONMESSAGE": "A request has been received for approval of RPL request .",
            "NOTIFICATIONTAG": "Request for approval of RPL request ",
            "NOTIFICATIONCODE": "Request for approval of RPL request "
        },
        PAYMENTRECEIVED: {
            "NOTIFICATIONTITLE": "Payment received from student ",
            "NOTIFICATIONMESSAGE": "Payment received from student successfully.",
            "NOTIFICATIONTAG": "Payment received from student ",
            "NOTIFICATIONCODE": "Payment received from student ",
        },
        APPROVEDRPLDCUMENT: {
            "NOTIFICATIONTITLE": "RPL document verification completed ",
            "NOTIFICATIONMESSAGE": "RPL document verification completed successfully.",
            "NOTIFICATIONTAG": "RPL document verification completed ",
            "NOTIFICATIONCODE": "RPL document verification completed ",
        }

    },
    ADMIN: {
        RPLSUMIT: {
            "NOTIFICATIONTITLE": 'RPL',
            "NOTIFICATIONMESSAGE": "RPL submitted successfully.",
            "NOTIFICATIONTAG": "RPL submitted successfully.",
            "NOTIFICATIONCODE": "RPL submitted successfully"
        },
        RPLADMINVERIFICATION: {
            "NOTIFICATIONTITLE": "RPL Verification completed by HETC",
            "NOTIFICATIONMESSAGE": "RPL verification has been completed by HETC successfully.",
            "NOTIFICATIONTAG": "RPL Verification completed by HETC",
            "NOTIFICATIONCODE": "RPL Verification completed by HETC"
        },
        RPLADMINCERTIFICATEISSUE: {
            "NOTIFICATIONTITLE": "Certificate has been issued by HETC",
            "NOTIFICATIONMESSAGE": "Certificate has been issued by HETC successfully.",
            "NOTIFICATIONTAG": "Certificate has been issued by HETC",
            "NOTIFICATIONCODE": "Certificate has been issued by HETC"
        }
    },
};

module.exports = NOTIFICATIONMSSAGE;