//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server.js');
let fs = require('fs');
let constants = require("../config/test-request.json");

let should = chai.should();
let expect = chai.expect;
let param = chai.param;
chai.use(chaiHttp);

//Our parent block
describe('StudentFunctionality', () => {
  	let firstRequest;
  	before(function(done) {
  		done();
  	});
  /*
  * Test the /POST /v1/requestSite/updateBooking API
  */
  describe('/POST /api/authTest/getMyTrainerList', () => {
      it('it should return my trainer list', (done) => {
        let requestData = {"studentId" : "262"};
        chai.request(server)
            .post('/api/authTest/getMyTrainerList')
            .send(requestData)
            .end((err, res) => {
              expect(res).to.have.status(200);
              expect(res).to.be.json;
              done();
            });
      });
  });
});