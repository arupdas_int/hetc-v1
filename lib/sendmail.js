/*
 *CODE FOR SEND EMAIL
 */
var SETTING = require("../config/setting");
var mailer = require("nodemailer");
// Use Smtp Protocol to send Email
var smtpTransport = mailer.createTransport("SMTP", {
    host: 'smtp.office365.com',
    port: 587,
    secure: true, // use SSL
    //service: "Outlook365",
    auth: {
        user: "admin@qwol.com.au",
        pass: "Bor90972"

    }
});

function commonTemplate(mailTemplate) {
    var template = '<body style="padding:0; margin:0;"><meta name="viewport" content="width=device-width, initial-scale=1">';
    template += '<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#fff; font-family: "Arial", sans-serif; font-size:14px; line-height: 20px; font-weight:400;">';
    template += '<tr>';
    template += '<td align="center" valign="top" width="100%" style="padding:0; margin:0;">';
    template += '<table cellspacing="0" cellpadding="0" border="0" width="100%" bgcolor="#ffffff" style="max-width:598px; border: 1px solid #03a9f4;">';
    template += '<tr>';
    template += '<td align="center" valign="top" width="100%" style="padding-top:20px; padding-bottom: 20px; padding-left:0; padding-right: 0; margin:0;" bgcolor="#03a9f4">';
    template += '<a style="padding:0; margin:0; width:148px; display:block; text-decoration:none; outline:none;" href="#" target="_blank"><img src="' + SETTING.commonLogoUrl + '" width="148" height="43" style="padding:0; margin:0; outline:none; border: 0;" /></a>';
    template += '</td>';
    template += '</tr>';
    template += '<tr>';
    template += '<td align="center" valign="top" width="100%" style="padding: 0; margin:0;">';
    template += '<img src="' + SETTING.commonRoundImageUrl + '" width="100%" height="70" style="padding:0; margin:0; outline:none; border: 0;" />';
    template += '</td>';
    template += '</tr>';
    template += '<tr>';
    template += '<td align="left" valign="top" width="100%" style="padding-top: 0; padding-right: 30px; padding-bottom:0; padding-left:30px; margin:0; text-align: left;">';
    template += '<h2 style="margin:0; font-family: "Arial", sans-serif; font-size: 20px; line-height: 24px; font-weight: bold; color: #555;"></h2>';
    template += '</td>';
    template += '</tr>';
    template += '<tr>';
    template += '<td align="left" valign="top" width="100%" style="padding-top: 20px; padding-bottom: 20px; padding-left: 30px; padding-right: 30px; margin:0; font-size: 16px; line-height:22px; font-weight: 400; color: #777777">';
    template += '<p style="font-size: 16px; line-height:22px; font-weight: 400; color: #777777; margin:0;">' + mailTemplate.html + '</p>';
    template += '<p style="font-size: 16px; line-height:22px; font-weight: 400; color: #777777; margin-top:20px; margin-right:0; margin-bottom:0; margin-left:0;">Thanks and kind regards <br> <strong style="font-size: 16px; line-height:22px; font-weight: bold; color: #03a9f4; margin:0;">HETC</strong></p>';
    template += '</td>';
    template += '</tr>';
    template += '<tr>';
    template += '<td align="center " valign="top " width="100% " style="padding: 0; margin:0; border-bottom: 10px solid #03a9f4; "></td>';
    template += '</tr>';
    template += '</table>';
    template += '</td>';
    template += '</tr>';
    template += '</table>';
    template += '</body>';
    mailcontent = {
        from: mailTemplate.from,
        to: mailTemplate.to,
        subject: mailTemplate.subject,
        html: template
    }
    send(mailcontent);
}

function sendMail(type, param) {
    switch (type) {
        case 'create':
            var mailcontent = {
                from: 'admin@qwol.com.au',
                to: param.email,
                subject: "HETC RPL Login",
                html: "<p>Hi " + param.name + ",<br><br>Greetings from HETC RPL <br><br> Please use this below link to login</p><br>Your login credential is as follow <br><b>Email :<b>" + param.email + "<br><b>Password :<b>" + param.password
            }
            commonTemplate(mailcontent);
            break;
        case 'shareresume':
            var mailcontent = {
                from: 'admin@qwol.com.au',
                to: param.email,
                subject: "HETC Resume shared with you",
                html: "<p>Hi ,<br><br>A resume shared with you. Please use this below link to download.</p> link :<b>" + param.link
            }
            commonTemplate(mailcontent);
            break;
        case 'forgotPass':
            var mailcontent = {
                from: 'admin@qwol.com.au',
                to: param.email,
                subject: "HETC RPL : Forgot Password",
                html: "<p>Hi " + param.username + ",<br><br> Please use the otp verification code for forgot password  .<br><br> <b>OTP</b> : " + param.otp + " "
            }
            commonTemplate(mailcontent);
            break;
        case 'notificationmail':
            var template = '<body style="padding:0; margin:0;"><meta name="viewport" content="width=device-width, initial-scale=1">';
            template += '<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#fff; font-family: "Arial", sans-serif; font-size:14px; line-height: 20px; font-weight:400;">';
            template += '<tr>';
            template += '<td align="center" valign="top" width="100%" style="padding:0; margin:0;">';
            template += '<table cellspacing="0" cellpadding="0" border="0" width="100%" bgcolor="#ffffff" style="max-width:598px; border: 1px solid #03a9f4;">';
            template += '<tr>';
            template += '<td align="center" valign="top" width="100%" style="padding-top:20px; padding-bottom: 20px; padding-left:0; padding-right: 0; margin:0;" bgcolor="#03a9f4">';
            template += '<a style="padding:0; margin:0; width:148px; display:block; text-decoration:none; outline:none;" href="#" target="_blank"><img src="' + SETTING.commonLogoUrl + '" width="148" height="43" style="padding:0; margin:0; outline:none; border: 0;" /></a>';
            template += '</td>';
            template += '</tr>';
            template += '<tr>';
            template += '<td align="center" valign="top" width="100%" style="padding: 0; margin:0;">';
            template += '<img src="' + SETTING.commonRoundImageUrl + '" width="100%" height="70" style="padding:0; margin:0; outline:none; border: 0;" />';
            template += '</td>';
            template += '</tr>';
            template += '<tr>';
            template += '<td align="left" valign="top" width="100%" style="padding-top: 0; padding-right: 30px; padding-bottom:0; padding-left:30px; margin:0; text-align: left;">';
            template += '<h2 style="margin:0; font-family: "Arial", sans-serif; font-size: 20px; line-height: 24px; font-weight: bold; color: #555;"></h2>';
            template += '</td>';
            template += '</tr>';
            template += '<tr>';
            template += '<td align="left" valign="top" width="100%" style="padding-top: 20px; padding-bottom: 20px; padding-left: 30px; padding-right: 30px; margin:0; font-size: 16px; line-height:22px; font-weight: 400; color: #777777">';
            template += '<p style="margin-top:0; margin-right:0; margin-bottom:15px; margin-left:0;"><strong style="font-size: 16px; line-height:22px; font-weight: bold; color: #555; margin:0;">Hi ' + param.name + '</strong> </p>';
            template += '<p style="font-size: 16px; line-height:22px; font-weight: 400; color: #777777; margin:0;">' + param.notificationmessage + '</p>';
            template += '<p style="font-size: 16px; line-height:22px; font-weight: 400; color: #777777; margin-top:20px; margin-right:0; margin-bottom:0; margin-left:0;">Thanks and kind regards <br> <strong style="font-size: 16px; line-height:22px; font-weight: bold; color: #03a9f4; margin:0;">HETC</strong></p>';
            template += '</td>';
            template += '</tr>';
            template += '<tr>';
            template += '<td align="center " valign="top " width="100% " style="padding: 0; margin:0; border-bottom: 10px solid #03a9f4; "></td>';
            template += '</tr>';
            template += '</table>';
            template += '</td>';
            template += '</tr>';
            template += '</table>';
            template += '</body>';
            var mailcontent = {
                from: 'admin@qwol.com.au',
                to: param.email,
                subject: "HETC RPL  Notification :" + param.notificationtitle,
                html: template
            };
            send(mailcontent);
            break;

        case 'otpverification':
            var mailcontent = {
                from: 'admin@qwol.com.au',
                to: param.email,
                subject: "HETC RPL : OTP Verification For Registration",
                html: "<p>Hi " + param.username + ",<br><br> Please use the otp verification code for Registration  .<br><br> <b>OTP</b> : " + param.otp + " "
            };
            commonTemplate(mailcontent);
            break;

        case 'accountverification':
            var mailcontent = {
                from: 'admin@qwol.com.au',
                to: param.email,
                subject: "HETC RPL : Your account is verified",
                html: "<p>Hi " + param.username + ",<br><br> Your account is verified. You can login now.   "
            };
            commonTemplate(mailcontent);
            break;

        case 'updateProfile':
            var mailcontent = {
                from: 'admin@qwol.com.au',
                to: param.email,
                subject: "HETC RPL : Update Profile",
                html: "<p>Hi " + param.username + ",<br><br> You have recently updated your password."
            };
            commonTemplate(mailcontent);
            break;

        case 'passChange':
            var mailcontent = {
                from: 'admin@qwol.com.au',
                to: param.email,
                subject: "HETC RPL : Change Password",
                html: "<p>Hi " + param.username + ",<br><br> You have recently changed your password."
            };
            commonTemplate(mailcontent);
            break;
        case 'succLoggedIn':
            var mailcontent = {
                from: 'admin@qwol.com.au',
                to: param.email,
                subject: "HETC RPL : logged in through portal",
                html: "<p>Hi " + param.username + ",<br><br> You have recently logged in through portal."
            };
            commonTemplate(mailcontent);
            break;

        case 'update':
            var mailcontent = {
                from: 'admin@qwol.com.au',
                to: param.email,
                subject: "HETC RPL Login",
                html: "<p>Hi " + param.name + ",<br><br> Your profile has been updated.<br><br>Your login credential is as follow <br><b>Email :<b>" + param.email + "<br><b>Password :<b>" + param.password
            };
            commonTemplate(mailcontent);
            break;
        case 'createParent':
            var mailcontent = {
                from: 'admin@qwol.com.au',
                to: param.email,
                subject: "HETC RPL App",
                html: "<p>Hi " + param.name + ",<br><br> Your Login credential is as follow.<br><br> <b>Email</b> : '" + param.email + "' <br><b>Password</b> : '" + param.password + "' "
            };
            commonTemplate(mailcontent);
            break;
        case 'addStudentToParent':
            var mailcontent = {
                from: 'admin@qwol.com.au',
                to: param.email,
                subject: "HETC RPL App",
                html: "<p>Hi " + param.name + ",<br><br> A new student has been assigned to you"
            };
            commonTemplate(mailcontent);
            break;
        case 'createUser':
            var mailcontent = {
                from: 'admin@qwol.com.au',
                to: param.email,
                subject: "HETC RPL Team",
                html: "<p>Hi " + param.name + ",<br><br>Greetings from HETC RPL <br><br> Please use this below link to login</p><p>LINK:" + param.school_login_url + "</p><br>Your login credential is as follow <br><br> Your Employee Code is : " + param.employeeCode + " <br/> Your Login credential is as follow.<br><br> <b>Email</b> : " + param.email + " <br><b>Password</b> : " + param.password + " "
            };
            commonTemplate(mailcontent);
            break;
        case 'contactMessage':
            var mailcontent = {
                from: 'admin@qwol.com.au',
                to: param.email,
                subject: "HETC RPL : Message From Parent",
                html: "<p>" + param.message + "</p></br></br><b>Child Name :</b><p>" + param.studentName + "</p><b>Roll No :</b><p>" + param.studentRoleNo + "</p><b>Class :</b><p>" + param.studentClass + "</p>"
            };
            commonTemplate(mailcontent);
            break;
        case 'resetPassword':
            var mailcontent = {
                from: 'admin@qwol.com.au',
                to: param.email,
                subject: "HETC RPL : Reset Password",
                html: "<p>Hi " + param.name + ",<br><br> Your password has been reset successfully.<br><br> Your Login credential is as follow.<br><br> <b>Email</b> : " + param.email + " <br><b>Password</b> : " + param.password
            };
            commonTemplate(mailcontent);
            break;
        case 'loginEmail':
            var mailcontent = {
                from: 'admin@qwol.com.au',
                to: param.email,
                subject: "HETC RPL : Login Notification",
                html: "<p>Hi " + param.name + ",<br><br> You have just logged in your account.<br><br> Please check."
            };
            commonTemplate(mailcontent);
            break;
        case 'adminFeedbackEmail':
            var mailcontent = {
                from: 'admin@qwol.com.au',
                to: param.email,
                subject: param.subject,
                html: "<p>Hello Admin ,<br>" + param.message
            };
            commonTemplate(mailcontent);
            break;
        case 'userFeedbackEmail':
            var mailcontent = {
                from: 'admin@qwol.com.au',
                to: param.email,
                subject: "HETC RPL : Feedback Sent Successfully",
                html: "<p>Hi " + param.name + ",<br><br> Thank you for your feedback.<br><br> We will contact you soon."
            };
            commonTemplate(mailcontent);
            break;

    }
}

function send(mailcontent) {
    smtpTransport.sendMail(mailcontent, function (error, response) {
        if (error) {
            console.log(error);
        } else {
            console.log("Message sent: " + response.message);
        }
        smtpTransport.close();
    });
}
module.exports = sendMail;