var gcm = require('node-gcm');
var node_module = require('../routes/export');
//SEND GROUP NOTIFICATION
function sendGroupNotification(msg, userArr, notificationTag, notificationDesc, notificationCode, notificationDate) {
    //console.log('user array ---------',userArr);
    var userArrLen = 0;
    userArrLen = userArr.length;
    var userDeviceCntArr = [];
    var deviceTokenArr = [];
    var deviceIdArr = [];

    var numOfClildIndex = parseInt(userArrLen / 1000);
    if (userArrLen > 0) {
        var pos = 0;
        var limit = '';
        if (numOfClildIndex > 0) {
            for (var i = 0; i <= numOfClildIndex; i++) {
                limit = 1000 * (i + 1);
                var dataAndroidArr = [];
                var dataIosArr = [];
                for (var j = pos; j < limit; j++) {
                    if (userArr[j] != undefined) {
                        // for android
                        if (userArr[j].device_type == 'android') {
                            dataAndroidArr.push(userArr[j].device_token);
                        }
                        // for iOS
                        if (userArr[j].device_type == 'ios') {
                            dataIosArr.push(userArr[j].device_token);
                        }
                    }
                }
                if (dataAndroidArr.length > 0) {
                    deviceTokenArr.push(dataAndroidArr);
                }
                if (dataIosArr.length > 0) {
                    deviceIdArr.push(dataIosArr);
                }
                if (limit <= userArrLen) {
                    pos = limit;
                }
            }
        } else {
            var androidArr = [];
            var iosArr = [];
            node_module.asyncForEach(userArr, function (item, index, arr) {
                // for android
                if (userArr[index].device_type == 'android') {
                    androidArr.push(userArr[index].device_token);
                }
                // for iOS
                if (userArr[index].device_type == 'ios') {
                    iosArr.push(userArr[index].device_token);
                }
            });
            if (androidArr.length > 0) {
                deviceTokenArr.push(androidArr);
            }
            if (iosArr.length > 0) {
                deviceIdArr.push(iosArr);
            }
        }
    }
    // initialize new androidGcm object
    //var sender = new gcm.Sender('AAAAhVVzkWY:APA91bFYKjAnXJHLsx7YHwejLhSQ1JmDr_axR3borHHJbYpyKRg0ovGq_ebTpJ83QIgfQHw2YMShk3tZVGR6yHGsosW5LaQjsQL0dseihDGKzeoRH3JhKU-4fzIjRKjSXG9Baf4S1yLQ');
    var sender = new gcm.Sender('AAAAf-4AZKU:APA91bE_AdxIzrlc7jXl_yktX_fojy1cMgd7XqIbZQ39HiNp5kY01V4IKj2Qzhne_7Z7w_SbX7IZg76SA7GlZYPRcOYWrkYq8RpVXidAeu-URVWFjOVxX7V4oBOfYPXUZZgCi17OVtNZ');

    // set message
    var message = new gcm.Message({
        priority: 'high',
        notification: {
            title: notificationTag,
            body: msg
        },
        data: {
            pushdata: {
                type: notificationCode,
                title: msg,
                body: notificationDesc,
                date: notificationDate
            }
        }
    });
    var regTokens = [];
    // push notification implement for android
    if (deviceTokenArr.length > 0) {
        node_module.asyncForEach(deviceTokenArr, function (item, index, arr) {
            regTokens = item;
            // send the message
            sender.send(message, {
                registrationTokens: regTokens
            }, function (err, data) {
                if (err) console.error('error-', err);
                else console.log('succ-', data);
            });
        });
    }
    // push notification implement for iOS
    if (deviceIdArr.length > 0) {
        node_module.asyncForEach(deviceIdArr, function (item, index, arr) {
            regTokens = item;
            // send the message
            sender.send(message, {
                registrationTokens: regTokens
            }, function (err, data) {
                if (err) console.error('error-', err);
                else console.log('succ-', data);
            });
        });
    }
}
module.exports = sendGroupNotification;